open Common

let poly = 10
let iter = 10

(*Building the zone graph *)
let rg = ZoneGraphInput.input_zone_graph ~verbose:0 Sys.argv.(1)

(* Build the polynomial ring *)
module P =
  Polynome.Make
    (Fl.Float)
    (struct
      let var_string = rg.ZoneGraph.var_string
    end)

module FunItPoly = Semantic.Make (P)

module EPN =
  ExpPoly.Make
    (P)
    (struct
      let smp = None
      let tvar = P.var_of_int 0
      let svar = P.var_of_int (P.nb_var - 1)
    end)

(* Build the functionnal iterator module based on the polynomial ring *)
module FunItEPN = Semantic.Make (EPN)

module EPS =
  ExpPoly.Make
    (P)
    (struct
      let smp = Some 2.0
      let tvar = P.var_of_int 0
      let svar = P.var_of_int (P.nb_var - 1)
    end)

(* Build the functionnal iterator module based on the polynomial ring *)
module FunItEPS = Semantic.Make (EPS)

let check_time =
  let fl = ref 0.0 in
  fun () ->
    let nf = Sys.time () in
    let ofl = !fl in
    fl := nf;
    nf -. ofl

let eval_f f p name =
  Printf.printf "%s \t:[" name;
  flush stdout;
  let w = fst (f ()) in
  for i = 1 to iter - 1 do
    ignore (Sys.opaque_identity (f ()));
    Printf.printf "|";
    flush stdout
  done;
  Printf.printf "] -> %g\n" (check_time ());
  Format.printf "weight: %a@." p w.(0)

let _ =
  Printf.printf "Init: %g\n" (check_time ());
  eval_f (fun () -> FunItPoly.iterate_functionnal rg poly) P.print "Poly";
  if not P.F.is_exact then
    eval_f
      (fun () -> FunItEPS.iterate_functionnal rg poly)
      EPS.print "ExpPolyConcreteS";
  eval_f
    (fun () -> FunItEPN.iterate_functionnal rg poly)
    EPN.print "ExpPolyFormalS"
