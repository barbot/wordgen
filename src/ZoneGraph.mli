type l = string
type miniedge = { target : int; reset : bool array; prob : float }

type ('a, 'b) transition = {
  action : l;
  miniedge : miniedge list;
  bound_guard : 'b Common.guard;
  is_contained_in_zone : bool;
  mutable weight : (int * 'a * 'a * 'a) array;
}

type ('a, 'b) loczone = {
  disc_name : string;
  init_zone : string;
  id : int;
  redcoord : int list list;
  transition : ('a, 'b) transition list;
  mutable loc_weight : 'a;
  clockmap : int array;
  is_accepting : bool;
  svg_graph : Format.formatter -> int * int;
}

type ('a, 'b) t = {
  init : int;
  nb_poly : int;
  number_of_component : int;
  statelist : ('a, 'b) loczone array;
  var_string : (string * bool) array;
  cardclocks : int;
  alphabet : l array;
  printer : ?pretty:bool -> Format.formatter -> 'a -> unit;
  is_deterministic : bool;
}

type 'a vspace = 'a * ('a -> 'a -> 'a) * (float -> 'a -> 'a)

val print :
  ((string * bool) array -> Format.formatter -> 'a -> unit) ->
  Format.formatter ->
  ('b, 'a) t ->
  unit

val print_dot :
  ('a, 'b) t ->
  ((string * bool) array -> Format.formatter -> 'b -> unit) ->
  Format.formatter ->
  (string * int * int) array

val of_json : (Common.bound -> 'a) -> Yojson.Basic.t -> (unit, 'a) t
val toDisjointSet : int list list -> int -> Puf.t

val copy :
  int ->
  'a * (int array -> 'b -> 'a) ->
  'c ->
  'c ->
  (?pretty:bool -> Format.formatter -> 'c -> unit) ->
  ('d, 'b) t ->
  ('c, 'a) t

val add_triplet :
  'a vspace ->
  int * 'a * 'a * 'a ->
  float ->
  int * 'a * 'a * 'a ->
  int * 'a * 'a * 'a

val iterate :
  ('a, 'b) t -> (int -> int -> ('a, 'b) transition -> miniedge -> unit) -> unit

val apply_functionnal :
  ('a, 'b) t ->
  int ->
  'a vspace ->
  ('a * int ->
  ('a, 'b) loczone ->
  ('a, 'b) transition ->
  miniedge ->
  int * 'a * 'a * 'a) ->
  (('a, 'b) loczone -> 'a * int -> 'a * int) ->
  ('a * int) array ->
  ('a * int) array

val copy_map_weight : ('a, 'b) t -> ('a -> 'a) -> ('a, 'b) t
val update_weight : ('a, 'b) t -> ('a -> 'a) -> unit
