(*

Copyright (C) 2019 LACL Universite Paris Est Creteil Val de Marne
Authors: Benoît Barbot
Licence: GPL-3.0-or-later

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*)

let ( |>> ) x f = match x with Some y -> f y | None -> None

let ( |>>> ) x f =
  match x with Some y -> ( try Some (f y) with _ -> None) | None -> None

let ( |>>| ) x v = match x with Some y -> y | None -> v

let ( |< ) x f =
  let () = f x in
  x

let ( |<>| ) f (x, y) = f x y

module StringSet = Set.Make (String)

let check_time =
  let init_timer = ref (Sys.time ()) in
  fun () ->
    let t2 = Sys.time () in
    let c = t2 -. !init_timer in
    init_timer := t2;
    c

module VarSet = struct
  type var = int
  type varset = (string * bool) array

  let var_of_int x = x
  let int_of_var x = x
  let zero_var = -1

  let find_var a x =
    let i = ref (-1) in
    Array.iteri (fun j (y, b) -> if x = y && not b then i := j) a;
    assert (!i >= 0);
    !i

  let to_string a =
    let cc =
      Array.fold_left
        (fun acc (v, b) -> if b then acc ^ v ^ "; " else acc)
        "clocks:{" a
    in
    let var =
      Array.fold_left
        (fun acc (v, b) -> if b then acc else acc ^ v ^ "; ")
        (cc ^ "}; vars:{") a
    in
    var ^ "}"
end

type bound = Finite of int * VarSet.var | Const of int | Infinite

let is_time = function Finite (_, 0) -> true | _ -> false

let build_bound a b =
  if b = 0 then Const a else if a = 0 then Const 0 else Finite (a, b)

type 'a guard = Punctual of 'a | Segment of 'a * 'a

let print_guard pb f = function
  | Segment (l, u) -> Format.fprintf f "[%a , %a]" pb l pb u
  | Punctual p -> Format.fprintf f "[%a]" pb p

let build_guard a b = if a = b then Punctual a else Segment (a, b)

let map_guard f ?g gb =
  let f2 = g |>>| f in
  match gb with
  | Segment (a, b) -> Segment (f a, f2 b)
  | Punctual a -> build_guard (f a) (f2 a)

module SBound = struct
  let map clockmap = function
    | Infinite -> Infinite
    | Const a -> Const a
    | Finite (a, b) ->
      
        let c = clockmap.(b) in
        if c <> 0 then Finite (a, c) else Const a

  let print vs f = function
    | Infinite -> Format.fprintf f "inf"
    | Const a -> Format.fprintf f "%i" a
    | Finite (0, b) -> Format.fprintf f "-%s" (fst vs.(b))
    | Finite (a, b) -> Format.fprintf f "%i-%s" a (fst vs.(b))

  let eval (_, clocks, _) = function
    | Infinite -> infinity
    | Const a -> float a
    | Finite (a, b) -> float a -. clocks.(VarSet.int_of_var b - 1)
end


let rec edge_colour = "green" :: "violet" :: "cyan" :: "orange" :: edge_colour

let list_iter_colour f l =
  let rec aux = function
    | [], _ -> ()
    | t :: q, tc :: qc ->
        f t tc;
        aux (q, qc)
    | _, [] -> assert false
  in
  aux (l, edge_colour)