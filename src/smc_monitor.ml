open Weight_structure

exception SMC_Not_terminating

module Make
    (Bt : ZoneGraphInput.BoundType)
    (W : Polynomial.WeightStructure)
    (St : Sampling.STATE with type p = W.t)
    (P : sig
      val target : float
    end) =
struct
  type t = unit
  type dyn = (int * float) * (int * float * float * float * float)
  type bound = Bt.t
  type state = St.t
  type weight = W.t
  type out = int * float * float

  let alpha = 0.01
  let init _ _ _ = ((), ((0, 0.0), (0, 0.0, 0.0, 0.0, 0.0)))
  let new_traj _ (_, glob) = ((0, 0.0), glob)

  let up_trans _ ((size, duration), glob) _ (_, time, _) _ _ =
    if size > 10000 then raise SMC_Not_terminating;
    ((size + 1, duration +. time), glob)

  let up_state _ _ ?smp:_ ?delay:_ _ _ _ _ = None
  let up_state_elapsed _ _ _ _ _ = ()

  let end_traj () ((size, dur), (n, msize, msize2, mduration, mduration2)) _ _ =
    let fsize = float size in
    let fn = float (n + 1) in

    let nmsize = (((fn -. 1.0) *. msize) +. fsize) /. fn in
    let nmsize2 = (((fn -. 1.0) *. msize2) +. (fsize *. fsize)) /. fn in

    let cilow, ciup = Math.comp_gaussian_ci alpha nmsize nmsize2 n in

    (*Format.printf "new traj size:%i@." size;*)
    if
      ((cilow < P.target && ciup < P.target +. 0.1)
      || (cilow > P.target && ciup > P.target -. 0.1))
      && n >= 100
    then raise Exit;
    ( ( (size, dur),
        (n + 1, nmsize, nmsize2, mduration +. dur, mduration2 +. (dur *. dur))
      ),
      true )

  let end_sampling () (_, (n, size, _, duration, _)) =
    (*let cilow, ciup = Math.comp_gaussian_ci alpha size size2 n in
      Format.fprintf o "run : %i, size:  %g [%g;%g] ,%g@?" n size cilow ciup
        (duration /. float n);*)
    (n, size -. P.target, duration /. float n)
end
