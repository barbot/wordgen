(*

Copyright (C) 2019 LACL Universite Paris Est Creteil Val de Marne
Authors: Benoît Barbot
Licence: GPL-3.0-or-later

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*)

val ( |>> ) : 'a option -> ('a -> 'b option) -> 'b option
val ( |>>> ) : 'a option -> ('a -> 'b) -> 'b option
val ( |>>| ) : 'a option -> 'a -> 'a
val ( |< ) : 'a -> ('a -> unit) -> 'a
val ( |<>| ) : ('a -> 'b -> 'c) -> 'a * 'b -> 'c

module StringSet : Set.S with type elt = string

val check_time : unit -> float

module VarSet : sig
  type var
  type varset = (string * bool) array

  val var_of_int : int -> var
  val int_of_var : var -> int
  val zero_var : var
  val find_var : varset -> string -> var
  val to_string : varset -> string
end

type bound = Finite of int * VarSet.var | Const of int | Infinite

val is_time : bound -> bool
val build_bound : int -> int -> bound

type 'a guard = Punctual of 'a | Segment of 'a * 'a

val print_guard :
  (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a guard -> unit

val build_guard : 'a -> 'a -> 'a guard
val map_guard : ('a -> 'b) -> ?g:('a -> 'b) -> 'a guard -> 'b guard

module SBound : sig
  val map : int array -> bound -> bound
  val print : VarSet.varset -> Format.formatter -> bound -> unit
  val eval : 'a * float array * float -> bound -> float
end

val list_iter_colour : ('a -> string -> unit) -> 'a list -> unit