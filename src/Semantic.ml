(*

Copyright (C) 2019 LACL Universite Paris Est Creteil Val de Marne
Authors: Benoît Barbot
Licence: GPL-3.0-or-later

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 *)
open Common
open Weight_structure

module type S = sig
  include Polynomial.WeightForSampling
  module Bt : ZoneGraphInput.BoundType

  val desc : string

  val compute :
    ?update:(unit -> unit) ->
    ?finish:((t, Bt.t) ZoneGraph.t -> unit) ->
    ?print_interm:string ->
    ('a, Bt.t) ZoneGraph.t ->
    int ->
    bool ->
    (t, Bt.t) ZoneGraph.t

  val compute_params :
    ?verbose:int ->
    ?seed:int ->
    float option * float option ->
    (float Sampling.transChoice * string Sampling.transChoice) array ->
    (t, Bt.t) ZoneGraph.t ->
    (float * float * float) option
    * (float * (float * float * float) * float) option
end

module Make (Bt : ZoneGraphInput.BoundType) (P : Polynomial.WeightForSampling) =
struct
  include P
  module Bt = Bt
  open ZoneGraph

  let desc = P.desc

  let map_reset state edge c =
    let cv = VarSet.int_of_var c in
    if snd P.var_string.(cv) then (
      (* weight constant wrt t *)
      assert (cv <> 0);
      if edge.reset.(cv) then VarSet.zero_var
      else VarSet.var_of_int state.clockmap.(cv))
    else c

  let weight_operator _ (weighttarget, degree_of_liberty) state trans edge =
    (* target do not reach accepting state *)
    if degree_of_liberty = -1 then (-1, P.zero, P.zero, P.zero)
    else
      (* weight_reset is the application the reset and clock map on the weight *)
      let weight_reset = map_var weighttarget (map_reset state edge) in
      (* Elapse time *)
      let te_weight_reset = elapse_time weight_reset tvar in

      (* Test for punctual guard if true do not increase degree of liberty *)
      match map_guard Bt.get_bound trans.bound_guard with
      | Punctual value ->
          let weight = apply_bound te_weight_reset tvar value in
          (*if weight <> zero && degree_of_liberty>0 then*)
          (degree_of_liberty, weight, P.zero, P.zero)
          (*else ((P.zero, 0), P.zero, P.zero)*)
      | Segment (lower_bound, upper_bound) ->
          (* Compute a primitive of the pdf *)
          let primitive = primitive te_weight_reset tvar in
          (* The cdf is the integral of the pdf between the lower bound and t *)
          let cdf = primitive -.. apply_bound primitive tvar lower_bound in
          (* The weight is the cdf apply on the upper bound *)
          let weight = apply_bound cdf tvar upper_bound in
          (degree_of_liberty + 1, weight, cdf, P.diff cdf tvar)

  (*let accepting_value state = if state.is_accepting then P.one else P.zero*)
  let accepting_value _ state =
    if state.is_accepting then (P.one, 0) else (P.zero, -1)
  (*else
    state.transition
    |> List.fold_left
         (fun acc trans ->
           if Bt.get_bound trans.lower_bound = Bt.get_bound trans.upper_bound
           then
             trans.miniedge
             |> List.fold_left
                  (fun acc2 e ->
                    if rg.(e.target).is_accepting then P.( +.. ) acc2 P.one
                    else acc2)
                  acc
           else acc)
         P.zero*)

  (* iterate one step of the volumetric computation compute boltzman *)
  let weightsPdfCdf rg i boltzmann stateweight =
    let boltzup state =
      if not boltzmann then fun x -> x
      else fun (vn, i) ->
        let v2, j = accepting_value rg.ZoneGraph.statelist state in
        (v2 +.. (P.var z *.. vn), min i j)
    in
    ZoneGraph.apply_functionnal rg i P.vector_space
      (weight_operator rg.ZoneGraph.statelist)
      boltzup stateweight

  let iterate_functionnal ?update rg n boltzmann =
    let rgp = ZoneGraph.copy n (Bt.zero, Bt.map) P.zero P.one P.print rg in
    let stateweight =
      ref
      @@ Array.map
           (fun x -> accepting_value rgp.ZoneGraph.statelist x)
           rgp.ZoneGraph.statelist
    in
    for i = 1 to n do
      stateweight := weightsPdfCdf rgp (n - i) boltzmann !stateweight;
      match update with Some up -> up () | None -> ()
    done;
    rgp

  let compute ?update ?finish ?print_interm rg n boltzmann =
    let rgp = iterate_functionnal ?update rg (max 0 n) boltzmann in
    Option.iter (fun f -> f rgp) finish;

    Option.iter
      (fun outf ->
        let fdot = open_out (Filename.chop_extension outf ^ ".dot") in
        ignore
        @@ ZoneGraph.print_dot rgp Bt.print
             (Format.formatter_of_out_channel fdot);
        close_out fdot)
      print_interm;

    rgp

  let comp_exp target s (f, df, ddf) =
    (*Format.printf "f: %a df: %a ddf:%a@." P.print f P.print f P.print ddf;*)
    match (to_float ~smp:s f, to_float ~smp:s df, to_float ~smp:s ddf) with
    | Some v, Some dv, Some ddv ->
        (*Format.printf "s: %g v: %g  dv: %g@." s v dv;
          if abs_float v > 10e100 && abs_float dv > 10e100 then assert false;*)
        ((-.dv /. v) -. target, (-.ddv /. v) +. (dv *. dv /. (v *. v)))
    | _ -> failwith "fail to evaluate"

  let comp_lim_inf (cl, dl, vl) (dcl, ddl, dvl) =
    (*Format.printf "dom1: %i,%i,%s, dom2: %i:%i:%s@." cl dl (F.to_string vl) dcl ddl (F.to_string dvl);*)
    if dcl - cl < 0 then F.zero
    else if dcl - cl = 0 then
      if ddl - dl < 0 then F.zero
      else if ddl - dl = 0 then F.neg (F.div dvl vl)
      else F.infinity
    else F.infinity

  let comp_lim_zero_p (cl, dl, vl) (dcl, ddl, dvl) =
    (* cl mal calculer *)
    let v = F.neg (F.div dvl vl) in
    assert (dcl = 0 && cl = 0);
    if ddl - dl > 0 then F.zero
    else if ddl - dl = 0 then v
    else F.mul v F.infinity

  let compute_s ?(verbose = 1) target rg =
    match target with
    | None -> None
    | Some expected_duration -> (
        (* Weight in initial location *)
        let w = rg.statelist.(rg.init).loc_weight in
        let dw = diff w s in
        let ddw = diff dw s in
        if verbose > 3 then (
          Format.printf "\nWeight : %a@." (P.print ~pretty:true) w;
          Format.printf "Weight': %a@." (P.print ~pretty:true) dw);

        let svarexp = var ~exp:(rg.nb_poly + 1) s in
        let wts = svarexp *.. w in
        let dwts = svarexp *.. dw in
        let ddwts = svarexp *.. ddw in
        if verbose > 4 then (
          Format.printf "Weight2 : %a@." (P.print ~pretty:true) wts;
          Format.printf "Weight2': %a@." (P.print ~pretty:true) dwts);

        let we = taylor_exp_s w (rg.nb_poly + 1) in
        let de = taylor_exp_s dw (rg.nb_poly + 2) in
        let dde = taylor_exp_s ddw (rg.nb_poly + 3) in
        if verbose > 5 then (
          Format.printf "Weight_taylor : %a@." (P.print ~pretty:true) we;
          Format.printf "Weight_taylor': %a@." (P.print ~pretty:true) de);

        (* computing limit for s -> -infty and s -> + infty *)
        let low_w, high_w = dominating_s w in
        let low_dw, high_dw = dominating_s dw in
        let liminf = F.to_float @@ comp_lim_inf low_w low_dw in
        let limsup = F.to_float @@ comp_lim_inf high_w high_dw in

        if verbose > 5 then (
          let a, b, c = low_w and a2, b2, c2 = high_w in
          Format.printf "wdom: (%i,%i,%g);(%i,%i,%g)@." a b (F.to_float c) a2 b2
            (F.to_float c2);
          let a, b, c = low_dw and a2, b2, c2 = high_dw in
          Format.printf "dwdom: (%i,%i,%g);(%i,%i,%g)@." a b (F.to_float c) a2
            b2 (F.to_float c2);
          Format.printf "range : [%g; %g]@." liminf limsup);

        let compv sval =
          if verbose > 3 then Format.printf "Time duration for s: %g -> " sval;
          let calc =
            if abs_float sval < 1.0 then
              (* When s very close to 0 use a taylor expansion to compute the duration*)
              if abs_float sval <= 0.01 then
                if sval = 0.0 then
                  (* when s=0 compute limit *)
                  let _, low_we = dominating_s we in
                  let _, low_dw = dominating_s de in

                  let lim = comp_lim_zero_p low_we low_dw in
                  (*Format.printf "f: %a df: %a ddf:%a@." P.print we P.print de P.print dde;
                    Format.printf "dominating f: %a df: %a -> %s@." print_dom low_we print_dom low_dw (F.to_string lim);*)
                  (P.one, P.const (F.neg lim), P.one)
                else (* computing taylor exp*)
                  (we, de, dde)
              else (wts, dwts, ddwts)
            else (w, dw, ddw)
          in
          let v, dv = comp_exp expected_duration sval calc in
          if verbose > 3 then Format.printf "(%g,%g)@." v dv;
          (v, dv)
        in

        (* computing limit for s -> 0 *)
        let exp0, _ = compv 0.0 in

        let result =
          if exp0 = Float.infinity then
            let range = (limsup, Float.infinity, Float.infinity) in
            if expected_duration < limsup then Error range
            else
              let x =
                Math.newton_raphson_iterate ~bound:(0.0, Float.infinity)
                  ~max_iter:1000 compv 1.0
              in
              let v, _ = compv x in
              Ok (x, range, v +. expected_duration)
          else
            let range = (limsup, exp0 +. expected_duration, liminf) in
            if expected_duration >= liminf || expected_duration < limsup then
              Error range
            else
              let x = Math.newton_raphson_iterate ~max_iter:1000 compv 1.0 in
              let v, _ = compv x in
              Ok (x, range, v +. expected_duration)
        in
        match result with
        | Ok (x, y, z) -> Some (x, y, z)
        | Error (sup, mid, inf) ->
            Format.printf "range:[%g; %g; %g]@." sup mid inf;
            Format.printf "Target outside range, aborting@.";
            exit 1)

  let compute_params ?(verbose = 1) ?seed targets template rgp =
    match targets with
    | Some t_length, t_duration ->
        let module State = Sampling.State (Bt) (P) in
        let module SMCMonitor =
          Smc_monitor.Make (Bt) (P) (State)
            (struct
              let target = t_length
            end)
        in
        let module SMCSampler = Sampling.Make (Bt) (P) (State) (SMCMonitor) in
        let make_estimate n boltz =
          let rgp2 =
            ZoneGraph.copy_map_weight rgp (fun p ->
                P.apply_const p P.z (P.F.of_float boltz))
          in
          let smp_full =
            if P.is_exp_poly then compute_s ~verbose t_duration rgp2 else None
          in
          let smp = smp_full |>>> fun (x, _, _) -> x in

          let module Samp = SMCSampler (struct
            let smp = smp
          end) in
          if
            P.to_float ?smp @@ rgp2.statelist.(rgp2.init).loc_weight
            |>>> (fun x -> x < 0.0)
            |>>| true
          then (0, -.t_length -. 1.0, 0.0, smp_full)
          else
            try
              let n, size, duration =
                Samp.sample ~max_iter:100 ~boltz ?seed
                  ~sampler:Low_disc_sampler.random rgp2 template n
              in
              (n, size, duration, smp_full)
            with Smc_monitor.SMC_Not_terminating ->
              (1, infinity, infinity, smp_full)
        in

        let b, v, smp_full =
          Math.bisect_increasing ~factor:0.001 ~strict:false ~relax:0.1
            ~up_bound:false ~low:(0.0, -.t_length, None) (0.0, 1.0) (fun b ->
              let n, size, duration, smp = make_estimate 10000 b in
              if verbose > 1 then
                Format.printf "|%i->%.2f@?" n (size +. t_length);
              if verbose > 2 then
                Format.printf " samples with z:%g s:%s -> size:%g duration:%g@."
                  b
                  (smp |>>> (fun (a, _, _) -> a) |>>> string_of_float |>>| "?")
                  (size +. t_length) duration;
              (size, 0.0, smp))
        in
        (Some (b, 0.0, v +. t_length), smp_full)
    | None, t_duration ->
        let smp_full =
          if P.is_exp_poly then compute_s ~verbose t_duration rgp else None
        in
        (None, smp_full)
end
