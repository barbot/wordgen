open ZoneGraph

module Make
    (W : Weight_structure.Polynomial.SamplingStruct)
    (Bt : ZoneGraphInput.BoundType)
    (St : Sampling.STATE with type p = W.t with type bound = Bt.t) =
struct
  type bound = Bt.t
  type state = St.t
  type weight = W.t
  type out = unit

  module StateMap = Map.Make (struct
    type t = Bt.state

    let compare (a1, b1, _) (a2, b2, _) = compare (a1, b1) (a2, b2)
  end)

  type t = Format.formatter * (weight, bound) ZoneGraph.t
  type dyn = int StateMap.t

  let init f zg _ =
    Format.printf "Using rejection sampling to solve non-determinism !@.";
    ((f, zg), StateMap.empty)

  let new_traj (_, zg) _ =
    StateMap.singleton (zg.init, Array.make zg.cardclocks 0.0, 0.0) 1

  let up_state _ _ ?smp:_ ?delay:_ _ _ _ _ = None
  let up_state_elapsed _ _ _ _ _ = ()

  let up_trans (_, zg) st _ (_, time, _) tr _ =
    (*StateMap.iter
        (fun state nb ->
          Format.fprintf outfile "(%a:%i)" (St.print_state ?trunc:None) state nb)
        st;
      Format.fprintf outfile "%f[%s] " time tr.ZoneGraph.action;*)
    StateMap.fold
      (fun st nb acc ->
        let filter (low, up) tr2 =
          low <= time && time <= up && tr.action = tr2.action
        in
        let available_trans = St.available_transition ~filter zg st in

        let st2 = St.elapse_time st time in
        List.fold_left
          (fun acc2 (tr, _) ->
            let st3 = St.fire st2 tr in
            StateMap.update st3
              (function Some i -> Some (i + nb) | None -> Some nb)
              acc2)
          acc available_trans)
      st StateMap.empty

  let end_traj (f, zg) st _ u =
    let nbocc =
      StateMap.fold
        (fun (i, _, _) nb acc ->
          if zg.statelist.(i).is_accepting then acc + nb else acc)
        st 0
    in
    if nbocc > 1 && false then Format.fprintf f "-> nbocc:%i" nbocc;
    (st, u < 1. /. float nbocc)

  let end_sampling _ _ = ()
end
