open Weight_structure

(* A module with functions to test *)
let var_string =
  [|
    ("t", false);
    ("x", true);
    ("y", true);
    ("z", true);
    ("s", false);
    ("z", false);
  |]

module P =
  Polynomial.Make
    (Flq.Q)
    (struct
      let var_string = var_string
    end)

module EP =
  ExpPoly.Make
    (P)
    (struct
      let smp = None
    end)

open P

let pr v =
  let open Format in
  print str_formatter v;
  flush_str_formatter ()

open Common.VarSet

let x = var_of_int 0
let y = var_of_int 1

(* The tests *)
let%test _ = pr zero = "0"
let%test _ = pr (const (F.of_float 1.0)) = "1"

let%test _ =
  pr (const (F.of_float 1.0))
  = pr (const (F.of_float 3.0) +.. const (F.of_float (-2.0)))

type p = Const of float | Var of (int * int) | Sum of p * p | Mult of p * p

let rec gen_poly = function
  | Const f -> const (F.of_float f)
  | Var (x, i) -> var ~exp:i (var_of_int x)
  | Sum (p1, p2) -> gen_poly p1 +.. gen_poly p2
  | Mult (p1, p2) -> gen_poly p1 *.. gen_poly p2

let p0 = var x +.. pow (var y) 3 +.. const (F.of_float 12.0)

let p1 =
  gen_poly
    (Sum
       ( Sum
           ( Mult
               ( Sum
                   ( Sum
                       ( Mult
                           (Sum (Const (-0.712508), Var (0, 1)), Const 1.86708),
                         Mult
                           ( Sum
                               ( Sum (Const (-1.49116), Const (-0.919918)),
                                 Mult
                                   ( Const 0.109705,
                                     Sum
                                       ( Mult
                                           ( Sum
                                               ( Sum
                                                   ( Var (0, 1),
                                                     Mult
                                                       ( Mult
                                                           ( Mult
                                                               ( Var (1, 0),
                                                                 Const 0.117616
                                                               ),
                                                             Const 0.375283 ),
                                                         Mult
                                                           ( Const (-1.545),
                                                             Mult
                                                               ( Sum
                                                                   ( Const
                                                                       1.65734,
                                                                     Mult
                                                                       ( Sum
                                                                           ( Sum
                                                                               ( 
                                                                               Const
                                                                                (-1.28601),
                                                                                Var
                                                                                ( 
                                                                                1,
                                                                                1
                                                                                )
                                                                               ),
                                                                             Sum
                                                                               ( 
                                                                               Const
                                                                                1.49235,
                                                                                Sum
                                                                                ( 
                                                                                Const
                                                                                1.16595,
                                                                                Const
                                                                                (-0.00564408)
                                                                                )
                                                                               )
                                                                           ),
                                                                         Mult
                                                                           ( Mult
                                                                               ( 
                                                                               Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Const
                                                                                (-1.31104),
                                                                                Const
                                                                                1.58718
                                                                                ),
                                                                                Mult
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Mult
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Mult
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Const
                                                                                (-0.521839),
                                                                                Var
                                                                                ( 
                                                                                1,
                                                                                0
                                                                                )
                                                                                ),
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Const
                                                                                0.899878,
                                                                                Const
                                                                                (-0.381689)
                                                                                ),
                                                                                Var
                                                                                ( 
                                                                                0,
                                                                                0
                                                                                )
                                                                                )
                                                                                ),
                                                                                Const
                                                                                (-0.266984)
                                                                                ),
                                                                                Var
                                                                                ( 
                                                                                0,
                                                                                0
                                                                                )
                                                                                ),
                                                                                Const
                                                                                (-0.510893)
                                                                                ),
                                                                                Var
                                                                                ( 
                                                                                1,
                                                                                1
                                                                                )
                                                                                ),
                                                                                Var
                                                                                ( 
                                                                                0,
                                                                                1
                                                                                )
                                                                                ),
                                                                                Const
                                                                                (-1.8416)
                                                                                ),
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Var
                                                                                ( 
                                                                                0,
                                                                                0
                                                                                ),
                                                                                Sum
                                                                                ( 
                                                                                Mult
                                                                                ( 
                                                                                Const
                                                                                0.824652,
                                                                                Const
                                                                                (-0.272448)
                                                                                ),
                                                                                Var
                                                                                ( 
                                                                                0,
                                                                                0
                                                                                )
                                                                                )
                                                                                ),
                                                                                Mult
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Mult
                                                                                ( 
                                                                                Var
                                                                                ( 
                                                                                0,
                                                                                0
                                                                                ),
                                                                                Sum
                                                                                ( 
                                                                                Const
                                                                                (-1.26636),
                                                                                Mult
                                                                                ( 
                                                                                Mult
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Mult
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Const
                                                                                1.52467,
                                                                                Const
                                                                                (-1.44636)
                                                                                ),
                                                                                Const
                                                                                (-1.81246)
                                                                                ),
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Var
                                                                                ( 
                                                                                0,
                                                                                1
                                                                                ),
                                                                                Const
                                                                                (-0.194834)
                                                                                ),
                                                                                Mult
                                                                                ( 
                                                                                Const
                                                                                0.0232993,
                                                                                Const
                                                                                1.24118
                                                                                )
                                                                                )
                                                                                ),
                                                                                Mult
                                                                                ( 
                                                                                Const
                                                                                1.10444,
                                                                                Const
                                                                                0.325027
                                                                                )
                                                                                ),
                                                                                Var
                                                                                ( 
                                                                                1,
                                                                                0
                                                                                )
                                                                                ),
                                                                                Mult
                                                                                ( 
                                                                                Const
                                                                                0.620903,
                                                                                Const
                                                                                0.454195
                                                                                )
                                                                                ),
                                                                                Const
                                                                                (-0.930799)
                                                                                ),
                                                                                Var
                                                                                ( 
                                                                                0,
                                                                                1
                                                                                )
                                                                                ),
                                                                                Const
                                                                                (-1.97381)
                                                                                )
                                                                                )
                                                                                ),
                                                                                Var
                                                                                ( 
                                                                                0,
                                                                                1
                                                                                )
                                                                                ),
                                                                                Const
                                                                                (-0.80359)
                                                                                )
                                                                                ),
                                                                                Var
                                                                                ( 
                                                                                1,
                                                                                0
                                                                                )
                                                                                ),
                                                                                Const
                                                                                (-1.00803)
                                                                                )
                                                                                )
                                                                                ),
                                                                                Const
                                                                                (-1.43013)
                                                                               ),
                                                                             Sum
                                                                               ( 
                                                                               Var
                                                                                ( 
                                                                                0,
                                                                                1
                                                                                ),
                                                                                Sum
                                                                                ( 
                                                                                Mult
                                                                                ( 
                                                                                Const
                                                                                1.33374,
                                                                                Var
                                                                                ( 
                                                                                0,
                                                                                1
                                                                                )
                                                                                ),
                                                                                Const
                                                                                0.640638
                                                                                )
                                                                               )
                                                                           ) )
                                                                   ),
                                                                 Var (1, 1) ) )
                                                       ) ),
                                                 Const 0.304289 ),
                                             Mult (Const 0.263273, Var (1, 1))
                                           ),
                                         Mult
                                           ( Var (0, 0),
                                             Sum
                                               ( Const (-1.2114),
                                                 Const (-0.0080391) ) ) ) ) ),
                             Mult
                               ( Mult
                                   ( Const (-0.222252),
                                     Mult
                                       ( Mult
                                           ( Sum
                                               ( Var (1, 1),
                                                 Mult
                                                   ( Const (-1.33187),
                                                     Const (-1.28762) ) ),
                                             Const (-1.35882) ),
                                         Mult
                                           ( Sum
                                               ( Sum
                                                   (Const 0.0746461, Var (0, 1)),
                                                 Const (-1.40822) ),
                                             Var (0, 1) ) ) ),
                                 Const 0.820138 ) ) ),
                     Sum (Const (-1.50144), Const 1.55265) ),
                 Sum (Const 1.5829, Const (-1.04482)) ),
             Var (1, 0) ),
         Const (-1.36201) ))

let p2 =
  gen_poly
    (Sum
       ( Const (-0.865672),
         Sum
           ( Mult (Const (-1.10576), Var (1, 0)),
             Sum
               ( Const (-0.376951),
                 Sum
                   ( Const 0.65211,
                     Sum
                       ( Mult
                           ( Const 0.213829,
                             Sum
                               ( Sum (Const 0.224512, Var (1, 0)),
                                 Mult
                                   ( Var (1, 0),
                                     Sum (Const (-1.50878), Var (0, 0)) ) ) ),
                         Const (-0.302529) ) ) ) ) ))

let p3 =
  gen_poly
    (Mult
       ( Sum
           ( Mult
               ( Sum
                   ( Sum
                       ( Var (1, 1),
                         Mult
                           ( Sum
                               ( Sum
                                   ( Sum
                                       ( Mult
                                           ( Sum
                                               ( Mult
                                                   ( Mult
                                                       ( Mult
                                                           ( Const 0.685563,
                                                             Var (1, 1) ),
                                                         Var (1, 1) ),
                                                     Const 1.64208 ),
                                                 Const (-1.90356) ),
                                             Const 1.33615 ),
                                         Var (1, 0) ),
                                     Mult
                                       ( Mult
                                           ( Sum
                                               ( Sum
                                                   ( Sum
                                                       ( Mult
                                                           ( Sum
                                                               ( Var (0, 0),
                                                                 Var (0, 1) ),
                                                             Mult
                                                               ( Sum
                                                                   ( Const
                                                                       0.315383,
                                                                     Const
                                                                       1.7297 ),
                                                                 Const
                                                                   (-1.40727) )
                                                           ),
                                                         Mult
                                                           ( Sum
                                                               ( Var (0, 1),
                                                                 Var (0, 1) ),
                                                             Const 0.428504 ) ),
                                                     Var (0, 0) ),
                                                 Const 1.84694 ),
                                             Mult
                                               ( Const 1.4396,
                                                 Sum
                                                   ( Const (-0.199541),
                                                     Const (-0.38356) ) ) ),
                                         Mult
                                           ( Mult
                                               ( Const 1.93179,
                                                 Mult
                                                   ( Const (-1.53149),
                                                     Const (-1.72291) ) ),
                                             Var (0, 1) ) ) ),
                                 Mult
                                   ( Sum
                                       ( Mult
                                           ( Sum
                                               ( Var (0, 1),
                                                 Sum
                                                   ( Const 0.429328,
                                                     Sum
                                                       ( Const 1.43481,
                                                         Const (-1.34698) ) ) ),
                                             Const (-1.34663) ),
                                         Mult
                                           ( Sum
                                               ( Mult
                                                   ( Const 1.39306,
                                                     Const 0.882996 ),
                                                 Const (-0.122817) ),
                                             Sum
                                               ( Const 1.96669,
                                                 Mult
                                                   ( Sum
                                                       ( Mult
                                                           ( Const 0.0203522,
                                                             Const (-1.94229) ),
                                                         Const 1.81295 ),
                                                     Mult
                                                       ( Var (1, 1),
                                                         Mult
                                                           ( Sum
                                                               ( Mult
                                                                   ( Const
                                                                       1.93618,
                                                                     Const
                                                                       (-0.134514)
                                                                   ),
                                                                 Mult
                                                                   ( Var (1, 1),
                                                                     Mult
                                                                       ( Const
                                                                           0.389754,
                                                                         Sum
                                                                           ( Const
                                                                               0.348551,
                                                                             Sum
                                                                               ( 
                                                                               Sum
                                                                                ( 
                                                                                Mult
                                                                                ( 
                                                                                Const
                                                                                (-1.31856),
                                                                                Const
                                                                                1.66497
                                                                                ),
                                                                                Mult
                                                                                ( 
                                                                                Const
                                                                                0.55114,
                                                                                Const
                                                                                (-1.72732)
                                                                                )
                                                                                ),
                                                                                Var
                                                                                ( 
                                                                                0,
                                                                                1
                                                                                )
                                                                               )
                                                                           ) )
                                                                   ) ),
                                                             Const 0.614747 ) )
                                                   ) ) ) ),
                                     Const 0.505168 ) ),
                             Sum
                               ( Mult (Const (-1.1361), Var (1, 1)),
                                 Sum (Const (-1.36438), Var (0, 1)) ) ) ),
                     Sum
                       ( Mult
                           ( Const 0.203666,
                             Mult
                               ( Const 1.37536,
                                 Sum (Const (-0.364922), Const (-0.620037)) ) ),
                         Var (0, 0) ) ),
                 Sum
                   ( Const (-0.258687),
                     Mult
                       ( Mult
                           ( Var (0, 0),
                             Mult (Const (-1.78965), Const (-0.162534)) ),
                         Const 1.80837 ) ) ),
             Const 1.45342 ),
         Mult (Const (-1.02642), Sum (Var (0, 1), Const (-0.464846))) ))

let p4 =
  gen_poly
    (Mult
       ( Mult (Var (0, 0), Const (-0.0212035)),
         Sum
           ( Sum
               ( Var (0, 0),
                 Sum (Mult (Var (0, 1), Const 1.5982), Const (-1.30035)) ),
             Sum (Const (-1.59391), Mult (Const (-1.26347), Const 1.77068)) ) ))

let p5 =
  gen_poly
    (Mult
       ( Mult
           ( Const (-0.532449),
             Sum
               ( Const 0.178978,
                 Sum
                   ( Const (-1.96509),
                     Mult
                       ( Sum
                           ( Mult (Const 0.675274, Const 0.845333),
                             Mult
                               ( Sum
                                   ( Sum
                                       ( Sum
                                           ( Mult (Const 0.344589, Const 1.4286),
                                             Const (-1.7311) ),
                                         Mult (Const (-0.459082), Var (1, 1)) ),
                                     Const 0.209464 ),
                                 Const 1.69843 ) ),
                         Var (0, 1) ) ) ) ),
         Const 1.6956 ))

let%test _ = degree zero = -1
let%test _ = degree p1 = 10
let%test _ = degree p2 = 0
let ep1 = EP.term (var x) (Const 5)
let ep2 = EP.term p0 (Finite (3, x))
let ep3 = EP.term (const (F.of_float 12.0)) (Const 0)

let%test _ =
  pr zero = pr (((p1 +.. p2) *.. p3) -.. ((p1 *.. p3) +.. (p2 *.. p3)))

let%test _ =
  pr zero = pr ((p3 *.. (p1 +.. p2)) -.. ((p1 *.. p3) +.. (p2 *.. p3)))

let%test _ = pr zero = pr (diff (primitive p1 x) x -.. p1)

let pd1 =
  var ~exp:3 x
  +.. (F.of_float (-2.0) **. var ~exp:2 x)
  -.. const (F.of_float 4.0)

let pd2 = var x -.. const (F.of_float 3.0)

let%test _ =
  let a, b = div_euclidean pd1 pd2 in
  (pd2 *.. a) +.. b = pd1

let%test _ =
  let i =
    P.integral p0 y (const @@ F.of_float (-10.0)) (const @@ F.of_float 15.0)
  in
  let i2 = P.apply_const i x (F.of_float 2.0) in
  i2 = const @@ F.of_float (42025.0 /. 4.0)

open EP

let epr ep =
  let open Format in
  print ~pretty:false str_formatter ep;
  flush_str_formatter ()

let%test _ = epr ep1 = epr (ep1 +.. ep2 -.. ep2)

let%test _ =
  Fl.Float.max (Fl.Float.of_float 3.5) (Fl.Float.of_float 5.0)
  = Fl.Float.of_float 5.0

let%test _ = Fl.Num.to_int (Fl.Num.of_float 396.0) = 396
let%test _ = Fl.Num.to_int (Fl.Num.of_float 3934567893456.0) = 3934559141888

module RF = Rational_fraction.Make (P)

let%test _ =
  let open RF in
  let x = (one /.. of_p pd1) +.. (one /.. of_p pd2) in
  let y = of_p_p (P.( +.. ) pd1 pd2) (P.( *.. ) pd1 pd2) in
  equal x y
