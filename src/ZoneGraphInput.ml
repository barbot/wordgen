(*

Copyright (C) 2019 LACL Universite Paris Est Creteil Val de Marne
Authors: Benoît Barbot
Licence: GPL-3.0-or-later

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*)

open Common
open Format

let string_of_file f =
  let fo = open_in f in
  let b = ref (input_line fo) in
  (match String.index_opt !b '=' with
  | Some eq -> b := String.sub !b (eq + 1) (String.length !b - eq - 1)
  | None -> ());
  (try
     while true do
       b := !b ^ input_line fo
     done
   with End_of_file -> ());
  close_in fo;
  !b

let precomputation_exists ?debug filename frequency boltzmann npoly s_formal
    exact_rational =
  try
    let fd = open_in (Filename.remove_extension filename ^ ".pcmp") in
    let hashmod = Digest.file filename in
    let hashpcmp = (input_value fd : Digest.t) in
    let freq = (input_value fd : float option) in
    let boltz = (input_value fd : float option) in
    let polyn = (input_value fd : int) in
    let exact_rat = (input_value fd : bool) in
    let s_formal_in = (input_value fd : bool) in
    let rg = input_value fd in
    (*let weight = input_value fd in*)
    if
      hashpcmp = hashmod && frequency = freq && boltzmann = boltz
      && npoly = polyn && exact_rational = exact_rat && s_formal = s_formal_in
    then (
      (debug |>>| fun _ -> ()) true;
      Some rg)
    else (
      (debug |>>| fun _ -> ()) false;
      None)
  with
  | Sys_error _ -> None (* file do not exists*)
  | Failure _ -> None (* Binary have changed*)
  | End_of_file -> None (* Corrupt file *)
  | _ -> None

let save_precomputation file rg frequency boltzmann npoly s_formal
    exact_rational =
  let fd = open_out (Filename.remove_extension file ^ ".pcmp") in
  let hashmod = Digest.file file in
  output_value fd (hashmod : Digest.t);
  output_value fd (frequency : float option);
  output_value fd (boltzmann : float option);
  output_value fd (npoly : int);
  output_value fd (exact_rational : bool);
  output_value fd (s_formal : bool);
  Marshal.to_channel fd rg [ Marshal.Closures ];
  close_out fd

(* Computing the reachability graph *)
let reachability ~verbose ~print_rg ~splitting_debug ta =
  (* Compute forward reachability zone graph *)
  if verbose > 0 then (
    printf "Computing forward reachability graph ... ";
    print_flush ());

  let tex_out =
    if splitting_debug <> "" then
      let ftikz, fd = Forward_reach.open_tikz splitting_debug in
      Some ((ftikz, fd), ta, verbose)
    else None
  in

  (* Reachability *)
  let reach =
    try Forward_reach.Bfs.reach ?debug:tex_out ta
    with x ->
      eprintf "Fail to build the forward reachability graph. ";
      raise x
  in

  (match tex_out with
  | Some ((ftikz, _), ta, _) ->
      Format.fprintf ftikz "\\\\\\digraph[scale=0.4]{greach}{%a}@."
        (fun f h ->
          ignore @@ Forward_reach.Bfs.print_graph_dot ~direct:true ta f h)
        reach
  | None -> ());

  let is_det = Forward_reach.Bfs.check_determinism reach in
  if verbose > 0 then (
    printf "%i states found, automaton is %s. [%.2fs]@." (Hashtbl.length reach)
      (if is_det then "deterministic" else "not deterministic")
      (check_time ());
    print_flush ());
  if print_rg then
    printf "Forward Reach : @.%a@."
      (Forward_reach.Bfs.print_reach_graph ta)
      reach;
  Forward_reach.Bfs.print_split_graph ?debug:tex_out 0 reach;
  (tex_out, reach, is_det)

(* Split the reachability graph and export it to zone graphe *)
let splitting ~verbose ~print_rg ta tex_out reach =
  if verbose > 0 then (
    printf "Splitting reachability graph ... ";
    print_flush ());
  let split_reach =
    try reach |> Forward_reach.Bfs.split ?debug:tex_out
    with x ->
      eprintf "Fail to split the reachability graph. ";
      raise x
  in

  tex_out
  |>> (fun ((ftikz, fd), _, _) ->
        Format.fprintf ftikz "\\\\\\digraph[scale=0.4]{gsplit}{%a}@."
          (fun f h ->
            ignore @@ Forward_reach.Bfs.print_graph_dot ~direct:true ta f h)
          split_reach;
        Format.fprintf ftikz "\\end{document}@.";
        close_out fd;
        None)
  |>>| ();

  if verbose > 0 then (
    Gc.full_major ();
    if tex_out <> None then printf "\027[1000DSplitting reachability graph ... ";
    printf "%i states and %i DBMs found. [%.2fs]@."
      (Hashtbl.length split_reach)
      Forward_reach.Bfs.(DBMHashSet.count weak_dbm_hash_set)
      (check_time ()));
  if print_rg then
    printf "After split : @.%a@."
      (Forward_reach.Bfs.print_reach_graph ta)
      split_reach;

  split_reach

module type BoundType = sig
  type t

  val zero : t
  val print : VarSet.varset -> Format.formatter -> t -> unit
  val map : int array -> t -> t
  val get_bound : t -> bound

  type state = int * float array * float

  (*val eval_min_bound : state -> t -> float
    val eval_max_bound : state -> t -> float*)
  val eval_guard : state -> t guard -> float * float

  val split_to_zg :
    Forward_reach.Bfs.Model.timed_automaton ->
    (Forward_reach.Bfs.state_space -> Forward_reach.Bfs.state_space) ->
    Forward_reach.Bfs.state_space ->
    (unit, t) ZoneGraph.t

  val of_common_bound : Common.bound -> t
end

module type S = sig
  type bt

  val input_zone_graph :
    ?verbose:int ->
    ?print_rg:bool ->
    ?splitting_debug:string ->
    ?export_zone_graph:string ->
    ?bound_all_guard:int ->
    string ->
    (unit, bt) ZoneGraph.t

  val input_from_string :
    ?verbose:int ->
    ?print_rg:bool ->
    ?splitting_debug:string ->
    string ->
    (unit, bt) ZoneGraph.t
end

module Make (Bt : BoundType) = struct
  type bt = Bt.t

  let zone_graph_from_ta ?(verbose = 1) ?(print_rg = false)
      ?(splitting_debug = "") ?export_zone_graph ta =
    if print_rg then
      Symrob_extract.TimedAutomaton.Uautomaton.print_timed_automaton
        Format.std_formatter ta;
    let texout, reach, is_det =
      reachability ~verbose ~print_rg ~splitting_debug ta
    in
    let split_reach_fun = splitting ~verbose ~print_rg ta texout in
    let rg = Bt.split_to_zg ta split_reach_fun reach in

    if print_rg then Format.printf "%a@." (ZoneGraph.print Bt.print) rg;
    (match export_zone_graph with
    | None -> ()
    | Some s ->
        let f = open_out s in
        let ff = formatter_of_out_channel f in
        Format.fprintf ff "ptavalue= %a@." (ZoneGraph.print Bt.print) rg;
        close_out f);
    { rg with is_deterministic = is_det }

  let parse_prism lexbuf reset =
    let open Prism in
    let open PrismType in
    let open Lexing in
    let print_position outx lexbuf =
      let pos = lexbuf.lex_curr_p in
      Printf.fprintf outx "%s:%d:%d" pos.pos_fname pos.pos_lnum
        (pos.pos_cnum - pos.pos_bol + 1)
    in
    try
      Preparser.main Prelexer.token !lexbuf;
      Lexing.flush_input !lexbuf;
      !lexbuf.lex_curr_p <- { !lexbuf.lex_curr_p with pos_lnum = 1 };
      reset ();
      let prismfile = ParserPrism.main LexerPrism.token !lexbuf in
      let eval_prism e =
        Type.eval
          ~iname:(fun x ->
            try List.assoc x (fst prismfile.consts) with Not_found -> None)
          ~fname:(fun x ->
            try List.assoc x (snd prismfile.consts) with Not_found -> None)
          e
      in
      let labels =
        List.map (fun (n, v) -> (n, eval_prism v)) prismfile.labels
      in
      { prismfile with labels }
    with
    | LexerPrism.SyntaxError msg ->
        Printf.fprintf stderr "%a: %s\n" print_position !lexbuf msg;
        failwith "Fail to parse Prism file format"
    | Parsing.Parse_error ->
        Printf.fprintf stderr "%a: Parsing error: unexpected token:'%s'\n"
          print_position !lexbuf (lexeme !lexbuf);
        failwith "Fail to parse Prism file format"

  let read_prism s name =
    let open Lexing in
    let lexbuf = Lexing.from_channel s in
    lexbuf.lex_curr_p <- { lexbuf.lex_curr_p with pos_fname = name };
    parse_prism (ref lexbuf) (fun () -> seek_in s 0)

  let read_from_string s =
    let open Lexing in
    let lexbuf = ref (Lexing.from_string s) in
    !lexbuf.lex_curr_p <- { !lexbuf.lex_curr_p with pos_fname = "input area" };
    parse_prism lexbuf (fun () -> lexbuf := Lexing.from_string s)

  let input_zone_graph ?(verbose = 1) ?(print_rg = false)
      ?(splitting_debug = "") ?export_zone_graph ?bound_all_guard regexp
      filename =
    let e = Filename.extension filename in
    match (regexp, e) with
    | Some re, _ ->
        let lexbuf = Lexing.from_string re in
        let open Regexp_lib in
        let re = Regexp.main Regexplex.tok lexbuf in
        let regauto = Automata.Notdet.of_regexp re in
        if print_rg then
          Format.printf "@.%a@." Automata.Notdet.print_dot regauto;
        let a = Automata.Notdet.to_det regauto in
        let ta = To_ta.ta_from_regexp a in
        zone_graph_from_ta ~verbose ~print_rg ~splitting_debug
          ?export_zone_graph ta
    | _, ".json" ->
        (* Input is already a splitted zone graph *)
        if verbose > 0 then printf "Reading splitted reachability graph ... ";
        let ptavalue = string_of_file filename in
        let j = Yojson.Basic.from_string ptavalue in
        let zg = ZoneGraph.of_json Bt.of_common_bound j in
        if verbose > 0 then
          printf "%i states found. [%.2fs]\n"
            (Array.length zg.ZoneGraph.statelist)
            (check_time ());
        if print_rg then Format.printf "@.%a@." (ZoneGraph.print Bt.print) zg;
        zg
        (*failwith "json parser deprecated"*)
    | _, ".xml" | _, ".prism" ->
        (* Input is a time automaton in prism or UPPAAL file format *)
        let ta =
          try
            if e = ".xml" then (
              let (* UPPAAL *)
              open Symrob_extract in
              let open TimedAutomaton.Uautomaton in
              if verbose > 0 then printf "Reading UPPAAL automaton file.";
              let pta = Uppaal_parser.UtaReader.nta_from_file filename in
              make_ta pta [])
            else
              (* Prism *)
              let open Prism in
              let pmod = read_prism (open_in filename) filename in
              if verbose > 0 then printf "Reading Prism automaton file.";
              if print_rg then PrismType.print_prism Format.std_formatter pmod;
              Prism.To_ta.ta_from_prism pmod
          with
          | Uppaal_parser.UtaReader.Parse_error e ->
              eprintf "Fail to build automaton. ";
              eprintf "%s" e;
              exit 1
          | x ->
              eprintf "Fail to build automaton. ";
              raise x
        in
        if verbose > 0 then printf " [%.2fs]@." (check_time ());
        let ta2 =
          match bound_all_guard with
          | None -> ta
          | Some b ->
              Symrob_extract.TimedAutomaton.Uautomaton.bound_all_guard b ta
        in
        zone_graph_from_ta ~verbose ~print_rg ~splitting_debug
          ?export_zone_graph ta2
    | _ -> failwith ("Unkown file extension " ^ e)

  let input_from_string ?(verbose = 1) ?(print_rg = false)
      ?(splitting_debug = "") ?bound_all_guard file_content =
    ignore @@ check_time ();
    (* Input is a time automaton in prism file format *)
    let ta =
      try
        if
          String.length file_content >= 5
          && String.sub file_content 0 5 = "<?xml"
        then (
          let (* UPPAAL *)
          open Symrob_extract in
          let open TimedAutomaton.Uautomaton in
          if verbose > 0 then printf "Reading UPPAAL automaton file.";
          let pta = Uppaal_parser.UtaReader.nta_from_string file_content in
          make_ta pta [])
        else
          (* Prism *)
          let pmod = read_from_string file_content in
          if verbose > 0 then printf "Reading Prism automaton file.";
          if print_rg then Prism.PrismType.print_prism Format.std_formatter pmod;
          Prism.To_ta.ta_from_prism pmod
      with x ->
        eprintf "Fail to build automaton. ";
        raise x
    in
    if verbose > 0 then printf " [%.2fs]@." (check_time ());
    let ta2 =
      match bound_all_guard with
      | None -> ta
      | Some b -> Symrob_extract.TimedAutomaton.Uautomaton.bound_all_guard b ta
    in
    zone_graph_from_ta ~verbose ~print_rg ~splitting_debug ta2
end

module LinearBound = struct
  type t = Common.bound
  type state = int * float array * float

  let get_bound x = x
  let zero = Const 0
  let map = Common.SBound.map
  let print = Common.SBound.print

  let eval_guard st g =
    let open Common in
    let g2 = map_guard (SBound.eval st) g in
    match g2 with Punctual a -> (a, a) | Segment (a, b) -> (a, b)

  let split_to_zg ta split taz =
    let split_reach = split taz in
    Forward_reach.Bfs.to_zone_graph ta split_reach

  let of_common_bound x = x
end

module GeneralBound = struct
  type t = Common.bound list
  type state = int * float array * float

  let get_bound _ = assert false
  let zero = [ Const 0 ]
  let map v = List.map (Common.SBound.map v)

  let print vx f l =
    let open Format in
    fprintf f "[%a]"
      (pp_print_list
         ~pp_sep:(fun _ _ -> pp_print_string f ", ")
         (Common.SBound.print vx))
      l

  let eval_min_bound st =
    List.fold_left (fun m b -> min m (Common.SBound.eval st b)) infinity

  let eval_max_bound st =
    List.fold_left (fun m b -> max m (Common.SBound.eval st b)) 0.0

  let eval_guard st g =
    let open Common in
    match g with
    | Punctual a -> (eval_max_bound st a, eval_min_bound st a)
    | Segment (a, b) -> (eval_max_bound st a, eval_min_bound st b)

  let split_to_zg ta _ taz = Forward_reach.Bfs.to_zone_graph_no_split ta taz
  let of_common_bound x = [ x ]
end
