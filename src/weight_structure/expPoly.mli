module Make : functor
  (P : Polynomial.S)
  (Param : sig
     val smp : float option
   end)
  -> sig
  include Polynomial.WeightStructure

  val is_exp_poly : bool
  val term : P.t -> Common.bound -> t

  module Heaviside : sig
    include Polynomial.WeightStructure
  end
  with type t = t
end
