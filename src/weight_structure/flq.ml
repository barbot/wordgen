module Q : Fl.FSIG = struct
  let is_integer w = Q.den w = Z.one
  let infinity = Q.inf

  let powi x i =
    if i = 0 then Q.one
    else if i > 0 then { Q.num = Z.pow x.Q.num i; Q.den = Z.pow x.Q.den i }
    else { Q.den = Z.pow x.Q.num (-i); Q.num = Z.pow x.Q.den (-i) }

  let is_exact = true

  include Q
end
