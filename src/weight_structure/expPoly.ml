(*

Copyright (C) 2019 LACL Universite Paris Est Creteil Val de Marne
Authors: Benoît Barbot
Licence: GPL-3.0-or-later

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*)

open Common

module Key = struct
  type t =
    | Finite of int * VarSet.var
    | Const of int
    | Infinite
    | TwoVars of int * VarSet.var * VarSet.var
    | FiniteContinuous of float * VarSet.var

  let of_bound = function
    | Common.Infinite -> Infinite
    | Const x -> Const x
    | Finite (i, v) -> Finite (i, v)

  let compare a b = compare a b
end

module Poly = Map.Make (Key)

module Make
    (P : Polynomial.S)
    (Param : sig
      val smp : float option
    end) =
struct
  module PE = struct
    module P = P
    module F = P.F

    (* The lhs is the constant term, the right hand side is interpreted as multyplied by e^sX *)
    type t = P.t Poly.t
    type fl = F.t

    let nb_var = P.nb_var
    let var_string = P.var_string
    let is_exp_poly = true
    let tvar = P.tvar
    let z = P.z
    let s = P.s

    let desc =
      let s =
        match Param.smp with
        | None -> "ExpPoly; s formal; " ^ VarSet.to_string var_string
        | Some s ->
            Printf.sprintf "ExpPoly; s=%g inlined; %s" s
              (VarSet.to_string var_string)
      in
      Printf.sprintf "[%s; %s]"
        (if P.F.is_exact then "exact rational" else "floating point")
        s

    type var = P.var

    let zero = Poly.empty

    let simp_int b f = function
      | 1 -> Format.fprintf f "+"
      | -1 -> Format.fprintf f "-"
      | x when x > 0 && b -> Format.fprintf f "+%i" x
      | x when x > 0 -> Format.fprintf f "%i" x
      | x when b -> Format.fprintf f "+%i" x
      | x -> Format.fprintf f "%i" x

    let simp_float b f = function
      | 1.0 -> Format.fprintf f "+"
      | -1.0 -> Format.fprintf f "-"
      | x when x > 0.0 && b -> Format.fprintf f "+%g" x
      | x when x > 0.0 -> Format.fprintf f "%g" x
      | x when b -> Format.fprintf f "+%g" x
      | x -> Format.fprintf f "%g" x

    let simp_bound ?(pretty = true) vs f = function
      | Key.Infinite -> Format.fprintf f "inf"
      | Const x when pretty -> Format.fprintf f "%as" (simp_int false) x
      | Const x -> Format.fprintf f "%i*s" x
      | Finite (0, v) when v = tvar -> Format.fprintf f "-st"
      | Finite (x, vi) when vi = tvar ->
          Format.fprintf f "-s(%i+%s)" (-x) (fst vs.(VarSet.int_of_var vi))
      | Finite (x, vi) ->
          Format.fprintf f "-s(%i-%s)" (-x) (fst vs.(VarSet.int_of_var vi))
      | TwoVars (_, _, _) -> assert false
      | FiniteContinuous (_, _) -> assert false

    let print ?(pretty = true) f p =
      if p = Poly.empty then Format.fprintf f "0";
      ignore
      @@ Poly.fold
           (fun k v b ->
             match (P.to_float v, pretty) with
             | None, true ->
                 if b then Format.fprintf f "+";
                 if k = Const 0 then P.print f v
                 else
                   Format.fprintf f "e^{%a}(%a)" (simp_bound var_string) k
                     (P.print ~pretty) v;
                 true
             | Some fv, true ->
                 if k <> Const 0 then
                   Format.fprintf f "%ae^{%a}" (simp_float b) fv
                     (simp_bound var_string) k
                 else
                   Format.fprintf f "%s%g"
                     (if b && fv >= 0.0 then "+" else "")
                     fv;
                 true
             | _, false ->
                 if b then Format.fprintf f "+";
                 Format.fprintf f "exp(%a)*(%a)"
                   (simp_bound ~pretty var_string)
                   k (P.print ~pretty:false) v;
                 true)
           p false

    let add_mon k v p =
      if v = P.zero then p
      else
        Poly.update k
          (function
            | None -> Some v
            | Some v2 ->
                let v3 = P.( +.. ) v v2 in
                if v3 <> P.zero then Some v3 else None)
          p

    let add a b =
      Poly.union
        (fun _ c1 c2 ->
          let s = P.( +.. ) c1 c2 in
          if s <> P.zero then Some s else None)
        a b

    let ( +.. ) a b = add a b

    (* let mult_scalar v p =
       if v = P.zero then Poly.empty
       else Poly.fold (fun k2 v2 p2 -> Poly.add k2 (P.( *..) v v2) p2) p Poly.empty*)
    let mult_scalar v p =
      if v = F.zero then Poly.empty
      else
        Poly.fold (fun k2 v2 p2 -> Poly.add k2 (P.( **. ) v v2) p2) p Poly.empty

    let ( **. ) v p = mult_scalar v p
    let ( -.. ) a b = add a (mult_scalar F.minus_one b)

    (* let mult_poly v p =
       Poly.fold (fun k v2 p2 ->
           add_mon k (P.( *..) v v2) p2
         )
           p
         Poly.empty*)

    let ( *.. ) a b =
      match Poly.choose_opt a with
      | Some (Const 0, p) ->
          assert (Poly.cardinal a = 1);
          Poly.fold
            (fun k2 v2 p2 -> Poly.add k2 (P.( *.. ) p v2) p2)
            b Poly.empty
      | Some _ -> assert false
      | None -> Poly.empty

    let vector_space = (zero, ( +.. ), fun x y -> F.of_float x **. y)
    let map f p = Poly.map f p

    let var ?exp v =
      let ex = P.var ?exp v in
      Poly.singleton (Const 0) ex

    let term p b = Poly.singleton (Key.of_bound b) p

    let const x =
      (* Const 0 means e-0 i.e. 1.0 *)
      Poly.singleton (Const 0) (P.const x)

    let one = const F.one

    (* let exppoly p var =
       let ca,a = copy zero in
       a.(var) <- p;
       ca,a
    *)

    (* let apply_ep ep f =
       Poly.fold (fun k v acc ->
           let k2,v2 = f k v in
           add_mon k2 v2 acc)
         ep Poly.empty*)

    let poly_of_key = function
      | Key.Infinite -> P.const F.infinity
      | Const a -> P.const (F.of_int a)
      | Finite (a, c) ->
          assert (c <> VarSet.var_of_int 0);
          (* use Const instead *)
          let rv = P.const (F.of_int a) in
          P.(rv -.. var c)
      | TwoVars (a, v1, v2) ->
          assert (v1 <> VarSet.var_of_int 0);
          assert (v2 <> VarSet.var_of_int 0);
          (* use Const instead *)
          let rv = P.const (F.of_int a) in
          P.(rv -.. var v1 -.. var v2)
      | FiniteContinuous (a, c) ->
          assert (c <> VarSet.var_of_int 0);
          let rv = P.const (F.of_float a) in
          P.(rv -.. var c)

    let map_var ep map =
      Poly.fold
        (fun k v acc ->
          let v2 = P.map_var v map in
          match k with
          | Infinite -> assert false
          | Finite (c, vi) ->
              let j = map vi in
              if j <> VarSet.zero_var then add_mon (Finite (c, j)) v2 acc
              else add_mon (Const c) v2 acc
          | Const _ -> add_mon k v2 acc
          | FiniteContinuous (c, vi) ->
              let j = map vi in
              add_mon (FiniteContinuous (c, j)) v2 acc
          | TwoVars (c, vi1, vi2) ->
              let j1 = map vi1 and j2 = map vi2 in
              if j1 = VarSet.zero_var && j2 = VarSet.zero_var then
                add_mon (Const c) v2 acc
              else if j1 = VarSet.zero_var then add_mon (Finite (c, j2)) v2 acc
              else if j2 = VarSet.zero_var then add_mon (Finite (c, j1)) v2 acc
              else add_mon (TwoVars (c, j1, j2)) v2 acc)
        ep Poly.empty

    let s_poly =
      match Param.smp with None -> P.var s | Some s -> P.const (F.of_float s)

    let var_set ep =
      let open Polynomial.VarSubSet in
      Poly.fold
        (fun k v acc ->
          let vs2 = union acc (P.var_set v) in
          match k with
          | Infinite | Const _ -> vs2
          | Finite (_, vi) | FiniteContinuous (_, vi) ->
              union vs2 (singleton vi)
          | TwoVars (_, vi1, vi2) ->
              union (union vs2 (singleton vi1)) (singleton vi2))
        ep empty

    let one_over_s =
      match Param.smp with
      | None -> P.var ~exp:(-1) s
      | Some s -> P.const (F.of_float (1.0 /. s))

    (* compute \int_X^\infty EP(t) e^{-s t} d dt *)
    let primitive ep t =
      Poly.fold
        (fun k v acc ->
          match k with
          | Infinite -> assert false
          | Finite (c, vi) when vi = t ->
              add_mon (Const c) (P.primitive v t) acc
          | Const c ->
              add_mon
                (Finite (c, t))
                (P.( **. ) F.minus_one (P.laplace v one_over_s t))
                acc
          | Finite (_, _) -> add_mon k (P.primitive v t) acc
          | _ -> assert false)
        ep Poly.empty

    let diff ep t =
      Poly.fold
        (fun k v acc ->
          match k with
          | Infinite -> assert false
          (* diff w.r. to s *)
          | Const c when t = s ->
              add_mon k P.(diff v t +.. (F.of_int c **. v)) acc
          | Finite (_, vi) when t = s ->
              assert (vi <> t);
              let p = poly_of_key k in
              add_mon k P.(diff v t +.. (p *.. v)) acc
          | Const _ -> add_mon k (P.diff v t) acc
          (* diff w.r. to t *)
          | Finite (_, vi) when vi = t && t = t ->
              add_mon k P.(diff v t -.. (s_poly *.. v)) acc
          (* diff w.r. to clocks *)
          | Finite (_, vi) when vi = t ->
              add_mon k P.(diff v t +.. (s_poly *.. v)) acc
          | Finite _ -> add_mon k (P.diff v t) acc
          | _ -> assert false)
        ep Poly.empty

    let rec taylor_exp_exp p n =
      if n = 0 then P.(one, one)
      else if n = 1 then (p, P.( +.. ) P.one p)
      else
        let nf, v = taylor_exp_exp p (n - 1) in
        let nf2 = P.( **. ) F.(div F.one (of_int n)) (P.( *.. ) p nf) in
        (nf2, P.( +.. ) nf2 v)

    let taylor_exp_s ep n =
      let cons =
        Poly.fold
          (fun k v acc ->
            let open P in
            match k with
            | Infinite -> assert false
            | Const c ->
                let p = F.of_int c **. var s in
                acc +.. ((snd @@ taylor_exp_exp p n) *.. v)
            | Finite _ ->
                let p = poly_of_key k *.. var s in
                acc +.. ((snd @@ taylor_exp_exp p n) *.. v)
            | _ -> assert false)
          ep P.zero
      in
      Poly.singleton (Const 0) cons

    let dominating_s ep =
      match Poly.choose_opt ep with
      | None -> ((0, 0, F.zero), (0, 0, F.zero))
      | Some (Const ct, vt) ->
          let (cl, vl), (ch, vh) =
            Poly.fold
              (fun k v (((cl, vl), (ch, vh)) as acc) ->
                match k with
                | Const c when c < cl -> ((c, v), (ch, vh))
                | Const c when c > ch -> ((cl, vl), (c, v))
                | Const _ -> acc
                | _ -> assert false)
              ep
              ((ct, vt), (ct, vt))
          in
          let _, (_, dl, vlf) = P.dominating_s vl in
          let (_, dh, vhf), _ = P.dominating_s vh in
          ((cl, dl, vlf), (ch, dh, vhf))
      | _ -> assert false

    let apply_bound ep var bound =
      let pb = P.of_bound bound in
      Poly.fold
        (fun k v acc ->
          let v2 = P.apply v var pb in
          match k with
          | Infinite -> assert false
          | Const _ -> add_mon k v2 acc
          | Finite (_, var2) when var2 <> var -> add_mon k v2 acc
          | Finite (a, _) -> (
              let isvart = if var = tvar then -1 else 1 in
              match (bound, Param.smp) with
              | Infinite, _ -> acc
              | Const const_bound, None ->
                  add_mon (Const (a + (isvart * const_bound))) v2 acc
              | Finite (const_bound, var_bound), None ->
                  add_mon
                    (Finite (a + (isvart * const_bound), var_bound))
                    v2 acc
              | Const const_bound, Some s ->
                  assert (not F.is_exact);
                  let v3 =
                    P.( **. )
                      (F.of_float @@ exp (s *. float (isvart * const_bound)))
                      v2
                  in
                  add_mon (Const 0) v3 acc
              | Finite (const_bound, var_bound), Some s ->
                  assert (not F.is_exact);
                  let v3 =
                    P.( **. )
                      (F.of_float @@ exp (s *. float (isvart * const_bound)))
                      v2
                  in
                  add_mon (Finite (0, var_bound)) v3 acc)
          | _ -> assert false)
        ep Poly.empty

    let elapse_time ep t = map (fun p -> P.elapse_time p t) ep

    (* Check consistency of parameter s*)
    let smpv ?smp () =
      match (smp, Param.smp) with
      | None, Some _ when F.is_exact -> assert false
      | None, Some x -> x
      | Some x, None -> x
      | Some a, Some b when a = b -> a
      | Some _, Some _ -> failwith "Conflicting value for s parameter"
      | None, None -> failwith "No value for s parameter"

    let apply_const ?smp ep var c =
      if var = z then
        Poly.fold
          (fun k v acc ->
            let v2 = P.apply_const ?smp v var c in
            add_mon k v2 acc)
          ep Poly.empty
      else
        let x = ref None in
        let sval () =
          match !x with
          | Some x -> x
          | None ->
              let y = smpv ?smp () in
              x := Some y;
              y
        in
        if var = s then assert (F.to_float c = sval ());
        Poly.fold
          (fun k v acc ->
            let v2 = P.apply_const ?smp v var c in
            match (k, var = s) with
            | Infinite, _ -> assert false
            | Const _, false -> add_mon k v2 acc
            | Finite (_, var2), false when var2 <> var -> add_mon k v2 acc
            | Finite (a, _), false ->
                let isvart = if var = tvar then -1.0 else 1.0 in
                let v3 =
                  P.( **. )
                    (F.of_float
                    @@ exp (sval () *. (float a +. (isvart *. F.to_float c))))
                    v2
                in
                add_mon (Const 0) v3 acc
            | Const a, true ->
                let v3 =
                  P.( **. ) (F.of_float @@ exp (sval () *. float a)) v2
                in
                add_mon (Const 0) v3 acc
            | Finite (a, v), true ->
                let v3 =
                  P.( **. ) (F.of_float @@ exp (sval () *. float a)) v2
                in
                add_mon (Finite (0, v)) v3 acc
            | _ -> assert false)
          ep Poly.empty

    let to_float ?smp p =
      let x = ref None in
      let smpv2 () =
        match !x with
        | Some x -> x
        | None ->
            let y = smpv ?smp () in
            x := Some y;
            y
      in
      Poly.fold
        (fun k v acc ->
          let v2 = P.apply_const v s (F.of_float (smpv2 ())) in
          match (acc, k, P.to_float v2) with
          | None, _, _ -> None
          | _, _, None -> None
          | _, Finite _, _ -> None
          | _, Infinite, _ -> None
          | Some f, Const e, Some vf ->
              Some (f +. (exp (smpv2 () *. float e) *. vf))
          | _ -> assert false)
        p (Some 0.0)

    let apply_all ?smp p tab =
      let _, p =
        Array.fold_left
          (fun (i, pit) v ->
            (i + 1, apply_const ?smp pit (VarSet.var_of_int i) (F.of_float v)))
          (1, p) tab
      in
      match to_float ?smp p with
      | Some x -> x
      | None -> raise Polynomial.Not_fully_evaluated

    let fully_apply_float ?smp p x f =
      let p2 = apply_const ?smp p x (F.of_float f) in
      match to_float ?smp p2 with
      | Some x -> x
      | None -> raise Polynomial.Not_fully_evaluated

    let find_root ?smp ?factor ?max_iter ?bound p ?diffp x guess_p =
      (*let p2 = match smp with
            None -> p
          | Some s -> apply_const ?smp p (P.var_of_int (P.nb_var-1)) (F.of_float s) in
        Format.eprintf "finding root for %a @." P.print p; *)
      let pp = match diffp with None -> diff p x | Some pp -> pp in
      let f v =
        let f0 = fully_apply_float ?smp p x v
        and f1 = fully_apply_float ?smp pp x v in
        (f0, f1)
      in
      try Math.newton_raphson_iterate ?factor ?max_iter ?bound f guess_p
      with Math.Not_converging (r, v) ->
        Format.eprintf "Alert Not converging %a --> %g -> %g@."
          (print ~pretty:true) p r v;
        r

    let inverse_laplace _ _ = failwith "not implemented"
  end

  include PE

  module Heaviside = struct
    include PE

    let diff _ _ = failwith "Not implemented"

    let desc =
      let s =
        match Param.smp with
        | None -> "HeavisidePoly; s formal; " ^ VarSet.to_string var_string
        | Some s ->
            Printf.sprintf "HeavisidePoly; s=%g inlined; %s" s
              (VarSet.to_string var_string)
      in
      Printf.sprintf "[%s; %s]"
        (if P.F.is_exact then "exact rational" else "floating point")
        s

    let get_piecewise p =
      let acc =
        Poly.fold
          (fun k _ acco ->
            match (k, acco) with
            | Finite (x, _), Some acc -> Some (-x :: acc)
            | _ -> None)
          p (Some [])
      in
      match acc with
      | None -> []
      | Some l ->
          let l2 = List.sort compare l in
          let rec aux = function
            | [] -> []
            | [ a ] ->
                let p2 = Poly.fold (fun _ v acc -> P.( +.. ) acc v) p P.zero in
                [ (a, max_int, p2) ]
            | a :: b :: q ->
                let p2 =
                  Poly.fold
                    (fun k v acc ->
                      match k with
                      | Finite (x, _) when -x < b -> P.( +.. ) acc v
                      | _ -> acc)
                    p P.zero
                in
                (a, b, p2) :: aux (b :: q)
          in
          aux l2

    let simp_bound f b =
      let pv x =
        if x = tvar then "-t" else "+" ^ fst var_string.(VarSet.int_of_var x)
      in
      match b with
      | Key.Infinite -> Format.fprintf f "inf"
      | Const x -> Format.fprintf f "%as" (simp_int false) x
      | Finite (0, _) -> ()
      | Finite (x, vi) -> Format.fprintf f "*u(%s-%i)" (pv vi) (-x)
      | TwoVars (x, v1, v2) when x = 0 ->
          Format.fprintf f "*u(%s%s)"
            (fst var_string.(VarSet.int_of_var v2))
            (pv v1)
      | TwoVars (x, v1, v2) ->
          Format.fprintf f "*u(%s-%i%s)"
            (fst var_string.(VarSet.int_of_var v2))
            (-x) (pv v1)
      | FiniteContinuous (x, v1) -> Format.fprintf f "*u(%s-%g)" (pv v1) (-.x)

    let print ?(pretty = true) f p =
      let open Format in
      if p = Poly.empty then fprintf f "0";
      let piecewise = get_piecewise p in
      if piecewise = [] || true then
        ignore
        @@ Poly.fold
             (fun k v b ->
               match P.to_float v with
               | None ->
                   if b then fprintf f "+";
                   if k = Const 0 then P.print ~pretty f v
                   else fprintf f "(%a)%a" (P.print ~pretty) v simp_bound k;
                   true
               | Some fv ->
                   if k <> Const 0 then
                     fprintf f "%a%a" (simp_float b) fv simp_bound k
                   else fprintf f "%s%g" (if b && fv >= 0.0 then "+" else "") fv;
                   true)
             p false
      else
        List.iter
          (fun (a, b, p2) ->
            fprintf f "]%i,%a[->%a,@ " a
              (fun f x ->
                if x = max_int then pp_print_string f "inf"
                else pp_print_int f b)
              b (P.print ~pretty) p2)
          piecewise

    let apply_const ?smp:_ ep var c =
      if var = z then assert false else if var = s then assert false;
      Poly.fold
        (fun k v acc ->
          let v2 = P.apply_const v var c in
          match k with
          | Infinite -> assert false
          | Const _ -> add_mon k v2 acc
          | (Finite (_, var2) | FiniteContinuous (_, var2)) when var2 <> var ->
              add_mon k v2 acc
          | Finite (a, _) ->
              let isvart = if var = tvar then -1.0 else 1.0 in
              if float a +. (isvart *. F.to_float c) >= 0.0 then
                add_mon (Const 0) v2 acc
              else acc
          | FiniteContinuous (a, _) ->
              let isvart = if var = tvar then -1.0 else 1.0 in
              if a +. (isvart *. F.to_float c) >= 0.0 then
                add_mon (Const 0) v2 acc
              else acc
          | TwoVars (a, va1, va2) ->
              assert (va2 = var);
              add_mon (FiniteContinuous (float a +. F.to_float c, va1)) v2 acc)
        ep Poly.empty

    let to_float ?smp:_ p =
      Poly.fold
        (fun k v _ ->
          if k <> Const 0 then (
            Format.printf "fail to evaluate %a" (print ~pretty:true) p;
            assert false);
          P.to_float v)
        p (Some 0.0)

    let apply_all ?smp p tab =
      let _, p =
        Array.fold_left
          (fun (i, pit) v ->
            (i + 1, apply_const ?smp pit (VarSet.var_of_int i) (F.of_float v)))
          (1, p) tab
      in
      match to_float ?smp p with
      | Some x -> x
      | None -> raise Polynomial.Not_fully_evaluated

    let fully_apply_float ?smp:_ p x f =
      let p2 = apply_const p x (F.of_float f) in
      match to_float p2 with
      | Some x -> x
      | None -> raise Polynomial.Not_fully_evaluated

    let inverse_laplace time_var ep =
      let poly =
        Poly.fold
          (fun k v acc ->
            (*Format.fprintf Format.std_formatter "\t%a -> " PE.print
              (add_mon k v PE.zero);*)
            match k with
            | Infinite -> assert false
            (* | Const c when t < float (-c) -> acc*)
            | Const c ->
                let invlap = P.inverse_laplace time_var v in
                let invlap2 =
                  P.(
                    apply invlap time_var (var time_var +.. const (F.of_int c)))
                in
                (*Format.fprintf Format.std_formatter "c:%i:%a@." c print
                  (add_mon (Finite (c, time_var)) invlap2 PE.zero);*)
                add_mon (Finite (c, time_var)) invlap2 acc
            | Finite (c, v1) ->
                let isvart =
                  if v1 <> tvar then P.var v1 else P.(zero -.. var v1)
                in
                let invlap = P.inverse_laplace time_var v in
                let invlap2 =
                  P.(
                    apply invlap time_var
                      (var time_var +.. const (F.of_int c) +.. isvart))
                in
                (*Format.fprintf Format.std_formatter "f%a@." print
                  (add_mon (TwoVars (c, v1, time_var)) invlap2 PE.zero);*)
                add_mon (TwoVars (c, v1, time_var)) invlap2 acc
            (* Format.eprintf
                 "Connot compute inverse laplace transform %a -> %a@." PE.print
                 ep (PE.simp_bound var_string) b;
               assert false*)
            | FiniteContinuous _ -> assert false
            | TwoVars _ -> assert false)
          ep Poly.empty
      in

      (*Format.fprintf Format.std_formatter "%a -> %a@." PE.print ep print poly;*)
      poly

    let find_root ?smp ?factor ?max_iter ?bound p ?diffp x guess_p =
      let pp = match diffp with None -> assert false | Some pp -> pp in
      let f v =
        let f0 = fully_apply_float ?smp p x v
        and f1 = fully_apply_float ?smp pp x v in
        (f0, f1)
      in
      try Math.newton_raphson_iterate ?factor ?max_iter ?bound f guess_p
      with Math.Not_converging (r, v) ->
        Format.eprintf "Alert Not converging %a --> %g -> %g@."
          (print ~pretty:true) p r v;
        r
  end
end

(*
module Test =struct

  module P = Polynome.Make( struct let var_string = [| "t"; "x"; "y"|] end)
  open Format

  let t = P.var_of_int 0
  let x = P.var_of_int 1
  let y = P.var_of_int 2

  let _ =
    let open P in
    let p = const 3.14 +.. var t +.. var x in
    printf "p=%a\n" print p;
    let p2 = p *.. p in
    printf "p^2=%a\n" print p2;
    printf "degree(0)=%i; degree(1.0)=%i; degree(p)=%i; degree(p2)=%i\n"
      (degree zero) (degree @@ const 1.0) (degree p) (degree p2);
    let p3 = (var t) *.. (var t) +.. (var x) *.. (var t) +.. (var y) in
    let p4 = laplace p3 (const 3.0) t in
    printf "p3=%a@, p4=%a@." print p3 print p4

  module EP = Make(P)(struct let smp = 0.333333 end)

  let _ =
    let open EP in
    let p = const 3.14 +.. exppoly (P.(+..) (P.var x) (P.var y)) (int_of_var x) in
    printf "p=%a\n" print p;
    (*let p2 = apply p (const (1./.3.)) x (P.(+..) (P.const 5.0) (P.var x) ) in
    printf "p2=%a\n" print p2;*)

end
*)
