module type FSIG = sig
  type t

  val equal : t -> t -> bool
  val zero : t
  val one : t
  val minus_one : t
  val infinity : t
  val neg : t -> t
  val add : t -> t -> t
  val sub : t -> t -> t
  val mul : t -> t -> t
  val powi : t -> int -> t
  val div : t -> t -> t
  val max : t -> t -> t
  val of_int : int -> t
  val to_int : t -> int
  val to_string : t -> string
  val of_float : float -> t
  val to_float : t -> float
  val is_integer : t -> bool
  val is_exact : bool
end

module Float : FSIG = struct
  include Float

  let zero = 0.0
  let one = 1.0
  let minus_one = -1.0
  let max x y = max x y
  let to_float x = x
  let of_float x = x
  let powi x i = pow x (float i)
  let is_integer x = x = floor x
  let is_exact = false
end

module Num : FSIG = struct
  open Big_int
  open Ratio

  type t = ratio

  let equal = eq_ratio
  let zero = create_ratio zero_big_int unit_big_int
  let one = create_ratio unit_big_int unit_big_int
  let minus_one = minus_ratio one
  let infinity = create_ratio unit_big_int unit_big_int
  let neg = minus_ratio
  let add = add_ratio
  let sub = sub_ratio
  let mul = mult_ratio
  let div = div_ratio
  let is_integer = is_integer_ratio
  let to_float = float_of_ratio
  let to_string = string_of_ratio
  let of_int = ratio_of_int
  let max = max_ratio

  let powi i e =
    if e >= 0 then power_ratio_positive_int i e
    else
      let i2 = inverse_ratio i in
      power_ratio_positive_int i2 (-e)

  let to_int i =
    assert (is_integer_ratio i);
    int_of_big_int (numerator_ratio i)

  let of_float x =
    let fr, exp = frexp x in
    let b1 = big_int_of_int (int_of_float (fr *. (2.0 ** 17.0))) in
    let exp2 = exp - 17 in
    let exp3 =
      if exp2 >= 0 then power_ratio_positive_int (ratio_of_int 2) exp2
      else
        power_ratio_positive_int
          (create_ratio unit_big_int (big_int_of_int 2))
          (-exp2)
    in
    mul (ratio_of_big_int b1) exp3

  let is_exact = true
end
