(*
Copyright (C) 2019 LACL Universite Paris Est Creteil Val de Marne
Authors: Benoît Barbot
Licence: GPL-3.0-or-later

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*)
open Common

module VarSubSet = Set.Make (struct
  type t = VarSet.var

  let compare a b = compare a b
end)

module type BASE = sig
  module F : Fl.FSIG

  type t
  type fl = F.t
  type var = VarSet.var

  val desc : string
  val tvar : var
  val var_string : Common.VarSet.varset
  val zero : t
  val one : t
  val var : ?exp:int -> var -> t
  val const : fl -> t
  val ( +.. ) : t -> t -> t
  val ( -.. ) : t -> t -> t
  val ( **. ) : fl -> t -> t
  val print : ?pretty:bool -> Format.formatter -> t -> unit
  val apply_const : ?smp:float -> t -> var -> fl -> t
  val apply_all : ?smp:float -> t -> float array -> float
  val fully_apply_float : ?smp:float -> t -> var -> float -> float
  val diff : t -> var -> t
  val is_exp_poly : bool
  val var_set : t -> VarSubSet.t
end

module type WeightStructure = sig
  include BASE

  val z : var
  val s : var
  val nb_var : int
  val ( *.. ) : t -> t -> t
  val vector_space : t * (t -> t -> t) * (float -> t -> t)
  val apply_bound : t -> var -> bound -> t
  val primitive : t -> var -> t
  val map_var : t -> (var -> var) -> t
  val elapse_time : t -> var -> t
  val to_float : ?smp:float -> t -> float option
  val taylor_exp_s : t -> int -> t
  val inverse_laplace : var -> t -> t
  val dominating_s : t -> (int * int * F.t) * (int * int * F.t)

  val find_root :
    ?smp:float ->
    ?factor:float ->
    ?max_iter:int ->
    ?bound:float * float ->
    t ->
    ?diffp:t ->
    var ->
    float ->
    float
end

module type SamplingStruct = sig
  include BASE

  val inverse_laplace : var -> t -> t

  val find_root :
    ?smp:float ->
    ?factor:float ->
    ?max_iter:int ->
    ?bound:float * float ->
    t ->
    ?diffp:t ->
    var ->
    float ->
    float
end

module type WeightForSampling = sig
  include WeightStructure
  module SamplingW : SamplingStruct with type t = t
end

module Poly = Map.Make (struct
  type t = int array

  let compare a b = compare a b
end)


module type S = sig
  include WeightStructure

  type ftp

  val degree : t -> int
  val div_euclidean : t -> t -> t * t
  val pow : t -> int -> t
  val apply : t -> var -> t -> t
  val to_const : t -> fl option
  val to_float_poly : t -> ftp
  val of_float_poly : ftp -> t
  val integral : t -> var -> t -> t -> t
  val iter_on_var : (var -> 'a -> 'a) -> 'a -> 'a
  val laplace : t -> t -> var -> t
  val of_bound : bound -> t
  val map_var : t -> (var -> var) -> t
  val unif : t -> t
  val find_root_sturm : ?factor:float -> t -> var -> float -> float
end

let iter_fun n f x =
  let rec aux i y = if i = n then y else aux (i + 1) (f i y) in
  aux 0 x

exception Not_fully_evaluated

module Make
    (F : Fl.FSIG)
    (K : sig
      val var_string : VarSet.varset
    end) =
struct
  include K
  include VarSet
  module F = F

  (*type var = VarSet.var*)
  type fl = F.t
  type t = fl Poly.t
  type ftp = float Poly.t

  let is_exp_poly = false

  (*let var_string = [|"t"; "x"; "y"; "z"; "x_1"; "x_2";"x_3";"x_4";"x_5";"x_6";"x_7"|]*)

  let tvar = VarSet.find_var var_string "t"
  let z = VarSet.find_var var_string "z"
  let s = VarSet.find_var var_string "s"

  let desc =
    Printf.sprintf "[%s; Poly; no s; %s]"
      (if F.is_exact then "exact rational" else "floating point")
      (VarSet.to_string var_string)

  let nb_var = Array.length var_string
  let degree_monome m = Array.fold_left ( + ) 0 m

  let degree' p =
    Poly.fold
      (fun m mc (d, c) ->
        let dm = degree_monome m in
        if dm > d then (dm, mc) else (d, c))
      p (-1, F.zero)

  let degree p = fst @@ degree' p

  let print_monome ?(pretty = true) f l =
    let open Format in
    ignore
    @@ Array.iteri
         (fun i x ->
           let s i = fst var_string.(i) in
           match x with
           | 0 -> ()
           | 1 when not pretty -> fprintf f "*%s" (s i)
           | 1 -> fprintf f "%s" (s i)
           | _ when not pretty -> fprintf f "*%s**%i" (s i) x
           | -1 -> fprintf f "%s⁻¹" (s i)
           | -2 -> fprintf f "%s⁻²" (s i)
           | -3 -> fprintf f "%s⁻³" (s i)
           | 2 -> fprintf f "%s²" (s i)
           | 3 -> fprintf f "%s³" (s i)
           | 4 -> fprintf f "%s⁴" (s i)
           | 5 -> fprintf f "%s⁵" (s i)
           | _ -> fprintf f "%s^%i" (s i) x)
         l

  let is_not_const l = Array.exists (fun x -> x <> 0) l

  let print ?(pretty = true) f a =
    if a = Poly.empty then Format.fprintf f "0";
    ignore
    @@ Poly.fold
         (fun k v b ->
           if b && v > F.zero then Format.fprintf f "+";
           Format.fprintf f "%s%a"
             (match (F.is_exact || not pretty, v, is_not_const k) with
             | false, v1, true when v1 = F.of_float 1.0 -> ""
             | false, v1, true when v1 = F.of_float (-1.0) -> "-"
             | false, v1, _ when v1 = F.of_float 0.5 -> "½"
             | false, v1, _ when v1 = F.of_float (-0.5) -> "-½"
             | false, v1, _ when v1 = F.of_float 0.25 -> "¼"
             | false, v1, _ when v1 = F.of_float 0.75 -> "¾"
             | false, v1, _ when v1 = F.of_float (-0.75) -> "-¾"
             | false, v1, _ when v1 = F.of_float (-0.25) -> "-¼"
             | _, x, _ when F.is_integer x && pretty ->
                 string_of_int (F.to_int x)
             | _, x, _ when F.is_integer x -> string_of_int (F.to_int x) ^ ".0"
             | _ ->
                 let s = F.to_string v in
                 if String.index_opt s '.' = None then s ^ ".0 " else s)
             (print_monome ~pretty) k;
           true)
         a false

  let monome coeff k = Poly.singleton coeff k
  let zero = Poly.empty

  let build_key_var exp v =
    Array.init nb_var (fun j -> if v = var_of_int j then exp else 0)

  let var ?exp:(e = 1) i =
    if i = zero_var then zero else monome (build_key_var e i) F.one

  and const f =
    if F.equal f F.zero then zero else monome (Array.make nb_var 0) f

  let one = const F.one
  let mult_monome_keys k1 k2 = Array.mapi (fun i i2 -> i2 + k1.(i)) k2

  let is_zero x =
    (* Compenssate for numerical imprecision *)
    abs_float (F.to_float x) < 1e-100
  (*F.equal x F.zero*)

  let add a b =
    Poly.union
      (fun _ c1 c2 ->
        let s = F.add c1 c2 in
        if not (is_zero s) then Some s else None)
      a b

  let ( +.. ) a b = add a b

  let add_mon k v p =
    if F.equal v F.zero then p
    else
      Poly.update k
        (function
          | None -> Some v
          | Some v2 ->
              let v3 = F.add v v2 in
              if not (F.equal v3 F.zero) then Some v3 else None)
        p

  let mult_scalar v p =
    if F.equal v F.zero then Poly.empty
    else Poly.fold (fun k2 v2 p2 -> Poly.add k2 (F.mul v v2) p2) p Poly.empty

  let ( **. ) v p = mult_scalar v p

  let mult_monome k1 v1 p =
    Poly.fold
      (fun k2 v2 p2 -> add_mon (mult_monome_keys k1 k2) (F.mul v1 v2) p2)
      p Poly.empty

  let ( -.. ) a b = add a (mult_scalar (F.neg F.one) b)

  let mult p1 p2 =
    Poly.fold (fun k v pacc -> add pacc (mult_monome k v p2)) p1 Poly.empty

  let ( *.. ) a b = mult a b

  let var_set p =
    Poly.fold
      (fun k _ acc ->
        snd
        @@ Array.fold_left
             (fun (i, acc2) exp ->
               if exp > 0 then (i + 1, VarSubSet.add (var_of_int i) acc2)
               else (i + 1, acc2))
             (0, acc) k)
      p VarSubSet.empty

  (*let get_var_set p = VarSubSet.to_list (var_set p)*)

  let div_euclidean p1 p2 =
    let vss1 = var_set p1 and vss2 = var_set p2 in
    assert (VarSubSet.cardinal vss1 <= 1 && VarSubSet.cardinal vss2 <= 1);
    let vss = VarSubSet.union vss1 vss2 in
    assert (VarSubSet.cardinal @@ vss <= 1);
    match VarSubSet.choose_opt vss with
    | None ->
        let _, c1 = degree' p1 and _, c2 = degree' p2 in
        (const (F.div c1 c2), zero)
    | Some cvar ->
        let rec div acc p1 p2 =
          let d1, c1 = degree' p1 and d2, c2 = degree' p2 in
          if d1 < d2 then (acc, p1)
          else
            let key = build_key_var (d1 - d2) cvar in
            let coeff = F.div c1 c2 in
            let p3 = p1 -.. mult_monome key coeff p2 in
            (*let m = monome key coeff in
              Format.printf "monome:%a p3:%a@." print m print p3;*)
            div (add_mon key coeff acc) p3 p2
        in
        div Poly.empty p1 p2

  let unif p =
    let _, c = degree' p in
    F.div F.one c **. p

  let vector_space = (zero, ( +.. ), fun x y -> F.of_float x **. y)

  let rec pow p = function
    | 0 -> const F.one
    | 1 -> p
    (*    | i -> mult p (pow p (i-1))*)
    | i when i mod 2 = 0 ->
        let p2 = pow p (i / 2) in
        mult p2 p2
    | i ->
        let p2 = pow p (i / 2) in
        mult p (mult p2 p2)

  let proj_monome i l =
    let a = l.(int_of_var i) in
    let b = Array.copy l in
    b.(int_of_var i) <- 0;
    (a, b)

  let apply_monome k v x p =
    let expo, k2 = proj_monome x k in
    assert (expo >= 0);
    if expo > 0 then mult_monome k2 v (pow p expo) else monome k v

  let add_apply_monome_const k v x f acc =
    let expo, k2 = proj_monome x k in
    if expo <> 0 then add_mon k2 (F.mul v (F.powi f expo)) acc
    else add_mon k v acc

  let apply_const ?smp:_ p x c =
    Poly.fold (fun k v pacc -> add_apply_monome_const k v x c pacc) p Poly.empty

  let map_var p f =
    Poly.fold
      (fun k v pacc ->
        let k2 = Array.make nb_var 0 in
        let is_zero = ref false in
        for i = 0 to Array.length k - 1 do
          let fi = int_of_var @@ f @@ var_of_int i in
          if fi <> int_of_var zero_var then k2.(fi) <- k2.(fi) + k.(i)
          else if k.(i) > 0 then is_zero := true
        done;
        if !is_zero then pacc
        else
          match Poly.find_opt k2 pacc with
          | None -> Poly.add k2 v pacc
          | Some v2 ->
              let v3 = F.add v v2 in
              let pacc2 = Poly.remove k2 pacc in
              if v3 <> F.zero then Poly.add k2 v3 pacc2 else pacc2)
      p zero

  let to_float_poly p = Poly.map F.to_float p
  let of_float_poly p = Poly.map F.of_float p

  let to_float ?smp:_ p =
    match Poly.cardinal p with
    | 0 -> Some 0.0
    | 1 ->
        let k, v = Poly.choose p in
        if is_not_const k then None else Some (F.to_float v)
    | _ -> None

  let to_const p =
    match Poly.cardinal p with
    | 0 -> Some F.zero
    | 1 ->
        let k, v = Poly.choose p in
        if is_not_const k then None else Some v
    | _ -> None

  let apply p1 x p2 =
    match to_const p2 with
    | None ->
        Poly.fold (fun k v pacc -> pacc +.. apply_monome k v x p2) p1 Poly.empty
    | Some c -> apply_const p1 x c

  let primitive p xv =
    let x = int_of_var xv in
    Poly.fold
      (fun k v it ->
        let a = k.(x) in
        let k2 = Array.copy k in
        k2.(x) <- a + 1;
        add_mon k2 (F.div v (F.of_int (a + 1))) it)
      p Poly.empty

  let diff p xv =
    let x = int_of_var xv in
    Poly.fold
      (fun k v it ->
        let a = k.(x) in
        if a <> 0 then (
          let k2 = Array.copy k in
          k2.(x) <- a - 1;
          add_mon k2 (F.mul v (F.of_int a)) it)
        else it)
      p Poly.empty

  let integral p x p1 p2 =
    let prim = primitive p x in
    apply prim x p2 -.. apply prim x p1

  let iter_on_var f x = iter_fun nb_var (fun vi -> f (var_of_int vi)) x

  let elapse_time p t =
    iter_on_var
      (fun c acc ->
        if snd var_string.(int_of_var c) then apply acc c (var t +.. var c)
        else acc)
      p

  let fully_apply ?smp p x f =
    match to_const @@ apply_const ?smp p x f with
    | Some x -> x
    | None -> raise Not_fully_evaluated

  (* let apply_all p tab =
     match to_float @@ snd @@ Array.fold_left
                                (fun (i,pit) v -> i+1,apply_const pit (var_of_int i) v) (0,p) tab with
       None -> raise Not_fully_evaluated
     | Some x -> x*)

  let fully_apply_float ?smp p x f =
    F.to_float (fully_apply ?smp p x (F.of_float f))

  (*let eval_key tab key =
    snd
    @@ Array.fold_left
         (fun (i, acc) tv ->
           let expi = float key.(i) in
           (i + 1, acc *. (tv ** expi)))
         (1, 1.0) tab*)

  let eval_key tab key =
    snd
    @@ Array.fold_left
         (fun (i, acc) keyi ->
           let tv =
             if keyi <> 0 then
               if i - 1 < 0 || i - 1 >= Array.length tab then
                 raise Not_fully_evaluated
               else tab.(i - 1)
             else 1.0
           in
           (i + 1, acc *. (tv ** float keyi)))
         (0, 1.0) key

  let apply_all ?smp p tab =
    Poly.fold
      (fun k v it ->
        let sval =
          match smp with None -> 1.0 | Some _ -> float k.(Array.length k - 1)
        in
        it +. (sval *. F.to_float v *. eval_key tab k))
      p 0.0

  let of_bound = function
    | Infinite -> const F.infinity
    | Const a -> const (F.of_int a)
    | Finite (a, c) ->
        assert (c <> var_of_int 0);
        (* use Const instead *)
        let rv = const (F.of_int a) in
        rv -.. var c

  let apply_bound p x = function
    | Infinite -> failwith "Not Conververging, set frequency>0 "
    | Const c -> apply_const p x (F.of_int c)
    | b ->
        let p2 = of_bound b in
        Poly.fold (fun k v pacc -> pacc +.. apply_monome k v x p2) p Poly.empty

  (* let one = monome [|0;0;0|] 1.0
     and t = monome [|1;0;0|] 1.0
     and x_1 = monome [|0;1;0|] 1.0
     and x_2 = monome [|0;0;1|] 1.0;;

     let poly = t*..t *.. t -.. (2.0**.one)  in
         let f = find_root poly 0 1.0 in
         print_endline (string_of_float f)
  *)

  (* Format.printf "%a\n" print (apply (x_1*..x_1 +.. x_2) 1 (2.0 **. one));;
     Format.printf "%a\n" print (integral (x_1*..x_1 +.. x_2) 1 one (3.0 **. one) );;
     Format.printf "%a\n" print ((x_1 -.. x_2) *.. (x_1 +.. x_2) );;
     let v = fully_apply (x_1*..x_1 +.. one) 1 2.0 in
               match v with
           None -> print_endline "None"
         | Some f -> print_endline (string_of_float f);;
  *)

  let laplace p smp xv =
    (* smp =  1/s *)
    let x = int_of_var xv in
    let n = Poly.fold (fun k _ i -> max k.(x) i) p 0 in
    let a = Array.make (n + 1) zero in
    Poly.iter
      (fun k v ->
        a.(k.(x)) <- add_mon (snd @@ proj_monome (var_of_int x) k) v a.(k.(x)))
      p;
    let cip = ref zero and ptilde = ref zero in
    for i = n downto 0 do
      let ci = mult smp (a.(i) +.. (F.of_int (i + 1) **. !cip)) in
      ptilde :=
        !ptilde
        +.. mult_monome
              (Array.init nb_var (fun j -> if j = x then i else 0))
              F.one ci;
      cip := ci
    done;
    !ptilde

  let inverse_laplace time_var p =
    let rec fact n = if n < 2 then 1 else n * fact (n - 1) in
    let p2 =
      Poly.fold
        (fun k v acc ->
          let a, b = proj_monome s k in
          if a < 0 then (
            b.(int_of_var time_var) <- -1 - a;
            add_mon b (F.div v (F.of_int (fact (-1 - a)))) acc)
          else acc)
        p zero
    in
    (*Format.fprintf Format.std_formatter "\t\t%a -> %a@." print p print p2;*)
    p2

  let taylor_exp_s p _ = p

  let dominating_s p =
    let svar = int_of_var s in
    match Poly.choose_opt p with
    | None -> ((0, 0, F.zero), (0, 0, F.zero))
    | Some (a, vt) ->
        let (cl, vl), (ch, vh) =
          Poly.fold
            (fun k v (((cl, vl), (ch, vh)) as acc) ->
              match k.(svar) with
              | c when c < cl -> ((c, v), (ch, vh))
              | c when c > ch -> ((cl, vl), (c, v))
              | _ -> acc)
            p
            ((a.(svar), vt), (a.(svar), vt))
        in
        ((0, cl, vl), (0, ch, vh))

  let find_root ?smp ?factor ?max_iter ?bound p ?diffp x guess_p =
    (*let p2 = match smp with
          None -> p
        | Some s -> apply_const ?smp p (P.var_of_int (P.nb_var-1)) (F.of_float s) in
      Format.eprintf "finding root for %a @." P.print p; *)
    let pp = match diffp with None -> diff p x | Some pp -> pp in
    let f v =
      let f0 = fully_apply_float ?smp p x v
      and f1 = fully_apply_float ?smp pp x v in
      (f0, f1)
    in
    try Math.newton_raphson_iterate ?factor ?max_iter ?bound f guess_p
    with Math.Not_converging (r, v) ->
      Format.eprintf "Alert Not converging %a --> %g -> %g@."
        (print ~pretty:true) p r v;
      r

  let sturm_sequence p z =
    let rec aux p1 p2 =
      (*Format.printf "sturm sequence %a@." print p1;*)
      match degree p2 with
      | -1 -> [ p1 ]
      | 0 -> [ p1; p2 ]
      | _ ->
          let _, r = div_euclidean p1 p2 in
          p1 :: aux p2 (zero -.. r)
    in
    aux p (diff p z)

  let compute_change pl var x =
    let rec aux = function
      | [] -> (0, 0.0)
      | [ a ] -> (0, a)
      | p1 :: p2 :: q ->
          let chr, l = aux (p2 :: q) in
          if p2 = 0.0 then if p1 *. l >= 0.0 then (chr, l) else (1 + chr, l)
          else if p1 *. p2 >= 0.0 then (chr, p2)
          else (1 + chr, p2)
    in
    fst
      (aux
         (List.map (fun p -> Option.get @@ to_float @@ apply_const p var x) pl))

  let find_root_sturm ?(factor = 1e-10) p var start =
    (*Format.printf "find root turm %a@." print p;*)
    let st = sturm_sequence p var in
    let cc v = compute_change st var (F.of_float v) in
    let rec dicho low up st lowval =
      if st then
        if up -. low <= factor then low
        else
          let m = (low +. up) /. 2.0 in
          let nbc = cc m in
          if nbc <> lowval then dicho low m true lowval else dicho m up true nbc
      else
        let upval = cc up in
        if upval <> lowval then dicho low up true lowval
        else if up < 1. /. factor then dicho up (2.0 *. up) false upval
        else up
    in

    dicho start 1.0 false (cc start)
end

(*
module Test =struct

  module P = Make( struct let var_string = [| "t"; "x"; "y"|] end)
  open Format

  let t = P.var_of_int 0
  let x = P.var_of_int 1
  let y = P.var_of_int 2

  let _ =
    let open P in
    let p = const 3.14 +.. var t +.. var x in
    printf "p=%a\n" print p;
    let p2 = mult p p in
    printf "p^2=%a\n" print p2;
    printf "degree(0)=%i; degree(1.0)=%i; degree(p)=%i; degree(p2)=%i\n"
      (degree zero) (degree @@ const 1.0) (degree p) (degree p2);
    let p3 = (var t) *.. (var t) +.. (var x) *.. (var t) +.. (var y) +.. const 2.0 in
    let p4 = laplace p3 (const 3.0) t in
    let p5 = map_var p3 (function v when v=t -> x | v -> v) in
    printf "p3=%a@, p4=%a@,p5=%a@." print p3 print p4 print p5

end
 *)
