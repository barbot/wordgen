(* Copy from Boost/math/tools/roots.hpp thus imperative style adapted with bisection*)
exception Zero_derivative of float
exception Not_converging of float * float

let newton_raphson_iterate ?(factor = 1e-9) ?(max_iter = 20) ?bound f guess_p =
  let min, max =
    match bound with
    | None -> (ref (-.max_float), ref max_float)
    | Some (x, y) -> (ref x, ref y)
  in

  let guess = ref guess_p in
  let result = ref (if guess_p = infinity then 1.0 else guess_p) in
  let delta = ref max_float
  and delta1 = ref max_float
  and delta2 = ref max_float in
  try
    for _ = 0 to max_iter do
      delta2 := !delta1;
      delta1 := !delta;
      let f0, f1 = f !result in
      (*Format.printf "nr x:%g f:%g df:%g minmax: %g;%g@." !result f0 f1 !min !max;*)
      if f0 = 0.0 then raise Exit;
      if f1 = 0.0 then (
        (*use bisection to find some slope *)
        if f0 < 0.0 then min := Stdlib.max !min !result
        else max := Stdlib.min !max !result;
        result := (!min +. !max) /. 2.0 (*raise (Zero_derivative !result)*))
      else (
        delta := f0 /. f1;
        (*Format.printf "delta: %g delta2:%g@." !delta !delta2;*)
        if abs_float (!delta *. 2.0) > abs_float !delta2 then (
          (*Format.printf "test@.";*)
          let shift =
            if !delta > 0.0 then (!result -. !min) /. 2.0
            else (!result -. !max) /. 2.0
          in
          if !result <> 0.0 && abs_float shift > abs_float !result then
            delta := copysign (0.9 *. abs_float !result) !delta
          else delta := shift;
          delta1 := 3.0 *. !delta;
          delta2 := 3.0 *. !delta);
        (*Format.printf "test2@.";*)
        guess := !result;
        result := !result -. !delta;
        if !result <= !min then (
          delta := 0.5 *. (!guess -. !min);
          result := !guess -. !delta;
          if !result = !min || !result = !max then raise Exit)
        else if !result >= !max then (
          delta := 0.5 *. (!guess -. !max);
          result := !guess -. !delta;
          if !result = !min || !result = !max then raise Exit);
        if !delta > 0.0 then max := !guess else min := !guess;
        if abs_float (!result *. factor) >= abs_float !delta then raise Exit)
    done;
    let f0, f1 = f !result in
    if f1 = 0.0 then raise (Zero_derivative !result);
    raise (Not_converging (!result, f0))
  with Exit -> !result

let rec bisect_increasing ?(factor = 1e-10) ?(strict = true) ?(relax = 0.0) ?low
    ?(up_bound = true) (bmin, bmax) f_to_evaluate =
  (* fmin use to detect vertical asymptote *)
  let xfmin, yfmin, a_val =
    match low with
    | None ->
        let yf, _, a = f_to_evaluate bmin in
        (bmin, yf, a)
    | Some v -> v
  in

  (*Format.printf "bisect [%g; %g] low:%g, %g @?" bmin bmax xfmin yfmin;*)
  if up_bound then (
    if bmax -. bmin < factor then (bmin, yfmin, a_val)
    else
      let m = 0.5 *. (bmin +. bmax) in

      (*if bmax -. bmin <= factor then m*)
      let fx, _, a = f_to_evaluate m in
      (*Format.printf "eval f(%g)=%g @?" m fx;*)
      (*Printf.printf "dicho min:%g; max :%g f:%g\n" bmin bmax fx;*)
      (*Detect asymtote *)
      let detect_asymptote =
        if m < xfmin then false
        else
          let cf = classify_float (fx *. yfmin) in
          cf = FP_nan || cf = FP_infinite || (fx < yfmin && strict)
      in
      if detect_asymptote then
        Format.printf "detect asymptot fx:%f yfmin:%f m:%f" fx yfmin m;
      let width = bmax -. bmin in
      if abs_float fx < factor || width < factor /. 10. then (*stop *) (m, fx, a)
      else if detect_asymptote || fx > 0.0 then
        (* go left *)
        (*Format.printf "left\n";*)
        bisect_increasing ~relax ~strict ~factor ?low
          (max 0.0 (bmin -. (relax *. width)), m +. (relax *. width))
          f_to_evaluate
      else
        (* go right *)
        (*Format.printf "right\n";*)
        bisect_increasing ~relax ~strict ~factor ~low:(m, fx, a)
          (m -. (relax *. width), bmax +. (relax *. width))
          f_to_evaluate)
  else
    let fx, _, _ = f_to_evaluate bmax in
    if classify_float fx = FP_nan || fx > 0.0 || fx < yfmin then
      (* Go left true upperbound *)
      bisect_increasing ~factor ~strict ~relax ~up_bound:true ?low (bmin, bmax)
        f_to_evaluate
    else
      (* Go right extend upperbound *)
      bisect_increasing ~factor ~strict ~relax ~up_bound:false ?low
        (bmin, 2.0 *. bmax)
        f_to_evaluate

(* Taken from owl: owl_base_maths.ml *)
let signum x =
  if FP_nan = classify_float x then nan
  else if x > 0. then 1.
  else if x < 0. then ~-.1.
  else 0.

let erf x =
  let a = 0.254829592 in
  let b = -0.284496736 in
  let c = 1.421413741 in
  let d = -1.453152027 in
  let e = 1.061405429 in
  let p = 0.327591100 in
  let sign = signum x in
  let x = abs_float x in
  let t = 1. /. (1. +. (p *. x)) in
  let y =
    1.
    -. ((((((((e *. t) +. d) *. t) +. c) *. t) +. b) *. t) +. a)
       *. t
       *. exp (-.x *. x)
  in
  sign *. y

let erfc x = 1. -. erf x

let comp_gaussian_ci epsilon m m2 n =
  let fn = float n in
  let z = erf (1.0 -. (epsilon /. 2.0)) in
  let s = (fn -. 1.0) *. (m2 -. (m *. m)) /. fn in
  (m -. (z *. s /. fn), m +. (z *. s /. fn))
