open Common

module Make (P : Polynomial.S) = struct
  module P = P
  module F = P.F

  type var = P.var
  type t = P.t * P.t
  type fl = P.F.t

  let var_string = P.var_string

  let desc =
    Printf.sprintf "[%s; %s]"
      (if P.F.is_exact then "exact rational" else "floating point")
      ("RationalFraction; " ^ VarSet.to_string var_string)

  let is_exp_poly = false
  let nb_var = P.nb_var
  let zero = (P.zero, P.one)
  let one = (P.one, P.one)

  let var ?(exp = 1) v =
    if exp > 0 then (P.var ~exp v, P.one)
    else if exp = 0 then one
    else (P.one, P.var ~exp:(-exp) v)

  let const s = (P.const s, P.one)
  let of_p p = (p, P.one)
  let of_p_p p1 p2 = (p1, p2)

  let var_set (p1,p2) = 
    Polynomial.VarSubSet.union (P.var_set p1) (P.var_set p2)

  let rec pgcd p1 p2 =
    (*Format.printf "PGCD %a %a @." P.print p1 P.print p2;*)
    if p2 = P.zero then p1
    else
      let _, r = P.div_euclidean p1 p2 in
      pgcd p2 r

  let equal (a1, b1) (a2, b2) = P.( *.. ) a1 b2 = P.( *.. ) a2 b1

  let print ?(pretty = true) f (a, b) =
    if b = P.one then P.print f a
    else Format.fprintf f "(%a)/(%a)" (P.print ~pretty) a (P.print ~pretty) b

  let unif (p1, p2) =
    let p = P.unif @@ pgcd p1 p2 in
    let p1d, _ = P.div_euclidean p1 p in
    let p2d, _ = P.div_euclidean p2 p in
    (*assert (r1 = P.zero && r2 = P.zero); may fail due to numerical error*)
    match P.to_const p2d with
    | Some f -> (P.(P.F.div P.F.one f **. p1d), P.one)
    | None ->
        (*Format.printf "%a -> pgcd:%a -> %a @." print (p1, p2) P.print p print
          (p1d, p2d);*)
        (p1d, p2d)

  let ( +.. ) (a1, b1) (a2, b2) =
    unif (P.( +.. ) (P.( *.. ) a1 b2) (P.( *.. ) a2 b1), P.( *.. ) b1 b2)

  let ( -.. ) (a1, b1) (a2, b2) =
    unif (P.( -.. ) (P.( *.. ) a1 b2) (P.( *.. ) a2 b1), P.( *.. ) b1 b2)

  let ( **. ) s (a, b) = (P.( **. ) s a, b)
  let ( *.. ) (a1, b1) (a2, b2) = unif (P.( *.. ) a1 a2, P.( *.. ) b1 b2)
  let ( /.. ) (a1, b1) (a2, b2) = unif (P.( *.. ) a1 b2, P.( *.. ) b1 a2)
  let vector_space = (zero, ( +.. ), fun x y -> F.of_float x **. y)
  let apply_bound (p1, p2) v b = P.(apply_bound p1 v b, apply_bound p2 v b)

  let apply_const ?smp (p1, p2) v f =
    unif P.(apply_const ?smp p1 v f, apply_const ?smp p2 v f)

  let apply_all ?smp (p1, p2) a = P.(apply_all ?smp p1 a /. apply_all ?smp p2 a)
  let diff (p1, p2) v = P.((p2 *.. diff p1 v) -.. (p1 *.. diff p2 v), p2 *.. p2)

  let to_float ?smp (p1, p2) =
    match (P.to_float ?smp p1, P.to_float ?smp p2) with
    | Some a, Some b -> Some (a /. b)
    | _ -> None

  let fully_apply_float ?smp (p1, p2) v f =
    P.fully_apply_float ?smp p1 v f /. P.fully_apply_float ?smp p2 v f

  let elapse_time (p1, p2) v = P.(elapse_time p1 v, elapse_time p2 v)
  let map_var (p1, p2) f = P.(map_var p1 f, map_var p2 f)
  let dominating_s _ = failwith "Not Implemented"
  let primitive _ = failwith "Not Implemented"
  let taylor_exp_s _ = failwith "Not Implemented"
  let inverse_laplace _ = failwith "Not Implemented"
  let eval_for_exact_duration _ = failwith "Not Implemented"

  let foldmap_range g f (a, b) =
    let rec aux acc n =
      let n = succ n in
      if n > b then acc else aux (g acc (f n)) n
    in
    aux (f a) a

  let fold_range f init (a, b) =
    let rec aux acc n = if n > b then acc else aux (f acc n) (succ n) in
    aux init a

  (* Some less-general support functions for 'solve'. *)
  let swap_elem m i j =
    let x = m.(i) in
    m.(i) <- m.(j);
    m.(j) <- x

  let maxtup a b = if snd a > snd b then a else b

  let augmented_matrix m b =
    Array.(init (length m) (fun i -> append m.(i) [| b.(i) |]))

  (* Solve Ax=b for x, using gaussian elimination with scaled partial pivot,
   * and then back-substitution of the resulting row-echelon matrix. Taken
   * from rosetta stone project. *)
  let solve m b =
    let n = Array.length m in
    let n' = n - 1 in
    (* last index = n-1 *)
    let s =
      Array.(
        map
          (fold_left
             (fun (a1, b1) (a2, b2) ->
               if P.degree a1 > P.degree a2 then (a1, b1) else (a2, b2))
             zero)
          m)
    in
    (* scaling vector *)
    let a = augmented_matrix m b in

    for k = 0 to pred n' do
      (* Scaled partial pivot, to preserve precision *)
      let pair i = (i, a.(i).(k) /.. s.(i)) in
      let i_max, _ = foldmap_range maxtup pair (k, n') in
      swap_elem a k i_max;
      swap_elem s k i_max;

      (* Eliminate one column *)
      for i = k + 1 to n' do
        let tmp = a.(i).(k) /.. a.(k).(k) in
        for j = k + 1 to n do
          a.(i).(j) <- a.(i).(j) -.. (tmp *.. a.(k).(j))
        done
      done
    done;
    (* Backward substitution; 'b' is in the 'nth' column of 'a' *)
    let x = Array.copy b in
    (* just a fresh array of the right size and type *)
    for i = n' downto 0 do
      let minus_dprod t j = t -.. (x.(j) *.. a.(i).(j)) in
      x.(i) <- fold_range minus_dprod a.(i).(n) (i + 1, n') /.. a.(i).(i)
    done;
    x

  let print_mat m =
    Array.iter
      (fun v ->
        v |> Array.iter (fun p -> Format.printf "\t%a" (print ~pretty:true) p);
        Format.printf "@.")
      m

  let find_root ?smp ?factor ?max_iter ?bound p ?diffp x guess_p =
    (*let p2 = match smp with
          None -> p
        | Some s -> apply_const ?smp p (P.var_of_int (P.nb_var-1)) (F.of_float s) in
      Format.eprintf "finding root for %a @." P.print p; *)
    let pp = match diffp with None -> diff p x | Some pp -> pp in
    let f v =
      let f0 = fully_apply_float ?smp p x v
      and f1 = fully_apply_float ?smp pp x v in
      (f0, f1)
    in
    try Math.newton_raphson_iterate ?factor ?max_iter ?bound f guess_p
    with Math.Not_converging (r, v) ->
      Format.eprintf "Alert Not converging %a --> %g -> %g@."
        (print ~pretty:true) p r v;
      r

  (*Array.iteri (fun i x -> Format.printf "%i: %a@." i pr x) x*)
end
