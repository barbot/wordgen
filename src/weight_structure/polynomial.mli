(*

Copyright (C) 2019 LACL Universite Paris Est Creteil Val de Marne
Authors: Benoît Barbot
Licence: GPL-3.0-or-later

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*)

module VarSubSet:Set.S with type elt = Common.VarSet.var

module type BASE = sig
  module F : Fl.FSIG

  type t
  type fl = F.t
  type var = Common.VarSet.var

  val desc : string
  val tvar : var
  val var_string : Common.VarSet.varset
  val zero : t
  val one : t
  val var : ?exp:int -> var -> t
  val const : fl -> t
  val ( +.. ) : t -> t -> t
  val ( -.. ) : t -> t -> t
  val ( **. ) : fl -> t -> t
  val print : ?pretty:bool -> Format.formatter -> t -> unit
  val apply_const : ?smp:float -> t -> var -> fl -> t
  val apply_all : ?smp:float -> t -> float array -> float
  val fully_apply_float : ?smp:float -> t -> var -> float -> float
  val diff : t -> var -> t
  val is_exp_poly : bool
  val var_set : t -> VarSubSet.t
end

module type WeightStructure = sig
  include BASE

  val z : var
  val s : var
  val nb_var : int
  val ( *.. ) : t -> t -> t
  val vector_space : t * (t -> t -> t) * (float -> t -> t)
  val apply_bound : t -> var -> Common.bound -> t
  val primitive : t -> var -> t
  val map_var : t -> (var -> var) -> t
  val elapse_time : t -> var -> t
  val to_float : ?smp:float -> t -> float option
  val taylor_exp_s : t -> int -> t
  val inverse_laplace : var -> t -> t

  val dominating_s : t -> (int * int * F.t) * (int * int * F.t)
  (** [dominating_s p] returns the domminating terms in +- infinity of the polynimials for variable s
    the two integers are the polynomial and exponenential exponent *)

  val find_root :
    ?smp:float ->
    ?factor:float ->
    ?max_iter:int ->
    ?bound:float * float ->
    t ->
    ?diffp:t ->
    var ->
    float ->
    float
end

module type SamplingStruct = sig
  include BASE

  val inverse_laplace : var -> t -> t

  val find_root :
    ?smp:float ->
    ?factor:float ->
    ?max_iter:int ->
    ?bound:float * float ->
    t ->
    ?diffp:t ->
    var ->
    float ->
    float
end

module type WeightForSampling = sig
  include WeightStructure
  module SamplingW : SamplingStruct with type t = t
end

exception Not_fully_evaluated

module type S = sig
  include WeightStructure

  type ftp

  val degree : t -> int
  val div_euclidean : t -> t -> t * t
  val pow : t -> int -> t
  val apply : t -> var -> t -> t
  val to_const : t -> fl option
  val to_float_poly : t -> ftp
  val of_float_poly : ftp -> t
  val integral : t -> var -> t -> t -> t
  val iter_on_var : (var -> 'a -> 'a) -> 'a -> 'a
  val laplace : t -> t -> var -> t
  val of_bound : Common.bound -> t
  val map_var : t -> (var -> var) -> t
  val unif : t -> t
  val find_root_sturm : ?factor:float -> t -> var -> float -> float
end

module Make : functor
  (F : Fl.FSIG)
  (K : sig
     val var_string : Common.VarSet.varset
   end)
  -> S
