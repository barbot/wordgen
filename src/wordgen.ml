(*

Copyright (C) 2019 LACL Universite Paris Est Creteil Val de Marne
Authors: Benoît Barbot
Licence: GPL-3.0-or-later

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*)

open Common
open Wordgen_lib
open Arguments

(* Parsing Arguments *)
let _ = Format.set_margin 190
let narg = ref 0

(* Parse command line arguments *)
let _ =
  Arg.parse spec_caml_arg
    (fun st ->
      incr narg;
      if !regexp <> None then incr narg;
      match !narg with
      | 1 -> infile := st
      | 2 -> outfilename := Some st
      | _ -> raise @@ Arg.Bad "Too many arguments")
    usage_str;
  if !narg < 1 && !Arguments.regexp = None then (
    Arg.usage spec_caml_arg usage_str;
    exit 1);
  (if !verbose > 1 then
     let cmd = build_cmd spec_full in
     print_endline ("parameters : " ^ cmd));
  post_parse ()

let gnuplot_draw =
  Compute.main
    (module Weight_structure.Flq.Q)
    !print_rg !infile !regexp !frequency !npoly !no_cache !expected_duration
    !boltzmann_param !expected_size !rational_impl !verbose !splitting_debug
    !export_zone_graph !outfilename !out_style !delta_robustness !random_seed
    !is_interactive !max_iteration !sampler !store_traj !template
    !is_duration_exact !nbtraj

(* start gnuplot on the computed data*)
let _ =
  let data = !outfilename |>>| "gnuplotout.dat" in
  let gnucmd =
    match !gnuplot_cmd with Some x -> Some x | None -> gnuplot_draw
  in
  match (!outfilename, !gnuplot_driver, gnucmd) with
  | Some _, Some driver, Some gnucmd ->
      let open Printf in
      let gnuplot = Unix.open_process_out "tee gnuplot.gp | gnuplot" in
      let ext =
        match String.index_opt driver ' ' with
        | Some sp -> String.sub driver 0 sp
        | None -> driver
      in

      fprintf gnuplot "set terminal %s\n" driver;
      fprintf gnuplot "set output '%s.%s'\n"
        (Filename.remove_extension data)
        ext;
      fprintf gnuplot "set pointsize 0.1\n";
      fprintf gnuplot "%s" gnucmd;
      ignore @@ Unix.close_process_out gnuplot
  | None, Some _, _ ->
      Format.eprintf "No output file specified, cannot use gnuplot\n"
  | _, Some _, None -> Format.eprintf "Nothing to plot, cannot use gnuplot\n"
  | _, None, _ -> ()
