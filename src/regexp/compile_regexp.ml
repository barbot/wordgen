open Regexp_lib

let regexp s =
  let lexbuf = Lexing.from_string s in
  try Regexp.main Regexplex.tok lexbuf
  with exn ->
    let curr = lexbuf.Lexing.lex_curr_p in
    let line = curr.Lexing.pos_lnum in
    let cnum = curr.Lexing.pos_cnum - curr.Lexing.pos_bol in
    let tok = Lexing.lexeme lexbuf in
    Format.eprintf "Parse error: %s,%i,%i@." tok line cnum;
    raise exn

let _ =
  let reg = regexp Sys.argv.(1) in
  (*let reg = regexp "z<a*(<b>_[1,2]|<c>_[3,4])>_[2,5]" in*)
  (*let reg = regexp "<a<b<c*>_[0,1]>_[0,2]>_[0,3]" in*)
  (*Format.printf "%a\n" Regexp_type.print reg;*)
  (*let a =
      let open Automata.Notdet in
      let a1 = of_string "a" in
      let a2 = of_char 'b' in
      let a3 = of_char 'c' in
      let a4 = plus (merge a1 (cat a2 a3)) in
      (*Format.printf "%a" print_dot a4;*)
      to_det a4
    in
    Random.self_init ();*)
  (*Format.printf "%a@." Regexp_type.print reg;*)
  let regauto = Automata.Notdet.of_regexp reg in
  Automata.Notdet.print_dot Format.std_formatter regauto
(*let a = Automata.Notdet.to_det regauto in
  let open Automata.Det in
  to_prism Format.std_formatter a;
  print Format.std_formatter a;
  let vol = compute_volume a 0 in
  for _ = 1 to 0 do
    let l = sample a.init a vol in
    List.iter (fun x -> print_char x) l;
    print_newline ()
  done*)
