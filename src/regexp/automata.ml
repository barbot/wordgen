let print_array p f v = Array.iter (fun x -> p f x) v
let print_list p f v = List.iter (fun x -> p f x) v

type clock = int
type l = char

module Tr = struct
  type t = l * (clock * int option * int option) list * int * clock list

  let print_clock f = function
    | 0 -> Format.pp_print_string f "x"
    | 1 -> Format.pp_print_string f "y"
    | 2 -> Format.pp_print_string f "z"
    | 3 -> Format.pp_print_string f "t"
    | i -> Format.fprintf f "c_%i" (i - 3)

  let print_guard f g =
    if g <> [] then
      let open Format in
      fprintf f ",%a"
        (pp_print_list
           ~pp_sep:(fun f () -> pp_print_string f "&")
           (fun f x ->
             match x with
             | c, Some a, Some b -> fprintf f "%i <= %a <= %i" a print_clock c b
             | c, Some a, None -> fprintf f "%i <= %a" a print_clock c
             | c, None, Some b -> fprintf f "%a <= %i" print_clock c b
             | _, None, None -> ()))
        g

  let print_reset f r =
    if r <> [] then
      let open Format in
      fprintf f ",[%a]"
        (pp_print_list ~pp_sep:(fun f () -> pp_print_string f ",") print_clock)
        r

  let print f (l, g, _, r) =
    let open Format in
    fprintf f "%c%a%a" l print_guard g print_reset r

  let print_prism f s (l, g, s2, r) =
    let open Format in
    fprintf f "\t[%c] (state=%i)%a -> (state'=%i)%a;@." l s
      (pp_print_list (fun _ x ->
           match x with
           | c, Some a, Some b -> fprintf f "&(c%i>%i)&(c%i<%i)" c a c b
           | c, Some a, None -> fprintf f "&(c%i>%i)" c a
           | c, None, Some b -> fprintf f "&(c%i<%i)" c b
           | _, None, None -> ()))
      g s2
      (pp_print_list (fun _ c -> fprintf f "&(c%i=0)" c))
      r

  let compare t1 t2 = compare t1 t2
end

module Det = struct
  type t = {
    init : int;
    clocks : int;
    trans : Tr.t list array;
    final : bool array;
  }

  let print_trans f (a, _, t, _) =
    Format.fprintf f "@[{@ \"action\":\"%c\",@ \"target\":%i}@]" a t

  let print_state f id tr b =
    Format.fprintf f
      "@[{\"id\":%i,@ \"transition\":[@[%a@]],@ \"is_accepting\":%b}@]" id
      (Format.pp_print_list
         ~pp_sep:(fun _ _ -> Format.fprintf f ",@,")
         Tr.print)
      tr b

  let print f t =
    Format.fprintf f "{\"statelist\":[@[";
    Array.iteri
      (fun i l ->
        if i > 0 then Format.fprintf f ",@;";
        print_state f i l t.final.(i))
      t.trans;
    Format.fprintf f "@]],@, \"init\":%i}@." t.init

  let to_prism f t =
    let open Format in
    fprintf f "pta@.module m@[@.state:[0..%i] init %i;@."
      (Array.length t.trans - 1)
      t.init;
    Array.iteri (fun s tl -> List.iter (Tr.print_prism f s) tl) t.trans;
    fprintf f "@]endmodule@.label \"accepting\" = true";
    Array.iteri
      (fun i b -> if not b then fprintf f " & (state != %i)" i)
      t.final;
    fprintf f ";@."

  let init_volume a = Array.map (function false -> 0.0 | true -> 1.0) a.final

  let iterate_volume a oc =
    assert (a.clocks = 0);
    let n = Array.length a.trans in
    let nc = Array.make n 0.0 in
    for i = 0 to n - 1 do
      nc.(i) <-
        List.fold_left (fun x (_, _, s, _) -> x +. oc.(s)) 0.0 a.trans.(i)
    done;
    nc

  let compute_volume a n =
    let t = ref [ init_volume a ] in
    for _ = 1 to n do
      let v = List.hd !t in
      Array.iter
        (fun x ->
          print_float x;
          print_string " ")
        v;
      print_newline ();
      t := iterate_volume a v :: !t
    done;
    !t

  exception Empty

  let rec min_list = function
    | [] -> raise Empty
    | [ t ] -> t
    | (_, _, v1) :: (x, y, v2) :: q when v2 < v1 -> min_list ((x, y, v2) :: q)
    | t :: _ :: q -> min_list (t :: q)

  let rec sample s a = function
    | [] -> []
    | t :: q ->
        let l, s2, _ =
          a.trans.(s)
          |> List.map (function l, _, s2, _ -> (l, s2, t.(s2)))
          |> List.map (function l, s2, v ->
                 (l, s2, -.log (Random.float 1.0) /. v))
          |> min_list
        in
        l :: sample s2 a q
end

module Notdet = struct
  module TrSet = Set.Make (Tr)

  type t = {
    init : int list;
    clocks : int;
    trans : TrSet.t array;
    final : bool array;
    is_last_tc : bool;
  }

  let empty =
    {
      init = [ 0 ];
      clocks = 0;
      trans = [| TrSet.empty |];
      final = [| false |];
      is_last_tc = false;
    }

  let epsilon =
    {
      init = [ 0 ];
      clocks = 0;
      trans = [| TrSet.empty |];
      final = [| true |];
      is_last_tc = false;
    }

  let of_char c =
    {
      init = [ 0 ];
      clocks = 0;
      trans = [| TrSet.singleton (c, [], 1, []); TrSet.empty |];
      final = [| false; true |];
      is_last_tc = false;
    }

  let of_string s =
    let n = String.length s in
    {
      init = [ 0 ];
      clocks = 0;
      trans =
        Array.init (n + 1) (fun i ->
            if i < n then TrSet.singleton (s.[i], [], i + 1, [])
            else TrSet.empty);
      final = Array.init (n + 1) (fun i -> i = n);
      is_last_tc = false;
    }

  let print f t =
    let open Format in
    fprintf f "{@[ init=[%a];@.trans=[|%a|];@.final=[|%a|]@]} "
      (print_list (fun f x -> Format.fprintf f "%i, " x))
      t.init
      (print_array (fun f x ->
           fprintf f "{";
           TrSet.iter (Tr.print f) x;
           fprintf f "}"))
      t.trans
      (print_array (fun f x -> fprintf f "%b, " x))
      t.final

  let print_dot f t =
    Format.fprintf f "digraph notdet {@[@.%a%a%a@]}"
      (fun f a ->
        Array.iteri
          (fun i x ->
            match x with
            | true -> Format.fprintf f "node[shape=doublecircle]; %i;@." i
            | false -> Format.fprintf f "node[shape=circle]; %i;@." i
            (*| Epsilon -> Format.fprintf f "node[shape=box3d]; %i;@." i*))
          a)
      t.final
      (fun f a ->
        List.iter
          (fun i ->
            Format.fprintf f "node[shape=point]; i%i;@.i%i -> %i;@." i i i)
          a)
      t.init
      (fun f a ->
        Array.iteri
          (fun i x ->
            TrSet.iter
              (fun ((_, _, j, _) as tr) ->
                Format.fprintf f "%i -> %i [label=\"%a\"];@." i j Tr.print tr)
              x)
          a)
      t.trans

  let emonde a =
    let n = Array.length a.trans in
    let compt = Array.make n 0 in
    List.iter (fun i -> compt.(i) <- compt.(i) + 1) a.init;
    let rec deep_first_search = function
      | [] -> ()
      | t :: q ->
          let l2 =
            TrSet.fold
              (fun (_, _, j, _) acc ->
                compt.(j) <- compt.(j) + 1;
                if compt.(j) = 1 then j :: acc else acc)
              a.trans.(t) q
          in
          deep_first_search l2
    in
    deep_first_search a.init;
    (*Array.iter
      (fun tab ->
        TrSet.iter (fun (_, _, j, _) -> compt.(j) <- compt.(j) + 1) tab)
      a.trans;*)
    let n2 = Array.fold_left (fun x y -> if y > 0 then x + 1 else x) 0 compt in
    let trans = Array.make n2 TrSet.empty in
    let final = Array.make n2 false in
    let map = Array.make n 0 in
    ignore
    @@ Array.fold_left
         (fun (i, j) c ->
           if c > 0 then (
             trans.(i) <- a.trans.(j);
             final.(i) <- a.final.(j);
             map.(j) <- i;
             (i + 1, j + 1))
           else (i, j + 1))
         (0, 0) compt;
    let init = List.map (fun x -> map.(x)) a.init in
    Array.iteri
      (fun i tr ->
        trans.(i) <- TrSet.map (fun (l, g, j, r) -> (l, g, map.(j), r)) tr)
      trans;
    { init; clocks = a.clocks; trans; final; is_last_tc = a.is_last_tc }

  let translate_tr ?(add_reset = []) ns nc (l, g, s, r) =
    ( l,
      List.map (fun (c, a, b) -> (c + nc, a, b)) g,
      ns + s,
      add_reset @ List.map (fun c -> c + nc) r )

  let ajout_init shift a2 p =
    (* add transition starting from the initial state of a2 to final state of a1*)
    List.fold_left
      (fun it s ->
        TrSet.fold (fun tr it2 -> TrSet.add (shift tr) it2) a2.trans.(s) it)
      p a2.init

  let cat a1 a2 =
    let n1 = Array.length a1.trans and n2 = Array.length a2.trans in
    let nc1 = a1.clocks in
    let lc1 = List.init a1.clocks (fun i -> i) in
    let lc2 = List.init a2.clocks (fun i -> i + nc1) in
    let shift_a2 = translate_tr ~add_reset:lc1 n1 nc1 in
    let trans =
      Array.init (n1 + n2) (fun i ->
          if i < n1 then
            let p =
              (* Add a reset of all clocks of a2 to  a1*)
              TrSet.map (fun (l, g, t, r) -> (l, g, t, lc2 @ r)) a1.trans.(i)
            in
            if a1.final.(i) then ajout_init shift_a2 a2 p else p
          else TrSet.map shift_a2 a2.trans.(i - n1))
    in
    let initfinal = List.exists (fun i -> a2.final.(i)) a2.init in
    let final =
      Array.init (n1 + n2) (fun i ->
          if i < n1 then initfinal && a1.final.(i) else a2.final.(i - n1))
    in
    emonde
      {
        init = a1.init;
        clocks = nc1 + a2.clocks;
        trans;
        final;
        is_last_tc = a2.is_last_tc;
      }

  let intersect a1 a2 =
    let n1 = Array.length a1.trans and n2 = Array.length a2.trans in
    let nc1 = a1.clocks in
    let trans =
      Array.init (n1 * n2) (fun k ->
          let i = k mod n1 and j = k / n1 in
          TrSet.fold
            (fun (l1, g1, t1, r1) acc ->
              TrSet.fold
                (fun ((l2, _, _, _) as tr2) acc2 ->
                  if l1 = l2 then
                    let _, g2, t2, r2 = translate_tr 0 nc1 tr2 in
                    TrSet.add (l1, g1 @ g2, (t2 * n1) + t1, r1 @ r2) acc2
                  else acc2)
                a2.trans.(j) acc)
            a1.trans.(i) TrSet.empty)
    in
    let init =
      List.fold_left
        (fun acc i ->
          List.fold_left (fun acc2 j -> ((j * n1) + i) :: acc2) acc a2.init)
        [] a1.init
    in
    let final =
      Array.init (n1 * n2) (fun k ->
          let i = k mod n1 and j = k / n1 in
          a1.final.(i) && a2.final.(j))
    in
    emonde { init; clocks = nc1 + a2.clocks; trans; final; is_last_tc = false }

  let merge a1 a2 =
    let n1 = Array.length a1.trans and n2 = Array.length a2.trans in
    {
      init = a1.init @ List.map (fun x -> x + n1) a2.init;
      clocks = max a1.clocks a2.clocks;
      trans =
        Array.init (n1 + n2) (fun i ->
            if i < n1 then a1.trans.(i)
            else TrSet.map (translate_tr n1 0) a2.trans.(i - n1));
      final =
        Array.init (n1 + n2) (fun i ->
            if i < n1 then a1.final.(i) else a2.final.(i - n1));
      is_last_tc = false;
    }

  let star a =
    let n = Array.length a.trans in
    let add_reset = List.init a.clocks (fun i -> i) in
    let trans =
      Array.init n (fun i ->
          let p =
            TrSet.map
              (fun ((_, _, j, _) as tr) ->
                if a.is_last_tc && a.final.(j) then
                  translate_tr ~add_reset 0 0 tr
                else tr)
              a.trans.(i)
          in
          if a.final.(i) then
            List.fold_left
              (fun it s ->
                TrSet.fold
                  (fun tr it2 -> TrSet.add (translate_tr 0 0 tr) it2)
                  a.trans.(s) it)
              p a.init
          else p)
    in

    let final = Array.copy a.final in
    List.iter (fun x -> final.(x) <- true) a.init;
    emonde { a with trans; final }

  let plus a = cat a (star a)

  let time_constraint a t1 t2 =
    let nclock = a.clocks in
    {
      a with
      clocks = nclock + 1;
      trans =
        Array.map
          (TrSet.map (fun (l, g, t, r) ->
               ( l,
                 ( nclock,
                   (if t1 = 0 || not a.final.(t) then None else Some t1),
                   Some t2 )
                 :: g,
                 t,
                 r )))
          a.trans;
      is_last_tc = true;
    }

  let reverse a =
    let n = Array.length a.trans in
    let init =
      snd
      @@ Array.fold_left
           (fun (i, l) x -> if x then (i + 1, i :: l) else (i + 1, l))
           (0, []) a.final
    in
    let final = Array.make n false in
    List.iter (fun i -> final.(i) <- true) a.init;
    let trans = Array.make n TrSet.empty in
    Array.iteri
      (fun i trset ->
        TrSet.iter
          (fun (l, g, j, r) -> trans.(j) <- TrSet.add (l, g, i, r) trans.(j))
          trset)
      a.trans;
    { init; trans; clocks = a.clocks; final; is_last_tc = false }

  module IntSet = Set.Make (struct
    type t = int

    let compare i j = i - j
  end)

  let is_deterministic a =
    if List.length a.init > 1 then false
    else
      Array.for_all
        (fun trset ->
          TrSet.fold
            (fun (l, _, _, _) acc ->
              Option.bind acc (fun vacc ->
                  if List.mem l vacc then None else Some (l :: vacc)))
            trset (Some [])
          <> None)
        a.trans

  let determinize a =
    assert (a.clocks = 0);
    let init = IntSet.of_list a.init in
    let set = Hashtbl.create 10 in
    Hashtbl.add set init (ref None);
    let cmpt = ref 0 in
    let to_exp = ref [ init ] in
    while !to_exp <> [] do
      let ss = List.hd !to_exp in
      to_exp := List.tl !to_exp;
      let trfun = Hashtbl.create 2 in
      IntSet.iter
        (fun s ->
          TrSet.iter
            (fun (l, _, s2, _) ->
              match Hashtbl.find_opt trfun l with
              | None -> Hashtbl.add trfun l (IntSet.singleton s2)
              | Some tset -> Hashtbl.replace trfun l (IntSet.add s2 tset))
            a.trans.(s))
        ss;
      Hashtbl.find set ss := Some (trfun, !cmpt);
      incr cmpt;
      Hashtbl.iter
        (fun _ s ->
          if not @@ Hashtbl.mem set s then (
            Hashtbl.add set s (ref None);
            to_exp := s :: !to_exp))
        trfun
    done;
    let trans = Array.make !cmpt TrSet.empty
    and final = Array.make !cmpt false in
    Hashtbl.iter
      (fun ss x ->
        match !x with
        | None -> assert false
        | Some (trfun, s) ->
            let tr =
              Hashtbl.fold
                (fun l ss2 trset ->
                  match !(Hashtbl.find set ss2) with
                  | None -> assert false
                  | Some (_, c2) -> TrSet.add (l, [], c2, []) trset)
                trfun TrSet.empty
            in
            trans.(s) <- tr;
            final.(s) <- IntSet.exists (fun x -> a.final.(x)) ss)
      set;
    { init = [ 0 ]; clocks = 0; trans; final; is_last_tc = false }

  let minimize a = determinize @@ reverse @@ determinize @@ reverse a

  let rec iterate a = function
    | 0, None -> star a
    | 0, Some 0 -> epsilon
    | 0, Some b -> merge a (iterate a (0, Some (b - 1)))
    | n, b -> cat a (iterate a (n - 1, b))

  let rec of_regexp = function
    | Regexp_type.Empty -> epsilon
    | Char c -> of_char c
    | CharSet [] -> empty
    | CharSet [ c ] -> of_char c
    | CharSet (t :: q) -> merge (of_char t) (of_regexp (CharSet q))
    | Or (r1, r2) -> merge (of_regexp r1) (of_regexp r2)
    | Cat (r1, r2) -> cat (of_regexp r1) (of_regexp r2)
    | Iterate (a, b, r) -> iterate (of_regexp r) (a, b)
    | TimeConstraint (a, t1, t2) -> time_constraint (of_regexp a) t1 t2
    | Intersection (r1, r2) -> intersect (of_regexp r1) (of_regexp r2)

  let to_det a =
    let a2 = if a.clocks = 0 then minimize a else a in
    (*assert (is_deterministic a2);*)
    {
      Det.init = List.hd a2.init;
      clocks = a.clocks;
      trans =
        Array.map (fun tr -> TrSet.fold (fun tr l -> tr :: l) tr []) a2.trans;
      final = Array.map (fun x -> x) a2.final;
    }
end

(*
let testa =
  let open Det in
  {
    init = 0;
    trans =
      [|
        [ ('a', 0); ('b', 1) ]; [ ('b', 0); ('a', 2) ]; [ ('a', 1); ('b', 2) ];
      |];
    final = [| true; false; false |];
  }*)
