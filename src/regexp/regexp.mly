%{
open Regexp_type

%}

%token <char> CHAR
%token LSQBRK, RSQBRK
%token LBRK, RBRK
%token LPAR, RPAR
%token LANGLE, RANGLE
%token STAR
%token PLUS
%token AND
%token PIPE
%token DASH, LOW_DASH
%token QUESTION
%token EOF
%token COMMA
%token <int>NUM

%left PIPE

%type <Regexp_type.t> main
%start main
%%

regexp:
  /*| CHAR { 
let s = $1 in
let l = ref (Char (s.[0])) in
for i=1 to String.length s -1 do
  l := Cat( !l , Char s.[i]);
done;
 !l }*/
  | CHAR { Char $1}
  | regexp regexp { Cat($1,$2) }
  | LPAR regexp RPAR { $2 }
  | regexp PIPE regexp { Or( $1,$3) }
  | regexp AND regexp { Intersection($1, $3) }
  | regexp STAR { Iterate(0,None, $1) }
  | regexp PLUS { Iterate(1,None, $1) }
  | regexp QUESTION { Iterate(0,Some 1, $1) }
  | LSQBRK charset RSQBRK {CharSet $2}
  | LANGLE regexp RANGLE LOW_DASH LSQBRK NUM COMMA NUM RSQBRK {TimeConstraint($2,$6,$8)}
 
charset:
  CHAR { [$1] }
  | CHAR  charset { $1 :: $2 }
  | CHAR DASH CHAR {
    let x = $1 in
    let rec aux y =
     if x = y then [x]
     else y :: (aux (char_of_int ((int_of_char y)-1))) in aux $3 
   }
  

main:
  regexp EOF  {$1}
