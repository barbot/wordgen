{
open Regexp

}

let letter = ['a'-'z' 'A'-'Z']
let digit = ['0'-'9']
let float = digit+(['.']digit*)?
let white = [' ' '\t']+

  rule tok = parse
   white    {tok lexbuf}
  | ['/']['/'][^'\n']*    {tok lexbuf}
  | '(' {LPAR}
  | ')' {RPAR}
  | ']' {RSQBRK}
  | '[' {LSQBRK}
  | '>' {RANGLE}
  | '<' {LANGLE}
  | '}' {RBRK}
  | '{' {LBRK}
  | '*' {STAR}
  | '+' {PLUS}
  | '&' {AND}
  | '?' {QUESTION}
  | '|' {PIPE}
  | '-' {DASH}
  | ',' {COMMA}
  | '_' {LOW_DASH}
  | letter {CHAR ((Lexing.lexeme lexbuf).[0]) }
  | digit+ {NUM (int_of_string (Lexing.lexeme lexbuf))}
  | "\n" { EOF }
  | eof { EOF }
