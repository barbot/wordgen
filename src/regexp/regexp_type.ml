type t =
  | Empty
  | Char of char
  | CharSet of char list
  | Or of t * t
  | Cat of t * t
  | Iterate of int * int option * t
  | TimeConstraint of t * int * int
  | Intersection of t * t

let rec print f = function
  | Empty -> ()
  | Char c -> Format.fprintf f "%c" c
  | CharSet l ->
      Format.fprintf f "[%a]" (Format.pp_print_list Format.pp_print_char) l
  | Or (lhs, rhs) -> Format.fprintf f "(%a|%a)" print lhs print rhs
  | Intersection (lhs, rhs) -> Format.fprintf f "(%a&%a)" print lhs print rhs
  | Cat (lhs, rhs) -> Format.fprintf f "%a%a" print lhs print rhs
  | Iterate (0, None, lhs) -> Format.fprintf f "(%a)*" print lhs
  | Iterate (1, None, lhs) -> Format.fprintf f "(%a)+" print lhs
  | Iterate (0, Some 1, lhs) -> Format.fprintf f "(%a)?" print lhs
  | Iterate (a, Some b, lhs) when a = b ->
      Format.fprintf f "(%a){%i}" print lhs a
  | Iterate (a, None, lhs) -> Format.fprintf f "(%a){%i,}" print lhs a
  | Iterate (a, Some b, lhs) -> Format.fprintf f "(%a){%i,%i}" print lhs a b
  | TimeConstraint (a, t1, t2) -> Format.fprintf f "<%a>_[%i,%i]" print a t1 t2
