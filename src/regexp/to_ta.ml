(*

Copyright (C) 2019 LACL Universite Paris Est Creteil Val de Marne
Authors: Benoît Barbot
Licence: GPL-3.0-or-later

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*)

open Automata.Det
open Uppaal_parser.Uta

let var_of_int i = "c" ^ string_of_int i

let guard_of_gl g =
  let voc c = Variable (var_of_int c) in
  List.fold_left
    (fun acc x ->
      match x with
      | c, Some t1, Some t2 ->
          GuardGreater (voc c, Constant t1)
          :: GuardLess (voc c, Constant t2)
          :: acc
      | c, None, Some t2 -> GuardLess (voc c, Constant t2) :: acc
      | c, Some t1, None -> GuardGreater (voc c, Constant t1) :: acc
      | _, None, None -> acc)
    [] g

let edge_from_rule ~add_dummy locs i (l, g, j, r) =
  let resetl = List.map (fun c -> (var_of_int c, Constant 0)) r in
  {
    edgeSource = (List.nth locs i).locId;
    edgeGuard = [ guard_of_gl g ];
    edgeUpdates =
      (if add_dummy then ("dummy_clock", Constant 0) :: resetl else resetl);
    edgeUpdatesPos = (0, 0);
    edgeGuardPos = (0, 0);
    edgeSync = Some (SendChan (String.make 1 l));
    edgeSyncPos = (0, 0);
    edgeTarget = (List.nth locs j).locId;
    edgeNails = [];
    edgeProb = 1.0;
  }

let template_from_automata m =
  let gen_loc i =
    {
      locId = string_of_int i;
      locPos = (0, 0);
      locName = string_of_int i;
      locNamePos = (0, 0);
      locInvar = [];
      locInvarPos = (0, 0);
      locCommitted = false;
      locUrgent = false;
    }
  in
  let tempLocations = List.init (Array.length m.trans) gen_loc in
  let add_dummy =
    m.clocks > 0
    && Array.exists
         (fun s -> List.exists (fun (_, _, _, r) -> r = []) s)
         m.trans
  in
  let clocks = List.init m.clocks (fun i -> var_of_int i) in
  {
    tempName = "regexp";
    tempLocations;
    tempInitialLocation = List.nth tempLocations m.init;
    tempClocks = (if add_dummy then "dummy_clock" :: clocks else clocks);
    tempVars = [];
    tempEdges =
      snd
      @@ Array.fold_left
           (fun (i, l) el ->
             ( i + 1,
               List.fold_left
                 (fun l2 e -> edge_from_rule ~add_dummy tempLocations i e :: l2)
                 l el ))
           (0, []) m.trans;
  }

let ta_from_regexp t =
  let sysTemplate = template_from_automata t in
  let sys =
    {
      sysTemplates = [ sysTemplate ];
      sysProcessInstances = [];
      sysVars = [];
      sysClocks = [];
      sysChans = [];
      sysQuery = EmptyQuery;
    }
  in
  Symrob_extract.TimedAutomaton.Uautomaton.make_ta sys
    [
      ( "accepting",
        ( [],
          snd
          @@ Array.fold_left
               (fun (i, l) b -> if b then (i + 1, i :: l) else (i + 1, l))
               (0, []) t.final ) );
    ]
