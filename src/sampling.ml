open Format
open ZoneGraph
open Weight_structure

type 'a transChoice = Label of 'a | Rand of float | Fixed

type samplingMeth =
  | Exact
  | Receding of int
  | Driven of (float transChoice * ZoneGraph.l transChoice) list

let proba_of_string s =
  let f = float_of_string @@ String.trim s in
  assert (f >= 0.0 && f <= 1.0);
  f

let rec timeword_parser s =
  match String.index_opt s '[' with
  | None -> []
  | Some i ->
      let stime = String.trim @@ String.sub s 0 i in
      let f =
        if stime = "_" then Fixed
        else if stime.[0] = '@' then
          Label (float_of_string @@ String.sub stime 1 (String.length stime - 1))
        else Rand (proba_of_string stime)
      in
      let s2 = String.sub s i (String.length s - i) in
      let j = String.index s2 ']' in
      let saction = String.sub s2 1 (j - 1) in
      let c =
        if saction = "_" then Fixed
        else try Rand (proba_of_string saction) with _ -> Label saction
      in
      (f, c)
      :: timeword_parser (String.sub s2 (j + 1) (String.length s2 - j - 1))

module type STATE = sig
  type p
  type bound
  type t = int * float array * float

  val get_loc : t -> int

  exception Deadlock of t

  val init : ('a, 'b) ZoneGraph.t -> float -> t
  val print_state : ?trunc:int -> Format.formatter -> t -> unit
  val print_state_long : ('a, 'b) ZoneGraph.t -> Format.formatter -> t -> unit
  val eval_poly_state : ?smp:float -> t -> p -> p
  val eval_poly_state_full : ?smp:float -> t -> p -> float

  val sample_time :
    ?smp:float ->
    ?max_iter:int ->
    ?delay:float ->
    int ->
    t ->
    (p, bound) ZoneGraph.transition ->
    float ->
    float * float * float

  val choose_weight : (float * 'a * 'b) list -> float -> 'a option

  val build_filter :
    float transChoice * l transChoice ->
    (float * float -> (p, bound) ZoneGraph.transition -> bool) option

  val filter_transition :
    filter:(float * float -> (p, bound) ZoneGraph.transition -> bool) ->
    ((p, bound) ZoneGraph.transition * (float * float)) list ->
    ((p, bound) ZoneGraph.transition * (float * float)) list

  val available_transition :
    ?filter:(float * float -> (p, bound) ZoneGraph.transition -> bool) ->
    (p, bound) ZoneGraph.t ->
    t ->
    ((p, bound) ZoneGraph.transition * (float * float)) list

  val compute_weight :
    ?smp:float ->
    ?boltz:float ->
    ?tolerance:float ->
    ?delay:float ->
    (p, bound) ZoneGraph.t ->
    int ->
    t ->
    ((p, bound) ZoneGraph.transition * (float * float)) list ->
    (float * (p, bound) ZoneGraph.transition * (float * float)) list

  val sample_trans :
    ?smp:float ->
    ?boltz:float ->
    ?tolerance:float ->
    ((p, bound) ZoneGraph.transition * (float * float)) list ->
    (p, bound) ZoneGraph.t ->
    int ->
    t ->
    float ->
    (p, bound) ZoneGraph.transition

  val elapse_time : t -> float -> t
  val fire : t -> (p, bound) ZoneGraph.transition -> t
end

module State (Bt : ZoneGraphInput.BoundType) (W : Polynomial.SamplingStruct) =
struct
  type p = W.t
  type bound = Bt.t
  type t = Bt.state

  exception Deadlock of t

  let rem_var = Common.VarSet.find_var W.var_string "T"
  let get_loc (loc, _, _) = loc
  let init rg f = (rg.init, Array.make rg.cardclocks 0.0, f)

  let truncate ?trunc v =
    match trunc with
    | None -> v
    | Some t ->
        let exp = 10. ** float_of_int t in
        float (Float.to_int (v *. exp)) /. exp

  let print_state ?trunc f (id, tab, _) =
    Format.fprintf f "@[%i @ @[%a@]@]" id
      (fun _ ->
        Array.iter (fun x ->
            let y = truncate ?trunc x in
            Format.fprintf f "%g @ " y))
      tab

  let print_state_long rg f (id, tab, _) =
    let st = rg.statelist.(id) in
    Format.fprintf f "@[id:%i@ %s@ %a@]" st.id st.disc_name
      (fun _ ->
        Array.iteri (fun i x ->
            Format.fprintf f "%s:%f @ " (fst rg.var_string.(i + 1)) x))
      tab

  let eval_poly_state ?smp (_, tab, remaining_time) p =
    let p2 = ref (W.apply_const ?smp p rem_var (W.F.of_float remaining_time)) in
    for i = 0 to Array.length tab - 1 do
      p2 :=
        W.apply_const ?smp !p2
          (Common.VarSet.var_of_int (i + 1))
          (W.F.of_float tab.(i))
    done;
    !p2

  let eval_poly_state_full ?smp (_, tab, remaining_time) p =
    let p2 = W.apply_const p rem_var (W.F.of_float remaining_time) in
    W.apply_all ?smp p2 tab

  (* Sample firing time of transition trans using random number u*)
  let sample_time ?smp ?(max_iter = 20) ?delay i state trans u =
    let low, up = Bt.eval_guard state trans.bound_guard in
    (*Format.eprintf "%f,%f @." low up;*)
    assert (low <= up);
    assert (0.0 <= low);
    if Array.length trans.weight = 0 then
      (* Isotropic sampling return uniform *)
      (low, low +. (u *. (up -. low)), up)
    else if u = 0.0 then (low, low, up)
    else if u = 1.0 then (low, up, up)
    else if low = up then (low, low, up)
    else
      let open W in
      let _, _, cdf, pdf =
        match delay with
        | Some d when d > low ->
            assert (d < up);
            let _, _, cdf, _ = trans.weight.(i) in
            let cdf2 =
              cdf -.. apply_const ?smp cdf tvar (F.of_float (max low d))
            in
            (0, zero, cdf2, diff cdf2 tvar)
        | _ -> trans.weight.(i)
      in

      let cdfap = eval_poly_state ?smp state cdf in
      let pdfap = eval_poly_state ?smp state pdf in
      let invweight = 1.0 /. W.fully_apply_float ?smp cdfap tvar up in
      let pb = (F.of_float invweight **. cdfap) -.. (const @@ F.of_float u) in
      let diffcdf = F.of_float invweight **. pdfap in
      try
        let time =
          try
            find_root ?smp ~max_iter pb ~diffp:diffcdf ~bound:(low, up) tvar
              (0.5 *. (low +. up))
          with Math.Zero_derivative _ ->
            find_root ?smp ~max_iter pb ~diffp:diffcdf ~bound:(low, up) tvar 0.0
        in
        if time < low || time > up then (
          Format.eprintf
            "Error: fail to compute time : (%i->[%s]) cdf:%a; diff:%a -> %g \
             [%g,%g]@."
            (get_loc state) trans.action (print ~pretty:true) pb
            (print ~pretty:true) diffcdf time low up;
          assert false);
        (low, time, up)
      with Math.Zero_derivative x ->
        Format.eprintf
          "Alert Fail to sample distribution %a at %g -> %a = %g @."
          (print ~pretty:true) cdf x (print ~pretty:true) cdfap u;
        raise (Math.Zero_derivative x)

  let compute_weight ?smp ?boltz ?(tolerance = 1.0e-2) ?delay rg i
      ((id, tab, remaining_time) as st) filter_trans =
    let state = rg.statelist.(id) in
    let max_deg =
      List.fold_left
        (fun acc (trans, _) ->
          if Array.length trans.weight > 0 then
            let deg, _, _, _ = trans.weight.(i) in
            max deg acc
          else acc)
        (-1) filter_trans
    in

    (* compute weight *)
    filter_trans
    |> List.filter_map (fun (trans, b) ->
           if Array.length trans.weight <= 0 then
             (*Isotropic sampling constant weight*)
             Some (1.0, trans, b)
           else
             let degree_of_liberty, weight, cdf, _ = trans.weight.(i) in
             let weight2 =
               match delay with
               | None -> weight
               | Some d ->
                   let low, up = b in
                   assert (d <= up);
                   if d < low then weight
                   else
                     W.(
                       apply_const ?smp cdf tvar F.(of_float up)
                       -.. apply_const ?smp cdf tvar F.(of_float d))
             in

             if degree_of_liberty < max_deg || weight2 = W.zero then None
             else
               let w = eval_poly_state_full ?smp st weight2 in
               if
                 w < -.tolerance
                 (* should be <0 but account for numerical error *)
               then (
                 Format.eprintf "Error: Negative weight : (%a->[%s]) %a->%g@."
                   (print_state ?trunc:None) (id, tab, remaining_time)
                   trans.action (W.print ~pretty:true) weight w;
                 assert false);
               Some (max 0.0 w, trans, b))
    (*|< (fun l ->
      Format.printf "[@[%a@]]@." (Format.pp_print_list (fun f (w,tr) ->
                                      let low = eval_bound (id,tab) tr.lower_bound
                                      and up = eval_bound (id,tab) tr.upper_bound in
                                      Format.fprintf f "[%c] %g;%g -> %g" tr.action low up w )) l)*)
    |>
    fun l ->
    match boltz with
    | Some z when state.is_accepting ->
        let wz = 1.0 /. z in
        ( wz,
          {
            action = "0";
            miniedge = [];
            bound_guard = Common.Punctual Bt.zero;
            is_contained_in_zone = false;
            weight = [||];
          },
          (0.0, 0.0) )
        :: l
    | _ -> l

  let choose_weight accu u =
    let totalw, accl =
      List.fold_left
        (fun (acc, l) (w, trans, _) ->
          (acc +. w, ((acc, acc +. w), trans) :: l))
        (0.0, []) accu
    in
    let skip = u *. totalw in
    let n = List.length accl in
    match accl with
    | [] -> None
    | [ (_, tr) ] -> Some tr
    | _ when u = 1.0 -> Some (snd @@ List.nth accl (n - 1))
    | _ when skip > 0.0 ->
        Some (snd @@ List.find (fun ((x, y), _) -> x <= skip && skip < y) accl)
    | _ ->
        Some (snd @@ List.nth accl (min (int_of_float (u *. float n)) (n - 1)))

  (* sample next transition from state using random number u *)
  let sample_trans ?smp ?boltz ?tolerance trans_list rg i st u =
    let tr_list = compute_weight ?smp ?boltz ?tolerance rg i st trans_list in
    match choose_weight tr_list u with
    | None -> raise (Deadlock st)
    | Some x -> x

  (* Wait 'time' time unit *)
  let elapse_time (id, tab, rt) time =
    let tab2 = Array.mapi (fun _ v -> v +. time) tab in
    (id, tab2, rt -. time)

  (* fire transition trans *)
  let fire (_, tab, rt) trans =
    let edge = List.hd trans.miniedge in
    let tab2 =
      Array.mapi (fun i v -> if edge.reset.(i + 1) then 0.0 else v) tab
    in
    (edge.target, tab2, rt)

  (*Format.printf "%a@ " print_state state;
    Format.printf "low:%f@.up:%f@.invweight:%f@." low up invweight;
    Format.printf "cdf:%a->%a@." P.print (invweight **. cdf) P.print (invweight **. cdfap);
    Format.printf "pdf:%a->%a@." P.print (invweight **. pdf) P.print (invweight **. pdfap);
      Format.printf " ---> %f @." time;*)

  (*let available_trans rg (id, _) =
    let state = rg.statelist.(id) in
    state.transition*)

  let filter_transition ~filter trans_list =
    trans_list
    |> List.filter_map (function tr, (low, up) ->
           (match filter with
           | ff when ff (low, up) tr -> Some (tr, (low, up))
           | _ -> None))

  let available_transition ?filter rg ((id, _, _) as st) =
    let state = rg.statelist.(id) in
    (* All transitions *)
    state.transition
    (* Add bound *)
    |> List.map (fun tr ->
           let low, up = Bt.eval_guard st tr.bound_guard in
           (tr, (low, up)))
    (* filter them *)
    |> List.filter (fun (_, (low, up)) -> low <= up)
    |>
    match filter with
    | Some ff -> filter_transition ~filter:ff
    | None -> fun x -> x

  let build_filter = function
    | Label t, Label l ->
        Some
          (fun (low, up) tr -> low <= t && t <= up && tr.ZoneGraph.action = l)
    | Label t, _ -> Some (fun (low, up) _ -> low <= t && t <= up)
    | _, Label l -> Some (fun _ tr -> tr.ZoneGraph.action = l)
    | _ -> None
end

module type MONITOR = sig
  type weight
  type bound
  type state
  type t
  type dyn
  type out

  val init :
    Format.formatter ->
    (weight, bound) ZoneGraph.t ->
    (string -> (float transChoice * string transChoice) list) ->
    t * dyn

  val new_traj : t -> dyn -> dyn

  val up_state :
    t ->
    dyn ->
    ?smp:float ->
    ?delay:float ->
    state ->
    ((weight, bound) ZoneGraph.transition * (float * float)) list ->
    int ->
    float * float ->
    (float transChoice * string transChoice) option

  val up_state_elapsed : t -> dyn -> state -> float -> int -> unit

  val up_trans :
    t ->
    dyn ->
    state ->
    float * float * float ->
    (weight, bound) ZoneGraph.transition ->
    int ->
    dyn

  val end_traj : t -> dyn -> bool -> float -> dyn * bool
  val end_sampling : t -> dyn -> out
end

module Make
    (Bt : ZoneGraphInput.BoundType)
    (W : Polynomial.SamplingStruct)
    (St : STATE with type p = W.t and type bound = Bt.t and type t = Bt.state)
    (OF : MONITOR
            with type weight = W.t
             and type bound = Bt.t
             and type state = St.t)
    (Param : sig
      val smp : float option
    end) =
struct
  let sample_traj ?smp ?boltz ~max_iter ~update_callback
      ~monitor_state:(monitor_stat, monitor_dyn) ?exact_duration rgpoly
      template_word ~samplers:(samp1, samp2) =
    let lengthword = Array.length template_word in
    (* Initialisation *)
    let state =
      ref (St.init rgpoly (Option.value ~default:0.0 exact_duration))
    in
    let mon_dyn = ref monitor_dyn in
    let i = ref 0 in
    let continue = ref true in
    try
      while !i < lengthword && !continue do
        let j = !i in
        let u1 = samp1 j and u2 = samp2 j in
        update_callback ();
        (* Index of polynome depending of sampling method*)
        let polyindex = max 0 (j - lengthword + rgpoly.nb_poly) in

        (* Compute available transition according to state and filter*)
        let available_transition =
          St.available_transition
            ?filter:(St.build_filter template_word.(j))
            rgpoly !state
        in

        (* Hook the monitor if there is an override apply it to the list of transition *)
        let override =
          OF.up_state monitor_stat !mon_dyn ?smp !state available_transition
            polyindex (u1, u2)
        in
        let filtered_transition =
          match Option.bind override St.build_filter with
          | None -> available_transition
          | Some filter -> St.filter_transition ~filter available_transition
        in

        (* Choose the random value *)
        let ueff =
          match (snd template_word.(j), override) with
          | _, Some (_, Rand u) -> u
          | Rand u, _ -> u
          | _ -> u1
        in

        (*Choose the transition*)
        let tr =
          St.sample_trans ?smp ?boltz filtered_transition rgpoly polyindex
            !state ueff
        in

        (* Sample time *)
        let low, time, up =
          match (fst template_word.(j), override) with
          | Label t, None | _, Some (Label t, _) ->
              let low, up = Bt.eval_guard !state tr.bound_guard in
              (low, t, up)
          | _ when exact_duration <> None && !i = lengthword - 1 ->
              (fun (_, _, x) -> (x, x, x)) !state
          | Rand u, None | _, Some (Rand u, _) ->
              St.sample_time ?smp ~max_iter polyindex !state tr u
          | Fixed, _ | _, Some (Fixed, _) ->
              St.sample_time ?smp ~max_iter polyindex !state tr u2
        in
        (* Update state *)
        state := St.elapse_time !state time;
        OF.up_state_elapsed monitor_stat !mon_dyn !state time polyindex;
        if tr.action = "0" then continue := false
        else (
          state := St.fire !state tr;
          mon_dyn :=
            OF.up_trans monitor_stat !mon_dyn !state (low, time, up) tr
              polyindex);
        if boltz = None then incr i
      done;
      !mon_dyn
    with Exit -> !mon_dyn

  let sample ?(outfile = Format.std_formatter) ?boltz ?seed ?(verbose = 1)
      ?exact_duration ~max_iter ~sampler ?(store_traj = false) rgpoly
      template_word nbtraj =
    (* Sampling *)
    (* Most of the code handle printing, Most of the work done by FunIt.sample_traj*)
    (* Compute template if not provided *)
    let lengthword = Array.length template_word in
    Low_disc_sampler.init ?seed lengthword;

    (*printing progress and state *)
    let print_bar = (not (outfile == Format.std_formatter)) && verbose > 0 in
    let update_callback =
      let cmp = ref 0 in
      function
      | () ->
          (try
             if
               print_bar && nbtraj > 1
               && !cmp mod ((lengthword - 1) * nbtraj / 100) = 0
             then (
               printf "|";
               print_flush ())
           with _ -> ());
          incr cmp
    in
    if print_bar then printf "Sampling: [";
    print_flush ();
    let monitor_stat, md = OF.init outfile rgpoly timeword_parser in
    let mon_dyn = ref md in

    (*Main Sampling Loop*)
    (try
       let i = ref 0 in
       let k = ref 0 in
       while !i < nbtraj do
         let md1 = OF.new_traj monitor_stat !mon_dyn in
         let md2 =
           sample_traj ?smp:Param.smp ?boltz ~max_iter ?exact_duration
             ~update_callback ~monitor_state:(monitor_stat, md1)
             ~samplers:
               ((fun j -> Low_disc_sampler.random j !k), fun j -> sampler j !k)
             rgpoly template_word
         in
         let md3, accept =
           OF.end_traj monitor_stat md2 store_traj
             (Low_disc_sampler.random (-1) !k)
         in
         if accept then incr i;
         mon_dyn := md3;
         incr k
       done
     with
    | St.Deadlock st ->
        if print_bar then printf "]@.";
        eprintf "A deadlock occured in state: %a@."
          (St.print_state ?trunc:None)
          st
    | Exit -> (
        (*give a chance to print debug information*)
        try ignore (OF.end_traj monitor_stat !mon_dyn store_traj 0.0)
        with _ -> ()));
    let r = OF.end_sampling monitor_stat !mon_dyn in
    if print_bar then printf "] [%.2fs]@." (Common.check_time ());
    r
end

module CombineMonitor
    (M1 : MONITOR)
    (M2 : MONITOR
            with type bound = M1.bound
            with type state = M1.state
            with type weight = M1.weight) :
  MONITOR
    with type weight = M1.weight
     and type bound = M1.bound
     and type state = M1.state
     and type out = M1.out = struct
  type t = M1.t * M2.t
  type dyn = M1.dyn * M2.dyn
  type bound = M1.bound
  type state = M1.state
  type weight = M1.weight
  type out = M1.out

  let init formatter zonegraph f =
    let s1, d1 = M1.init formatter zonegraph f in
    let s2, d2 = M2.init formatter zonegraph f in
    ((s1, s2), (d1, d2))

  let new_traj (s1, s2) (d1, d2) = (M1.new_traj s1 d1, M2.new_traj s2 d2)

  let up_trans (s1, s2) (d1, d2) state timing tr i =
    (M1.up_trans s1 d1 state timing tr i, M2.up_trans s2 d2 state timing tr i)

  let up_state (s1, s2) (d1, d2) ?smp ?delay state available_transition i fl =
    let a = M1.up_state s1 d1 ?smp ?delay state available_transition i fl in
    let _ = M2.up_state s2 d2 ?smp ?delay state available_transition i fl in
    a

  let up_state_elapsed (s1, s2) (d1, d2) state time i =
    M1.up_state_elapsed s1 d1 state time i;
    M2.up_state_elapsed s2 d2 state time i

  let end_traj (s1, s2) (d1, d2) b u =
    let r1, a1 = M1.end_traj s1 d1 b u in
    if a1 then
      let r2, a2 = M2.end_traj s2 d2 b u in
      ((r1, r2), a2)
    else ((r1, d2), false)

  let end_sampling (s1, s2) (d1, d2) =
    let a = M1.end_sampling s1 d1 in
    let _ = M2.end_sampling s2 d2 in
    a
end
