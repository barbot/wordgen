module type SYM = sig
  type t

  val content : t ref
  val def : t
  val list : string list
  val of_string : string -> t
  val to_string : t -> string
end

type arg_t =
  | Bool of bool ref
  | Set_string of string option ref
  | Set_int of int option ref
  | Set_float of float option ref
  | Int of int ref * int
  | Float of float ref * float
  | String of string ref * string
  | Symbol of (module SYM)

(*string list * 'a ref * 'a * ('a -> string) * (string -> 'a)*)

let to_caml_arg (opt, x, help) =
  match x with
  | Bool b -> (opt, Arg.Set b, help)
  | Set_string sr -> (opt, Arg.String (fun s -> sr := Some s), help)
  | Set_int ir -> (opt, Arg.Int (fun i -> ir := Some i), help)
  | Set_float fr -> (opt, Arg.Float (fun f -> fr := Some f), help)
  | Int (ir, d) -> (opt, Arg.Set_int ir, Printf.sprintf "%s (default:%i)" help d)
  | Float (fr, d) ->
      (opt, Arg.Set_float fr, Printf.sprintf "%s (default:%g)" help d)
  | String (sr, d) ->
      (opt, Arg.Set_string sr, Printf.sprintf "%s (default:%s)" help d)
  | Symbol s ->
      let module S = (val s : SYM) in
      ( opt,
        Arg.Symbol (S.list, fun s -> S.content := S.of_string s),
        Printf.sprintf "%s (default:%s)" help (S.to_string S.def) )

let build_cmd_elem (opt, x, _) =
  match x with
  | Bool a when !a -> " " ^ opt
  | Bool _ -> ""
  | Set_string a -> (
      match !a with
      | Some v ->
          if String.contains v ' ' then Printf.sprintf " %s '%s'" opt v
          else Printf.sprintf " %s %s" opt v
      | None -> "")
  | String (s, ds) ->
      if !s = ds then ""
      else if String.contains !s ' ' then Printf.sprintf " %s '%s'" opt !s
      else Printf.sprintf " %s %s" opt !s
  | Set_float a -> (
      match !a with Some v -> Printf.sprintf " %s %g" opt v | None -> "")
  | Float (s, ds) -> if !s = ds then "" else Printf.sprintf " %s %g" opt !s
  | Set_int a -> (
      match !a with Some v -> Printf.sprintf " %s %i" opt v | None -> "")
  | Int (s, ds) -> if !s = ds then "" else Printf.sprintf " %s %i" opt !s
  | Symbol s ->
      let module S = (val s : SYM) in
      if !S.content = S.def then ""
      else Printf.sprintf " %s %s" opt (S.to_string !S.content)

let reset_elem (_, x, _) =
  match x with
  | Bool a -> a := false
  | Set_int a -> a := None
  | Set_float a -> a := None
  | Set_string a -> a := None
  | Int (a, d) -> a := d
  | Float (a, d) -> a := d
  | String (a, d) -> a := d
  | Symbol s ->
      let module S = (val s : SYM) in
      S.content := S.def

let reset l = List.iter reset_elem l

let build_cmd l =
  let str =
    List.fold_left (fun acc elem -> acc ^ build_cmd_elem elem) "wordgen" l
  in
  str

let npoly = ref 3
let no_cache = ref false
let infile = ref ""
let regexp = ref None

(*let outfile = ref Format.std_formatter*)
let outfilename = ref (None : string option)
let nbtraj = ref 10
let verbose = ref 1
let splitting_debug = ref ""
let out_style = ref Wordgen_lib.OutFormat.Timeword
let is_interactive = ref false
let store_traj = ref false
let print_rg = ref false
let sampler = ref Wordgen_lib.Low_disc_sampler.Random
let gnuplot_driver = ref None
let gnuplot_cmd = ref None
let export_zone_graph = ref None
let max_iteration = ref 20
let rational_impl = ref false
let random_seed = ref None
let frequency = ref None
let expected_duration = ref None
let is_duration_exact = ref false
let boltzmann_param = ref None
let expected_size = ref None
let delta_robustness = ref None

(* hidden param *)
let receding = ref None
let template_string = ref None

(*computed param *)

let sampling_meth = ref Wordgen_lib.Sampling.Exact

let template =
  ref (Array.make 1 (Wordgen_lib.Sampling.Fixed, Wordgen_lib.Sampling.Fixed))

let spec_minimal =
  [
    ("--debug", Bool print_rg, "Print the intermediate representation");
    ("--poly", Int (npoly, 3), "Number of polynome Iteration");
    ("--traj", Int (nbtraj, 10), "Number of trajectories");
    ("--receding", Set_int receding, "trajectory length");
    ( "--sampler",
      Symbol
        (module struct
          include Wordgen_lib.Low_disc_sampler

          let list = [ "random"; "halton"; "kronecker" ]
          let content = sampler
        end),
      " choose sampler" );
    ("--seed", Set_int random_seed, "seed of the random sampler");
    ( "--output-format",
      Symbol
        (module struct
          type t = Wordgen_lib.OutFormat.output_style

          let list = Wordgen_lib.OutFormat.style_list
          let content = out_style
          let def = Wordgen_lib.OutFormat.Timeword
          let of_string = Wordgen_lib.OutFormat.style_of_string
          let to_string = Wordgen_lib.OutFormat.string_of_style
        end),
      " choose the format of the output" );
    ("-v", Int (verbose, 1), "Verbose level");
    ( "--exact-rational",
      Bool rational_impl,
      "Use exact arithmetic for computation" );
  ]

let spec_short =
  [
    ("--regexp", Set_string regexp, "Take a regular expression as input");
    ( "--expected-duration",
      Set_float expected_duration,
      "time word duration parameter" );
    ("--exact-duration", Bool is_duration_exact, "sample duration exactly");
    ("--expected-size", Set_float expected_size, "time word size parameter");
    (*"-o", Arg.String (fun s -> outfile := Format.formatter_of_out_channel @@ open_out s), "Output file for trajectories";*)
    ( "--template",
      Set_string template_string,
      "specify a template for time words; template look like '0.2[a]_[b]'" );
    ( "--max-iteration",
      Int (max_iteration, 20),
      "Number of iteration for Newton method" );
    ("--apericube", Bool store_traj, "Compute apericube");
    
  ]

let spec_full =
  spec_minimal @ spec_short
  @ [
    ("--boltzmann", Set_float boltzmann_param, "boltzmann parameter z");
    
    ("--frequency", Set_float frequency, "frequency parameter s");
    
      ("--interactive", Bool is_interactive, "Interactive mode");
      ("-i", Bool is_interactive, "Interactive mode");
      ("--no-cache", Bool no_cache, "disable caching");
      ("--gnuplot-driver", Set_string gnuplot_driver, "launch gnuplot");
      ( "--export-splitreach",
        Set_string export_zone_graph,
        "Export the splitted zone graph" );
      ( "--splitting-debug",
        String (splitting_debug, ""),
        "Export debug information on the splitting in tex format" );
    ( "--generate-perturbation",
      Set_float delta_robustness,
      "generate perturbated timed word" );
    ( "--gnuplot-cmd",
      Set_string gnuplot_cmd,
      "Command to pass to gnuplot for display" );
        ]

let post_parse () =
  (match (!receding, !template_string) with
  | None, None -> sampling_meth := Exact
  | Some r, None -> sampling_meth := Receding r
  | None, Some t -> (
      try sampling_meth := Driven (Wordgen_lib.Sampling.timeword_parser t)
      with _ -> failwith "Fail to parse driving command")
  | Some _, Some _ -> failwith "Set receding horizon or a template not both");
  (match (!sampling_meth, !expected_duration) with
  | Receding r, Some t ->
      expected_duration := Some (t *. float !npoly /. float r)
  | Driven l, Some t ->
      expected_duration := Some (t *. float !npoly /. float (List.length l))
  | _ -> ());
  (template :=
     let open Wordgen_lib.Sampling in
     match !sampling_meth with
     | Exact when !npoly >= 0 -> Array.make !npoly (Fixed, Fixed)
     | Exact (*receding 3*) -> Array.make 3 (Fixed, Fixed)
     | Receding x -> Array.make x (Fixed, Fixed)
     | Driven l -> Array.of_list l);
  if !regexp <> None then no_cache := true

let spec_caml_arg = List.map to_caml_arg spec_full
let usage_str = "wordgen automaton.prism [outputfile]"
