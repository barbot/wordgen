(*

Copyright (C) 2019 LACL Universite Paris Est Creteil Val de Marne
Authors: Benoît Barbot
Licence: GPL-3.0-or-later

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*)
open Common

type l = string
type miniedge = { target : int; reset : bool array; prob : float }

type ('a, 'b) transition = {
  action : l;
  miniedge : miniedge list;
  bound_guard : 'b guard;
  is_contained_in_zone : bool;
  mutable weight : (int * 'a * 'a * 'a) array;
}

type ('a, 'b) loczone = {
  disc_name : string;
  init_zone : string;
  id : int;
  redcoord : int list list;
  transition : ('a, 'b) transition list;
  mutable loc_weight : 'a;
  clockmap : int array;
  is_accepting : bool;
  svg_graph : Format.formatter -> int * int;
}

type ('a, 'b) t = {
  init : int;
  nb_poly : int;
  number_of_component : int;
  statelist : ('a, 'b) loczone array;
  var_string : (string * bool) array;
  cardclocks : int;
  alphabet : l array;
  printer : ?pretty:bool -> Format.formatter -> 'a -> unit;
  is_deterministic : bool;
}

type 'a vspace = 'a * ('a -> 'a -> 'a) * (float -> 'a -> 'a)

(*type 'a bound_transition = ('a,bound) transition*)
(*type 'a bt = ('a,bound) t*)

let print_reset vs f m =
  let first = ref true in
  Format.fprintf f "@[[%a]@]"
    (fun o a ->
      for i = 1 to Array.length a - 1 do
        if a.(i) then
          Format.fprintf o "%s %s"
            (if !first then (
               first := false;
               "")
             else ",")
            (fst vs.(i))
      done)
    m.reset

let print_miniedge rg f m =
  Format.fprintf f "@[(%f,%i,%a)@]" m.prob m.target (print_reset rg) m

let print_miniedge_list rg f = function
  | [ t ] when t.prob = 1.0 ->
      Format.fprintf f "\"target\":%i,@, \"reset\": %a" t.target
        (print_reset rg) t
  | [ t ] ->
      Format.fprintf f "\"target\":%i,@, \"prob\": %g,@, \"reset\": %a" t.target
        t.prob (print_reset rg) t
  | ml ->
      Format.fprintf f "\"miniedge\":[@[%a@]]"
        (fun f -> List.iter (print_miniedge rg f))
        ml

let print_poly rg f t =
  let pl f = rg.printer f in
  if Array.length t.weight > 0 then
    let i, weight, cdf, pdf = t.weight.(0) in
    Format.fprintf f
      ", \"weight\":%a,@ \"degree_of_liberty\":%i,@ \"cdf\":%a,@ \"pdf\":%a,@ "
      pl weight i pl cdf pl pdf

let print_transition rg pb f t =
  let vs = rg.var_string in
  Format.fprintf f
    "{@[<hov 2>@,\
     \"action\":\"%s\",@ \"zone\":%a, @ \"is_contained_in_zone\": %b,@ %a@ %a@ \
     @]}"
    t.action
    (print_guard (pb vs))
    t.bound_guard t.is_contained_in_zone (print_miniedge_list vs) t.miniedge
    (print_poly rg) t

let print_redcoord f rc =
  Format.pp_print_list
    ~pp_sep:(fun o () -> Format.fprintf o ", ")
    (fun o l ->
      Format.fprintf o "[%a]"
        (Format.pp_print_list
           ~pp_sep:(fun o () -> Format.fprintf o ", ")
           (fun o a -> Format.pp_print_int o a))
        l)
    f rc

let print_loczone rg pb f r =
  Format.fprintf f
    "{@[<v 2>@ \"id\":%i,@ \"disc_name\":@[<hov>\"%s\"@],@ \
     \"init_zone\":@[<hov>\"%s\"@],@ \"is_accepting\":%b,@ \"redcoord\":[%a],@ \
     \"clockmap\":[@[%a@]],@ \"weight\":%a,@ \"transitions\":[@[<v 2>@ \
     %a]@]@]@,\
     }@ " r.id r.disc_name r.init_zone r.is_accepting print_redcoord r.redcoord
    (fun f ->
      Array.iteri (fun i x ->
          Format.fprintf f "%s%s "
            (if i > 0 then "," else "")
            (if x = 0 then "0" else fst rg.var_string.(x))))
    r.clockmap (rg.printer ~pretty:false) r.loc_weight
    (Format.pp_print_list
       ~pp_sep:(fun f () -> Format.fprintf f ",@ ")
       (fun f g -> print_transition rg pb f g))
    r.transition

let print pb f rg =
  Format.fprintf f
    "{@[<v 2>@ \"init\":%i,@ \"cardclocks\":%i,@ \"statelist\":[@ @[<v 2>%a@]@ \
     ]@]}"
    rg.init rg.cardclocks
    (fun f a ->
      Array.iteri
        (fun i state ->
          if i > 0 then Format.fprintf f ",";
          print_loczone rg pb f state)
        a)
    rg.statelist

let print_transition_dot rg pb f source t col =
  match t.miniedge with
  | [ m ] ->
      Format.fprintf f "l_%i -> l_%i [color=\"%s\" label=\"%s; %a;%a\"]@." source m.target col
        t.action
        (print_guard (pb rg))
        t.bound_guard (print_reset rg) m
  | _ -> ()

let print_loczone_dot _ f r =
  let w, h = r.svg_graph Format.str_formatter in
  Format.fprintf f
    "l_%i [shape=%s, width=\"%fpt\", height=\"%fpt\", label=\"%i\"]@." r.id
    (if r.is_accepting then "box3d" else "box")
    (float w /. 70.0)
    (float h /. 70.0)
    r.id (*r.disc_name r.init_zone*);
  (Format.flush_str_formatter (), w, h)

let print_dot rg pb f =
  Format.fprintf f "digraph D{@[<hov 2>@.";
  Format.fprintf f "node[shape=point]; init;@.init -> l_0;@.";
  let svg_array =
    Array.map (fun lz -> print_loczone_dot rg f lz) rg.statelist
  in
  Array.iter
    (fun lz ->
      Common.list_iter_colour 
        (fun tr col -> print_transition_dot rg.var_string pb f lz.id tr col)
        lz.transition)
    rg.statelist;
  Format.fprintf f "@]}@.";
  svg_array

open Yojson.Basic

let get_in s l =
  match List.assoc_opt s l with
  | Some e -> e
  | None ->
      Format.fprintf Format.err_formatter "Value %s not found in [%s]" s
        (to_string (`Assoc l));
      failwith "Fail to parse JSON"

let miniedge_of_json t =
  t |> Util.to_assoc |> fun l ->
  let prob =
    List.assoc_opt "prob" l |>>> Util.to_number
    |>>| (List.assoc_opt "volume" l |>>> Util.to_number
         |>>| (List.assoc_opt "weight" l |>>> Util.to_number |>>| 1.0))
  in
  {
    target = get_in "target" l |> Util.to_int;
    reset =
      List.assoc_opt "reset" l |>>> Util.to_list |>>| []
      |> (fun l -> `Int 0 :: l)
      |> Array.of_list |> Array.map Util.to_int
      |> Array.map (function 0 -> true | _ -> false);
    prob;
  }

let transition_of_json fb t =
  ( t |> Util.to_assoc |> fun l ->
    match
      List.assoc_opt "zone" l |>>> Util.to_list |>>| [] |> List.map Util.to_int
    with
    | [ z0; z1; z2; z3 ] -> (l, (build_bound z0 z1, build_bound z2 z3))
    | [ z0; z1 ] -> (l, (build_bound z0 z1, Const 0))
    | [] -> (l, (Const 0, Const 0))
    | x -> failwith ("Fail to parse zone" ^ string_of_int (List.length x)) )
  |> fun (l, (lb, ub)) ->
  let is_contained_in_zone =
    try List.assoc "is_zone_contained" l |> Util.to_bool
    with Not_found -> false
  in
  let miniedge =
    match
      List.assoc_opt "miniedge" l
      |>>> Util.to_list |>>> List.map miniedge_of_json
    with
    | Some e -> e
    | None -> [ miniedge_of_json t ]
  in

  let lower_bound = fb (if is_contained_in_zone then Const 0 else lb) in
  let upper_bound = fb ub in

  {
    action = get_in "action" l |> Util.to_string;
    miniedge;
    bound_guard = build_guard lower_bound upper_bound;
    is_contained_in_zone;
    weight = ([||] : (int * unit * unit * unit) array);
  }

let int_list_json x = x |> Util.to_list |> List.map Util.to_int

let loczone_of_json fb _ t =
  t |> Util.to_assoc |> fun l ->
  let redcoord =
    List.assoc_opt "redcoord" l
    |>>> Util.to_list |>>| [] |> List.map int_list_json
  in
  let id = List.assoc "id" l |> Util.to_int in
  {
    id;
    disc_name =
      List.assoc_opt "name" l |>>> Util.to_string |>>| "s_" ^ string_of_int id;
    init_zone = "";
    redcoord;
    transition =
      List.assoc "transition" l |> Util.to_list
      |> List.map (transition_of_json fb);
    loc_weight = ();
    clockmap = ([||] : int array);
    is_accepting = List.assoc_opt "is_accepting" l |>>> Util.to_bool |>>| true;
    svg_graph = (fun _ -> (0, 0));
  }

let of_json fb t =
  t |> Util.to_assoc |> fun l ->
  let cardclocks = List.assoc_opt "cardclocks" l |>>> Util.to_int |>>| 0 in
  let var_string =
    Array.init
      (cardclocks + 1 (*s*) + 1 (*z*) + 1 (*T*) + 1)
      (fun i ->
        match i with
        | 0 -> ("t", false) (* The variable 0 is overloaded as 0 and t ! *)
        | i when i = cardclocks + 1 -> ("s", false)
        | i when i = cardclocks + 1 + 1 -> ("z", false)
        | i when i = cardclocks + 1 + 1 + 1 -> ("T", false)
        | 1 -> ("x", true)
        | 2 -> ("y", true)
        | _ -> ("x_" ^ string_of_int i, true))
  in
  {
    init = List.assoc_opt "init" l |>>> Util.to_int |>>| 0;
    nb_poly = 0;
    statelist =
      List.assoc "statelist" l |> Util.to_list
      |> List.map (loczone_of_json fb cardclocks)
      |> Array.of_list;
    number_of_component = 1;
    cardclocks;
    var_string;
    alphabet =
      List.assoc_opt "alphabet" l
      |>>> Util.to_list |>>| []
      |> List.map (fun x -> Util.to_string x)
      |> Array.of_list;
    printer = (fun ?pretty:_ _ _ -> ());
    is_deterministic = false;
  }

let toDisjointSet li m =
  List.fold_left
    (fun res1 li2 ->
      List.fold_left (fun res2 v -> Puf.union res2 (List.hd li2) v) res1 li2)
    (Puf.create m) li

(* Build a new zone graph augmented with volumetric data*)
let copy_trans nbpoly (z, f_map_bound) clockmap zero tr =
  {
    tr with
    bound_guard =
      map_guard
        (fun g -> if tr.is_contained_in_zone then z else f_map_bound clockmap g)
        ~g:(fun g -> f_map_bound clockmap g)
        tr.bound_guard;
    weight = Array.make nbpoly (-1, zero, zero, zero);
  }

let copy_loczone nbpoly fbt cardclocks zero one loczone =
  (* The var at index 0 is either the 'zero' clock or 't' -1 +1 to remember that*)
  let disSet = toDisjointSet loczone.redcoord (cardclocks + 1) in
  let clockmap =
    Array.init (cardclocks + 1) (fun i ->
        if i = 0 then 0 else Puf.find disSet i)
  in
  {
    loczone with
    transition =
      List.map (copy_trans nbpoly fbt clockmap zero) loczone.transition;
    clockmap;
    loc_weight = one;
  }

let copy nbpoly fbt zero one printer rg =
  {
    rg with
    nb_poly = nbpoly;
    statelist =
      Array.map (copy_loczone nbpoly fbt rg.cardclocks zero one) rg.statelist;
    printer;
  }

let add_with_punctual (_, ( +.. ), _) (a1, (a2 : int)) (d1, d2) =
  if a2 < d2 then (d1, d2) else if a2 > d2 then (a1, a2) else (a1 +.. d1, a2)

let add_triplet (_, ( +.. ), ( **. )) (deg1, a, b, c) p (deg2, d, e, f) =
  if deg1 < deg2 then (deg2, p **. d, p **. e, p **. f)
  else if deg1 > deg2 then (deg1, a, b, c)
  else (deg1, a +.. (p **. d), b +.. (p **. e), c +.. (p **. f))

(*let app_triplet g (a, b, c) (d, e, f) = (g a d, g b e, g c f)*)

let iterate rg f =
  rg.statelist
  |> Array.iteri (fun i state ->
         state.transition
         |> List.iter (fun trans ->
                trans.miniedge
                |> List.iter (fun edge -> f i edge.target trans edge)))

let apply_functionnal rg i ((zero, _, _) as vect_space) f g stateweight =
  rg.statelist
  (* For all states *)
  |> Array.map (fun state ->
         state.transition
         (* Iterate over transition *)
         |> List.fold_left
              (fun weight trans ->
                let ((deg, w1, _, _) as weight_vect) =
                  trans.miniedge
                  (* Iterate over miniedge *)
                  |> List.fold_left
                       (fun acc edge ->
                         (* Get weight of target state as polynome in clock*)
                         let weighttarget = stateweight.(edge.target) in
                         let contrib = f weighttarget state trans edge in
                         add_triplet vect_space acc edge.prob contrib)
                       (-1, zero, zero, zero)
                in
                trans.weight.(i) <- weight_vect;
                add_with_punctual vect_space weight (w1, deg))
              (zero, -1)
         |> g state
         |< fun (x, _) -> state.loc_weight <- x
         (*Initalize sum*))

let copy_map_weight rg f =
  {
    rg with
    statelist =
      rg.statelist
      |> Array.map (fun state ->
             {
               state with
               transition =
                 List.map
                   (fun trans ->
                     {
                       trans with
                       weight =
                         Array.map
                           (fun (i, w1, w2, w3) -> (i, f w1, f w2, f w3))
                           trans.weight;
                     })
                   state.transition;
               loc_weight = f state.loc_weight;
             });
  }

let update_weight rg f =
  rg.statelist
  |> Array.iter (fun state ->
         state.transition
         |> List.iter (fun trans ->
                trans.weight <-
                  Array.map
                    (fun (i, w1, w2, w3) -> (i, f w1, f w2, f w3))
                    trans.weight);
         state.loc_weight <- f state.loc_weight)
