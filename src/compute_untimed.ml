open Weight_structure

module Compute (Bt : ZoneGraphInput.BoundType) (P : Polynomial.S) = struct
  include Rational_fraction.Make (P)
  module Bt = Bt

  let accepting_value state = if state.ZoneGraph.is_accepting then one else zero
  let z = P.z
  let s = P.s
  let tvar = P.tvar

  let fix_point_operator rg =
    let open ZoneGraph in
    let n = Array.length rg.statelist in

    let mat = Array.make_matrix n n zero in
    iterate rg (fun i j _ e ->
        mat.(i).(j) <-
          mat.(i).(j) +.. (F.of_float e.prob **. of_p P.(zero -.. var z)));
    Array.iteri (fun i _ -> mat.(i).(i) <- one +.. mat.(i).(i)) mat;
    (*print_mat mat;*)
    let b = Array.map accepting_value rg.statelist in
    let x = solve mat b in
    Array.iteri
      (fun i state -> state.loc_weight <- x.(i))
      rg.ZoneGraph.statelist;
    iterate rg (fun _ _ t e ->
        let i, a, b, c = t.weight.(0) in
        t.weight.(0) <- (i, a +.. (F.of_float e.prob **. x.(e.target)), b, c))

  let compute_z ?(verbose = 1) ?finish ?seed:_ expected_length rg =
    let w = rg.ZoneGraph.statelist.(rg.init).loc_weight in
    let dw = diff w z in
    let ((num, denom) as size) = var z *.. dw /.. w in
    if verbose > 1 then
      Format.printf "\nGenerating function : %a@." (print ~pretty:true) size;
    let first_pole = P.find_root_sturm denom z 0.0 in
    let elf = F.of_float expected_length in
    let eq = P.( -.. ) num (P.( **. ) elf denom) in
    let eq' = P.diff eq z in

    let tf zv =
      match
        ( P.to_float @@ P.apply_const eq z (F.of_float zv),
          P.to_float @@ P.apply_const eq' z (F.of_float zv) )
      with
      | Some x, Some y -> (x, y)
      | _ -> failwith "Fail to evaluate"
    in

    let compv zval =
      if verbose > 3 then Format.printf "Size length for z: %g -> " zval;
      let s2, s2' = tf zval in

      if verbose > 3 then Format.printf "(%g)@." s2;
      (s2, s2')
    in
    let x =
      Math.newton_raphson_iterate ~max_iter:100 ~bound:(0.0, first_pole) compv
        (first_pole /. 2.0)
      (*Common.newton_raphson_iterate ~bound:(0.0, Float.infinity) ~max_iter:1000
        compv 0.1*)
    in

    let v =
      match to_float @@ apply_const size z (F.of_float x) with
      | Some x -> x
      | _ -> failwith "Fail to evaluate"
    in

    Option.iter (fun f -> f (x, first_pole, v)) finish;

    Some (x, first_pole, v)

  let compute ?update:_ ?finish ?print_interm:_ rg _ _ =
    let rgp = ZoneGraph.copy 1 (Bt.zero, Bt.map) zero one print rg in
    fix_point_operator rgp;
    Option.iter (fun f -> f rgp) finish;
    rgp

  (*let compute_s ?verbose:_ ?finish:_ _ _ = None*)

  let compute_params ?(verbose = 1) ?seed (t_length, _) _ rg =
    match t_length with
    | Some t -> (compute_z ~verbose ?seed t rg, None)
    | _ -> (None, None)
end
