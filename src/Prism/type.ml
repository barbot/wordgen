(*

Copyright (C) 2019 LACL Universite Paris Est Creteil Val de Marne
Authors: Benoît Barbot
Licence: GPL-3.0-or-later

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*)

let rec print_list2 fu sep file = function
  | [] -> ()
  | [ t ] -> fu file t
  | t :: q -> Format.fprintf file "%a%s%a" fu t sep (print_list2 fu sep) q

type cmp = EQ | SG | SL | GE | LE | NEQ

type _ expr' =
  | Bool : bool -> bool expr'
  | Int : int -> int expr'
  | Float : float -> float expr'
  | Not : bool expr' -> bool expr'
  | And : bool expr' * bool expr' -> bool expr'
  | Or : bool expr' * bool expr' -> bool expr'
  | IntAtom : int expr' * cmp * int expr' -> bool expr'
  | FloatAtom : float expr' * cmp * float expr' -> bool expr'
  | BoolAtom : bool expr' * cmp * bool expr' -> bool expr'
  | Plus : 'a expr' * 'a expr' -> 'a expr'
  | Minus : 'a expr' * 'a expr' -> 'a expr'
  | Mult : 'a expr' * 'a expr' -> 'a expr'
  | Floor : float expr' -> int expr'
  | Ceil : float expr' -> int expr'
  | CastInt : int expr' -> float expr'
  | CastBool : bool expr' -> int expr'
  | IntName : string -> int expr'
  | FloatName : string -> float expr'
  | BoolName : string -> bool expr'
  | Div : float expr' * float expr' -> float expr'
  | Exp : float expr' -> float expr'
  | FunCall : string * 'a expr' list -> 'a expr'
  | If : (bool expr' * 'a expr' * 'a expr') -> 'a expr'

type full_expr =
  | BoolExpr of bool expr'
  | IntExpr of int expr'
  | FloatExpr of float expr'

type cmdAttr =
  | Close
  | ParseInt of int expr'
  | ParseFloat of float expr'
  | ParseBool of bool expr'
  | ParseDistr of string * float expr' list
  | XMLInt of string
  | XMLFloat of string
  | XMLBool of string
  | XMLDistr of string

let neg_cmp = function
  | EQ -> NEQ
  | SG -> LE
  | SL -> GE
  | GE -> SL
  | LE -> SG
  | NEQ -> EQ

let eval_cmp x y = function
  | EQ -> x = y
  | NEQ -> x != y
  | SG -> x > y
  | SL -> x < y
  | GE -> x >= y
  | LE -> x <= y

let rec neg_bool = function
  | Bool x -> Bool (not x)
  | BoolName x -> Not (BoolName x)
  | Not e -> e
  | IntAtom (ie1, cmp, ie2) -> IntAtom (ie1, neg_cmp cmp, ie2)
  | FloatAtom (ie1, cmp, ie2) -> FloatAtom (ie1, neg_cmp cmp, ie2)
  | BoolAtom (ie1, cmp, ie2) -> BoolAtom (ie1, neg_cmp cmp, ie2)
  | And (e1, e2) -> Or (neg_bool e1, neg_bool e2)
  | Mult (e1, e2) -> Or (neg_bool e1, neg_bool e2)
  | Or (e1, e2) -> And (neg_bool e1, neg_bool e2)
  | Plus (e1, e2) -> And (neg_bool e1, neg_bool e2)
  | Minus (e1, e2) -> Or (neg_bool e1, e2)
  | FunCall (e, l) -> Not (FunCall (e, l))
  | If (c, e1, e2) -> If (c, neg_bool e1, neg_bool e2)

let rec eval :
    type a.
    ?iname:(string -> int expr' option) ->
    ?fname:(string -> float expr' option) ->
    ?bname:(string -> bool expr' option) ->
    a expr' ->
    a expr' =
 fun ?iname:(iv = fun _ -> None) ?fname:(fv = fun _ -> None)
     ?bname:(bv = fun _ -> None) x ->
  let ev x = eval ~iname:iv ~fname:fv ~bname:bv x in
  match x with
  | IntName s -> ( match iv s with Some e -> ev e | None -> x)
  | FloatName s -> ( match fv s with Some e -> ev e | None -> x)
  | BoolName s -> ( match bv s with Some e -> ev e | None -> x)
  | Plus (e1, e2) -> (
      match (ev e1, ev e2) with
      | Int y, Int z -> Int (y + z)
      | Float y, Float z -> Float (y +. z)
      | Bool y, Bool z -> Bool (y || z)
      | ee1, ee2 -> (
          match ee2 with
          | Int 0 -> ee1
          | Int y when y < 0 -> Minus (ee1, Int (-y))
          | Float 0.0 -> ee1
          | Float y when y < 0.0 -> Minus (ee1, Float (-.y))
          | _ -> Plus (ee1, ee2)))
  | Not be -> ( match ev be with Bool b -> Bool (not b) | x -> Not x)
  | And (e1, e2) -> (
      match (ev e1, ev e2) with
      | Bool y, z -> if y then z else Bool false
      | y, Bool z -> if z then y else Bool false
      | ee1, ee2 -> And (ee1, ee2))
  | Or (e1, e2) -> (
      match (ev e1, ev e2) with
      | Bool y, z -> if not y then z else Bool true
      | y, Bool z -> if not z then y else Bool true
      | ee1, ee2 -> Or (ee1, ee2))
  | BoolAtom (e1, cmp, e2) -> (
      match (ev e1, cmp, ev e2) with
      | Bool y, EQ, z -> if y then z else ev (Not z)
      | Bool y, NEQ, z -> if not y then z else z
      | z, EQ, Bool y -> if y then z else ev (Not z)
      | z, NEQ, Bool y -> if not y then z else z
      | y, EQ, z -> Or (And (y, z), And (ev (Not y), ev (Not z)))
      | ee1, cmp, ee2 -> BoolAtom (ee1, cmp, ee2))
  | Mult (e1, e2) -> (
      match (ev e1, ev e2) with
      | Int y, Int z -> Int (y * z)
      | Float y, Float z -> Float (y *. z)
      | Float y, z ->
          if y = 1.0 then z else if y = 0.0 then Float 0.0 else Mult (Float y, z)
      | z, Float y ->
          if y = 1.0 then z else if y = 0.0 then Float 0.0 else Mult (Float y, z)
      | Int y, z -> if y = 1 then z else if y = 0 then Int 0 else Mult (Int y, z)
      | z, Int y -> if y = 1 then z else if y = 0 then Int 0 else Mult (Int y, z)
      | Bool y, Bool z -> Bool (y && z)
      | ee1, ee2 -> Mult (ee1, ee2))
  | Div (e1, e2) -> (
      match (ev e1, ev e2) with
      | Float y, Float z -> Float (y /. z)
      | z, Float 1.0 -> z
      | ee1, ee2 -> Div (ee1, ee2))
  | Minus (e1, e2) -> (
      match (ev e1, ev e2) with
      | Int y, Int z -> Int (y - z)
      | Float y, Float z -> Float (y -. z)
      | ee1, ee2 -> Minus (ee1, ee2))
  | CastInt e -> ( match ev e with Int i -> Float (float i) | y -> CastInt y)
  | IntAtom (ie1, c, ie2) -> (
      match (ev ie1, ev ie2) with
      | Int i1, Int i2 -> Bool (eval_cmp i1 i2 c)
      | x, y -> IntAtom (x, c, y))
  | FloatAtom (fe1, c, fe2) -> (
      match (ev fe1, ev fe2) with
      | Float f1, Float f2 -> Bool (eval_cmp f1 f2 c)
      | x, y -> FloatAtom (x, c, y))
  | Bool b -> Bool b
  | Int i -> Int i
  | Float f -> Float f
  | Floor f -> (
      match ev f with Float fv -> Int (int_of_float (floor fv)) | x -> Floor x)
  | Ceil f -> (
      match ev f with Float fv -> Int (int_of_float (ceil fv)) | x -> Floor x)
  | CastBool be -> (
      match ev be with
      | Bool true -> Int 1
      | Bool false -> Int 0
      | x -> CastBool x)
  | Exp f -> ( match ev f with Float fv -> Float (exp fv) | x -> Exp x)
  | If (c, e1, e2) -> (
      match ev c with
      | Bool true -> ev e1
      | Bool false -> ev e2
      | x -> If (x, ev e1, ev e2))
  | FunCall ("max", [ e1; e2 ]) -> (
      match (ev e1, ev e2) with
      | Int y, Int z -> Int (max y z)
      | Float y, Float z -> Float (max y z)
      | ee1, ee2 -> FunCall ("max", [ ee1; ee2 ]))
  | FunCall ("min", [ e1; e2 ]) -> (
      match (ev e1, ev e2) with
      | Int y, Int z -> Int (min y z)
      | Float y, Float z -> Float (min y z)
      | ee1, ee2 -> FunCall ("max", [ ee1; ee2 ]))
  | x -> x

module MultiSet (Ot : Map.OrderedType) = struct
  include Map.Make (Ot)

  let add_multi e t =
    try
      let i = find e t in
      remove e t |> add e (i + 1)
    with Not_found -> add e 1 t
end

module StringSet = Set.Make (String)
module StringMap = MultiSet (String)

let printH_cmp f e =
  let open Format in
  match e with
  | EQ -> fprintf f "="
  | SG -> fprintf f ">"
  | SL -> fprintf f "<"
  | GE -> fprintf f ">="
  | LE -> fprintf f "<="
  | NEQ -> fprintf f "!="

let print_pretty_float = function
  | x when x = 0.5 -> "1/2"
  | x when x = 1. /. 3. -> "1/3"
  | x when x = 2. /. 3. -> "2/3"
  | x when x = 1. /. 4. -> "1/4"
  | x when x = 3. /. 4. -> "3/4"
  | x when x = 1. /. 5. -> "1/5"
  | x when x = 2. /. 5. -> "2/5"
  | x when x = 3. /. 5. -> "3/5"
  | x when x = 4. /. 5. -> "4/5"
  | x when x = 1. /. 6. -> "1/6"
  | x when x = 5. /. 6. -> "5/6"
  | x when x = 1. /. 7. -> "1/7"
  | x when x = 1. /. 8. -> "1/8"
  | x when x = 3. /. 8. -> "3/8"
  | x when x = 5. /. 8. -> "5/8"
  | x when x = 7. /. 8. -> "7/8"
  | x when x = 1. /. 9. -> "1/9"
  | x when x = 2. /. 9. -> "2/9"
  | x when x = 4. /. 9. -> "4/9"
  | x when x = 5. /. 9. -> "5/9"
  | x when x = 7. /. 9. -> "7/9"
  | x when x = 8. /. 9. -> "8/9"
  | x when x = 1. /. 10. -> "1/10"
  | x when x = 3. /. 10. -> "3/10"
  | x when x = 7. /. 10. -> "7/10"
  | x when x = 9. /. 10. -> "9/10"
  | x when x = 1. -> "1"
  | x -> string_of_float x

let rec printH_expr : type a. 'b -> a expr' -> unit =
  let open Format in
  fun f x ->
    match eval x with
    | Int i -> fprintf f "%i" i
    | Float i -> fprintf f "%g" i
    | Bool i -> fprintf f "%B" i
    | IntName s -> fprintf f "%s" s
    | FloatName s -> fprintf f "%s" s
    | BoolName s -> fprintf f "%s" s
    | Plus (e1, e2) -> fprintf f "(%a+%a)" printH_expr e1 printH_expr e2
    | Minus (e1, e2) -> fprintf f "(%a-%a)" printH_expr e1 printH_expr e2
    | Mult (e1, e2) -> fprintf f "(%a*%a)" printH_expr e1 printH_expr e2
    | Ceil e -> fprintf f "ceil(%a)" printH_expr e
    | Floor e -> fprintf f "floor(%a)" printH_expr e
    | Not e -> fprintf f "(!%a)" printH_expr e
    | And (e1, e2) -> fprintf f "(%a & %a)" printH_expr e1 printH_expr e2
    | Or (e1, e2) -> fprintf f "(%a | %a)" printH_expr e1 printH_expr e2
    | IntAtom (e1, c, e2) ->
        fprintf f "(%a %a %a)" printH_expr e1 printH_cmp c printH_expr e2
    | FloatAtom (e1, c, e2) ->
        fprintf f "(%a %a %a)" printH_expr e1 printH_cmp c printH_expr e2
    | BoolAtom (e1, c, e2) ->
        fprintf f "(%a %a %a)" printH_expr e1 printH_cmp c printH_expr e2
    | CastInt x -> printH_expr f x
    | CastBool x -> printH_expr f x
    | Div (e1, e2) -> fprintf f "(%a/%a)" printH_expr e1 printH_expr e2
    | Exp x -> fprintf f "exp(%a)" printH_expr x
    | FunCall (fu, []) -> fprintf f "%s()" fu
    | FunCall (fu, t :: q) ->
        fprintf f "%s(%a" fu printH_expr t;
        List.iter (fun x -> fprintf f ",%a" printH_expr x) q;
        fprintf f ")"
    | If (c, e1, e2) ->
        fprintf f "(%a ? %a : %a)" printH_expr c printH_expr e1 printH_expr e2

(*let to_string_expr e=
  Printf.sprintf "%a" printH_expr ees
*)

let print_full_expr o = function
  | BoolExpr b -> printH_expr o b
  | FloatExpr f -> printH_expr o f
  | IntExpr i -> printH_expr o i

exception FailToEvaluate

let eval_or_die :
    type a.
    ?iname:(string -> int expr' option) ->
    ?fname:(string -> float expr' option) ->
    ?bname:(string -> bool expr' option) ->
    a expr' ->
    a =
 fun ?iname:(iv = fun _ -> None) ?fname:(fv = fun _ -> None)
     ?bname:(bv = fun _ -> None) x ->
  match eval ~iname:iv ~fname:fv ~bname:bv x with
  | Int i -> i
  | Float f -> f
  | Bool b -> b
  | x ->
      printH_expr Format.err_formatter x;
      output_string stderr "\n";
      raise FailToEvaluate
