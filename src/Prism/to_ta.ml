(*

Copyright (C) 2019 LACL Universite Paris Est Creteil Val de Marne
Authors: Benoît Barbot
Licence: GPL-3.0-or-later

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*)

open PrismType
open Uppaal_parser.Uta
open Common

let filter_map f l =
  List.fold_left
    (fun acc v -> match f v with Some x -> x :: acc | None -> acc)
    [] l

let rec expression_of_expr e =
  let open Type in
  match e with
  | Int i -> Constant i
  | IntName n -> Variable n
  | Plus (x, y) -> Sum (expression_of_expr x, expression_of_expr y)
  | Minus (x, y) -> Subtraction (expression_of_expr x, expression_of_expr y)
  | Mult (x, y) -> Product (expression_of_expr x, expression_of_expr y)
  | Ceil fe ->
      let f = eval_or_die fe in
      Constant (int_of_float (ceil f))
  | Floor fe ->
      let f = eval_or_die fe in
      Constant (int_of_float (floor f))
  | _ ->
      Type.printH_expr Format.err_formatter e;
      invalid_arg "Fail to convert Prism expresssion to Uppaal ones"

let rec guard_of_boolexpr e =
  let eoe a = expression_of_expr a in
  let open Type in
  match e with
  | Bool true -> [ [] ]
  | IntAtom (lhs, EQ, rhs) -> [ [ GuardEqual (eoe lhs, eoe rhs) ] ]
  | IntAtom (lhs, SL, rhs) -> [ [ GuardLess (eoe lhs, eoe rhs) ] ]
  | IntAtom (lhs, LE, rhs) -> [ [ GuardLeq (eoe lhs, eoe rhs) ] ]
  | IntAtom (lhs, SG, rhs) -> [ [ GuardGreater (eoe lhs, eoe rhs) ] ]
  | IntAtom (lhs, GE, rhs) -> [ [ GuardGeq (eoe lhs, eoe rhs) ] ]
  | IntAtom (lhs, NEQ, rhs) -> [ [ GuardNEqual (eoe lhs, eoe rhs) ] ]
  | And (lhs, rhs) ->
      let lhs_r = guard_of_boolexpr lhs and rhs_r = guard_of_boolexpr rhs in
      List.fold_left
        (fun acc conj_l ->
          List.fold_left
            (fun acc2 conj_r -> (conj_l @ conj_r) :: acc2)
            acc rhs_r)
        [] lhs_r
  | Or (lhs, rhs) -> guard_of_boolexpr lhs @ guard_of_boolexpr rhs
  | _ -> invalid_arg "Fail to convert Prism Guard to Uppaal ones"

let guard_of_conj c =
  let open Guard in
  let open Type in
  c |> to_list
  |> List.map (function s, cmp, expr ->
         (let e = expression_of_expr expr in
          match cmp with
          | EQ -> GuardEqual (Variable s, e)
          | SL -> GuardLess (Variable s, e)
          | LE -> GuardLeq (Variable s, e)
          | SG -> GuardGreater (Variable s, e)
          | GE -> GuardGeq (Variable s, e)
          | _ -> invalid_arg "Fail to convert Prism Guard to Uppaal ones"))

let edge_from_rule ~add_dummy s (l, g, up) =
  let update_of_rule = function
    | [ (_, upl) ] ->
        let l =
          upl
          |> filter_map (function
               | str, IntUp it -> Some (str, expression_of_expr it)
               | _ -> None)
        in
        if add_dummy then ("dummy_clock", Constant 0) :: l else l
    | _ -> invalid_arg "PTA not suported "
  in

  let pr_of_rule = function
    | [ (p, _) ] -> Type.eval_or_die p
    | _ -> invalid_arg "PTA not suported "
  in

  {
    edgeSource = s.locId;
    edgeGuard = List.map guard_of_conj g;
    edgeUpdates = update_of_rule up;
    edgeUpdatesPos = (0, 0);
    edgeGuardPos = (0, 0);
    edgeSync = (l |>>> fun x -> SendChan x);
    edgeSyncPos = (0, 0);
    edgeTarget = s.locId;
    edgeNails = [];
    edgeProb = pr_of_rule up;
  }

let template_from_module m =
  let open PrismType in
  let uniqueLoc =
    {
      locId = "UID";
      locPos = (0, 0);
      locName = "UID";
      locNamePos = (0, 0);
      locInvar = (match m.invariant with [] -> [] | c :: _ -> guard_of_conj c);
      locInvarPos = (0, 0);
      locCommitted = false;
      locUrgent = false;
    }
  in

  let tempClocks =
    m.varlist |> filter_map (function ClockK x -> Some x | _ -> None)
  in

  let add_dummy =
    tempClocks <> []
    && List.exists
         (fun (_, _, l) ->
           List.exists
             (fun (_, r) ->
               List.for_all (fun (x, _) -> not (List.mem x tempClocks)) r)
             l)
         m.actionlist
  in
  {
    tempName = m.name;
    tempLocations = [ uniqueLoc ];
    tempInitialLocation = uniqueLoc;
    tempClocks = (if add_dummy then "dummy_clock" :: tempClocks else tempClocks);
    tempVars =
      m.varlist
      |> filter_map (function
           | IntK (s, _, v) -> Some (Var (s, Type.eval_or_die v))
           | _ -> None);
    tempEdges = List.map (edge_from_rule ~add_dummy uniqueLoc) m.actionlist;
  }

let ta_from_prism (t : PrismType.prism_file) =
  let open Symrob_extract.TimedAutomaton.Uautomaton in
  let sys =
    {
      sysTemplates =
        t.modlist
        |> filter_map (function
             | Full m -> Some (template_from_module m)
             | _ -> None);
      sysProcessInstances = [];
      sysVars = [];
      sysClocks = [];
      sysChans = [];
      sysQuery = EmptyQuery;
    }
  in
  make_ta sys
    (List.map (fun (s, g) -> (s, (guard_of_boolexpr g, [ 0 ]))) t.labels)
(*make_ta
  (guard_of_transition, invariant_of_discrete_state)
  sys 1 0
  (List.map (fun (s, g) -> (s, guard_of_boolexpr g)) t.labels)*)
