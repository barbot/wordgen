module type S = sig
  include Weight_structure.Polynomial.WeightForSampling
  module Bt : ZoneGraphInput.BoundType

  val desc : string

  val compute :
    ?update:(unit -> unit) ->
    ?finish:((t, Bt.t) ZoneGraph.t -> unit) ->
    ?print_interm:string ->
    ('a, Bt.t) ZoneGraph.t ->
    int ->
    bool ->
    (t, Bt.t) ZoneGraph.t

  val compute_params :
    ?verbose:int ->
    ?seed:int ->
    float option * float option ->
    (float Sampling.transChoice * string Sampling.transChoice) array ->
    (t, Bt.t) ZoneGraph.t ->
    (float * float * float) option
    * (float * (float * float * float) * float) option
end

module Make
    (Bt : ZoneGraphInput.BoundType)
    (P : Weight_structure.Polynomial.WeightForSampling) :
  S
    with type t = P.t
     and type var = P.var
     and type Bt.t = Bt.t
     and type Bt.state = Bt.state
