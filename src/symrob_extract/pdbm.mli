module type ALGEBRA = sig
  type t

  val hash : t -> int
  val add : t -> t -> t
  val sub : t -> t -> t
  val dual : t -> t
  val min : t -> t -> t
  val max : t -> t -> t
  val compare : t -> t -> int
  val infty : t
  val zero : t
  val is_zero : t -> bool
  val is_infty : t -> bool
  val to_string : t -> string
  val print : Format.formatter -> t -> unit
  val compare_int : t -> int -> int
  val weak_from_int : int -> t
  val to_int : t -> int
  val strict_from_int : int -> t
  val scale : t -> int -> t
  val opening : t -> t
  val strengthening : t -> t
end

module type GENERIC_DBM = sig
  module Alg : ALGEBRA

  type t
  type clock = Symrob_common.ClockSet.t

  val elementAt : t -> int -> int -> Alg.t
  val copy : t -> t
  val hash : t -> int
  val create : int -> t
  val dimension : t -> int
  val is_empty : t -> bool
  val punctual : t -> bool
  val reduce : t -> unit

  (*val set_constraint : t -> int -> int -> Alg.t -> unit*)
  val constrain : t -> int -> int -> Alg.t -> unit
  val assign_constraint : t -> int -> int -> Alg.t -> unit
  val to_string : t -> string
  val print : Format.formatter -> t -> unit

  val to_tikz :
    ?grid:bool ->
    ?scope:string ->
    string Uppaal_parser.Uta.VarContext.t ->
    ?z:int ->
    int * int ->
    Format.formatter ->
    t ->
    unit

  val to_svg :
    ?grid:bool ->
    ?scope:string ->
    string Uppaal_parser.Uta.VarContext.t ->
    int array ->
    int * int ->
    Format.formatter ->
    t ->
    unit

  val intersect : t -> t -> unit
  val pretime : t -> unit
  val posttime : t -> unit
  val free : t -> clock -> unit
  val unreset : t -> clock -> unit
  val reset : t -> clock -> unit
  val leq : t -> t -> bool
  val equal : t -> t -> bool
  val closure_leq : int array -> t -> t -> bool
  val string_of_var : string Uppaal_parser.Uta.VarContext.t -> int -> string
  val extrapolate_lu : t -> int array -> int array -> unit

  val pretty_print :
    ?do_escape_tex:bool ->
    ?minimalist:bool ->
    ?context:string Uppaal_parser.Uta.VarContext.t ->
    Format.formatter ->
    t ->
    unit

  val is_rectangular : t -> bool
  val from_flat : (int * Symrob_common.dbm_inequality_type) array -> int -> t
  val opening : t -> unit
  val max_bound : t -> int
  val max_bounds : t -> int array
  val cClosure : t -> int -> unit
  val minus : t -> t -> t list
  val is_splitted : t -> bool
  val get_lower_upper : int array -> t -> Common.bound Common.guard

  val get_lower_upper_no_split :
    int array -> t -> Common.bound list Common.guard

  val remove_useless : t -> unit
  val split_guard : t -> t list * int list
  val get_reduced_coordinate : t -> int list list
end

exception EmptyDBM
exception Not_split

module SplitableDBM : GENERIC_DBM
module Guard = SplitableDBM
