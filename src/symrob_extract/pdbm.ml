open Symrob_common

module type ALGEBRA = sig
  type t

  val hash : t -> int
  val add : t -> t -> t
  val sub : t -> t -> t
  val dual : t -> t
  val min : t -> t -> t
  val max : t -> t -> t
  val compare : t -> t -> int
  val infty : t
  val zero : t
  val is_zero : t -> bool
  val is_infty : t -> bool
  val to_string : t -> string
  val print : Format.formatter -> t -> unit
  val compare_int : t -> int -> int

  val weak_from_int : int -> t
  (** Get a weak constraint from given int; e.g. for the classic algebra,
      [weak_from_int v] yields (v,DBM_WEAK) *)

  val to_int : t -> int
  val strict_from_int : int -> t
  val scale : t -> int -> t
  val opening : t -> t
  val strengthening : t -> t
end

module type GENERIC_DBM = sig
  module Alg : ALGEBRA

  type t
  type clock = ClockSet.t

  val elementAt : t -> int -> int -> Alg.t
  val copy : t -> t
  val hash : t -> int
  val create : int -> t
  val dimension : t -> int
  val is_empty : t -> bool
  val punctual : t -> bool
  val reduce : t -> unit

  (*val set_constraint : t -> int -> int -> Alg.t -> unit*)
  val constrain : t -> int -> int -> Alg.t -> unit
  val assign_constraint : t -> int -> int -> Alg.t -> unit
  val to_string : t -> string
  val print : Format.formatter -> t -> unit

  val to_tikz :
    ?grid:bool ->
    ?scope:string ->
    string Uppaal_parser.Uta.VarContext.t ->
    ?z:int ->
    int * int ->
    Format.formatter ->
    t ->
    unit

  val to_svg :
    ?grid:bool ->
    ?scope:string ->
    string Uppaal_parser.Uta.VarContext.t ->
    int array ->
    int * int ->
    Format.formatter ->
    t ->
    unit

  val intersect : t -> t -> unit
  val pretime : t -> unit
  val posttime : t -> unit
  val free : t -> ClockSet.t -> unit
  val unreset : t -> ClockSet.t -> unit
  val reset : t -> ClockSet.t -> unit
  val leq : t -> t -> bool
  val equal : t -> t -> bool
  val closure_leq : int array -> t -> t -> bool
  val string_of_var : string Uppaal_parser.Uta.VarContext.t -> int -> string
  val extrapolate_lu : t -> int array -> int array -> unit

  val pretty_print :
    ?do_escape_tex:bool ->
    ?minimalist:bool ->
    ?context:string Uppaal_parser.Uta.VarContext.t ->
    Format.formatter ->
    t ->
    unit

  val is_rectangular : t -> bool

  val from_flat : (int * dbm_inequality_type) array -> int -> t
  (** From array representation of guards, and given dimension, return a dbm *)

  val opening : t -> unit
  val max_bound : t -> int
  val max_bounds : t -> int array
  val cClosure : t -> int -> unit

  val minus : t -> t -> t list
  (** minus d1 d2 compute a set of dbm whose union is equal to d1\d2*)

  val is_splitted : t -> bool
  val get_lower_upper : int array -> t -> Common.bound Common.guard

  val get_lower_upper_no_split :
    int array -> t -> Common.bound list Common.guard

  val remove_useless : t -> unit
  val split_guard : t -> t list * int list
  val get_reduced_coordinate : t -> int list list
end

exception EmptyDBM

exception Not_split
(** All DBM operations raise this exception if the resulting DBM is empty *)

module GenericDBM =
functor
  (Alg : ALGEBRA)
  ->
  struct
    type t = Alg.t array array

    (** Create a DBM with no constraints. The dimension is the number of clocks plus one *)
    let create dimension =
      let result = Array.make dimension (Array.make dimension Alg.infty) in
      for i = 0 to dimension - 1 do
        result.(i) <- Array.make dimension Alg.infty
      done;
      for i = 0 to dimension - 1 do
        result.(i).(i) <- Alg.zero;
        result.(0).(i) <- Alg.zero
      done;
      result

    let elementAt t i j = t.(i).(j)
    let dimension mdbm = Array.length mdbm

    let hash mdbm =
      let h = ref 0 in
      let ithash y = h := (31 * !h) + Alg.hash y in

      Array.iter (fun x -> Array.iter (fun y -> ithash y) x) mdbm;
      !h

    let to_string mdbm =
      let dim = dimension mdbm in
      let b = Buffer.create (dim * dim * 20) in
      for i = 0 to dim - 1 do
        for j = 0 to dim - 1 do
          Printf.bprintf b "%s\t" (Alg.to_string mdbm.(i).(j))
        done;
        Printf.bprintf b "\\\n"
      done;
      Buffer.contents b

    let print o mdbm =
      let dim = dimension mdbm in
      for i = 0 to dim - 1 do
        for j = 0 to dim - 1 do
          Format.fprintf o "%s\t" (Alg.to_string mdbm.(i).(j))
        done;
        Format.fprintf o "\n"
      done

    let copy t =
      let nt0 = Array.make (dimension t) Alg.zero in
      let nt = Array.make (dimension t) nt0 in
      let dim = dimension t in
      for i = 0 to dim - 1 do
        nt.(i) <- Array.copy t.(i)
      done;
      nt

    let is_empty mdbm =
      (* Assert reduce*)
      let dimension = dimension mdbm in

      let rec aux i =
        if i = dimension then false
        else if Alg.compare Alg.zero mdbm.(i).(i) = 1 then true
        else aux (i + 1)
      in
      aux 0

    let max_bound mdbm =
      let dim = dimension mdbm in
      let m = ref 0 in
      for i = 0 to dim - 1 do
        for j = 0 to dim - 1 do
          if not @@ Alg.is_infty mdbm.(i).(j) then
            m := max !m (abs @@ Alg.to_int mdbm.(i).(j))
        done
      done;
      !m

    let reduce mdbm =
      let dim = dimension mdbm in
      for k = 0 to dim - 1 do
        for i = 0 to dim - 1 do
          for j = 0 to dim - 1 do
            let sum = Alg.add mdbm.(i).(k) mdbm.(k).(j) in
            mdbm.(i).(j) <- Alg.min mdbm.(i).(j) sum
          done;
          (*raise as soon as possible*)
          if Alg.compare mdbm.(i).(i) Alg.zero = -1 then raise_notrace EmptyDBM
        done
      done

    let max_bounds mdbm =
      let dim = dimension mdbm in
      let m2 = copy mdbm in
      reduce m2;
      let m = Array.make dim 0 in
      for i = 1 to dim - 1 do
        if not @@ Alg.is_infty m2.(i).(0) then
          m.(i) <- max (abs @@ Alg.to_int m2.(0).(i)) (abs @@ Alg.to_int m2.(i).(0))
      done;
      m

    (* Specialization of Floyd's shortest path algorithm
             * when a constraint dbm[b,a] is tightened (easier
             * to have b,a: see Rokicki's thesis p 171).
              * Algorithm:
       *
       * for j in 0..dim-1 do
       *   if dbm[b,j] > dbm[b,a]+dbm[a,j] then
       *      dbm[b,j] = dbm[b,a]+dbm[a,j]
       *   endif
       * done
       *
       * for i in 0..dim-1 do
       *   if dbm[i,a] > dbm[i,b]+dbm[b,a] then
       *      dbm[i,a]=dbm[i,b]+dbm[b,a]
       *      for j in 0..dim-1 do
       *        if dbm[i,j] > dbm[i,a]+dbm[a,j] then
       *           dbm[i,j]=dbm[i,a]+dbm[a,j]
       *        endif
       *      done
       *   endif
       * done
    *)
    let reduce_ij mdbm b a =
      let dim = dimension mdbm in
      for j = 0 to dim - 1 do
        let sum = Alg.add mdbm.(b).(a) mdbm.(a).(j) in
        mdbm.(b).(j) <- Alg.min mdbm.(b).(j) sum
      done;
      for i = 0 to dim - 1 do
        let sum = Alg.add mdbm.(i).(b) mdbm.(b).(a) in
        if Alg.compare mdbm.(i).(a) sum > 0 then (
          mdbm.(i).(a) <- sum;
          for j = 0 to dim - 1 do
            let sum = Alg.add mdbm.(i).(a) mdbm.(a).(j) in
            mdbm.(i).(j) <- Alg.min mdbm.(i).(j) sum
          done);
        if Alg.compare mdbm.(i).(i) Alg.zero = -1 then raise_notrace EmptyDBM
      done

    let cClosure mdbm m =
      if not @@ is_empty mdbm then
        let dim = dimension mdbm in
        for i = 0 to dim - 1 do
          for j = 0 to dim - 1 do
            let v = mdbm.(i).(j) in
            if (not @@ Alg.is_infty v) && m < Alg.to_int v then
              mdbm.(i).(j) <- Alg.infty
            else if (not @@ Alg.is_infty v) && Alg.to_int v < -m then
              mdbm.(i).(j) <- Alg.strict_from_int (-m)
          done
        done

    (*
    (** The same as reduce but does not raise an exception on empty DBM *)
    let silent_reduce mdbm =
      let dim = dimension mdbm in
      for i = 0 to dim - 1 do
        for j = 0 to dim - 1 do
          for k = 0 to dim - 1 do
            mdbm.(i).(j) <-
              Alg.min mdbm.(i).(j) (Alg.add mdbm.(i).(k) mdbm.(k).(j))
          done
        done
      done
*)

    (** Set constraint but do not reduce *)
    let assign_constraint mdbm x y k = mdbm.(x).(y) <- k

    (** Set the constraint of mdbm on component (x,y) to k *)
    let set_constraint mdbm x y k =
      let k = if x = 0 then Alg.min Alg.zero k else k in
      let dim = dimension mdbm in
      if x < 0 || x >= dim || y < 0 || y >= dim then
        raise
          (Failure
             (Printf.sprintf
                "Clock index out of bounds: %d or %d; dimension is %d" x y dim));
      mdbm.(x).(y) <- k
    (*;
      reduce mdbm*)

    (** *Add* to mdbm the constraint k on component (x,y) (intuitively x - y <= k).
        This is equivalent to intersecting m1 with this constraint. *)
    let constrain mdbm x y k =
      (*Format.printf "constrain x%i -x%i %a old is %a\n" x y (Alg.print) k (Alg.print) mdbm.(x).(y);*)
      let dim = dimension mdbm in
      if x < 0 || x >= dim || y < 0 || y >= dim then
        raise
          (Failure
             (Printf.sprintf
                "Clock index out of bounds: %d or %d; dimension is %d" x y dim));
      set_constraint mdbm x y (Alg.min mdbm.(x).(y) k)
    (*;
      reduce mdbm*)

    (** m1 is modified to contain the DBM m1&m2 *)
    let intersect m1 m2 =
      let dim = dimension m1 in
      if dimension m2 <> dim then
        raise (Failure "Intersection of DBMs with different dimensions");
      for i = 0 to dim - 1 do
        for j = 0 to dim - 1 do
          m1.(i).(j) <- Alg.min m1.(i).(j) m2.(i).(j)
        done
      done
    (*;reduce m1*)

    (** Time predecessors a.k.a. down operation *)
    let pretime m1 =
      let dim = dimension m1 in
      for i = 0 to dim - 1 do
        m1.(0).(i) <- Alg.zero
      done
    (*;
      reduce m1*)

    (** Time successors a.k.a. up operation *)
    let posttime m1 =
      let dim = dimension m1 in
      for i = 1 to dim - 1 do
        m1.(i).(0) <- Alg.infty
      done

    (* Note that the result is already normalized *)

    (** Free all constraints on clocks clocks *)
    let free m1 clocks =
      let dim = dimension m1 in
      for i = 0 to dim - 1 do
        if ClockSet.mem i clocks then (
          m1.(0).(i) <- Alg.zero;
          m1.(i).(0) <- Alg.infty)
      done;
      for i = 0 to dim - 1 do
        for j = 0 to dim - 1 do
          if
            i <> j && i <> 0 && j <> 0
            && (ClockSet.mem i clocks || ClockSet.mem j clocks)
          then m1.(i).(j) <- Alg.infty
        done
      done
    (*;reduce m1*)

    (** The reverse of the reset(clocks) operation: intersect with the guard
     * clocks=0, and free the clocks *)
    let unreset m1 clocks =
      let dim = dimension m1 in
      let reset = create dim in
      ClockSet.iter
        (fun c ->
          set_constraint reset c 0 Alg.zero;
          set_constraint reset c 0 Alg.zero)
        clocks;
      intersect m1 reset;
      free m1 clocks

    let reset m1 clocks =
      free m1 clocks;
      ClockSet.iter (fun c -> m1.(c).(0) <- Alg.zero) clocks
    (*;reduce m1*)

    (** Inclusion check: returns true iff m1 is included in m2*)
    let leq m1 m2 =
      let dim = dimension m1 in
      if dimension m2 <> dim then
        raise (Failure "Comparison of DBMs with different dimensions");
      try
        Array.iteri
          (fun i m1row ->
            Array.iteri
              (fun j a -> if Alg.compare a m2.(i).(j) > 0 then raise Found)
              m1row)
          m1;
        true
      with
      | Found -> false
      | _ as a -> raise a

    let equal m1 m2 =
      let dim = dimension m1 in
      if dimension m2 <> dim then
        raise (Failure "Comparison of DBMs with different dimensions");
      try
        for i = 0 to dim - 1 do
          for j = 0 to dim - 1 do
            if Alg.compare m1.(i).(j) m2.(i).(j) <> 0 then raise Found
          done
        done;
        true
      with
      | Found -> false
      | _ as a -> raise a

    (*let constrainByName context mdbm x y k =
      mdbm.(Uppaal_parser.Uta.VarContext.var2index context x).(Uppaal_parser.Uta
                                                               .VarContext
                                                               .var2index
                                                                 context y) <-
        Alg.min
          mdbm.(Uppaal_parser.Uta.VarContext.var2index context x).(Uppaal_parser
                                                                   .Uta
                                                                   .VarContext
                                                                   .var2index
                                                                     context y)
          k;
      reduce mdbm*)

    let is_rectangular mdbm =
      let dim = dimension mdbm in
      let o = create dim in
      for i = 0 to dim - 1 do
        o.(i).(0) <- mdbm.(i).(0);
        o.(0).(i) <- mdbm.(0).(i)
      done;
      reduce o;
      if equal o mdbm then true else false

    (*let bounded_universe lu_bounds =
      let alpha_bounds = Array.map (fun (l, u) -> max l u) lu_bounds in
      let largest_constant = Array.fold_left ( + ) 0 alpha_bounds in
      (* The global invariant that bounds the clocks, given the largets constants that appear *)
      let dim = Array.length lu_bounds in
      let z = create dim in
      for i = 0 to dim - 1 do
        for j = 0 to dim - 1 do
          constrain z i j (Alg.weak_from_int largest_constant)
        done
      done;
      z*)

    let opening mdbm =
      let dim = dimension mdbm in
      for i = 0 to dim - 1 do
        for j = 0 to dim - 1 do
          mdbm.(i).(j) <- Alg.opening mdbm.(i).(j)
        done
      done
  end

module ClassicDBMAlgebra = struct
  type t = int * dbm_inequality_type

  let infty_val = max_int
  let hash (a, _) = a
  let infty = (infty_val, DBM_STRICT)
  let to_int (i, _) = i
  let weak_from_int i = (i, DBM_WEAK)
  let strict_from_int i = (i, DBM_STRICT)
  let scale (n, ineq) s = ((if n = infty_val then infty_val else n * s), ineq)

  let add a b =
    match (a, b) with
    | (a, _), (b, _) when a = infty_val || b = infty_val -> infty
    | (k, tk), (l, tl) when tk = DBM_STRICT || tl = DBM_STRICT ->
        (k + l, DBM_STRICT)
    | (k, _), (l, _) -> (k + l, DBM_WEAK)

  let sub a b =
    match (a, b) with
    | (a, _), (b, _) when a = infty_val || b = infty_val -> infty
    | (k, tk), (l, tl) when tk = DBM_STRICT || tl = DBM_STRICT ->
        (k - l, DBM_STRICT)
    | (k, _), (l, _) -> (k - l, DBM_WEAK)

  let dual a =
    match a with
    | x, _ when x = infty_val -> a
    | x, DBM_STRICT -> (-x, DBM_WEAK)
    | x, DBM_WEAK -> (-x, DBM_STRICT)

  let compare a b =
    match (a, b) with
    | (k, _), (l, _) when l = infty_val && k = infty_val -> 0
    | (k, _), _ when k = infty_val -> 1
    | _, (l, _) when l = infty_val -> -1
    | (k, _), (l, _) when k < l -> -1
    | (k, _), (l, _) when k > l -> 1
    | (_, ink), (_, inl) -> compare_inequality_type ink inl

  let compare_int a bint = compare a (bint, DBM_WEAK)
  let min a b = match compare a b with 0 | -1 -> a | _ -> b
  let max a b = match compare a b with 0 | -1 -> b | _ -> a
  let zero = (0, DBM_WEAK)
  let is_zero a = a = zero
  let is_infty (a, _) = a = infty_val

  (* this is faster *)

  let to_string = function
    | a when a = infty -> "<INF"
    | a, DBM_WEAK -> "<=" ^ string_of_int a
    | a, DBM_STRICT -> "<" ^ string_of_int a

  let print o = function
    | a when a = infty -> Format.fprintf o "<INF"
    | a, DBM_WEAK -> Format.fprintf o "<=%i" a
    | a, DBM_STRICT -> Format.fprintf o "<%i" a

  let opening (i, _) = (i, DBM_WEAK)
  let strengthening (i, _) = (i, DBM_STRICT)
end

module ClassicDBM = struct
  module Alg = ClassicDBMAlgebra
  include GenericDBM (ClassicDBMAlgebra)

  exception Punctual

  (* (** Create an empty dbm *)
     let empty dimension =
       let t = create dimension in
       t.(0).(0) <- (-1, DBM_WEAK);
       t*)

  (** Checks if the dbm contains an equality constraint *)
  let punctual mdbm =
    let dim = dimension mdbm in
    try
      for i = 1 to dim - 1 do
        match (mdbm.(i).(0), mdbm.(0).(i)) with
        | a, _ when Alg.is_infty a -> ()
        | _, b when Alg.is_infty b -> ()
        | (a, _), (b, _) when a = -b -> raise Punctual
        | _ -> ()
      done;
      false
    with
    | Punctual -> true
    | _ as a -> raise a

  (** Diagonal LU Bounds extrapolation *)
  let extrapolate_lu guard lbounds ubounds =
    let dim = dimension guard in
    (*
     for i = 1 to dim - 1 do
     printf "Clock %d, L: %d, U: %d\n" i lbounds.(i) ubounds.(i);
     done;
     *)
    let to_number = fst in
    for i = 1 to dim - 1 do
      let infij = to_number guard.(0).(i) < -lbounds.(i) in
      for j = 0 to dim - 1 do
        if i = j then ()
        else
          let bound = to_number guard.(i).(j) in
          if
            infij
            || bound > lbounds.(i)
            || to_number guard.(0).(j) < -ubounds.(j)
          then guard.(i).(j) <- Alg.infty
      done
    done;
    (* 1st row *)
    for j = 1 to dim - 1 do
      if to_number guard.(0).(j) < -ubounds.(j) then
        guard.(0).(j) <-
          (if ubounds.(j) >= 0 then (-ubounds.(j), DBM_STRICT) else Alg.zero)
    done;
    reduce guard

  let from_flat boundar dim =
    let g = create dim in
    for i = 0 to dim - 1 do
      for j = 0 to dim - 1 do
        g.(i).(j) <- boundar.((i * dim) + j)
      done
    done;
    g

  (** [closure_leq alpha m1 m2] Check whether m1 \not\subseteq closure_alpha(m2) *)
  let closure_leq alpha m1 m2 =
    let dim = dimension m1 in
    try
      for x = 1 to dim - 1 do
        if
          Alg.compare m2.(x).(0) m1.(x).(0) = -1
          && Alg.compare_int m2.(x).(0) alpha.(x) <= 0
        then raise Found
        else if
          Alg.compare m2.(0).(x) m1.(0).(x) = -1
          && Alg.compare_int m1.(0).(x) (-alpha.(x)) >= 0
        then raise Found;
        for y = 1 to dim - 1 do
          if
            x <> y
            && Alg.compare_int m1.(0).(x) (-alpha.(x)) >= 0
            && Alg.compare m2.(y).(x) m1.(y).(x) = -1
          then
            let aym0x = (alpha.(y), DBM_WEAK) in
            if Alg.compare m2.(y).(x) (Alg.add aym0x m1.(0).(x)) <= 0 then
              raise Found
        done
      done;
      true
    with Found -> false

  let string_of_var context i =
    let s = Uppaal_parser.Uta.VarContext.index2var context i in
    try
      let i = String.rindex s '.' in
      String.sub s (i + 1) (String.length s - i - 1)
    with Not_found -> s

  let is_useless mdbm i b =
    let dim = dimension mdbm in
    (*Printf.printf "check usefullness %i %b\n%s" i b (to_string mdbm);*)
    try
      if b then
        for j = 1 to dim - 1 do
          (*Printf.printf "(%i,%i,%s,%s -> %i)" i j (Alg.to_string mdbm.(i).(0)) (Alg.to_string (Alg.add mdbm.(i).(j) mdbm.(j).(0))) (Alg.compare mdbm.(i).(0) (Alg.add mdbm.(i).(j) mdbm.(j).(0)));*)
          if
            j != i
            && Alg.compare mdbm.(i).(0) (Alg.add mdbm.(i).(j) mdbm.(j).(0)) >= 0
          then raise_notrace Exit
        done
      else
        for j = 1 to dim - 1 do
          if
            j != i
            && Alg.compare mdbm.(0).(i) (Alg.add mdbm.(0).(j) mdbm.(j).(i)) >= 0
          then raise_notrace Exit
        done;
      (*print_endline ("useless"^(string_of_int i));*)
      false
    with Exit -> true

  let remove_useless mdbm =
    let dim = dimension mdbm in
    (*reduce mdbm;*)
    for i = 0 to dim - 1 do
      if is_useless mdbm i true then mdbm.(i).(0) <- Alg.infty;
      if is_useless mdbm i false then mdbm.(0).(i) <- Alg.infty
    done

  let pretty_print ?(do_escape_tex = false) ?(minimalist = true) ?context chan
      omdbm =
    let mdbm = copy omdbm in
    if minimalist then (
      reduce mdbm;
      remove_useless mdbm)
    else reduce mdbm;
    let string_of_var i =
      match context with
      | Some x ->
          if do_escape_tex then escape_tex @@ string_of_var x i
          else string_of_var x i
      | None -> "c_" ^ string_of_int i
    in

    let dimension = dimension mdbm in
    let outputlist = Queue.create () in
    let _print i j =
      let m, ineq = mdbm.(i).(j) in
      if
        i <> 0 && j <> 0
        && (Alg.compare mdbm.(i).(0) Alg.zero = 0
           || Alg.compare mdbm.(j).(0) Alg.zero = 0)
      then ()
      else if mdbm.(i).(j) = Alg.zero && mdbm.(j).(i) = Alg.zero then (
        if i < j then
          Queue.add
            (Printf.sprintf "%s=%s" (string_of_var i) (string_of_var j))
            outputlist)
      else if j = 0 then
        Queue.add
          (Printf.sprintf "%s %s %s" (string_of_var i) (string_of_ineq ineq)
             (string_of_int m))
          outputlist
      else if i = 0 then
        Queue.add
          (Printf.sprintf "%s %s %s" (string_of_int (-m)) (string_of_ineq ineq)
             (string_of_var j))
          outputlist
      else if i < j && not (Alg.is_infty mdbm.(j).(i)) then
        let m2, ineq2 = mdbm.(j).(i) in
        Queue.add
          (Printf.sprintf "%s%s %s-%s %s%s" (string_of_int (-m2))
             (string_of_ineq ineq2) (string_of_var i) (string_of_var j)
             (string_of_ineq ineq) (string_of_int m))
          outputlist
    in
    let open_paranthesis_of_ineq = function
      | DBM_WEAK -> "["
      | DBM_STRICT -> "("
    in
    let closed_paranthesis_of_ineq = function
      | DBM_WEAK -> "]"
      | DBM_STRICT -> ")"
    in
    if is_empty mdbm then Format.fprintf chan "false"
    else if mdbm = create dimension then Format.fprintf chan "true";
    for i = 1 to dimension - 1 do
      let mi0, ineqi0 = mdbm.(i).(0) in
      let m0i, ineq0i = mdbm.(0).(i) in
      let string_neg_int = function
        | a when Alg.is_infty (a, DBM_STRICT) -> "-Infty"
        | a -> string_of_int (-a)
      and string_infty_int = function
        | a when Alg.is_infty (a, DBM_STRICT) -> "Infty"
        | a -> string_of_int a
      in
      if Alg.is_infty mdbm.(i).(0) && Alg.is_infty mdbm.(0).(i) then ()
      else if mi0 = 0 && m0i = 0 then
        Queue.add (Printf.sprintf "%s=0" (string_of_var i)) outputlist
      else if
        (not (Alg.is_infty mdbm.(i).(0)))
        (*m0i <> 0 &&*) || (not (Alg.is_infty mdbm.(0).(i)))
        || ineq0i <> DBM_WEAK
      then
        if Alg.is_infty mdbm.(0).(i) then
          Queue.add
            (Printf.sprintf "%s %s %i" (string_of_var i)
               (if ineqi0 = DBM_WEAK then "<=" else "<")
               mi0)
            outputlist
        else if Alg.is_infty mdbm.(i).(0) then
          Queue.add
            (Printf.sprintf "%s %s %i" (string_of_var i)
               (if ineq0i = DBM_WEAK then ">=" else ">")
               (-m0i))
            outputlist
        else
          Queue.add
            (Printf.sprintf "%s in %s%s,%s%s" (string_of_var i)
               (open_paranthesis_of_ineq ineq0i)
               (string_neg_int m0i) (string_infty_int mi0)
               (closed_paranthesis_of_ineq ineqi0))
            outputlist
    done;
    if not minimalist then
      for i = 0 to dimension - 1 do
        for j = 0 to dimension - 1 do
          if i = 0 || j = 0 || i = j || Alg.is_infty mdbm.(i).(j) then
            ( (*continue*) )
          else _print i j
        done
      done;
    let hasPre = ref false in
    Queue.iter
      (fun s ->
        if s <> "" && s <> "\n" then (
          if !hasPre then (
            Format.fprintf chan ";@ ";
            hasPre := false);
          Format.fprintf chan "%s" s;
          hasPre := true))
      outputlist

  (*let pretty_print_uppaal chan context mdbm =
    let uppaal_string_of_ineq = function
      | DBM_WEAK -> "&lt;="
      | DBM_STRICT -> "&lt;"
    in
    _pretty_print chan context uppaal_string_of_ineq mdbm*)
end

module SplitableDBM = struct
  include ClassicDBM

  type clock = ClockSet.t

  let rec add_dbm_list m l =
    let l2, b2 = add_dbm_list_aux m l in
    if b2 then m :: l2 else l2

  and add_dbm_list_aux m = function
    | [] -> ([], true)
    (* t subset of m, discard t*)
    | t :: q when leq t m -> (fst @@ add_dbm_list_aux m q, true)
    (* m subset of t, discard m*)
    | t :: q when leq m t -> (t :: q, false)
    (* m and t incomparable *)
    | t :: q ->
        (*Format.eprintf "Incomparable !!@,";
          (try
             let inter = copy m in
             intersect inter t;
             reduce inter;
             Format.eprintf "Not empty !!@.%a inter@.%a =@.%a@."
               (fun f -> pretty_print ~minimalist:false f)
               t
               (fun f -> pretty_print ~minimalist:false f)
               m
               (fun f -> pretty_print ~minimalist:false f)
               inter
           with EmptyDBM -> ());*)
        let l2, b2 = add_dbm_list_aux m q in
        (t :: l2, b2)
  (*Format.eprintf "Incomparable !!@ ";
    let inter = copy m in
    intersect inter t;
    reduce inter;
    let m4 = minus m inter in
    let l4 = List.fold_left (fun acc v -> add_dbm_list v acc) (t :: q) m4 in
    (l4, false)*)

  and minus m1 m2 =
    (*Format.eprintf "minus (%a) (%a) ->@ "
      (fun f x -> pretty_print f x)
      m1
      (fun f x -> pretty_print f x)
      m2;*)
    (*Format.eprintf "New sub@.";*)
    if is_empty m2 then [ m1 ]
    else if is_empty m1 then []
    else
      let l = ref [] in
      let k = copy m1 in
      let dim = dimension m1 in
      (*reduce m1;*)
      for i = 0 to dim - 1 do
        for j = 0 to dim - 1 do
          if i <> j && not (Alg.is_infty m2.(i).(j)) then (
            let m3 = copy k in
            constrain k i j (Alg.opening m2.(i).(j));
            set_constraint m3 j i (Alg.strengthening @@ Alg.dual @@ m2.(i).(j));
            (try
               reduce_ij m3 j i;
               l := add_dbm_list m3 !l
             with EmptyDBM -> ());
            try reduce_ij k i j with EmptyDBM -> ())
        done
      done;
      (*assert (
        List.for_all
          (fun v1 ->
            List.for_all
              (fun v2 ->
                if v1 <> v2 then (
                  let vint = copy v1 in
                  intersect vint v2;
                  is_empty vint)
                else true)
              !l)
          !l);*)
      (*Format.eprintf "[%a]@." (Format.pp_print_list pretty_print) !l;*)
      !l

  (*let to_tikz2 ?grid ?scope context ?z (x, y) f dbm2 =
    let string_of_var i =
      let s = Uppaal_parser.Uta.VarContext.index2var context i in
      try
        let i = String.rindex s '.' in
        String.sub s (i + 1) (String.length s - i - 1)
      with Not_found -> s
    in

    let dbm = copy dbm2 in
    Format.fprintf f "%% x axis = %s; y axis = %s@;" (string_of_var x)
      (string_of_var y);
    (*Format.fprintf f "%%%a@;" (fun f -> pretty_print f context) dbm;*)
    let b = 7 in
    let sep = 3.5 +. Random.float 1.0 in
    let atoi i j = Alg.to_int dbm.(i).(j) in
    if dbm.(x).(0) = dbm.(0).(x) && (not @@ Alg.is_infty dbm.(x).(0)) then
      Format.fprintf f "\\draw[thick] (%i,0) -- (%i,%i);@." (atoi x 0)
        (atoi x 0) b
    else (
      if not @@ Alg.is_infty dbm.(x).(0) then
        Format.fprintf f
          "\\draw[decorate,decoration={coil,segment length=%fpt}] (%i,0) -- \
           (%i,%i);@;"
          sep (atoi x 0) (atoi x 0) b;
      if not @@ Alg.is_infty dbm.(0).(x) then
        Format.fprintf f
          "\\draw[decorate,decoration={coil,segment length=%fpt}] (%i,%i) -- \
           (%i,0);@;"
          sep (atoi 0 x) b (atoi 0 x));
    if dbm.(y).(0) = dbm.(0).(y) && (not @@ Alg.is_infty dbm.(y).(0)) then
      Format.fprintf f "\\draw[thick] (%i,%i) -- (0,%i);@;" b (atoi y 0)
        (atoi y 0)
    else (
      if not @@ Alg.is_infty dbm.(y).(0) then
        Format.fprintf f
          "\\draw[decorate,decoration={coil,segment length=%fpt}] (%i,%i) -- \
           (0,%i);@;"
          sep b (atoi y 0) (atoi y 0);
      if not @@ Alg.is_infty dbm.(0).(y) then
        Format.fprintf f
          "\\draw[decorate,decoration={coil,segment length=%fpt}] (0,%i) -- \
           (%i,%i);@;"
          sep
          (-atoi 0 y)
          b
          (-atoi 0 y));
    let a1 = atoi y x in
    if not @@ Alg.is_infty dbm.(y).(x) then
      Format.fprintf f
        "\\draw[decorate,decoration={coil,segment length=%fpt}] (%i,%i) -- \
         (0,%i);@;"
        sep b (b + a1) a1;
    let a2 = atoi x y in
    if not @@ Alg.is_infty dbm.(x).(y) then
      Format.fprintf f
        "\\draw[decorate,decoration={coil,segment length=%fpt}] (0,%i) -- \
         (%i,%i);@;"
        sep (-a2) b (b - a2);
    ()*)

  (* try to print dbm of dimension 3 *)
  let order3 a b c =
    match
      match
        List.sort (fun (x, y) _ -> if x = 0 || y = 0 then -1 else 1) [ a; b; c ]
      with
      | [ (0, a); b; c ] ->
          (0, a)
          :: List.sort
               (fun (x, y) _ ->
                 if x = a || y = a || x = 0 || y = 0 then -1 else 1)
               [ b; c ]
      | [ (a, 0); b; c ] ->
          (a, 0)
          :: List.sort
               (fun (x, y) _ ->
                 if x = a || y = a || x = 0 || y = 0 then -1 else 1)
               [ b; c ]
      | _ -> assert false
    with
    | [ a; b; c ] -> (a, b, c)
    | _ -> assert false

  let rank a b c =
    match
      List.sort (fun (x, _) (y, _) -> compare x y) [ a; b; c ] |> List.map snd
    with
    | [ a; b; c ] -> (a, b, c)
    | _ -> assert false

  let def dbm a a0 ?c ?(c0 = 0) = function
    | b, 0 -> Some (b, Alg.to_int dbm.(b).(0))
    | 0, b -> Some (b, -Alg.to_int dbm.(0).(b))
    | b, x when x = a -> Some (b, a0 + Alg.to_int dbm.(b).(x))
    | x, b when x = a -> Some (b, a0 - Alg.to_int dbm.(x).(b))
    | b, x when Some x = c -> Some (c0, a0 + Alg.to_int dbm.(b).(x))
    | x, b when Some x = c -> Some (c0, a0 - Alg.to_int dbm.(x).(b))
    | _ -> None

  let inter3 dbm a b c =
    let open Common in
    let p1, p2, p3 = order3 a b c in
    match
      def dbm (-1) 0 p1 |>> fun (a, a0) ->
      def dbm a a0 p2 |>> fun (b, b0) ->
      def dbm a a0 ~c:b ~c0:b0 p3 |>>> fun c -> rank (a, a0) (b, b0) c
    with
    | Some x -> x
    | None ->
        let pa f (i, j) = Format.fprintf f "(%i,%i)" i j in
        Format.eprintf "@.fail to intersect hyperplan %a %a %a " pa a pa b pa c;
        Format.eprintf "fail to intersect hyperplan %a %a %a @." pa p1 pa p2 pa
          p3;
        assert false

  let draw_path dbm f l =
    let open Format in
    let rec drec f = function
      | (t1, t2, t3) :: q ->
          let x, y, z = inter3 dbm t1 t2 t3 in
          fprintf f "(%i,%i,%i)" x y z;
          if List.length q > 0 then fprintf f " -- ";
          drec f q
      | [] -> ()
    in
    Format.fprintf f "\\draw %a  ;@;" drec l

  let to_tikz ?(grid = true) ?scope context ?z (x, y) f dbm2 =
    (*let z = None in*)
    let string_of_var i =
      let s = Uppaal_parser.Uta.VarContext.index2var context i in
      try
        let i = String.rindex s '.' in
        String.sub s (i + 1) (String.length s - i - 1) |> escape_tex
      with Not_found -> escape_tex s
    in

    let dbm = copy dbm2 in
    reduce dbm;
    (*to_tikz2 context (x,y) f dbm2;*)
    Format.fprintf f "%% x axis = %s [%i] ; y axis = %s [%i]@;"
      (string_of_var x) x (string_of_var y) y;
    Option.iter
      (fun za ->
        Format.fprintf f "%% z axis = %s [%i];@;" (string_of_var za) za)
      z;
    (*Format.fprintf f "%%%a@;" (fun f -> print f ) dbm;
      Format.fprintf f "%%%a@;" (fun f -> pretty_print f context ) dbm;*)
    let atoi i j = Alg.to_int dbm.(i).(j) in
    let max_l f l = List.fold_left f min_int l in
    let up_max x y =
      if y = fst Alg.infty then x else if x = fst Alg.infty then y else max x y
    in
    let add x y =
      if x = fst Alg.infty then x else if y = fst Alg.infty then y else x + y
    in
    let sub x y =
      if x = fst Alg.infty then x else if y = fst Alg.infty then y else x - y
    in

    let x1 = sub 0 (atoi 0 x) in
    let y1 = add x1 (atoi y x) in
    let y2 = atoi y 0 in
    let x2 = sub y2 (atoi y x) in
    let x3 = atoi x 0 in
    let y3 = sub x3 (atoi x y) in
    let y4 = sub 0 (atoi 0 y) in
    let x4 = add y4 (atoi x y) in
    let maxx = max_l up_max [ x1; x2; x3; x4 ] in
    let maxy = max_l up_max [ y1; y2; y3; y4 ] in

    let mx x = min (maxx + 1) x and my y = min (maxy + 1) y in
    let pa f (x, y) = Format.fprintf f "(%i,%i)" (mx x) (my y) in
    Option.iter
      (fun s -> Format.fprintf f "\\begin{scope}[%s]@[<v 1>@;" s)
      scope;
    if grid then (
      Format.fprintf f
        "\\draw[step=1.0,black,ultra thin] (0.0,0.0) grid (%i+0.3,%i+0.3);@;"
        maxx maxy;
      Format.fprintf f "\\node at (%i+0.4,0.0) {%s};@;" maxx (string_of_var x);
      Format.fprintf f "\\node at (0.0,%i+0.4) {%s};@;" maxy (string_of_var y);
      Option.iter
        (fun za ->
          Format.fprintf f
            "\\draw[black,ultra thin] (0.0,0.0,0.0) -- (0,0,1.3);@;";
          Format.fprintf f "\\node at (0.0,0.0,1.4) {%s};@;" (string_of_var za))
        z);
    (match z with
    | None ->
        Format.fprintf f
          "\\draw[fill opacity=0.2,fill] %a -- %a -- %a -- %a -- %a --%a -- \
           cycle ;@;"
          pa (x1, y1) pa (x2, y2) pa (x3, y2) pa (x3, y3) pa (x4, y4) pa (x1, y4)
    | Some za ->
        (*let mz x = x in
          let pa f (x, y, z) =
            Format.fprintf f "(%i,%i,%i)" (mx x) (my y) (mz z)
          in*)

        (*let z1 = x1 + atoi za x in
          let z2 = atoi za 0 in
          let xz2 = z2 - atoi za x in
          let yz2 = z2 - atoi za y in
          let z3 = x3 - atoi x za in
          let z4 = -atoi 0 za in
          let xz4 = z4 + atoi x za in
          let yz4 = z4 + atoi y za in

          Format.fprintf f
            "\\draw[fill opacity=0.2,fill] %a -- %a -- %a -- %a -- %a -- %a -- \
             cycle ;@;"
            pa (x1, y1, z1) pa (xz2, yz2, z2) pa (x3, y2, z2) pa (x3, y3, z3) pa
            (xz4, yz4, z4) pa (x1, y1, z4));*)
        (*let x0 = -atoi 0 x in
          let y0 = -atoi 0 y in
          let z0 = -atoi 0 za in

          let x1 = z0 + atoi x za in

          let y1 = x0 + atoi x y in

          let y2 = z0 + atoi za y in

          let x2 = atoi x 0 in
          let z1 = x1 - atoi x za in

          let y3 = atoi y 0 in*)
        (*Format.fprintf f "%%%a" print dbm;
          draw_path dbm f
            [ (0, x); (0, y); (0, za); (x, za); (y, za); (x, 0); (y, 0); (za, 0) ]);*)
        draw_path dbm f
          [
            ((0, x), (0, y), (0, za));
            ((0, x), (y, x), (0, za));
            ((y, za), (y, x), (0, za));
            ((y, za), (x, za), (0, za));
            ((x, y), (x, za), (0, za));
            ((x, y), (0, y), (0, za));
            ((0, x), (0, y), (0, za));
          ]);

    Option.iter (fun _ -> Format.fprintf f "@]\\end{scope}@;") scope

  let to_svg ?(grid = true) ?scope context bounds (x2, y2) f dbm2 =
    let x = if x2 >= Array.length bounds then 0 else x2 in
    let y = if y2 >= Array.length bounds then 0 else y2 in

    let xscale x = 40 * x in
    let yscale x = 40 * (bounds.(y) + 1 - x) in
    let open Format in
    (*let z = None in*)
    let string_of_var i =
      let s = Uppaal_parser.Uta.VarContext.index2var context i in
      try
        let i = String.rindex s '.' in
        String.sub s (i + 1) (String.length s - i - 1) |> escape_tex
      with Not_found -> escape_tex s
    in

    let dbm = copy dbm2 in
    reduce dbm;
    (*to_tikz2 context (x,y) f dbm2;*)
    (*Format.fprintf f "%% x axis = %s [%i] ; y axis = %s [%i]@;"
      (string_of_var x) x (string_of_var y) y;*)
    (*Format.fprintf f "%%%a@;" (fun f -> print f ) dbm;
      Format.fprintf f "%%%a@;" (fun f -> pretty_print f context ) dbm;*)
    let atoi i j = Alg.to_int dbm.(i).(j) in

    let add x y =
      if x = fst Alg.infty then x else if y = fst Alg.infty then y else x + y
    in
    let sub x y =
      if x = fst Alg.infty then x else if y = fst Alg.infty then y else x - y
    in

    let x1 = sub 0 (atoi 0 x) in
    let y1 = add x1 (atoi y x) in
    let y2 = atoi y 0 in
    let x2 = sub y2 (atoi y x) in
    let x3 = atoi x 0 in
    let y3 = sub x3 (atoi x y) in
    let y4 = sub 0 (atoi 0 y) in
    let x4 = add y4 (atoi x y) in
    let maxx = bounds.(x) in
    let maxy = bounds.(y) in

    let mx x = min (maxx + 1) x and my y = min (maxy + 1) y in
    let pa f (x, y) =
      Format.fprintf f "%i,%i" (xscale (mx x)) (yscale (my y))
    in
    Option.iter (fun s -> Format.fprintf f "<g %s>@[<v 1>@;" s) scope;
    if grid then (
      for i = 0 to maxx do
        fprintf f
          "<line x1=\"%i\" y1=\"%i\" x2=\"%i\" y2=\"%i\" fill=\"none\" \
           stroke=\"gray\" stroke-width=\"1\"/>"
          (xscale i)
          (yscale maxy - 10)
          (xscale i) (yscale 0)
      done;
      for j = 0 to maxy do
        fprintf f
          "<line x1=\"%i\" y1=\"%i\" x2=\"%i\" y2=\"%i\" fill=\"none\" \
           stroke=\"gray\" stroke-width=\"1\"/>"
          (xscale 0) (yscale j)
          (xscale maxx + 10)
          (yscale j)
      done;
      fprintf f
        "<text stroke-width=\"1\" stroke=\"black\" x=\"%i\" y=\"%i\">%s</text>@;"
        (xscale maxx) (yscale 0) (string_of_var x);
      fprintf f
        "<text stroke-width=\"1\" stroke=\"black\" x=\"%i\" y=\"%i\">%s</text>@;"
        (xscale 0)
        (yscale maxy - 16)
        (string_of_var y));

    fprintf f
      "<polygon points=\"%a %a %a %a %a %a %a\" fill-opacity=\"0.2\" \
       stroke-opacity=\"0.8\" stroke-linecap=\"round\"/>@;"
      pa (x1, y1) pa (x2, y2) pa (x3, y2) pa (x3, y3) pa (x4, y4) pa (x1, y4) pa
      (x1, y1);

    Option.iter (fun _ -> Format.fprintf f "@]</g>@;") scope

  (*let pretty_print chan clock dbm =
    let dbm2 = copy dbm in
    remove_useless dbm2;
    pretty_print chan clock dbm2*)

  let get_reduced_coordinate mdbm =
    (*silent_reduce mdbm;*)
    let dim = dimension mdbm in
    let rC = Array.make dim 0 in
    let rec get_eq i j =
      if j = dim then [ i ]
      else if Alg.compare mdbm.(i).(j) mdbm.(j).(i) = 0 then (
        rC.(j) <- i;
        j :: get_eq i (j + 1))
      else get_eq i (j + 1)
    in
    let ref0 = ref [ 0 ] in
    for i = 1 to dim - 1 do
      if Alg.is_zero mdbm.(i).(0) then ref0 := i :: !ref0 else rC.(i) <- i
    done;
    let res = ref [ List.rev !ref0 ] in
    for i = 1 to dim - 1 do
      if rC.(i) = i then res := (List.rev @@ get_eq i (i + 1)) :: !res
    done;
    List.rev !res

  let is_splitted mdbm =
    let lbs =
      fst
      @@ Array.fold_left
           (fun (set, i) v ->
             if i > 0 && not (Alg.is_infty v) then (set + 1, i + 1)
             else (set, i + 1))
           (0, 0) mdbm.(0)
    in
    let ubs =
      fst
      @@ Array.fold_left
           (fun (set, i) v ->
             if i > 0 && not (Alg.is_infty v.(0)) then (set + 1, i + 1)
             else (set, i + 1))
           (0, 0) mdbm
    in
    lbs = 1 && ubs <= 1

  let close_guard mdbm =
    let dim = Array.length mdbm in
    for i = 0 to dim - 1 do
      for j = 0 to dim - 1 do
        mdbm.(i).(j) <- (fst mdbm.(i).(j), DBM_WEAK)
      done
    done

  (*let open_guard mdbm =
    let dim = Array.length mdbm in
    for i = 0 to dim - 1 do
      for j = 0 to dim - 1 do
        mdbm.(i).(j) <- (fst mdbm.(i).(j), DBM_STRICT)
      done
    done*)

  let iter_split_upper mdbm res lbs ubs l u =
    try
      (*Format.printf "split iter l:%i u:%i@." l u;
        Format.printf "dbm:\n%a@." print mdbm;*)
      let ng = copy mdbm in

      lbs
      |> IntSet.iter (fun j ->
             if j <> l then constrain ng j l (Alg.sub mdbm.(0).(l) mdbm.(0).(j)));
      ubs
      |> IntSet.iter (fun j ->
             if j <> u then constrain ng u j (Alg.sub mdbm.(u).(0) mdbm.(j).(0)));
      reduce ng;
      (*Format.printf "dbm:%a\n" print ng;*)
      res := ng :: !res
    with EmptyDBM -> (*Format.printf "Empty dbm:\n";*) ()

  let split_guard mdbm_init =
    let mdbm = copy mdbm_init in
    let res = ref [] in
    (*close_guard mdbm;*)
    remove_useless mdbm;
    (*open_guard mdbm;*)
    let lbs =
      fst
      @@ Array.fold_left
           (fun (set, i) v ->
             if i > 0 && not (Alg.is_infty v) then (IntSet.add i set, i + 1)
             else (set, i + 1))
           (IntSet.empty, 0) mdbm.(0)
    in
    let ubs =
      fst
      @@ Array.fold_left
           (fun (set, i) v ->
             if i > 0 && not (Alg.is_infty v.(0)) then (IntSet.add i set, i + 1)
             else (set, i + 1))
           (IntSet.empty, 0) mdbm
    in

    (*Format.printf "dbm lbs:%i ubs:%i :\n%a@." (IntSet.cardinal lbs) (IntSet.cardinal ubs) print mdbm;*)
    lbs
    |> IntSet.iter (fun l ->
           if IntSet.cardinal ubs = 0 then
             iter_split_upper mdbm res lbs ubs l (-1)
           else ubs |> IntSet.iter (iter_split_upper mdbm res lbs ubs l));
    (!res, IntSet.fold (fun x y -> x :: y) (IntSet.union lbs ubs) [])

  exception Break of Common.bound

  let get_lower_upper clockmap mdbm =
    let open Common in
    let dim = dimension mdbm in
    if dim <= 1 then Punctual (Const 0)
    else (
      (* Not sure for close guard *)
      close_guard mdbm;
      remove_useless mdbm;
      if not @@ is_splitted mdbm then raise Not_split;

      let dim = dimension mdbm in
      let low =
        try
          for i = 1 to dim - 1 do
            if not @@ Alg.is_infty mdbm.(0).(i) then
              if Alg.is_zero mdbm.(0).(i) then raise_notrace (Break (Const 0))
              else
                raise_notrace
                  (Break (build_bound (abs (Alg.to_int mdbm.(0).(i))) i))
          done;
          assert false
        with Break b -> SBound.map clockmap b
      in
      let up =
        try
          for i = 1 to dim - 1 do
            if not @@ Alg.is_infty mdbm.(i).(0) then
              raise_notrace
                (Break (build_bound (abs (Alg.to_int mdbm.(i).(0))) i))
          done;
          Infinite
        with Break b -> SBound.map clockmap b
      in
      build_guard low up)

  let get_lower_upper_no_split clockmap mdbm =
    let open Common in
    let dim = dimension mdbm in
    if dim <= 1 then Punctual [ Const 0 ]
    else (
      (* Not sure for close guard *)
      close_guard mdbm;
      remove_useless mdbm;
      let low = ref [] in
      for i = 1 to dim - 1 do
        if not @@ Alg.is_infty mdbm.(0).(i) then
          if Alg.is_zero mdbm.(0).(i) then low := Const 0 :: !low
          else
            low :=
              SBound.map clockmap
                (build_bound (abs (Alg.to_int mdbm.(0).(i))) i)
              :: !low
      done;
      let up = ref [] in
      for i = 1 to dim - 1 do
        if not @@ Alg.is_infty mdbm.(i).(0) then
          up :=
            SBound.map clockmap (build_bound (abs (Alg.to_int mdbm.(i).(0))) i)
            :: !up
      done;
      Common.build_guard !low !up)
end

module Guard = SplitableDBM
