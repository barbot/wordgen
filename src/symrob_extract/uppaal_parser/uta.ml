exception Var_already_defined
exception Var_undefined of string

module VarContext = struct
  type 'a t = {
    index2var : (int, 'a) Hashtbl.t;
    var2index : ('a, int) Hashtbl.t;
    mutable nextVarIndex : int;
  }

  let create () =
    {
      var2index = Hashtbl.create 10;
      index2var = Hashtbl.create 10;
      nextVarIndex = 0;
    }

  let get_var2index vc = vc.var2index
  let index_of_var vc var = Hashtbl.find vc.var2index var

  let var_of_index vc index =
    try Hashtbl.find vc.index2var index with
    | Not_found ->
        Printf.eprintf "No var with index %d\n Size: %d\n" index
          (Hashtbl.length vc.index2var);
        Hashtbl.iter
          (fun index var -> Printf.eprintf "CLOCK: %d %s\n" index var)
          vc.index2var;
        flush stderr;
        raise Not_found
    | _ as e -> raise e

  let index2var = var_of_index
  let var2index = index_of_var
  let mem vc var = Hashtbl.mem vc.var2index var

  let add vc ?id var =
    if mem vc var then raise Var_already_defined;
    match id with
    | None ->
        let index = vc.nextVarIndex in
        vc.nextVarIndex <- vc.nextVarIndex + 1;
        Hashtbl.add vc.index2var index var;
        Hashtbl.add vc.var2index var index
    | Some id ->
        Hashtbl.add vc.index2var id var;
        Hashtbl.add vc.var2index var id

  let size vc = Hashtbl.length vc.index2var
  let iter f vc = Hashtbl.iter f vc.var2index
end

type expression =
  | Constant of int
  | Variable of string
  | Sum of expression * expression
  | Product of expression * expression
  | Subtraction of expression * expression
  | Division of expression * expression

type atomic_guard =
  | GuardLeq of expression * expression
  | GuardLess of expression * expression
  | GuardGeq of expression * expression
  | GuardGreater of expression * expression
  | GuardEqual of expression * expression
  | GuardNEqual of expression * expression

let rec replace_exp s e1 e2 =
  let rep = replace_exp s e1 in
  match e2 with
  | Variable c when c = s -> e1
  | Sum (lhs, rhs) -> Sum (rep lhs, rep rhs)
  | Product (lhs, rhs) -> Sum (rep lhs, rep rhs)
  | Subtraction (lhs, rhs) -> Sum (rep lhs, rep rhs)
  | Division (lhs, rhs) -> Sum (rep lhs, rep rhs)
  | x -> x

let replace_ag s e1 e2 =
  let rep = replace_exp s e1 in
  match e2 with
  | GuardLeq (lhs, rhs) -> GuardLeq (rep lhs, rep rhs)
  | GuardLess (lhs, rhs) -> GuardLess (rep lhs, rep rhs)
  | GuardGeq (lhs, rhs) -> GuardGeq (rep lhs, rep rhs)
  | GuardGreater (lhs, rhs) -> GuardGreater (rep lhs, rep rhs)
  | GuardEqual (lhs, rhs) -> GuardEqual (rep lhs, rep rhs)
  | GuardNEqual (lhs, rhs) -> GuardNEqual (rep lhs, rep rhs)

(** Variable name and initial value *)
type variable = Var of (string * int) | ConstVar of (string * int)

let name_of_var = function Var (name, _) -> name | ConstVar (name, _) -> name

type temp_declaration = string list * variable list * string list
(** a declaration is: Clock list, variable list, channel list *)

type guard = atomic_guard list
type disc_guard = guard list
type update = (string * expression) list
type position = int * int

type location = {
  mutable locId : string;
  mutable locPos : position;
  mutable locName : string;
  mutable locNamePos : position;
  mutable locInvar : guard;
  mutable locInvarPos : position;
  mutable locCommitted : bool;
  mutable locUrgent : bool;
}

type chan = SendChan of string | RecvChan of string

type edge = {
  mutable edgeSource : string;
  mutable edgeGuard : guard list;
  mutable edgeGuardPos : position;
  mutable edgeSync : chan option;
  mutable edgeSyncPos : position;
  mutable edgeUpdates : update;
  mutable edgeUpdatesPos : position;
  mutable edgeTarget : string;
  mutable edgeNails : position list;
  mutable edgeProb : float;
}

type template = {
  tempName : string;
  tempLocations : location list;
  tempInitialLocation : location;
  tempEdges : edge list;
  tempClocks : string list;
  tempVars : variable list;
}

type prop_query =
  | AndQuery of prop_query * prop_query
  | OrQuery of prop_query * prop_query
  | NotQuery of prop_query
  | AtomicQuery of string

type query = ReachQuery of prop_query | EmptyQuery

type system = {
  sysTemplates : template list;
      (** list of process names and their template types *)
  sysProcessInstances : (string * string) list;
  sysVars : variable list;
  sysClocks : string list;
  sysChans : string list;
  sysQuery : query;
}

let rec vars_of_exp = function
  | Constant _ -> []
  | Variable id -> [ id ]
  | Product (e1, e2) -> vars_of_exp e1 @ vars_of_exp e2
  | Sum (e1, e2) -> vars_of_exp e1 @ vars_of_exp e2
  | Division (e1, e2) -> vars_of_exp e1 @ vars_of_exp e2
  | Subtraction (e1, e2) -> vars_of_exp e1 @ vars_of_exp e2

let vars_of_atomic_guard = function
  | GuardLeq (e1, e2) -> vars_of_exp e1 @ vars_of_exp e2
  | GuardLess (e1, e2) -> vars_of_exp e1 @ vars_of_exp e2
  | GuardGeq (e1, e2) -> vars_of_exp e1 @ vars_of_exp e2
  | GuardGreater (e1, e2) -> vars_of_exp e1 @ vars_of_exp e2
  | GuardEqual (e1, e2) -> vars_of_exp e1 @ vars_of_exp e2
  | GuardNEqual (e1, e2) -> vars_of_exp e1 @ vars_of_exp e2

let instantiate_channel chan eval_var =
  try
    Scanf.sscanf chan "%s@[ %s@]" (fun base var ->
        match eval_var var with
        | None -> chan
        | Some v ->
            let nchan = base ^ "[" ^ string_of_int v ^ "]" in
            nchan)
  with x -> raise x (*Scanf.Scan_failure _ -> chan*)

let instantiate_channel_full chan eval_var =
  match chan with
  | RecvChan s -> RecvChan (instantiate_channel s eval_var)
  | SendChan s -> SendChan (instantiate_channel s eval_var)

let instantiate_edge var i edge =
  {
    edge with
    edgeGuard =
      List.map
        (fun x -> List.map (fun ag -> replace_ag var (Constant i) ag) x)
        edge.edgeGuard;
    edgeUpdates =
      List.map
        (fun (j, e) -> (j, replace_exp var (Constant i) e))
        edge.edgeUpdates;
    edgeSync =
      Option.map
        (fun x ->
          instantiate_channel_full x (fun y ->
              (*Printf.printf "'%s':'%s'\n" y var;*)
              if y = var then Some i else None))
        edge.edgeSync;
  }


let print_nta _ _ = ()
