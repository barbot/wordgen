open Symrob_common
open Pdbm
open Uppaal_parser.Uta

module type TIMED_AUTOMATON = sig
  module Dbm : GENERIC_DBM

  type timed_automaton
  type discrete_state
  type concrete_discrete_state = int array * int array
  type transition

  val clocks : timed_automaton -> string VarContext.t
  val nb_of_processes : timed_automaton -> int
  val is_single_module : timed_automaton -> bool

  val is_state_equal :
    timed_automaton -> discrete_state -> discrete_state -> bool

  val initial_discrete_state : timed_automaton -> discrete_state
  val to_concrete : discrete_state -> concrete_discrete_state

  val transitions_from :
    timed_automaton -> ?nosync:bool -> discrete_state -> transition list

  val transition_fields :
    timed_automaton ->
    transition ->
    discrete_state * Dbm.t * ClockSet.t * discrete_state

  val get_transition_action : transition -> string option
  val get_transition_prob : transition -> float
  val guard_of_transition : timed_automaton -> transition -> Dbm.t
  val invariant_of_discrete_state : timed_automaton -> discrete_state -> Dbm.t

  (*val is_urgent_or_committed : timed_automaton -> discrete_state -> bool*)
  val print_var :
    timed_automaton -> Format.formatter -> Common.VarSet.var -> unit

  val string_of_state : timed_automaton -> discrete_state -> string

  val print_discrete_state :
    Format.formatter -> timed_automaton -> discrete_state -> unit

  val eval_accepting : timed_automaton -> discrete_state -> bool
  val print_timed_automaton : Format.formatter -> timed_automaton -> unit

  val print_extended_state :
    Format.formatter ->
    ?do_escape_tex:bool ->
    timed_automaton ->
    discrete_state * Dbm.t ->
    unit

  val string_of_extended_state :
    timed_automaton -> discrete_state * Dbm.t -> string

  val initial_extended_state : timed_automaton -> discrete_state * Dbm.t

  (*val is_target : timed_automaton -> discrete_state -> bool*)
  val lu_bounds :
    timed_automaton -> discrete_state -> int array -> int array -> unit

  val max_bound : timed_automaton -> int
end

open Printf

module GenericUautomaton (Dbm : GENERIC_DBM) = struct
  (** Expression type used in the guard and updates
      involving variables. In contrast with Uta.expression
      used by the parser, the variables in this one are indexed
      by their unique integer identifiers and we distinguish clocks
  *)
  type expression =
    | Constant of int
    | Variable of int
    | Clock of int
    | Sum of expression * expression
    | Product of expression * expression
    | Subtraction of expression * expression
    | Division of expression * expression

  type atomic_guard =
    | GuardLeq of expression * expression
    | GuardLess of expression * expression
    | GuardGeq of expression * expression
    | GuardGreater of expression * expression
    | GuardEqual of expression * expression
    | GuardNEqual of expression * expression

  type guard = atomic_guard list
  type disc_guard = guard list
  type update = (int * expression) list

  type edge = {
    edgeSource : int;
    mutable edgeGuard : guard;
    edgeDiscGuard : disc_guard;
    edgeReset : ClockSet.t;
    edgeDiscUpdate : update;
    edgeTarget : int;
    edgeSync : chan option;
    mutable edgeProc : process option;
    edgeControllable : bool;
    edgeProb : float;
  }

  and location = {
    locId : int;
    mutable locName : string;
    locCommitted : bool;
    locUrgent : bool;
    locInvar : guard;
    mutable locEdges : edge list;
    mutable locProc : process option;
  }

  and process = {
    procName : string;
    procId : int;
    mutable procLocations : location array;
    procInitLoc : int;
  }

  type discrete_state = { stateLocs : location array; stateVars : int array }
  type concrete_discrete_state = int array * int array

  let to_concrete x = (Array.map (fun x -> x.locId) x.stateLocs, x.stateVars)

  type transition =
    | InternalTrans of discrete_state * edge
    | SyncTrans of discrete_state * edge * edge

  (** A succinct version of the above to be used in hash tables; we just don't need the varible valuations *)
  type _succinct_transition =
    | Suc_InternalTrans of edge
    | Suc_SyncTrans of edge * edge

  type timed_automaton = {
    procs : process array;
    clocks : string VarContext.t;
    vars : string VarContext.t;
    init : discrete_state;
    (* mutable query : query;*)
    mutable lowerLU : int array array array;
    mutable upperLU : int array array array;
    guards_tbl : (_succinct_transition, Dbm.t) Hashtbl.t;
    invars_tbl : (location array, Dbm.t) Hashtbl.t;
    labels : (string * (disc_guard * int list)) list;
  }

  let is_single_module ta = Array.length ta.procs = 1

  let max_bound ta =
    let rec m_a_exp m = function
      | Constant i -> max i m
      | Sum (e1, e2) -> m_a_exp (m_a_exp m e1) e2
      | Product (e1, e2) -> m_a_exp (m_a_exp m e1) e2
      | Subtraction (e1, e2) -> m_a_exp (m_a_exp m e1) e2
      | Division (e1, e2) -> m_a_exp (m_a_exp m e1) e2
      | _ -> m
    in
    let m_a_guard m = function
      | GuardLeq (e1, e2) -> m_a_exp (m_a_exp m e1) e2
      | GuardLess (e1, e2) -> m_a_exp (m_a_exp m e1) e2
      | GuardGeq (e1, e2) -> m_a_exp (m_a_exp m e1) e2
      | GuardGreater (e1, e2) -> m_a_exp (m_a_exp m e1) e2
      | GuardEqual (e1, e2) -> m_a_exp (m_a_exp m e1) e2
      | GuardNEqual (e1, e2) -> m_a_exp (m_a_exp m e1) e2
    in
    let m_guard m g = List.fold_left m_a_guard m g in
    (*let m_disc_guard m g = List.fold_left m_guard m g in*)
    let mproc p mp =
      Array.fold_right
        (fun loc m ->
          let m2 = m_guard m loc.locInvar in
          List.fold_left
            (fun m edge -> m_guard m edge.edgeGuard)
            m2 loc.locEdges)
        p.procLocations mp
    in

    let max_proc = Array.fold_right mproc ta.procs 0 in
    let max_guard =
      Hashtbl.fold
        (fun _ dbm m -> max m (Dbm.max_bound dbm))
        ta.guards_tbl max_proc
    in
    let max_invars =
      Hashtbl.fold
        (fun _ dbm m -> max m (Dbm.max_bound dbm))
        ta.invars_tbl max_guard
    in
    max_invars

  (************************* STRING and PRINT *********************)
  let rec string_of_exp ta e =
    let string_of_exp = string_of_exp ta in
    (function
      | Constant c -> sprintf "%d" c
      | Variable id -> VarContext.var_of_index ta.vars id
      | Clock id -> VarContext.var_of_index ta.clocks id
      | Product (e1, e2) ->
          sprintf "%s * %s" (string_of_exp e1) (string_of_exp e2)
      | Sum (e1, e2) ->
          sprintf "(%s + %s)" (string_of_exp e1) (string_of_exp e2)
      | Division (e1, e2) ->
          sprintf "%s / %s" (string_of_exp e1) (string_of_exp e2)
      | Subtraction (e1, e2) ->
          sprintf "(%s - %s)" (string_of_exp e1) (string_of_exp e2))
      e

  let string_of_atomic_guard ta =
    let string_of_exp = string_of_exp ta in
    function
    | GuardLeq (v, exp) ->
        sprintf "%s <= %s" (string_of_exp v) (string_of_exp exp)
    | GuardLess (v, exp) ->
        sprintf "%s < %s" (string_of_exp v) (string_of_exp exp)
    | GuardGeq (v, exp) ->
        sprintf "%s >= %s" (string_of_exp v) (string_of_exp exp)
    | GuardGreater (v, exp) ->
        sprintf "%s > %s" (string_of_exp v) (string_of_exp exp)
    | GuardEqual (v, exp) ->
        sprintf "%s == %s" (string_of_exp v) (string_of_exp exp)
    | GuardNEqual (v, exp) ->
        sprintf "%s != %s" (string_of_exp v) (string_of_exp exp)

  let rec string_of_guard ta = function
    | [] -> ""
    | [ x ] -> string_of_atomic_guard ta x
    | x :: y :: l ->
        (string_of_atomic_guard ta x ^ " && ") ^ string_of_guard ta (y :: l)

  let rec string_of_disc_guard ta = function
    | [] -> ""
    | [ x ] -> string_of_guard ta x
    | x :: y :: l ->
        (string_of_guard ta x ^ " && ") ^ string_of_disc_guard ta (y :: l)

  let string_of_updates ta ups =
    let ups_str =
      List.map
        (fun (var, exp) ->
          sprintf "%s = %s"
            (VarContext.var_of_index ta.vars var)
            (string_of_exp ta exp))
        ups
    in
    String.concat ", " ups_str

  (** Pretty Printing Functions *)
  let string_of_state ta state =
    let out = Buffer.create 50 in
    Array.iter
      (fun loc ->
        Buffer.add_string out loc.locName;
        Buffer.add_string out " ")
      state.stateLocs;
    if Array.length state.stateVars > 0 then (
      Buffer.add_string out " ";
      let oname = ref "null" in
      Array.iteri
        (fun i v ->
          let name = VarContext.index2var ta.vars i in
          let name2 =
            if String.starts_with ~prefix:!oname name then
              "*."
              ^ String.sub name (String.length !oname)
                  (String.length name - String.length !oname)
            else (
              (match String.rindex_opt name '.' with
              | None -> oname := "null"
              | Some i -> oname := String.sub name 0 (i + 1));
              name)
          in

          Buffer.add_string out (sprintf "%s:%d " name2 v))
        state.stateVars);
    (*    Buffer.add_string out "\n";*)
    Buffer.contents out

  let string_of_edge ta edge =
    let proc =
      match edge.edgeProc with
      | None -> failwith "Edge with no proc"
      | Some proc -> proc
    in
    let resetnames =
      let out = Buffer.create 100 in
      ClockSet.iter
        (fun ind ->
          let name = VarContext.index2var ta.clocks ind in
          Buffer.add_string out name;
          Buffer.add_string out " ")
        edge.edgeReset;
      Buffer.contents out
    in
    let sync =
      match edge.edgeSync with
      | None -> ""
      | Some (SendChan c) -> c ^ "!"
      | Some (RecvChan c) -> c ^ "?"
    in
    let discguardstr = string_of_disc_guard ta edge.edgeDiscGuard in
    let guardstr = string_of_guard ta edge.edgeGuard in
    sprintf
      "%s%s -> %s \tDiscGuard: %s \tDiscUpdate: %s \tGuard: %s \tResets:%s \
       \tSync:%s\tProb:%g"
      (if edge.edgeControllable then "" else "[E]")
      proc.procLocations.(edge.edgeSource).locName
      proc.procLocations.(edge.edgeTarget).locName discguardstr
      (string_of_updates ta edge.edgeDiscUpdate)
      guardstr resetnames sync edge.edgeProb

  let string_of_location ta loc =
    let out = Buffer.create 128 in
    let utter = Buffer.add_string out in
    utter (sprintf "Location %d: %s " loc.locId loc.locName);
    if loc.locCommitted then utter "committed ";
    utter (string_of_guard ta loc.locInvar);
    utter "\n";
    utter (sprintf "Has %d edges:\n" (List.length loc.locEdges));
    let edgestrlist = List.map (string_of_edge ta) loc.locEdges in
    utter (String.concat "\n" edgestrlist);
    utter "\n";
    List.iter
      (fun (s, (g, l)) ->
        if List.mem loc.locId l then (
          utter s;
          utter ":";
          utter @@ string_of_disc_guard ta g;
          utter "\n"))
      ta.labels;
    Buffer.contents out

  let string_of_process ta proc =
    let out = Buffer.create 1000 in
    let utter = Buffer.add_string out in
    utter (sprintf "Process(%d): %s\n" proc.procId proc.procName);
    Array.iter (fun loc -> utter (string_of_location ta loc)) proc.procLocations;
    utter (sprintf "Initial location id: %d\n" proc.procInitLoc);
    Buffer.contents out

  let print_var ta f i =
    let s2 =
      let s = VarContext.index2var ta.clocks (Common.VarSet.int_of_var i) in
      try
        let i = String.rindex s '.' in
        String.sub s (i + 1) (String.length s - i - 1)
      with Not_found -> s
    in
    Format.fprintf f "%s" s2

  let print_discrete_state chan ta state =
    Format.fprintf chan "%s\n" (string_of_state ta state)

  let print_timed_automaton chan ta =
    Format.fprintf chan "Timed automaton with %d clocks and %d processes\n"
      (VarContext.size ta.clocks)
      (Array.length ta.procs);
    Array.iter
      (fun proc ->
        Format.fprintf chan "%s\n-----\n" (string_of_process ta proc))
      ta.procs

  let print_extended_state chan ?(do_escape_tex = false) ta (state, dbm) =
    let st = string_of_state ta state in
    Format.fprintf chan "%s " (if do_escape_tex then escape_tex st else st);
    Dbm.pretty_print chan ~do_escape_tex ~minimalist:false ~context:ta.clocks
      dbm

  let string_of_extended_state ta (state, dbm) =
    string_of_state ta state ^ Dbm.to_string dbm

  (************************* TIMED_AUTOMATON interface *********************)
  let clocks ta = ta.clocks
  let initial_discrete_state ta = ta.init

  let is_state_equal _ s t =
    let exists2 f a1 a2 =
      let n = Array.length a1 in
      if n <> Array.length a2 then
        raise (Invalid_argument "Two arrays of different length");
      let i = ref 0 in
      while !i < n && not (f a1.(!i) a2.(!i)) do
        incr i
      done;
      !i < n
    in

    not
      (exists2 (fun l1 l2 -> not (l1.locId = l2.locId)) s.stateLocs t.stateLocs
      || exists2 (fun v1 v2 -> not (v1 = v2)) s.stateVars t.stateVars)

  let is_committed state =
    Array.exists (fun loc -> loc.locCommitted) state.stateLocs

  (* let is_target ta state =
     let rec eval = function
       | AtomicQuery(s) ->
         Array.exists (fun loc -> loc.locName = s) state.stateLocs
       | OrQuery(p1,p2) -> (eval p1) || (eval p2)
       | AndQuery(p1,p2) -> (eval p1) && (eval p2)
       | NotQuery(p) -> not (eval p)
     in
     match ta.query with
       EmptyQuery -> true
     | ReachQuery(pq) -> eval pq*)

  let lu_bounds ta state lbounds ubounds =
    Array.iteri
      (fun i _ ->
        lbounds.(i) <- min_int;
        ubounds.(i) <- min_int)
      lbounds;
    lbounds.(0) <- 0;
    ubounds.(0) <- 0;
    let nprocs = Array.length state.stateLocs in
    let nclocks = VarContext.size ta.clocks in
    for iproc = 0 to nprocs - 1 do
      let iloc = state.stateLocs.(iproc).locId in
      for cl = 0 to nclocks - 1 do
        lbounds.(cl) <- max lbounds.(cl) ta.lowerLU.(iproc).(iloc).(cl);
        ubounds.(cl) <- max ubounds.(cl) ta.upperLU.(iproc).(iloc).(cl)
      done
    done;
    Array.iteri
      (fun i _ ->
        if lbounds.(i) = min_int then lbounds.(i) <- max_int;
        if ubounds.(i) = min_int then ubounds.(i) <- max_int)
      lbounds

  let is_urgent_or_committed _ state =
    Array.exists (fun loc -> loc.locCommitted || loc.locUrgent) state.stateLocs

  let get_location_of_process state procid = state.stateLocs.(procid)

  let source_location_of_edge _ edge =
    let proc =
      match edge.edgeProc with
      | None -> failwith "Edge has no process"
      | Some proc -> proc
    in
    proc.procLocations.(edge.edgeSource)

  let get_process ta procid = ta.procs.(procid)
  let nb_of_processes ta = Array.length ta.procs

  let rec eval_disc_exp ta vars exp =
    try
      let k =
        match exp with
        | Constant c -> c
        | Variable id ->
            if id < 0 || id >= Array.length vars then
              failwith
                (sprintf "Var index %d out of bounds (%d)" id
                   (Array.length vars));
            vars.(id)
        | Clock _ -> raise Found
        | Product (e1, e2) ->
            eval_disc_exp ta vars e1 * eval_disc_exp ta vars e2
        | Sum (e1, e2) -> eval_disc_exp ta vars e1 + eval_disc_exp ta vars e2
        | Division (e1, e2) ->
            eval_disc_exp ta vars e1 / eval_disc_exp ta vars e2
        | Subtraction (e1, e2) ->
            eval_disc_exp ta vars e1 - eval_disc_exp ta vars e2
      in
      (*
      eprintf "%s -----> %d\n" (string_of_exp ta exp) k;
       *)
      k
    with
    | Found ->
        failwith ("Discrete expression contains clock: " ^ string_of_exp ta exp)
    | e -> raise e

  let eval_disc_guard ta state guard =
    if guard = [] then true
    else
      let eval = eval_disc_exp ta state.stateVars in
      let atomic = function
        | GuardLeq (e1, e2) -> eval e1 <= eval e2
        | GuardLess (e1, e2) -> eval e1 < eval e2
        | GuardGeq (e1, e2) -> eval e1 >= eval e2
        | GuardGreater (e1, e2) -> eval e1 > eval e2
        | GuardEqual (e1, e2) -> eval e1 = eval e2
        | GuardNEqual (e1, e2) -> eval e1 <> eval e2
      in

      List.fold_left
        (fun bor dg ->
          bor || List.fold_left (fun band ag -> band && atomic ag) true dg)
        false guard

  let eval_accepting ta state =
    match List.assoc_opt "accepting" ta.labels with
    | None -> true
    | Some (g, sl) ->
        (* TODO make better location condition right no only work with one process *)
        List.exists (fun locid -> state.stateLocs.(0).locId = locid) sl
        && eval_disc_guard ta state g

  let instantiate_channel_var ta state proc chan =
    instantiate_channel chan (fun var ->
        snd
        @@ Array.fold_left
             (fun (i, ov) v ->
               let name = VarContext.index2var ta.vars i in
               if name = proc.procName ^ "." ^ var then (i + 1, Some v)
               else (i + 1, ov))
             (0, None) state.stateVars)

  let replace_sync edge chan =
    let rep = function
      | None -> None
      | Some (RecvChan _) -> Some (RecvChan chan)
      | Some (SendChan _) -> Some (SendChan chan)
    in
    match edge with
    | InternalTrans (a, e) ->
        InternalTrans (a, { e with edgeSync = rep e.edgeSync })
    | SyncTrans (s, e1, e2) ->
        SyncTrans
          ( s,
            { e1 with edgeSync = rep e1.edgeSync },
            { e2 with edgeSync = rep e2.edgeSync } )

  let transitions_from ta ?(nosync = false) state =
    (* If nosync = true, all transition are internal transitions *)
    let committed = is_committed state in
    let transq = Queue.create () in
    (* Queue of synchronizing edges *)
    let rchan = Queue.create () in
    let schan = Queue.create () in
    let nproc = Array.length ta.procs in
    for i = 0 to nproc - 1 do
      let loc = state.stateLocs.(i) in
      let proc = ta.procs.(i) in
      let add_single = (not committed) || loc.locCommitted in
      List.iter
        (fun edge ->
          (*print_endline @@ string_of_edge ta edge;*)
          if eval_disc_guard ta state edge.edgeDiscGuard then
            match edge.edgeSync with
            | Some (SendChan c) ->
                if nosync then Queue.add (InternalTrans (state, edge)) transq
                else
                  Queue.add
                    (instantiate_channel_var ta state proc c, edge)
                    schan
            | Some (RecvChan c) ->
                if nosync then Queue.add (InternalTrans (state, edge)) transq
                else
                  Queue.add
                    (instantiate_channel_var ta state proc c, edge)
                    rchan
            | None ->
                if add_single then
                  Queue.add (InternalTrans (state, edge)) transq)
        loc.locEdges
    done;
    Queue.iter
      (fun (rname, redge) ->
        Queue.iter
          (fun (sname, sedge) ->
            (* Sync if same channels are used by different processes
               *)
            (*Format.printf "try to sync %s with %s@." rname sname;*)
            if rname = sname && redge.edgeProc <> sedge.edgeProc then
              (* and if state not committed or one of the participating states are *)
              let sloc = source_location_of_edge ta sedge in
              let rloc = source_location_of_edge ta redge in
              if (not committed) || sloc.locCommitted || rloc.locCommitted then
                Queue.add
                  (replace_sync (SyncTrans (state, redge, sedge)) rname)
                  transq)
          schan)
      rchan;
    Queue.fold (fun l tr -> tr :: l) [] transq

  let _guard_to_dbm ta state g =
    let nclocks = VarContext.size ta.clocks in
    let dbm = Dbm.create nclocks in
    let aux = function
      | GuardLeq (Clock c, e) ->
          let k = eval_disc_exp ta state e in
          Dbm.assign_constraint dbm c 0 (Dbm.Alg.weak_from_int k)
      | GuardLess (Clock c, e) ->
          let k = eval_disc_exp ta state e in
          Dbm.assign_constraint dbm c 0 (Dbm.Alg.strict_from_int k)
      | GuardGeq (Clock c, e) ->
          let k = eval_disc_exp ta state e in
          Dbm.assign_constraint dbm 0 c (Dbm.Alg.weak_from_int (-k))
      | GuardGreater (Clock c, e) ->
          let k = eval_disc_exp ta state e in
          Dbm.assign_constraint dbm 0 c (Dbm.Alg.strict_from_int (-k))
      | GuardEqual (Clock c, e) ->
          let k = eval_disc_exp ta state e in
          Dbm.assign_constraint dbm 0 c (Dbm.Alg.weak_from_int (-k));
          Dbm.assign_constraint dbm c 0 (Dbm.Alg.weak_from_int k)
      | _ as e -> failwith (sprintf "Bad Guard: %s" (string_of_guard ta [ e ]))
    in
    List.iter aux g;
    Dbm.reduce dbm;
    dbm

  let _copy_state state =
    {
      stateVars = Array.copy state.stateVars;
      stateLocs = Array.copy state.stateLocs;
    }

  (** Apply discrete update of edge to state, result written in state' *)
  let _apply_edge ta state edge state' =
    let aux (id, e) =
      state'.stateVars.(id) <- eval_disc_exp ta state.stateVars e
    in
    let tproc =
      match edge.edgeProc with
      | None -> failwith "Edge without process"
      | Some proc -> proc
    in
    state'.stateLocs.(tproc.procId) <- tproc.procLocations.(edge.edgeTarget);
    List.iter aux edge.edgeDiscUpdate

  let guard_of_transition ta tr =
    let to_succinct = function
      | InternalTrans (_, e) -> Suc_InternalTrans e
      | SyncTrans (_, e1, e2) -> Suc_SyncTrans (e1, e2)
    in
    let str = to_succinct tr in
    try Hashtbl.find ta.guards_tbl str with
    | Not_found -> (
        match tr with
        | InternalTrans (state, e) ->
            let g = _guard_to_dbm ta state.stateVars e.edgeGuard in
            Hashtbl.add ta.guards_tbl str g;
            g
        | SyncTrans (state, e1, e2) ->
            let g1 = _guard_to_dbm ta state.stateVars e1.edgeGuard in
            let g2 = _guard_to_dbm ta state.stateVars e2.edgeGuard in
            Dbm.intersect g1 g2;
            Hashtbl.add ta.guards_tbl str g1;
            g1)
    | _ as e -> raise e

  let simple_conjunction_or_die = function
    | [ x ] -> x
    | _ -> failwith "Unexpected disjunction"

  let invariant_of_discrete_state ta state =
    try Hashtbl.find ta.invars_tbl state.stateLocs with
    | Not_found ->
        let glob_inv =
          Array.fold_left (fun acc loc -> loc.locInvar @ acc) [] state.stateLocs
        in
        let inv = _guard_to_dbm ta state.stateVars glob_inv in
        Hashtbl.add ta.invars_tbl state.stateLocs inv;
        inv
    | _ as e -> raise e

  let transition_fields ta = function
    | InternalTrans (state, e) ->
        let state' = _copy_state state in
        _apply_edge ta state e state';
        ( state,
          _guard_to_dbm ta state.stateVars e.edgeGuard,
          e.edgeReset,
          state' )
    | SyncTrans (state, e1, e2) ->
        let state' = _copy_state state in
        _apply_edge ta state e1 state';
        _apply_edge ta state e2 state';
        let g1 = _guard_to_dbm ta state.stateVars e1.edgeGuard in
        let g2 = _guard_to_dbm ta state.stateVars e2.edgeGuard in
        Dbm.intersect g1 g2;
        (state, g1, ClockSet.union e1.edgeReset e2.edgeReset, state')

  let get_transition_action edge =
    match edge with
    | InternalTrans (_, e) | SyncTrans (_, e, _) -> (
        match e.edgeSync with
        | None -> None
        | Some (SendChan s) | Some (RecvChan s) -> Some s)

  let get_transition_prob edge =
    match edge with InternalTrans (_, e) | SyncTrans (_, e, _) -> e.edgeProb

  let initial_extended_state ta =
    let dim = VarContext.size (clocks ta) in
    let z = Dbm.create dim in
    (try
       for i = 0 to dim - 1 do
         Dbm.constrain z i 0 Dbm.Alg.zero;
         Dbm.constrain z 0 i Dbm.Alg.zero
       done;
       Dbm.posttime z;
       let inv = invariant_of_discrete_state ta ta.init in
       Dbm.intersect z inv;
       Dbm.reduce z
     with
    | EmptyDBM -> failwith "The initial state is empty\n"
    | _ as e -> raise e);
    (ta.init, z)

  (** Constructor Functions *)

  (** This is pretty much a direct translation from LocalLUNormalizer class of Verifix.
      @return a pair (lower,upper) corresponding to L and U values, three-dimensional arrays indexed by  process, location, and clock
      *)

  let _make_lu_table ta =
    let nclocks = VarContext.size ta.clocks in
    let maketable () =
      Array.map
        (fun proc ->
          Array.map
            (fun _ -> Array.init nclocks (fun _ -> -min_int))
            proc.procLocations)
        ta.procs
    in
    let upper = maketable () in
    let lower = maketable () in
    let process iproc iloc g =
      let aux = function
        | GuardLeq (Clock l, Constant r) ->
            upper.(iproc).(iloc).(l) <- max upper.(iproc).(iloc).(l) r
        | GuardLess (Clock l, Constant r) ->
            upper.(iproc).(iloc).(l) <- max upper.(iproc).(iloc).(l) r
        | GuardEqual (Clock l, Constant r) ->
            upper.(iproc).(iloc).(l) <- max upper.(iproc).(iloc).(l) r;
            lower.(iproc).(iloc).(l) <- max lower.(iproc).(iloc).(l) r
        | GuardGreater (Clock l, Constant r) ->
            lower.(iproc).(iloc).(l) <- max lower.(iproc).(iloc).(l) r
        | GuardGeq (Clock l, Constant r) ->
            lower.(iproc).(iloc).(l) <- max lower.(iproc).(iloc).(l) r
        | _ -> failwith "Cannot compute lu bounds: Guards not in normal form."
      in
      List.iter aux g
    in
    let close bounds iproc proc =
      let stable = ref false in
      while not !stable do
        stable := true;
        Array.iter
          (fun loc ->
            List.iter
              (fun edge ->
                let source = edge.edgeSource in
                let target = edge.edgeTarget in
                let sourceBnd = bounds.(iproc).(source) in
                let targetBnd = bounds.(iproc).(target) in
                for j = 1 to VarContext.size ta.clocks - 1 do
                  if not (ClockSet.mem j edge.edgeReset) then
                    if targetBnd.(j) > sourceBnd.(j) then (
                      sourceBnd.(j) <- targetBnd.(j);
                      stable := false)
                done)
              loc.locEdges)
          proc.procLocations
      done
    in
    Array.iteri
      (fun iproc proc ->
        (* Initialize *)
        Array.iteri
          (fun iloc loc ->
            List.iter
              (fun edge -> process iproc iloc edge.edgeGuard)
              loc.locEdges;
            process iproc iloc loc.locInvar)
          proc.procLocations;
        (* Close *)
        close lower iproc proc;
        close upper iproc proc)
      ta.procs;
    (lower, upper)

  (** Constructs a timed_automaton. The constructor function is parameterized
   *  by guard_of_transition and invariant_of_discrete_state so that we can
   *  instantiate it for other kinds of automata (enlarged automata below)
   *  in modules extending the current one.
       @param sys parsed system (Uta.system)
       @param scale scales all constants by scale
       @param enlarge enlarges all constants by enlarge (after having scaled)
       @param (guard_of_transition,invariant_of_discrete_state)
      *)
  let make_ta sys labels =
    let templates = sys.sysTemplates in
    (* Variable and clock contexts have initially keys of type (p,name)
     * where p is process option (None for global variables),
     * and name the name of the variable. *)
    let varcont = VarContext.create () in
    let var_init_values = Hashtbl.create 10 in
    let const_values = Hashtbl.create 10 in
    let clockcont = VarContext.create () in
    VarContext.add clockcont (None, "0");
    let constcont = VarContext.create () in
    (* Function to register global clocks and variables *)
    let register_vars tmp (clocks, vars) =
      List.iter
        (fun cl ->
          try VarContext.add clockcont (tmp, cl) with
          | Var_already_defined ->
              eprintf "Variable %s is already defined\n" cl;
              failwith "Error"
          | _ as e -> raise e)
        clocks;
      List.iter
        (fun var ->
          try
            match var with
            | Var (id, v) ->
                VarContext.add varcont (tmp, id);
                let index = VarContext.index_of_var varcont (tmp, id) in
                Hashtbl.add var_init_values index v
            | ConstVar (id, v) ->
                VarContext.add constcont (tmp, id);
                let index = VarContext.index_of_var constcont (tmp, id) in
                Hashtbl.add const_values index v
          with
          | Var_already_defined ->
              eprintf "Variable %s is already defined\n" (name_of_var var);
              failwith "Error"
          | _ as e -> raise e)
        vars
    in
    (* Register variables: first global ones then local ones *)
    register_vars None (sys.sysClocks, sys.sysVars);
    List.iter
      (fun tmp -> register_vars (Some tmp) (tmp.tempClocks, tmp.tempVars))
      templates;
    let nvars = VarContext.size varcont in
    let is_clock tmp var =
      if VarContext.mem clockcont (Some tmp, var) then true
      else if VarContext.mem clockcont (None, var) then true
      else false
    in
    let get_clock_id tmp var =
      if VarContext.mem clockcont (Some tmp, var) then
        VarContext.index_of_var clockcont (Some tmp, var)
      else VarContext.index_of_var clockcont (None, var)
    in
    (* Convert the given Uta.expression inside the template tmp to the local exp type
       by replacing variable names by their integer identifiers. Also instantiates constants
       and partially evaluates the arithmetic operations *)
    let convert_exp tmp exp =
      let rec eval = function
        | Uppaal_parser.Uta.Constant c -> Constant c
        | Uppaal_parser.Uta.Variable name ->
            (* We first check if the variable is a constant *)
            if VarContext.mem constcont (Some tmp, name) then
              (* Is it a local constant? *)
              let varid = VarContext.index_of_var constcont (Some tmp, name) in
              Constant (Hashtbl.find const_values varid)
            else if VarContext.mem constcont (None, name) then
              (* Is it a global constant ? *)
              let varid = VarContext.index_of_var constcont (None, name) in
              Constant (Hashtbl.find const_values varid)
            else if VarContext.mem varcont (Some tmp, name) then
              (* Is it a local variable? *)
              let varid = VarContext.index_of_var varcont (Some tmp, name) in
              Variable varid
            else if VarContext.mem varcont (None, name) then
              (* Is it a global variable? *)
              let varid = VarContext.index_of_var varcont (None, name) in
              Variable varid
            else if VarContext.mem clockcont (Some tmp, name) then
              (* Local clock *)
              let varid = VarContext.index_of_var clockcont (Some tmp, name) in
              Clock varid
            else if VarContext.mem clockcont (None, name) then
              (* Global clock *)
              let varid = VarContext.index_of_var clockcont (None, name) in
              Clock varid
            else (
              eprintf "Printing final VarContext\n";
              VarContext.iter
                (fun (t, name) ind ->
                  let scope =
                    match t with None -> "None" | Some tmp -> tmp.tempName
                  in
                  eprintf "\t%d <%s> : %s\n" ind name scope)
                varcont;
              printf "%b\n" (VarContext.mem varcont (Some tmp, name));
              failwith (sprintf "Undefined variable <%s>" name))
        | Uppaal_parser.Uta.Sum (e1, e2) -> (
            let ne1 = eval e1 in
            let ne2 = eval e2 in
            match (ne1, ne2) with
            | Constant v1, Constant v2 -> Constant (v1 + v2)
            | _ -> Sum (ne1, ne2))
        | Uppaal_parser.Uta.Subtraction (e1, e2) -> (
            let ne1 = eval e1 in
            let ne2 = eval e2 in
            match (ne1, ne2) with
            | Constant v1, Constant v2 -> Constant (v1 - v2)
            | _ -> Subtraction (ne1, ne2))
        | Uppaal_parser.Uta.Product (e1, e2) -> (
            let ne1 = eval e1 in
            let ne2 = eval e2 in
            match (ne1, ne2) with
            | Constant v1, Constant v2 -> Constant (v1 * v2)
            | _ -> Product (ne1, ne2))
        | Uppaal_parser.Uta.Division (e1, e2) -> (
            let ne1 = eval e1 in
            let ne2 = eval e2 in
            match (ne1, ne2) with
            | Constant v1, Constant v2 -> Constant (v1 / v2)
            | _ -> Division (ne1, ne2))
      in
      eval exp
    in
    let convert_atomic_guard tmp = function
      | Uppaal_parser.Uta.GuardLeq (e1, e2) ->
          GuardLeq (convert_exp tmp e1, convert_exp tmp e2)
      | Uppaal_parser.Uta.GuardLess (e1, e2) ->
          GuardLess (convert_exp tmp e1, convert_exp tmp e2)
      | Uppaal_parser.Uta.GuardGeq (e1, e2) ->
          GuardGeq (convert_exp tmp e1, convert_exp tmp e2)
      | Uppaal_parser.Uta.GuardGreater (e1, e2) ->
          GuardGreater (convert_exp tmp e1, convert_exp tmp e2)
      | Uppaal_parser.Uta.GuardEqual (e1, e2) ->
          GuardEqual (convert_exp tmp e1, convert_exp tmp e2)
      | Uppaal_parser.Uta.GuardNEqual (e1, e2) ->
          GuardNEqual (convert_exp tmp e1, convert_exp tmp e2)
    in
    let convert_guard tmp g =
      (* Make sure clock guards have the form Guard*(Clock(cl),Constant(r)) *)
      let normalize_atomic_guard = function
        | GuardLeq ((_ as k), Clock cl) -> GuardGeq (Clock cl, k)
        | GuardLess ((_ as k), Clock cl) -> GuardGreater (Clock cl, k)
        | GuardGeq ((_ as k), Clock cl) -> GuardLeq (Clock cl, k)
        | GuardGreater ((_ as k), Clock cl) -> GuardLess (Clock cl, k)
        | GuardEqual ((_ as k), Clock cl) -> GuardEqual (Clock cl, k)
        | e -> e
      in
      let aux ag = normalize_atomic_guard (convert_atomic_guard tmp ag) in
      let g = List.map aux g in
      g
    in

    let convert_disc_guard tmp g = List.map (convert_guard tmp) g in

    (* Instantiate constants inside updates *)
    let convert_update tmp up =
      List.map
        (fun (var, exp) ->
          let varid =
            try
              if VarContext.mem varcont (Some tmp, var) then
                VarContext.index_of_var varcont (Some tmp, var)
              else VarContext.index_of_var varcont (None, var)
            with _ -> failwith ("Unknown variable " ^ var)
          in
          (varid, convert_exp tmp exp))
        up
    in

    (* Get discrete guard from mixed guard *)
    let filter_disc_guard tmp g =
      let filt_ag ag =
        List.for_all
          (fun name -> not (is_clock tmp name))
          (vars_of_atomic_guard ag)
      in
      List.map (fun x -> List.filter filt_ag x) g
    in
    (* Get clock guard from mixed guard *)
    (* TODO Also check that there is no variable *)
    let filter_clock_guard tmp g =
      let filt_ag ag =
        List.exists (fun name -> is_clock tmp name) (vars_of_atomic_guard ag)
      in
      let ng =
        List.filter_map
          (fun x ->
            let conj = List.filter filt_ag x in
            if conj = [] then None else Some conj)
          g
      in

      match ng with
      | [] -> []
      | [ t ] -> t
      | _ -> failwith "disjunction of zone"
    in

    (* Make clock reset set *)
    let clock_update tmp up =
      let reset =
        List.map
          (fun (name, _) -> get_clock_id tmp name)
          (List.filter (fun (var, _) -> is_clock tmp var) up)
        |> List.fold_left (fun set cl -> ClockSet.add cl set) ClockSet.empty
      in
      reset
    in
    (* scale all constants by scale and enlarge by enlarge*)
    (*let scale_and_enlarge_cguard g =
      let rec eval = function
        | Sum(e1,e2)  ->
          let ne1 = eval e1 in
          let ne2 = eval e2 in
          (match (ne1,ne2) with
             Constant(v1), Constant(v2) -> Constant(v1 + v2)
           | _ -> Sum(ne1,ne2)
          )
        | Subtraction(e1,e2)  ->
          let ne1 = eval e1 in
          let ne2 = eval e2 in
          (match (ne1,ne2) with
             Constant(v1), Constant(v2) -> Constant(v1 - v2)
           | _ -> Subtraction(ne1,ne2)
          )
        | Product(e1,e2)  ->
          let ne1 = eval e1 in
          let ne2 = eval e2 in
          (match (ne1,ne2) with
             Constant(v1), Constant(v2) -> Constant(v1 * v2)
           | _ -> Product(ne1,ne2)
          )
        | Division(e1,e2)  ->
          let ne1 = eval e1 in
          let ne2 = eval e2 in
          (match (ne1,ne2) with
             Constant(v1), Constant(v2) -> Constant(v1 / v2)
           | _ -> Division(ne1,ne2)
          )
        | e -> e
      in
      let s = Constant(scale) in
      let d = Constant(enlarge) in
      let scale_and_enlarge = function
        | GuardLeq(Clock(_) as e1,e2) -> [GuardLeq(e1,eval (Sum(Product(e2,s),d)))]
        | GuardLess(Clock(_) as e1,e2) -> [GuardLess(e1,eval (Sum(Product(e2,s),d)))]
        | GuardGreater(Clock(_) as e1,e2) -> [GuardGreater(e1,eval (Subtraction(Product(e2,s),d)))]
        | GuardGeq(Clock(_) as e1,e2) -> [GuardGeq(e1,eval (Subtraction(Product(e2,s),d)))]
        | GuardEqual(Clock(_) as e1,e2) ->
          let se2 = Product(e2,s) in
          [GuardGeq(e1,eval (Subtraction(se2,d)));
           GuardLeq(e1,eval (Sum(se2,d)))
          ]
        | other ->
          failwith ("Cannot enlarge non-normalized clock guard: ")
        in
      List.fold_left (fun acc ag -> (scale_and_enlarge ag)@acc) [] g
      in*)
    (* Convert the (template,string) VarContext.t  to string VarContext.t
       by prepending the process names to variables and clocks.
       We will just extract these elements from the hash tables, along with their indices,
       so as to reinsert them in the same order in the new VarContext.
       These contexts are only for pretty printing and have no role in simulation
    *)
    let get_vc_elements vc =
      Hashtbl.fold
        (fun (tmp, name) index acc ->
          let prefix =
            match tmp with None -> "" | Some tmp -> tmp.tempName ^ "."
          in
          let name = sprintf "%s%s" prefix name in
          (index, name) :: acc)
        (VarContext.get_var2index vc)
        []
    in
    let clist = List.sort compare (get_vc_elements clockcont) in
    let vlist = List.sort compare (get_vc_elements varcont) in
    let clocks = VarContext.create () in
    let vars = VarContext.create () in
    List.iter (fun (_, name) -> VarContext.add clocks name) clist;
    List.iter (fun (_, name) -> VarContext.add vars name) vlist;
    let disc_update tmp up =
      List.filter (fun (var, _) -> not (is_clock tmp var)) up
    in
    (* Now we start making the timed automaton *)
    let make_proc id tmp =
      (* Assign integer identifiers to locations by their locId's.
         We assign id i to the i-th location in the list (see below)
      *)
      let open Uppaal_parser.Uta in
      let locids = Hashtbl.create 50 in
      List.iteri (fun i loc -> Hashtbl.add locids loc.locId i) tmp.tempLocations;
      let make_edge edge =
        {
          edgeSource = Hashtbl.find locids edge.edgeSource;
          edgeTarget = Hashtbl.find locids edge.edgeTarget;
          edgeDiscGuard =
            convert_disc_guard tmp (filter_disc_guard tmp edge.edgeGuard);
          edgeGuard =
            (*scale_and_enlarge_cguard*)
            convert_guard tmp (filter_clock_guard tmp edge.edgeGuard);
          edgeReset = clock_update tmp edge.edgeUpdates;
          edgeDiscUpdate = convert_update tmp (disc_update tmp edge.edgeUpdates);
          edgeSync = edge.edgeSync;
          edgeProc = None;
          edgeControllable = true;
          edgeProb = edge.edgeProb;
        }
      in
      let make_loc loc =
        let edges =
          let e =
            List.filter (fun edge -> edge.edgeSource = loc.locId) tmp.tempEdges
          in
          List.map make_edge e
        in
        {
          locId = Hashtbl.find locids loc.locId;
          locName = (if loc.locName <> "" then loc.locName else loc.locId);
          locCommitted = loc.locCommitted;
          locUrgent = loc.locUrgent;
          locInvar = (*scale_and_enlarge_cguard*) convert_guard tmp loc.locInvar;
          locEdges = List.rev edges;
          locProc = None;
        }
      in
      let locslist = List.map make_loc tmp.tempLocations in
      let locs = Array.of_list locslist in
      {
        procName = tmp.tempName;
        procId = id;
        procLocations = locs;
        procInitLoc = Hashtbl.find locids tmp.tempInitialLocation.locId;
      }
    in
    let procs = Array.of_list (List.mapi make_proc templates) in
    (* Fill in the edgeProc and locProc fields in all locations and edges *)
    Array.iter
      (fun proc ->
        Array.iter
          (fun loc ->
            loc.locName <- proc.procName ^ "." ^ loc.locName;
            loc.locProc <- Some proc;
            loc.locEdges |> List.iter (fun edge -> edge.edgeProc <- Some proc))
          proc.procLocations)
      procs;
    let initLocs =
      Array.map (fun proc -> proc.procLocations.(proc.procInitLoc)) procs
    in
    let initVars = Array.make nvars 0 in
    for i = 0 to nvars - 1 do
      initVars.(i) <- Hashtbl.find var_init_values i
    done;
    let ta =
      {
        procs;
        clocks;
        vars;
        init = { stateLocs = initLocs; stateVars = initVars };
        (*query = EmptyQuery;*)
        lowerLU = [||];
        upperLU = [||];
        guards_tbl = Hashtbl.create 1024;
        invars_tbl = Hashtbl.create 1024;
        labels =
          List.map
            (fun (s, (g, l)) ->
              let new_guard =
                List.map
                  (fun x ->
                    List.map (convert_atomic_guard (List.hd templates)) x)
                  g
              in
              let new_loc = l in

              (s, (new_guard, new_loc)))
            labels;
      }
    in

    let lower, upper = _make_lu_table ta in
    ta.lowerLU <- lower;
    ta.upperLU <- upper;
    (* Check restrictions:
     * 1) No discrete variables in clock guards
     *    ( This restriction could actually be lifted
     *      by redefining the hash tables as a function
     *      from state = stateLocs * stateVars)
     *)
    let check_ta ta =
      let check_guard_no_discrete g =
        let aux = function Variable _ -> raise Found | _ -> () in
        let atomic = function
          | GuardLess (e1, e2) ->
              aux e1;
              aux e2
          | GuardLeq (e1, e2) ->
              aux e1;
              aux e2
          | GuardGeq (e1, e2) ->
              aux e1;
              aux e2
          | GuardGreater (e1, e2) ->
              aux e1;
              aux e2
          | GuardEqual (e1, e2) ->
              aux e1;
              aux e2
          | GuardNEqual (e1, e2) ->
              aux e1;
              aux e2
        in
        try List.iter atomic g
        with x ->
          eprintf "Exception in %s\n" (string_of_guard ta g);
          raise x
      in

      try
        ta.procs
        |> Array.iter (fun proc ->
               proc.procLocations
               |> Array.iter (fun loc ->
                      check_guard_no_discrete loc.locInvar;
                      loc.locEdges
                      |> List.iter (fun edge ->
                             check_guard_no_discrete edge.edgeGuard)))
      with
      | Found -> failwith "We do not support discrete variables in clock guards"
      | _ as e -> raise e
    in
    check_ta ta;
    ta

  let bound_all_guard bigT ta =
    (* Do not work for a network of time automaton *)
    assert (Array.length ta.procs = 1);
    let proc = ta.procs.(0) in
    VarContext.add ta.clocks "theta_glob";
    let bound_guard =
      GuardLeq
        (Clock (VarContext.index_of_var ta.clocks "theta_glob"), Constant bigT)
    in
    let bound_guard_eq =
      GuardEqual
        (Clock (VarContext.index_of_var ta.clocks "theta_glob"), Constant bigT)
    in

    let sink_loc =
      {
        locId = Array.length proc.procLocations;
        locName = "sink_loc";
        locCommitted = false;
        locUrgent = false;
        locInvar = [];
        locEdges = [];
        locProc = None;
      }
    in

    ta.procs
    |> Array.iter (fun p ->
           p.procLocations
           |> Array.iter (fun loc ->
                  loc.locEdges <-
                    List.fold_left
                      (fun acc edge ->
                        { edge with edgeGuard = bound_guard :: edge.edgeGuard }
                        :: {
                             edge with
                             edgeTarget = sink_loc.locId;
                             edgeGuard = bound_guard_eq :: edge.edgeGuard;
                           }
                        :: acc)
                      [] loc.locEdges));

    proc.procLocations <- Array.append proc.procLocations [| sink_loc |];

    {
      ta with
      labels =
        List.map
          (function
            | "accepting", (a, _) -> ("accepting", (a, [ sink_loc.locId ]))
            | x -> x)
          ta.labels;
    }
end

module Uautomaton = struct
  module Dbm = Guard
  include GenericUautomaton (Guard)
end
