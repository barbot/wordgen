(*

Copyright (C) 2019 LACL Universite Paris Est Creteil Val de Marne
Authors: Benoît Barbot
Licence: GPL-3.0-or-later

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*)

open Symrob_extract
open Symrob_common


let open_tikz fn =
  let fd = open_out fn in
  let ftikz = Format.formatter_of_out_channel fd in
  Format.fprintf ftikz
    "\\documentclass{article}\n\
     \\usepackage[utf8]{inputenc}\n\
     \\usepackage{tikz}\n\
     \\usepackage{verbatim}\n\
     \\usepackage[pdf]{graphviz}\n\
     \\usepackage{morewrites}\n\
     \\usepackage{dot2texi}\n\
     \\usetikzlibrary{arrows}\n\
     \\usetikzlibrary{shapes}\n\
     \\usetikzlibrary{decorations.pathmorphing,calc,shapes,shapes.geometric,patterns}\n\
     \\begin{document}";
  (ftikz, fd)

module ExactReachability (Model : TimedAutomaton.TIMED_AUTOMATON) = struct
  module Dbm = Model.Dbm
  module Model = Model

  (* module LocZone = struct
       type t = Model.discrete_state * Dbm.t
       let equal (l1,z1) (l2,z2) =
         if Model.is_state_equal ta l1 l2 then
           Dbm.equal z1 z2
         else false
     end*)

  type zg_edge = {
    action : ZoneGraph.l;
    source : zg_node;
    prob : float;
    mutable guard : Dbm.t;
    reset : ClockSet.t;
    target : zg_node;
    mutable is_removed : bool;
    (* an edge can still be in candidate for splitting queue even if is as been removed*)
    mutable modified : bool;
  }

  and zg_node = {
    mutable id : int;
    disc_state : Model.discrete_state;
    mutable entry_zone : Dbm.t;
    mutable edges : zg_edge list;
    mutable back_edges : zg_edge list;
    mutable initial : bool;
    mutable accepting : bool;
    mutable visited : bool;
  }

  type state_space = (int, zg_node) Hashtbl.t

  module DBMHashSet = Weak.Make (struct
    include Dbm

    let hash x = Hashtbl.hash_param 100 1000 x
  end)

  let weak_dbm_hash_set = DBMHashSet.create 128

  let memoize_dbm x =
    match DBMHashSet.find_opt weak_dbm_hash_set x with
    | None ->
        DBMHashSet.add weak_dbm_hash_set x;
        x
    | Some y -> y

  let print_edge ta o edge =
    Format.fprintf o "([%a],%i-[%s%s]->%i), "
      (fun o x -> Dbm.pretty_print o ~context:(Model.clocks ta) x)
      edge.guard edge.source.id edge.action
      (if edge.prob = 1.0 then "" else "," ^ string_of_float edge.prob)
      edge.target.id

  let print_zone ta o zgn =
    let ez = Dbm.copy zgn.entry_zone in
    Dbm.remove_useless ez;
    Format.fprintf o "LocZone %i: %a -> { %a }" zgn.id
      (fun o2 -> Model.print_extended_state o2 ta)
      (zgn.disc_state, ez)
      (fun o2 el -> List.iter (fun e -> print_edge ta o2 e) el)
      zgn.edges

  let pb ta f = function
    | Common.Infinite -> Format.fprintf f "inf"
    | Common.Const a -> Format.fprintf f "%i" a
    | Common.Finite (a, b) -> Format.fprintf f "%i-%a" a (Model.print_var ta) b

  let remove_edge e =
    e.is_removed <- true;
    e.target.back_edges <-
      List.filter (fun x -> not (x == e)) e.target.back_edges;
    e.source.edges <- List.filter (fun x -> not (x == e)) e.source.edges

  let remove_empty ta stateset =
    let queue = Queue.create () in
    Hashtbl.iter (fun _ st -> Queue.add st queue) stateset;
    while not (Queue.is_empty queue) do
      let st = Queue.pop queue in
      if
        st.id <> 0 && st.edges = []
        && (not @@ Model.eval_accepting ta st.disc_state)
      then (
        List.iter
          (fun e ->
            Queue.add e.source queue;
            remove_edge e)
          st.back_edges;
        Hashtbl.remove stateset st.id)
    done;
    let nstateset = Hashtbl.create (Hashtbl.length stateset) in
    Hashtbl.add nstateset 0 (Hashtbl.find stateset 0);
    if Hashtbl.length nstateset <> Hashtbl.length stateset (*&& false*) then (
      ignore
      @@ Hashtbl.fold
           (fun oid st i ->
             if oid <> 0 then (
               st.id <- i;
               Hashtbl.add nstateset i st;
               i + 1)
             else i)
           stateset 1;
      nstateset)
    else stateset

  let string_of_var ta i =
    let context = Model.clocks ta in
    let s = Uppaal_parser.Uta.VarContext.index2var context i in
    try
      let i = String.rindex s '.' in
      String.sub s (i + 1) (String.length s - i - 1) |> escape_tex
    with Not_found -> escape_tex s

  let print_svg ?(c1 = 1) ?(c2 = 2) ta f st =
    let context = Model.clocks ta in
    let bounds = Dbm.max_bounds st.entry_zone in
    let x = if c1 >= Array.length bounds then 0 else c1 in
    let y = if c2 >= Array.length bounds then 0 else c2 in
    st.edges
    |> List.iter (fun edge ->
           let b2 = Dbm.max_bounds edge.guard in
           Array.iteri (fun i a -> bounds.(i) <- max a b2.(i)) bounds);

    Format.fprintf f
      "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><svg \
       version=\"1.1\" width=\"%i\" height=\"%i\" \
       xmlns=\"http://www.w3.org/2000/svg\"><g transform=\"translate(10 \
       -10)\">"
      (40 * (bounds.(x) + 1))
      (40 * (bounds.(y) + 1));

    Dbm.to_svg ~grid:true
      ~scope:"stroke=\"red\" fill=\"red\" stroke-width=\"5\"" context bounds
      (x, y) f st.entry_zone;

    st.edges
    |> Common.list_iter_colour (fun edge col ->
           let scope =
             Format.sprintf "stroke=\"%s\" fill=\"%s\" stroke-width=\"3\"" col
               col
           in
           (Dbm.to_svg ~grid:false ~scope context bounds (x, y)) f edge.guard);

    Dbm.to_svg ~grid:false
      ~scope:"stroke=\"red\" fill=\"red\" stroke-width=\"5\"" context bounds
      (x, y) f st.entry_zone;

    Format.fprintf f "<text x=\"%i\" y=\"%i\">[%i]%s</text>"
      (20 * (bounds.(x) + 1))
      30 st.id
      (Model.string_of_state ta st.disc_state);

    Format.fprintf f "</g></svg>@.";
    (40 * (bounds.(x) + 1), 40 * (bounds.(y) + 1))

   let print_graph_dot ?(direct = true) ta f g =
    (*let g = remove_empty ta g2 in*)
    (*Format.fprintf f "digraph G%i {" (Hashtbl.length g);*)
    g
    |> Hashtbl.iter (fun _ s ->
           let id = Array.init (Dbm.dimension s.entry_zone) (fun i -> i) in
           let ez = Dbm.copy s.entry_zone in
           Dbm.remove_useless ez;
           Format.fprintf f "%i [%sshape=%s, label = \"[%i] %a\"];\n" s.id
             (if s.visited then (
                s.visited <- false;
                "color=blue, ")
              else "")
             (if Model.eval_accepting ta s.disc_state then "box3d" else "box")
             s.id
             (fun f -> Model.print_extended_state f ta)
             (s.disc_state, ez);
           List.iter
             (fun e ->
               let g = Dbm.copy e.guard in
               Dbm.remove_useless g;
               let issplit = Dbm.is_splitted g in
               Format.fprintf f "%i -> %i [%s%s label=\"%s, %a\"];\n"
                 e.source.id e.target.id
                 (if issplit then
                    match Dbm.get_lower_upper id g with
                    | Punctual _ -> "color=green,"
                    | _ -> "color=red,"
                  else "")
                 (if e.modified then (
                    e.modified <- false;
                    "color=blue,")
                  else "")
                 e.action
                 (fun f ->
                   if issplit then fun g ->
                     let g = Dbm.get_lower_upper id g in
                     Common.print_guard (pb ta) f g
                   else fun x -> Dbm.pretty_print f ~context:(Model.clocks ta) x)
                 g)
             (if direct then s.edges else s.back_edges))

  (*Format.fprintf f "}@."*)

  let add_edge e =
    e.target.back_edges <- e :: e.target.back_edges;
    e.source.edges <- e :: e.source.edges

  let post ta ~extrapolate st (_, g, r, _) =
    let z = Dbm.copy st.entry_zone in
    Dbm.posttime z;
    (* intersect invariant *)
    Dbm.intersect z (Model.invariant_of_discrete_state ta st.disc_state);
    (*extrapolate*)
    extrapolate z;
    Dbm.intersect z g;
    Dbm.reduce z;
    let g2 = Dbm.copy z in
    Dbm.reset z r;
    Dbm.reduce z;
    (memoize_dbm g2, memoize_dbm z)

  let resetAll ta zone =
    let allclocks = ref ClockSet.empty in
    Uppaal_parser.Uta.VarContext.iter
      (fun _ i -> allclocks := ClockSet.add i !allclocks)
      (Model.clocks ta);
    Dbm.reset zone !allclocks;
    Dbm.reduce zone

  let print_reach ?debug st =
    match debug with
    | Some ((ftikz, _), ta, _) ->
        let context = Model.clocks ta in
        let ppdbm f = Dbm.pretty_print f ~do_escape_tex:true ~context in
        Format.fprintf ftikz
          "\\section{Forward reach: %i }\n\
          \          $%a$\n\
          \          entry zone:\\\\$%a$\\\\" st.id
          (fun f -> Model.print_extended_state f ~do_escape_tex:true ta)
          (st.disc_state, st.entry_zone)
          ppdbm st.entry_zone;

        let c1 = 1 and c2 = 2 in
        (let z = None in
         Format.fprintf ftikz
           "\\begin{tikzpicture}[z={(-0.3cm,-0.6cm)}]@[<v 1>@;";

         Dbm.to_tikz ~grid:false ~scope:"color=red,fill=red, ultra thick"
           context (c1, c2) ?z ftikz st.entry_zone;

         st.edges
         |> Common.list_iter_colour (fun edge col ->
                let scope = Format.sprintf "color=%s,fill=%s, thick" col col in
                (Dbm.to_tikz ~grid:true ~scope context ?z (c1, c2))
                  ftikz edge.guard);

         Format.fprintf ftikz "@]\\end{tikzpicture}@.")
        (*gl
          |> List.iter (fun edge ->
          Format.fprintf ftikz "\\begin{tikzpicture}[color=blue,fill=blue]@[<v 1>@; %a@]@;\\end{  tikzpicture}@."
          (*(fun f -> Dbm.pretty_print f (Model.clocks ta)) edge*)
          (Dbm.to_tikz (Model.clocks ta) (c1,c2)) edge;
          )*);

        st.edges
        |> Common.list_iter_colour (fun edge col ->
               Format.fprintf ftikz
                 "\\\\edge %s $ \\textcolor{%s}{\\rightarrow %i : %a}$@."
                 edge.action col edge.target.id
                 (fun f ->
                   Dbm.pretty_print ~do_escape_tex:true
                     ~context:(Model.clocks ta) f)
                 edge.guard)
    | _ -> ()

  module LocZone = struct
    type t = Model.concrete_discrete_state * Dbm.t

    let equal (a1, a2) (b1, b2) =
      if a1 <> b1 then false
      else if a2 == b2 then (*print_endline "hit";*)
        true
      else a2 = b2

    let hash (x, y) = Hashtbl.hash_param 100 1000 (x, y)
    (*let y2 = Obj.repr y in
      let y3 = Obj.with_tag Obj.int_tag y2 in
      Hashtbl.hash (x, (Obj.obj y3 : int))*)
  end

  module LocZoneHashtbl = Hashtbl.Make (LocZone)

  let check_one_reset ta rg =
    if Uppaal_parser.Uta.VarContext.size (Model.clocks ta) > 1 then
      rg
      |> Hashtbl.iter (fun _ loczone ->
             loczone.edges
             |> List.iter (fun edge ->
                    if ClockSet.is_empty edge.reset then (
                      Format.eprintf "@.No Reset : %a@. edge:%s@."
                        (fun f -> Model.print_extended_state f ta)
                        (loczone.disc_state, loczone.entry_zone)
                        edge.action;
                      assert false)))

  let check_determinism rg =
    try
      rg
      |> Hashtbl.iter (fun _ loczone ->
             loczone.edges
             |> List.iter (fun e1 ->
                    let g1 = Dbm.copy loczone.entry_zone in
                    Dbm.posttime g1;
                    Dbm.intersect g1 e1.guard;
                    loczone.edges
                    |> List.iter (fun e2 ->
                           if
                             e1.target.id <> e2.target.id
                             && e1.action = e2.action
                           then (
                             let inter = Dbm.copy g1 in
                             Dbm.intersect inter e2.guard;
                             try
                               Dbm.reduce inter;
                               if not (Dbm.punctual inter) then raise Exit
                             with Pdbm.EmptyDBM -> ()))));
      true
    with Exit -> false

  let reach ?debug ta =
    let idcount = ref 0 in
    let new_state loc zone =
      let a =
        {
          id = !idcount;
          disc_state = loc;
          entry_zone = zone;
          edges = [];
          back_edges = [];
          initial = !idcount = 0;
          accepting = Model.eval_accepting ta loc;
          visited = false;
        }
      in
      incr idcount;
      a
    in

    (*let dim = Uppaal_parser.Uta.VarContext.size (Model.clocks ta) + 1 in
      let int_lbounds = Array.make dim 0 in
        let int_ubounds = Array.make dim 0 in*)
    let int_max_bound = Model.max_bound ta in
    let stmap = LocZoneHashtbl.create 100 in
    let to_visit_list = Queue.create () in
    let linit, zinit = Model.initial_extended_state ta in
    let zinit2 =
      let zi = Dbm.copy zinit in
      Dbm.reduce zi;
      memoize_dbm zi
    in

    resetAll ta zinit2;
    let init = new_state linit zinit2 in
    LocZoneHashtbl.add stmap (Model.to_concrete linit, zinit2) init;
    Queue.add init to_visit_list;

    while not (Queue.is_empty to_visit_list) (*&& !idcount <1000*) do
      let st = Queue.pop to_visit_list in
      if !idcount mod 1000 = 0 && debug <> None then (
        Format.printf "\027[1000DComputing forward reachability graph ... %i "
          !idcount;
        Format.print_flush ());
      (*let context = Model.clocks ta in
        let ppdbm f = Dbm.pretty_print f context in
        Format.fprintf Format.std_formatter "%a:%a@."
          (fun f -> Model.print_extended_state f ta)
          (st.disc_state, st.entry_zone)
          ppdbm st.entry_zone);*)
      st.visited <- true;
      Model.transitions_from ta
        ~nosync:(Model.is_single_module ta)
        st.disc_state
      |> List.iter (fun edge ->
             let ((_, _, r, t) as e) = Model.transition_fields ta edge in
             (* z is the entry zone in post state; g2 is the guard intersected with TE(entryzone) and invariant  *)
             try
               let g2, z =
                 post
                   ta
                   (*~extrapolate:(fun z ->
                     Model.lu_bounds ta st.disc_state int_lbounds int_ubounds;
                     Dbm.extrapolate_lu z int_lbounds int_ubounds)*)
                   ~extrapolate:(fun z -> Dbm.cClosure z int_max_bound)
                   st e
               in

               let tstate =
                 match
                   LocZoneHashtbl.find_opt stmap (Model.to_concrete t, z)
                 with
                 | Some ps -> ps
                 | None ->
                     let ns = new_state t z in
                     LocZoneHashtbl.add stmap (Model.to_concrete t, z) ns;
                     Queue.add ns to_visit_list;
                     ns
               in
               add_edge
                 {
                   action =
                     (match Model.get_transition_action edge with
                     | None -> "t"
                     | Some s -> s);
                   source = st;
                   guard = g2;
                   prob = Model.get_transition_prob edge;
                   reset = r;
                   target = tstate;
                   is_removed = false;
                   modified = false (*for debug*);
                 }
             with Pdbm.EmptyDBM -> ());
      (* Guard unsatisfiable *)
      print_reach ?debug st
    done;
    let tab = Hashtbl.create (LocZoneHashtbl.length stmap) in
    LocZoneHashtbl.iter (fun _ s -> Hashtbl.add tab s.id s) stmap;
    check_one_reset ta tab;
    tab

  let print_split1 ?debug entry edge guardo cmpt il nig =
    match debug with
    | Some ((ftikz, _), ta, _) ->
        let context = Model.clocks ta in
        let guard = Dbm.copy guardo in
        let ppdbm f =
          Dbm.pretty_print f ~do_escape_tex:true ~minimalist:false ~context
        in
        Format.fprintf ftikz
          "\\section{Split %i: $%i -(%s)-> %i$}entry \
           zone:\\\\$%a$\\\\candidate guard:\\\\$%a$\\\\"
          cmpt edge.source.id edge.action edge.target.id ppdbm entry ppdbm guard;
        List.iter
          (fun reg -> Format.fprintf ftikz "not in guard: $%a$\\\\" ppdbm reg)
          nig;

        let pv x = Dbm.(escape_tex @@ string_of_var context x) in
        let cl =
          match il with
          | [ t1; t2 ] -> [ (t1, t2) ]
          | [ t1 ] ->
              Format.fprintf ftikz ": %s \\\\" (pv t1);
              [ (t1, t1) ]
          | [ t1; t2; t3 ] ->
              Format.fprintf ftikz "trisplit: %s %s %s \\\\" (pv t1) (pv t2)
                (pv t3);
              [ (t1, t2); (t1, t3); (t2, t3) ]
          | [ t1; t2; t3; t4 ] ->
              Format.fprintf ftikz "foursplit: %s %s %s %s\\\\" (pv t1) (pv t2)
                (pv t3) (pv t4);
              [ (t1, t2); (t1, t3); (t1, t4); (t2, t3); (t2, t4); (t3, t4) ]
          | _ -> assert false
        in
        cl
        |> List.iter (fun (c1, c2) ->
               Format.fprintf ftikz
                 "%i\\begin{tikzpicture}@[<v 1>@;%a@;%a@;\\end{tikzpicture}@."
                 edge.source.id
                 (Dbm.to_tikz context (c1, c2))
                 guard
                 (Dbm.to_tikz ~scope:"color=red,fill=red, ultra thick"
                    ~grid:false context (c1, c2))
                 entry
               (*gl
                 |> List.iter (fun edge ->
                 Format.fprintf ftikz "\\begin{tikzpicture}[color=blue,fill=blue]@[<v 1>@; %a@]@;\\end{  tikzpicture}@."
                 (*(fun f -> Dbm.pretty_print f (Model.clocks ta)) edge*)
                 (Dbm.to_tikz (Model.clocks ta) (c1,c2)) edge;
                 )*));
        (*|< List.iter (fun edge -> Dbm.remove_useless edge; Format.printf "splitted guard: %a\n"
          (fun   f -> Dbm.pretty_print f (Model.clocks ta)) edge)*)
        cl
    | _ -> []

  let print_split2 ?debug nsource cl =
    match debug with
    | Some ((ftikz, _), ta, _) ->
        let ng = Dbm.copy nsource.entry_zone in
        Dbm.remove_useless ng;
        assert (Dbm.is_splitted ng);
        let context = Model.clocks ta in
        if List.length cl > 1 then Format.fprintf ftikz "\\\\";
        cl
        |> List.iter (fun couple ->
               Format.fprintf ftikz
                 "%i\\begin{tikzpicture}[color=blue,fill=blue]@[<v 1>@;"
                 nsource.id;
               (*Dbm.to_tikz context couple ftikz ng;*)
               Dbm.to_tikz ~scope:"color=red,fill=red, ultra thick" ~grid:false
                 context couple ftikz nsource.entry_zone;

               nsource.edges
               |> Common.list_iter_colour (fun edge col ->
                      let scope =
                        Format.sprintf "color=%s,fill=%s, thick" col col
                      in
                      (Dbm.to_tikz ~grid:true ~scope context couple)
                        ftikz edge.guard);

               Format.fprintf ftikz "\\end{tikzpicture}@.");
        if List.length cl > 1 then Format.fprintf ftikz "\\\\";
        nsource.edges
        |> Common.list_iter_colour (fun edge col ->
               assert (nsource.id = edge.source.id);
               Format.fprintf ftikz
                 "\\\\ %i\\textcolor{%s}{ $\\rightarrow %i : %s , %a$} @."
                 nsource.id col edge.target.id edge.action
                 (fun a -> Dbm.pretty_print a ~do_escape_tex:true ~context)
                 edge.guard)
    | _ -> ()

  let print_split_graph ?debug cmp stateset =
    match debug with
    | Some ((ftikz, _), ta, x) when x > 1 ->
        Format.fprintf ftikz "\\\\\\digraph[width=\\textwidth]{g%i}{%a}@." cmp
          (fun f h -> ignore @@ print_graph_dot ~direct:true ta f h)
          stateset
    | _ -> ()

  (* Intersect guard with TE(source.entry_zone) and Unreset( target.entry_zone) *)
  let clean_edge e =
    let nguard = Dbm.copy e.guard in
    let past = Dbm.copy e.source.entry_zone in
    Dbm.posttime past;
    Dbm.intersect nguard past;
    let unr = Dbm.copy e.target.entry_zone in
    Dbm.unreset unr e.reset;
    Dbm.intersect nguard unr;
    if e.guard <> nguard then (
      Dbm.reduce nguard;
      e.guard <- memoize_dbm nguard)

  let check_duplicate stateset loczone =
    (* should not be necessary the splitting should not produce duplicate *)
    try
      Hashtbl.iter
        (fun k v ->
          if k <> loczone.id then
            if v.entry_zone = loczone.entry_zone then
              if
                List.length loczone.edges = List.length v.edges
                && List.for_all2
                     (fun e1 e2 ->
                       e1.action = e2.action && e1.target == e2.target
                       && e1.guard == e2.guard && e1.prob = e2.prob)
                     loczone.edges v.edges
                && List.length loczone.back_edges = List.length v.back_edges
                && List.for_all2
                     (fun e1 e2 ->
                       e1.action = e2.action && e1.source == e2.source
                       && e1.guard == e2.guard && e1.prob = e2.prob)
                     loczone.back_edges v.back_edges
              then raise Exit)
        stateset
    with Exit ->
      List.iter remove_edge loczone.back_edges;
      List.iter remove_edge loczone.edges

  (* Clean all edges from state if not removed moved to cand *)
  let update_state cand state =
    state.visited <- true;
    (* Just for debug *)
    state.edges
    |> List.filter (fun e ->
           try
             clean_edge e;
             e.modified <- true;
             (* Just for debug *)
             Queue.add e cand;
             false
           with Pdbm.EmptyDBM -> true)
    |> List.iter remove_edge;
    state.back_edges
    |> List.filter (fun e ->
           try
             clean_edge e;
             e.modified <- true;
             (* Just for debug *)
             Queue.add e cand;
             false
           with Pdbm.EmptyDBM -> true)
    |> List.iter remove_edge;

    if state.edges = [] && not state.accepting then
      List.iter remove_edge state.back_edges;

    if state.back_edges = [] && not state.initial then
      List.iter remove_edge state.edges

  let add_state stateset state =
    let nid = Hashtbl.length stateset in
    let nstate = { state with id = nid } in
    Hashtbl.add stateset nid nstate;
    nstate

  (* Make a copy of a state with a new entry zone. Copy all edges to and from this state *)
  let duplicate_state stateset state nz =
    let nid = Hashtbl.length stateset in
    let nloczone =
      { state with id = nid; entry_zone = nz; edges = []; back_edges = [] }
    in
    let direct = state.edges and back = state.back_edges in
    (* direct *)
    List.iter
      (fun e ->
        let e2 = { e with source = nloczone } in
        add_edge e2)
      direct;
    (*back*)
    List.iter
      (fun e ->
        let e2 = { e with target = nloczone } in
        add_edge e2)
      back;
    (*loop *)
    List.iter
      (fun e ->
        if e.target.id = e.source.id then
          let e2 = { e with source = nloczone; target = nloczone } in
          add_edge e2)
      direct;
    Hashtbl.add stateset nid nloczone;
    nloczone

  (* An helper function combining map, filter and indicate whether an element is the last *)
  let rec map_last f = function
    | [] -> []
    | [ t ] -> [ f true t ]
    | t :: q ->
        let t2 = f false t in
        t2 :: map_last f q

  let split_zone cmpt ?debug stateset cand edge =
    let st = edge.source in
    let entry = Dbm.copy st.entry_zone in

    (* Compute entry zone not covered by guard *)
    let guard_down = Dbm.copy edge.guard in
    Dbm.pretime guard_down;
    Dbm.reduce guard_down;
    let not_in_guard = Dbm.minus entry guard_down in

    (* Split the guard into a list of guard *)
    let gl, il = Dbm.split_guard edge.guard in

    (* Split if there is something to split *)
    if List.length not_in_guard (*>1 ||*) + List.length gl > 1 then (
      (* Some printing *)
      let cl = print_split1 ?debug entry edge edge.guard cmpt il not_in_guard in
      gl @ not_in_guard
      |> List.map (fun nz ->
             Dbm.pretime nz;
             Dbm.intersect nz entry;
             Dbm.reduce nz;
             memoize_dbm nz)
      (* for each new guard or entry state duplicate the origin state *)
      |> map_last (fun last nz ->
             let nsource =
               if last then (
                 (* reuse state to avoid cleaning *)
                 edge.source.entry_zone <- nz;
                 edge.source)
               else
                 (* duplicate the state *)
                 let ns = duplicate_state stateset edge.source nz in
                 ns
             in
             nsource)
      (* For each state update all guards *)
      |> List.iter (fun nsource ->
             update_state cand nsource;
             (*check_duplicate stateset nsource;*)
             print_split2 ?debug nsource cl);
      true)
    else false

  let split ?debug stateset =
    let cmp = ref 1 in

    (* Add all edges to cand *)
    let cand = Queue.create () in
    stateset
    |> Hashtbl.iter (fun _ loczone ->
           loczone.edges |> List.iter (fun edge -> Queue.add edge cand));

    (* Split all guard until cand is empty *)
    while not (Queue.is_empty cand) (*&& !cmp < 1000*) do
      incr cmp;
      if !cmp mod 1000 = 0 && debug <> None then (
        Format.printf "\027[1000DSplitting reachability graph [%i,%i,%i] " !cmp
          (Hashtbl.length stateset)
          (DBMHashSet.count weak_dbm_hash_set);
        Format.print_flush ());
      let edge = Queue.pop cand in

      if not edge.is_removed then
        let have_changed = split_zone !cmp ?debug stateset cand edge in
        (* Print new state *)
        if have_changed then print_split_graph ?debug !cmp stateset
    done;
    stateset

  let print_reach_graph ta o stateset =
    (*let stateset = remove_empty ta stateset2 in*)
    let pz = print_zone ta in
    let init = Hashtbl.find stateset 0 in
    Format.fprintf o "%a\n" pz init;
    Hashtbl.iter
      (fun _ loczone ->
        if loczone.id <> init.id then Format.fprintf o "%a\n" pz loczone)
      stateset

  let to_zone_graph_trans to_bound ta edge =
    let open ZoneGraph in
    let n = Dbm.dimension edge.guard in
    (*Dbm.remove_useless edge.guard;*)
    let me =
      {
        target = edge.target.id;
        reset =
          Array.init n (fun i ->
              if i = 0 then false
              else if ClockSet.mem i edge.reset then true
              else false);
        prob = edge.prob;
      }
    in

    (*Format.printf "edge  [%i]-%s->[%i]@." edge.source.id edge.action
      edge.target.id;*)

    (*Format.printf "%a@." (fun f -> Dbm.pretty_print f (Model.clocks ta)) edge.guard;*)
    let bound_guard =
      try to_bound edge.guard
      with Pdbm.Not_split ->
        Dbm.remove_useless edge.guard;
        Format.eprintf "Not split edge  [%i]-%s->[%i]@." edge.source.id
          edge.action edge.target.id;
        Format.eprintf "%a:@." (print_edge ta) edge;
        Format.eprintf "matrix @.%a@." Dbm.print edge.guard;
        Dbm.reduce edge.guard;
        Format.eprintf "reduce @.%a@." Dbm.print edge.guard;
        assert false
    in
    {
      action = edge.action;
      miniedge = [ me ];
      bound_guard;
      is_contained_in_zone = false;
      weight = Array.make 0 (-1, (), (), ());
    }

  let to_zone_graph_loczone to_bound ta (lz : zg_node) =
    let disc_name = String.trim @@ Model.string_of_state ta lz.disc_state in
    Format.fprintf Format.str_formatter "@[<h>%a@]"
      (fun f -> Dbm.pretty_print f ~context:(Model.clocks ta))
      lz.entry_zone;
    let init_zone =
      match Format.flush_str_formatter () with "true" -> "" | x -> x
    in

    let trim_edge =
      List.filter
        (fun e ->
          e.target.edges <> [] || Model.eval_accepting ta e.target.disc_state)
        lz.edges
    in
    let open ZoneGraph in
    let cardclocks = Dbm.dimension lz.entry_zone - 1 in
    let redcoord = Dbm.get_reduced_coordinate lz.entry_zone in
    let disSet = toDisjointSet redcoord (cardclocks + 1) in
    let clockmap =
      Array.init (cardclocks + 1) (fun i ->
          if i = 0 then 0 else Puf.find disSet i)
    in

    {
      disc_name;
      init_zone;
      id = lz.id;
      redcoord = Dbm.get_reduced_coordinate lz.entry_zone;
      clockmap;
      transition =
        List.map (to_zone_graph_trans (to_bound clockmap) ta) trim_edge;
      loc_weight = ();
      is_accepting = Model.eval_accepting ta lz.disc_state;
      svg_graph = (fun f -> print_svg ta f lz);
    }

  let to_zone_graph_aux to_bound ta full_stateset =
    (*let stateset = full_stateset in*)
    let stateset = remove_empty ta full_stateset in
    let size = Hashtbl.length stateset in
    let init = Hashtbl.find stateset 0 in
    let statelist = Array.make size (to_zone_graph_loczone to_bound ta init) in
    Hashtbl.iter
      (fun _ loczone ->
        statelist.(loczone.id) <- to_zone_graph_loczone to_bound ta loczone)
      stateset;
    let alphabet =
      Array.fold_left
        (fun acc st ->
          st.ZoneGraph.transition
          |> List.fold_left
               (fun acc2 tr -> Common.StringSet.add tr.ZoneGraph.action acc2)
               acc)
        Common.StringSet.empty statelist
      |> Common.StringSet.to_seq |> Array.of_seq
    in

    let vcontext = Model.clocks ta in
    let nclock = Uppaal_parser.Uta.VarContext.size vcontext in
    let var_string =
      Array.init
        (1 + nclock + 1 + 1)
        (function
          | 0 -> ("t", false) (* The variable 0 is overloaded as 0 and t ! *)
          | i when i = nclock -> ("z", false)
          | i when i = nclock + 1 -> ("s", false)
          | i when i = nclock + 2 -> ("T", false)
          | i ->
              let vn = Uppaal_parser.Uta.VarContext.var_of_index vcontext i in
              let vn2 = Filename.extension vn in
              if vn2 = "" then (vn, true)
              else (String.sub vn2 1 (String.length vn2 - 1), true))
    in
    {
      ZoneGraph.init = 0;
      nb_poly = 0;
      number_of_component = Model.nb_of_processes ta;
      statelist;
      var_string;
      cardclocks = Dbm.dimension init.entry_zone - 1;
      alphabet;
      printer = (fun ?pretty:_ _ _ -> ());
      is_deterministic = check_determinism full_stateset;
    }

  let to_zone_graph = to_zone_graph_aux Dbm.get_lower_upper
  let to_zone_graph_no_split = to_zone_graph_aux Dbm.get_lower_upper_no_split
end

open TimedAutomaton
module Bfs = ExactReachability (Uautomaton)
