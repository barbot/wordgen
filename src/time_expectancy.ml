(*

Copyright (C) 2019 LACL Universite Paris Est Creteil Val de Marne
Authors: Benoît Barbot
Licence: GPL-3.0-or-later

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *)

open Common
module ZGI = ZoneGraphInput.Make (ZoneGraphInput.LinearBound)

(*Building the zone graph *)
let rg = ZGI.input_zone_graph ~verbose:0 Sys.argv.(1)
let poly = if Array.length Sys.argv > 2 then int_of_string Sys.argv.(2) else 4

(* Build the polynomial ring *)
module P =
  Polynomial.Make
    (Flq.Q)
    (struct
      let var_string = rg.ZoneGraph.var_string
    end)

module Weight =
  ExpPoly.Make
    (P)
    (struct
      let smp = None
      let tvar = P.var_of_int 0
      let svar = P.var_of_int (P.nb_var - 1)
    end)
(* Build the functionnal iterator module based on the polynomial ring *)

module FunIt = Semantic.Make (ZoneGraphInput.LinearBound) (Weight)

let compute_weight rg =
  let svar = P.var_of_int (P.nb_var - 1) in
  let weights, rgpoly = FunIt.iterate_functionnal rg poly in
  let weight = weights.(rgpoly.init) in
  let diffw = Weight.diff weight svar in

  (*Format.printf "v: %a@.v': %a@." Weight.print weight Weight.print diffw;*)
  (weight, diffw)

let compv (w, d) s =
  if s = 0.0 then (
    let svar = Weight.var_of_int (P.nb_var - 1) in
    let we = Weight.taylor_exp_s w poly in
    (*Format.printf "vte: %a@." P.print we;*)
    let de = Weight.taylor_exp_s d (poly + 1) in
    Format.printf "vte: %a@." Weight.print de;
    let w0 = Weight.apply_const ~smp:0.0 we svar Weight.F.zero in
    let d0 = Weight.apply_const ~smp:0.0 de svar Weight.F.zero in
    match (Weight.to_float w0, Weight.to_float d0) with
    | Some v1, Some v2 -> (v1, v2)
    | _ -> failwith "fail to evaluate")
  else
    match (Weight.to_float ~smp:s w, Weight.to_float ~smp:s d) with
    | Some v1, Some v2 -> (v1, v2)
    | _ -> failwith "fail to evaluate"

let _ =
  let fv = compute_weight rg in
  for i = -10 to 10 do
    let freq = float i /. 0.1 in
    let v, diff = compv fv freq in
    Format.printf "%g\t%g\t%g\t%g@." freq v diff (-.diff /. v)
  done
