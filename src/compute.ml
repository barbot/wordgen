open Weight_structure

type param_type = No | Fixed of float | ToCompute of float

module type PARAM = sig
  val var_string : Common.VarSet.varset
  val exact_rationnal : bool
  val is_timed : bool
  val use_heaviside : float option
  val verbose : int
  val npoly : int
  val z_param : param_type
  val s_param : param_type
  val is_deterministic : bool
  val perturbation : float option
  val out_format : OutFormat.output_style
  val is_interactive : bool
end

let gen_param ~perturbation ~out_format ~is_interactive var_string rational_impl
    frequency expected_duration is_exact_duration boltzmann_param expected_size
    verbose npoly is_det =
  (module struct
    let exact_rationnal = rational_impl
    let var_string = var_string

    let use_heaviside =
      match expected_duration with
      | Some f when (not @@ Float.is_integer f) && is_exact_duration -> Some f
      | _ -> None

    let is_timed =
      Array.exists (function _, true -> true | _ -> false) var_string

    let s_param =
      match
        ( frequency,
          expected_duration,
          use_heaviside,
          is_exact_duration,
          is_timed )
      with
      (* No control over duration *)
      | None, None, _, _, _ -> No
      | None, Some _, None, true, true -> No
      (* Parameter s given by user *)
      | Some f, None, _, _, true -> Fixed f
      (* Compute s parameter from expectation or use inverse laplace *)
      | None, Some f, Some _, true, true -> ToCompute f
      | None, Some f, None, false, true -> ToCompute f
      (* Error *)
      | Some _, Some _, _, _, _ ->
          failwith "Set frequency or expected duration not both"
      | None, Some _, _, _, false | Some _, None, _, _, false ->
          failwith "No expected duration for untimed model"
      | None, Some _, Some _, false, _ -> assert false

    let z_param =
      match (boltzmann_param, expected_size, is_timed) with
      | None, None, true -> No
      | Some f, None, _ -> Fixed f
      | None, Some f, _ -> ToCompute f
      | None, None, false ->
          failwith "Set an expected size (--expected-size [N]) "
      | _ -> failwith "Set boltzmann parameter or expected size not both"

    let verbose = verbose
    let npoly = npoly
    let is_deterministic = is_det
    let perturbation = perturbation
    let out_format = out_format
    let is_interactive = is_interactive
  end : PARAM)

module type COMPUTE = sig
  module Weight : Semantic.S

  val compute_weight :
    bool ->
    string ->
    ('a, Weight.Bt.t) ZoneGraph.t ->
    (Weight.t, Weight.Bt.t) ZoneGraph.t

  val compute_params :
    ?seed:int ->
    (float Sampling.transChoice * string Sampling.transChoice) array ->
    (Weight.t, Weight.Bt.t) ZoneGraph.t ->
    float option * float option

  val sample :
    float option ->
    ?outfile:Format.formatter ->
    ?boltz:float ->
    ?seed:int ->
    ?exact_duration:float ->
    max_iter:int ->
    sampler:(int -> int -> float) ->
    ?store_traj:bool ->
    (Weight.t, Weight.Bt.t) ZoneGraph.t ->
    (float Sampling.transChoice * string Sampling.transChoice) array ->
    int ->
    unit
end

module Instantiate
    (FlR : Fl.FSIG)
    (Bound : ZoneGraphInput.BoundType)
    (Param : PARAM)
    () : COMPUTE with type Weight.Bt.t = Bound.t = struct
  module Rational_Impl =
    (val if Param.exact_rationnal then (module FlR : Fl.FSIG)
         else (module Fl.Float : Fl.FSIG))

  module Polynomial_Impl = Polynomial.Make (Rational_Impl) (Param)

  module ExpPoly_Impl =
    ExpPoly.Make
      (Polynomial_Impl)
      (struct
        let smp =
          if Param.exact_rationnal then None
          else match Param.s_param with Fixed f -> Some f | _ -> None
      end)

  module W =
    (val if Param.s_param = No then
           (module struct
             include Polynomial_Impl
             module SamplingW = Polynomial_Impl
           end : Polynomial.WeightForSampling)
         else
           (module struct
             include ExpPoly_Impl

             module SamplingW =
               (val if Param.use_heaviside <> None then
                      (module ExpPoly_Impl.Heaviside : Polynomial.SamplingStruct
                        with type t = ExpPoly_Impl.t)
                    else (module ExpPoly_Impl))
           end))

  module Weight =
    (val if Param.is_timed then
           (module Semantic.Make (Bound) (W) : Semantic.S
             with type Bt.t = Bound.t)
         else
           (module struct
             module W = Compute_untimed.Compute (Bound) (Polynomial_Impl)
             include W
             module SamplingW = W
           end))

  let compute_weight print_rg input_file_name rg =
    if Param.verbose > 0 then
      Format.printf "Computing Distribution%s -> %i: [@?" Weight.desc
        Param.npoly;
    let rgpoly2 =
      Weight.compute
        ~update:(fun () -> if Param.verbose > 0 then Format.printf "|@?")
        ~finish:(fun rg ->
          if Param.verbose > 0 then
            Format.printf "] [%.2fs]@." (Common.check_time ());
          if print_rg && Param.verbose > 4 then
            Format.printf "%a@." (ZoneGraph.print Bound.print) rg)
        ?print_interm:
          (if print_rg && Param.verbose > 4 then Some input_file_name else None)
        rg Param.npoly (Param.z_param <> No)
    in

    let rgpoly =
      if Param.use_heaviside <> None then
        let r =
          ZoneGraph.copy_map_weight rgpoly2
            (Weight.SamplingW.inverse_laplace
               (Common.VarSet.find_var Weight.var_string "T"))
        in
        { r with ZoneGraph.printer = Weight.SamplingW.print }
      else rgpoly2
    in

    let istate = rgpoly.statelist.(rgpoly.init) in
    let deglib =
      if Param.npoly < 0 then -1
      else if Param.npoly = 0 then 1
      else
        List.fold_left
          (fun acc tr ->
            let d, _, _, _ = tr.ZoneGraph.weight.(0) in
            max acc d)
          (-1) istate.transition
    in
    if Param.verbose > 0 then
      Format.printf "Volume in initial state:%a, degree of liberty:%i@."
        (Weight.SamplingW.print ~pretty:true)
        istate.loc_weight deglib;
    (*if Param.use_heaviside then
      let laplace_inverse =
        Weight.Heaviside.inverse_laplace
          Common.(VarSet.find_var rg.var_string "T")
          istate.loc_weight
      in
      Format.printf "Volume laplace-1 :%a@." Weight.Heaviside.print
        laplace_inverse);*)
    if Weight.zero = istate.loc_weight && Param.npoly >= 0 then
      Printf.eprintf
        "Initial state have a weight of 0 (i.e. no reachable accepting state) \
         abort\n";
    rgpoly

  (* Compute parameters z and s *)
  let compute_params ?seed template rgpoly =
    let z_target =
      match Param.z_param with
      | No -> None
      | Fixed _ -> None
      | ToCompute target ->
          if Param.verbose > 0 then
            Format.printf "Computing boltzmann parameter z for E[N]=%g @?"
              target;
          Some target
    in
    let smp_target =
      match Param.s_param with
      | No -> None
      | Fixed _ -> None
      | ToCompute _ when Param.use_heaviside <> None -> None
      | ToCompute target ->
          if Param.verbose > 0 then
            Format.printf "Computing laplace parameter s for E[T]=%g @?" target;
          Some target
    in

    let boltz_full, smp_full =
      Weight.compute_params ~verbose:Param.verbose
        ?seed:(match seed with None -> None | Some s -> Some ((s * 11) + 7))
        (z_target, smp_target) template rgpoly
    in

    let boltz =
      match (Param.z_param, boltz_full) with
      | No, _ -> None
      | Fixed f, _ -> Some f
      | ToCompute _, Some (c, conv_rad, nt) ->
          if Param.verbose > 0 then (
            if conv_rad > 0.0 then
              Format.printf "radius of convergence:  %g" conv_rad;
            Format.printf " -> z=%g;E[N]=%g  [%.2fs]@." c nt
              (Common.check_time ()));
          Some c
      | _ -> None
    in

    let smp =
      match (Param.s_param, smp_full) with
      | No, _ -> None
      | Fixed f, _ -> Some f
      | ToCompute _, Some (x, (low, mid, up), nt) ->
          if Param.verbose > 0 then (
            Format.printf "range:[%g; %g; %g]" low mid up;
            Format.printf " -> s=%g;E[T]=%g  [%.2fs]@." x nt
              (Common.check_time ()));
          Some x
      | _ -> None
    in

    (*Option.iter
      (fun b ->
        ZoneGraph.update_weight rgpoly (fun p ->
            Weight.SamplingW.apply_const p
              (Common.VarSet.find_var Weight.var_string "T")
              (Weight.SamplingW.F.of_float b)))
      Param.use_heaviside;*)
    Option.iter
      (fun b ->
        ZoneGraph.update_weight rgpoly (fun p ->
            Weight.apply_const p Weight.z (Weight.F.of_float b)))
      boltz;

    (match smp with
    | Some sparam when not Weight.F.is_exact ->
        ZoneGraph.update_weight rgpoly (fun p ->
            Weight.apply_const ?smp p Weight.s (Weight.F.of_float sparam))
    | _ -> ());

    (boltz, smp)

  let sample smp =
    let module State = Sampling.State (Weight.Bt) (Weight.SamplingW) in
    let module Printer =
      OutFormat.Make (Weight.SamplingW) (Weight.Bt) (State) (Param)
    in
    let module Monitor =
      (val if Param.is_deterministic then (module Printer)
           else
             let module Matcher =
               Undet_matcher.Make (Weight.SamplingW) (Weight.Bt) (State)
             in
             (module Sampling.CombineMonitor (Matcher) (Printer))
          : Sampling.MONITOR
          with type weight = Weight.t
           and type bound = Weight.Bt.t
           and type state = State.t
           and type out = Printer.out)
    in
    let module M =
      Sampling.Make (Bound) (Weight.SamplingW) (State) (Monitor)
        (struct
          let smp = smp
        end)
    in
    M.sample ~verbose:Param.verbose
end

open Format

(* Main  *)
let main exact_arithm print_rg infile regexp frequency npoly no_cache
    expected_duration boltzmann_param expected_size rational_impl verbose
    splitting_debug export_zone_graph outfilename out_style perturbate seed
    is_interactive max_iteration sampler store_traj template is_duration_exact
    nbtraj =
  (* check if precomputation exists *)
  let precomp_exists =
    if print_rg || splitting_debug <> "" || no_cache then None
    else
      ZoneGraphInput.precomputation_exists
        ~debug:(fun b ->
          if verbose > 0 then
            if b then
              printf "Precomputation file found ! [%.2f]@."
                (Common.check_time ())
            else
              printf
                "Precomputation file found but file have change discard !@.")
        infile frequency boltzmann_param npoly
        (frequency = None && expected_duration <> None)
        rational_impl
  in

  (* Use splitted or general bound, general only for isotropic sampling *)
  let module Bound =
    (val if npoly < 0 then
           (module ZoneGraphInput.GeneralBound : ZoneGraphInput.BoundType)
         else (module ZoneGraphInput.LinearBound))
  in
  let module ZGI = ZoneGraphInput.Make (Bound) in
  (*Building the zone graph
    None means that a precomputation file exists
  *)
  let rgopt =
    if precomp_exists = None then
      Some
        (ZGI.input_zone_graph ~verbose ~print_rg ~splitting_debug
           ?export_zone_graph
           ?bound_all_guard:
             (if is_duration_exact then
                match expected_duration with
                | None -> None
                | Some f when Float.is_integer f -> Some (int_of_float f)
                | Some _ -> None
              else None)
           regexp infile)
    else None
  in

  let module Param =
    (val gen_param ~out_format:out_style ~perturbation:perturbate
           ~is_interactive
           (match (rgopt, precomp_exists) with
           | Some a, _ -> a.ZoneGraph.var_string
           | _, Some b -> b.ZoneGraph.var_string
           | None, None -> assert false)
           rational_impl frequency expected_duration is_duration_exact
           boltzmann_param expected_size verbose npoly
           (match (rgopt, precomp_exists) with
           | Some a, _ -> a.is_deterministic
           | _, Some _ -> false
           | _ -> assert false))
  in
  let module Compute =
    Instantiate ((val exact_arithm : Fl.FSIG)) (Bound) (Param) ()
  in
  let rgpoly =
    match (precomp_exists, rgopt) with
    | Some content, _ ->
        if verbose > 0 then
          printf "Reading Distribution%s -> %i@." Compute.Weight.desc npoly;
        content
    | _, Some rg -> Compute.compute_weight print_rg infile rg
    | None, None -> assert false
  in

  (*Save computation*)
  if precomp_exists = None && not no_cache then (
    if verbose > 0 then printf "Saving Precomputation @?";
    ZoneGraphInput.save_precomputation infile rgpoly frequency boltzmann_param
      npoly
      (frequency = None && expected_duration <> None)
      rational_impl;
    printf "[%.2fs]@." (Common.check_time ()));

  let boltz, smp = Compute.compute_params ?seed template rgpoly in

  (* tidying up memory after the distribution computation*)
  Gc.compact ();

  if print_rg then printf "%a@." (ZoneGraph.print Bound.print) rgpoly;

  let outfile =
    match outfilename with
    | Some st -> formatter_of_out_channel @@ open_out st
    | None -> Format.std_formatter
  in

  Compute.sample smp ~outfile ?seed ?boltz ~max_iter:max_iteration
    ?exact_duration:(if is_duration_exact then expected_duration else None)
    ~sampler:(Low_disc_sampler.get_sampler sampler)
    ~store_traj rgpoly template nbtraj;

  let open Common in
  let data = outfilename |>>| "gnuplotout.dat" in
  OutFormat.plot_of_style rgpoly out_style store_traj nbtraj data
    (expected_duration <> None)
    template
