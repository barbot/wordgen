(*

Copyright (C) 2019 LACL Universite Paris Est Creteil Val de Marne
Authors: Benoît Barbot
Licence: GPL-3.0-or-later

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*)

let random _ _ = Random.float 1.0

let prime =
  [|
    2; 3; 5; 7; 11; 13; 17; 19; 23; 29; 31; 37; 41; 43; 47; 53; 59; 61; 67; 71;
  |]

let halton base index =
  let b = prime.(base) in
  let f = ref 1.0 and i = ref index and r = ref 0.0 and bf = float b in
  while !i > 0 do
    f := !f /. bf;
    r := !r +. (!f *. float (!i mod b));
    i := !i / b
  done;
  !r

let phi = 1.61803398874989484820458683436563
let phi2 = 1.32471795724474602596090885447809

let phin n =
  let n2 = max 2 n in
  Weight_structure.Math.newton_raphson_iterate ~factor:1e-15 ~max_iter:1000
    (fun v ->
      let nm = float (n2 - 1) in
      ((v ** float n2) -. v -. 1.0, (float n2 *. (v ** nm)) -. 1.0))
    1.1

let kronecker_state = ref phi

let init ?seed n =
  (match seed with None -> Random.self_init () | Some i -> Random.init i);
  kronecker_state := phin n

let kronecker_phi base index =
  let b = base + 1 in
  mod_float (float_of_int index *. (1.0 /. (!kronecker_state ** float b))) 1.0

type t = Random | Halton | Kronecker

let def = Random

let to_string = function
  | Halton -> "halton"
  | Kronecker -> "kronecker"
  | Random -> "random"

let of_string = function
  | "halton" -> Halton
  | "kronecker" -> Kronecker
  | _ -> Random

let get_sampler = function
  | Random -> random
  | Halton -> halton
  | Kronecker -> kronecker_phi
