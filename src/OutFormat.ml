type output_style =
  | Void
  | Word
  | Timestamp
  | Timeword
  | TimewordState
  | StateList
  | StateListDelay
  | StateListFull
  | TimeAndLabels
  | Debug
  | CoSim
  | Json
  | TimeLine

let style_list =
  [
    "timeword";
    "timestamp";
    "word";
    "timeword_state";
    "state_list";
    "state_list_delay";
    "state_list_full";
    "time_and_label";
    "debug";
    "void";
    "cosim";
    "json";
    "timeline";
  ]

let style_of_string = function
  | "void" -> Void
  | "timestamp" -> Timestamp
  | "timeword" -> Timeword
  | "word" -> Word
  | "timeword_state" -> TimewordState
  | "state_list" -> StateList
  | "state_list_delay" -> StateListDelay
  | "state_list_full" -> StateListFull
  | "debug" -> Debug
  | "time_and_label" -> TimeAndLabels
  | "cosim" -> CoSim
  | "json" -> Json
  | "timeline" -> TimeLine
  | _ -> Void

let string_of_style = function
  | Void -> "void"
  | Timestamp -> "timestamp"
  | Timeword -> "timeword"
  | Word -> "word"
  | TimewordState -> "timeword_state"
  | StateList -> "state_list"
  | StateListDelay -> "state_list_delay"
  | StateListFull -> "state_list_full"
  | TimeAndLabels -> "time_and_label"
  | Debug -> "debug"
  | CoSim -> "cosim"
  | Json -> "json"
  | TimeLine -> "timeline"

open Format

module Make
    (W : Weight_structure.Polynomial.SamplingStruct)
    (Bt : ZoneGraphInput.BoundType)
    (State : Sampling.STATE with type p = W.t with type bound = Bt.t)
    (P : sig
      val out_format : output_style
      val is_interactive : bool
      val perturbation : float option
    end) =
struct
  open State

  type bound = Bt.t
  type weight = W.t
  type state = State.t
  type out = unit

  type monitor_state = {
    total_time : float;
    total_length : int;
    apericube : int;
  }

  type t =
    Format.formatter
    * Buffer.t
    * Format.formatter
    * (weight, bound) ZoneGraph.t
    * (string ->
      (float Sampling.transChoice * string Sampling.transChoice) list)

  type dyn = monitor_state

  let init outfile rg parse_action =
    let buff = Buffer.create 1024 in
    let outformat =
      match P.out_format with
      | (Debug | CoSim | _) when P.is_interactive -> outfile
      | _ -> Format.formatter_of_buffer buff
    in

    if P.out_format = Json then fprintf outfile "[";
    let mon_state = { total_time = 0.0; total_length = 0; apericube = 0 } in
    (match P.out_format with
    | TimeLine ->
        Format.pp_set_margin outfile max_int;
        fprintf outfile "%f %a %a %a@." 0.0
          (fun _ a ->
            Array.iter (fun (x, b) -> if b then fprintf outfile "%s " x) a)
          rg.ZoneGraph.var_string
          (fun _ a -> Array.iter (fun x -> fprintf outfile "%s " x) a)
          rg.ZoneGraph.alphabet (print_state_long rg) (State.init rg 0.0)
    | _ -> ());
    ((outfile, buff, outformat, rg, parse_action), mon_state)

  let new_traj (_, buff, outfile, _, _) _ =
    Buffer.clear buff;
    if P.out_format = Json then fprintf outfile "@[<v 3>[";
    { total_time = 0.0; total_length = 0; apericube = 0 }

  let end_traj (realout, buff, outfile, _, _) mon_state store_traj _ =
    if P.out_format = Json then fprintf outfile "{\"final\":true}@]]@],@;";
    if store_traj then
      Format.fprintf outfile "%i\t%i\t%g" mon_state.apericube
        mon_state.total_length mon_state.total_time;
    if P.out_format <> Void || store_traj then fprintf outfile "@.";
    Format.pp_print_string realout (Buffer.contents buff);
    Format.pp_print_flush realout ();
    (mon_state, true)

  let end_sampling (realout, _, _, _, _) _ =
    if P.out_format = Json then fprintf realout "[]]@."

  module StringMap = Map.Make (String)

  let clean_state s =
    s |> String.split_on_char ' '
    |> List.map (fun x -> String.split_on_char ':' x)
    |> List.map (function _ :: x :: _ -> x | x :: _ -> x | [] -> "")
    |> String.concat " "

  let print_timeline rgpoly ct (loc, tab, _) outfile e =
    let print_letter_as_signal rg f e =
      rg.ZoneGraph.alphabet
      |> Array.map (fun x -> if x = e then 1 else 0)
      |> Array.iter (fun x -> fprintf f "%i " x)
    in

    fprintf outfile "@[<h 0>%f@ " ct;
    Array.iter (fun x -> Format.fprintf outfile "%f@ " x) tab;
    fprintf outfile "%a %s \"%s\"@]@."
      (print_letter_as_signal rgpoly)
      e
      (clean_state rgpoly.ZoneGraph.statelist.(loc).disc_name)
      (if e = "0" then " " else e)

  let cmp_trans t1 t2 =
    let f = function _, tr1, (low, _) -> (tr1, low) in
    let tr1, low1 = f t1 in
    let tr2, low2 = f t2 in
    if tr1.ZoneGraph.action <> tr2.ZoneGraph.action then
      compare tr1.ZoneGraph.action tr2.ZoneGraph.action
    else compare low1 low2

  let rec up_state (realout, buff, outfile, rgpoly, parse_action) monitor_state
      ?smp ?delay (st : State.t) available_transition_samp i (u1, u2) =
    (match P.out_format with
    | TimewordState -> fprintf outfile "(%a)@." (print_state ?trunc:None) st
    | StateList -> fprintf outfile "%a@." (print_state ?trunc:None) st
    | Json ->
        fprintf outfile "@[<v 2>{ \"state\":\"%a\",@ " (print_state ?trunc:None)
          st
    | StateListFull | StateListDelay ->
        fprintf outfile "%a\t%i@." (print_state ?trunc:None) st 1
    | CoSim | Debug -> (
        let trans_list =
          State.available_transition
            ~filter:(fun (_, up) _ -> Option.value ~default:0.0 delay <= up)
            rgpoly st
          (*available_transition*)
          |> compute_weight ?smp ?delay rgpoly i st
          (* To remove  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*)
          |> List.sort cmp_trans
        in

        let selected = choose_weight trans_list u1 in
        match P.out_format with
        | CoSim ->
            fprintf outfile
              "{@[\"state\":\"%s\",@ \"clocks\":[%a],@ \"current_time\": %g,@ \
               \"constrained_delay\": %s,@ \"transition\":[@[<v 0>%a@]]@]}@."
              rgpoly.statelist.(get_loc st).disc_name
              (fun f (_, x, _) ->
                Array.iteri
                  (fun i x ->
                    Format.fprintf f "%s@ %f" (if i > 0 then "," else "") x)
                  x)
              st monitor_state.total_time
              (Option.map string_of_float delay |> Option.value ~default:"null")
              (fun f ->
                List.iteri (fun i (weight, tr, _) ->
                    let target =
                      rgpoly.statelist.((List.hd tr.ZoneGraph.miniedge).target)
                    in
                    let low, time, up = sample_time ?smp ?delay i st tr u2 in

                    let ct = monitor_state.total_time in
                    fprintf f
                      "%s@[<h 0>{\"label\":\"%s\",@ \"weight\":%g,@ \
                       \"is_selected\":%b,@ \"min_time\": %g,@ \
                       \"max_time\":%g,@ \"time\":%g,@ \"relative_time\":%g,@ \
                       \"target\":\"%s\"}@]"
                      (if i > 0 then "," else "")
                      tr.ZoneGraph.action weight (selected = Some tr)
                      (low +. ct) (up +. ct) (time +. ct) time target.disc_name))
              trans_list
        | Debug ->
            fprintf outfile "@[<h 0>%a@.  @[<v 2>@[<v 0>%a@]@]@]@."
              (print_state_long rgpoly) st
              (fun f ->
                List.iter (fun (w, tr, (low, up)) ->
                    if Array.length tr.ZoneGraph.weight > 0 then
                      let deg, weight, _, _ = tr.ZoneGraph.weight.(i) in
                      let target =
                        rgpoly.statelist.((List.hd tr.ZoneGraph.miniedge).target)
                      in
                      fprintf f "%c[%s]-[%g;%g]--(%a,%i)->%g->%i:\"%s\"@,"
                        (if selected = Some tr then '*' else ' ')
                        tr.ZoneGraph.action low up (W.print ~pretty:true) weight
                        deg w target.id target.disc_name
                    else
                      fprintf f "%c[%s]-[%g;%g]@,"
                        (if selected = Some tr then '*' else ' ')
                        tr.ZoneGraph.action low up))
              trans_list
        | _ -> assert false)
    | TimeLine -> print_timeline rgpoly monitor_state.total_time st outfile "0"
    | Void | Word | Timestamp | Timeword | TimeAndLabels -> ());

    if P.is_interactive then
      let rec aux () =
        try
          if P.out_format <> CoSim then fprintf outfile ">@?";
          match read_line () with
          | "step" | "s" -> Some (List.hd @@ parse_action "_[_]")
          | "exit" -> exit 0
          | "reset" -> raise Exit
          | "help" ->
              fprintf outfile
                "available command : help, step, reset, exit, \"t[a]\" with t \
                 a time and a an action@.";
              aux ()
          | x when x.[0] = '>' ->
              let d = float_of_string String.(sub x 1 (length x - 1)) in
              up_state
                (realout, buff, outfile, rgpoly, parse_action)
                monitor_state ?smp
                ~delay:(Option.value ~default:0.0 delay +. d)
                (st : State.t)
                available_transition_samp i (u1, u2)
          | instr ->
              let l = parse_action instr in
              Some (List.hd l)
        with
        | Exit -> raise Exit
        | End_of_file -> exit 1
        | _ -> aux ()
      in
      aux ()
    else None

  let up_state_elapsed (_, _, outfile, rgpoly, _) monitor_state st time _ =
    match P.out_format with
    | TimeLine ->
        print_timeline rgpoly (monitor_state.total_time +. time) st outfile "0"
    | StateListFull | StateListDelay ->
        fprintf outfile "%a" (print_state ?trunc:None) st
    | _ -> ()

  let up_trans (_, _, outfile, rgpoly, _) mon_stat st (low, sampled_time, up) tr
      i =
    let time =
      match P.perturbation with
      | None -> sampled_time
      | Some delta ->
          let low2 = max low (sampled_time -. delta) in
          let up2 = min up (sampled_time +. delta) in
          let u = Low_disc_sampler.random 0 0 in
          low2 +. (u *. (up2 -. low2))
    in
    let t2 = int_of_float (1.0 *. time) in
    let m2 =
      {
        total_time = mon_stat.total_time +. sampled_time;
        total_length = mon_stat.total_length + 1;
        apericube = (2 * mon_stat.apericube) + t2;
      }
    in

    (match P.out_format with
    | Debug when Array.length tr.ZoneGraph.weight > 0 ->
        let _, weight, cdf, _ = tr.ZoneGraph.weight.(i) in
        fprintf outfile "((%a)/(%a))->%f[%s]\n" (W.print ~pretty:true) cdf
          (W.print ~pretty:true) weight time tr.ZoneGraph.action
    | Debug -> fprintf outfile "->%f[%s]\n" time tr.ZoneGraph.action
    | Timestamp -> fprintf outfile "%f\t" time
    | Word -> fprintf outfile "%s" tr.ZoneGraph.action
    | Json ->
        fprintf outfile "\"action\":\"%s\",@ \"delay\":%f}@],@;"
          tr.ZoneGraph.action time
    | Timeword | TimewordState ->
        fprintf outfile "%f[%s] " time tr.ZoneGraph.action
    | StateListFull ->
        fprintf outfile "\t%i@." (int_of_char tr.ZoneGraph.action.[0])
    | StateListDelay ->
        fprintf outfile "\t%i@.@." (int_of_char tr.ZoneGraph.action.[0])
    | TimeAndLabels ->
        fprintf outfile "%f\t%i@." m2.total_time
          (int_of_char tr.ZoneGraph.action.[0])
    | TimeLine ->
        print_timeline rgpoly m2.total_time st outfile tr.ZoneGraph.action
    | StateList | Void | CoSim -> ());
    m2
end

let plot_of_style rgpoly style store_traj ntraj data is_exp_poly template =
  let open Printf in
  match (style, store_traj) with
  | StateListFull, _ when ntraj < 50 ->
      Some
        (sprintf "plot '%s' u 2:3:4 w linespoint lc variable pt 7 ps 0.25" data)
  | StateListDelay, _ ->
      Some
        (sprintf "plot '%s' u 2:3:4 w linespoint lc variable pt 7 ps 0.25" data)
  | StateListFull, _ ->
      Some (sprintf "plot '%s' u 2:3:4 w points pt 7 ps 0.25 lc variable" data)
  | StateList, _ -> Some (sprintf "plot '%s' u 2:3 w points" data)
  | _, true ->
      Some
        (sprintf
           "set xrange [-0.5 :*]\n\
            binwidth = 0.1\n\
            binstart= -0.5\n\
            set ytics nomirror\n\
            set y2tics\n\
            set boxwidth 0.9*binwidth\n\
            set style fill solid 0.5\n\
            plot '%s' i 0 u \
            (binwidth*(floor(($2-binstart)/binwidth)+0.5)+binstart):(1.0) \
            smooth freq w boxes title 'size', '' i 0 u \
            (binwidth*(floor(($3-binstart)/binwidth)+0.5)+binstart):(1.0) \
            smooth freq w boxes axes x1y1 title 'duration'\n"
           data)
  (* fprintf gnuplot "set xrange [-0.5 :*]\nbinwidth = 1.0\nbinstart= -0.5\nset boxwidth 0.9*binwidth\nset style fill solid 0.5\n";
      fprintf gnuplot "plot '%s' i 0 u (binwidth*(floor(($1-binstart)/binwidth)+0.5)+binstart):(1.0) smooth freq w boxes\n" data*)
  | Timestamp, _ when Array.length template = 3 ->
      Some
        (sprintf
           "set style fill transparent solid 0.2 noborder\n\
            set style circle radius 0.1\n\
            set view equal xyz\n\
            set view 50,135\n\
            splot '%s'" data)
  | Timestamp, _ ->
      Some
        (sprintf
           "set style fill transparent solid 0.2 noborder\n\
            set style circle radius 0.001\n\
            plot '%s' u 1:2 notitle with circles lc rgb \"blue\"" data)
  | TimeAndLabels, _ -> Some (sprintf "plot '%s' u 1:2 w linespoints pt 2" data)
  | TimeLine, _ ->
      let open ZoneGraph in
      let nbalphabet = Array.length rgpoly.alphabet in
      Some
        (sprintf
           "\n\
           \      set y2range [0.0:4.5]\n\
           \      set yrange [-1.2:]\n\
           \      plot for [col=2:%i] '%s' u 1:col w line title columnhead lw \
            2 axes x1y1,for [col=%i:%i] '' u 1:col w line title columnhead lw \
            4 axes x1y2"
           (rgpoly.cardclocks + 1) data (rgpoly.cardclocks + 2)
           (nbalphabet + rgpoly.cardclocks + 1))
  | Void, _ when is_exp_poly ->
      let open Format in
      fprintf str_formatter
        "set samples 1000\n\
         %s\n\
         set yrange [0.0:]\n\
         u(x)=x<0 ? 0 : 1\n\
         v(%s) = %a\n"
        "set xrange [-0.0:]" "T"
        (rgpoly.printer ~pretty:false)
        rgpoly.statelist.(rgpoly.init).loc_weight;
      fprintf str_formatter "plot v(x)";
      Some (flush_str_formatter ())
  | _ -> None
