open Common
open Weight_structure
(* Build with `ocamlbuild -pkg alcotest simple.byte` *)

module VarStruct = struct
  let var_string =
    [| ("t", false); ("x", true); ("y", true); ("z", false); ("s", false) |]
end

(* A module with functions to test *)
module P = Polynomial.Make (Flq.Q) (VarStruct)
module Papprox = Polynomial.Make (Fl.Float) (VarStruct)

module EP =
  ExpPoly.Make
    (P)
    (struct
      let smp = None
    end)

open P

let pr v =
  let open Format in
  print str_formatter v;
  flush_str_formatter ()

let poly = Alcotest.testable print (fun x y -> P.(x -.. y = zero))

let polyAprox =
  Alcotest.testable Papprox.print (fun x y -> Papprox.(x -.. y = zero))

let x = VarSet.var_of_int 1
let y = VarSet.var_of_int 2

(* The tests *)
let test_zero () = Alcotest.(check string) "same string" "0" (pr zero)

let test_one () =
  Alcotest.(check string) "same string" "1" (pr (const (F.of_float 1.0)))

let test_sum () =
  Alcotest.(check poly)
    "same value"
    (const (F.of_float 1.0))
    (const (F.of_float 3.0) +.. const (F.of_float (-2.0)))

type p = Const of float | Var of (int * int) | Sum of p * p | Mult of p * p

let rec gen_poly = function
  | Const f -> const (F.of_float f)
  | Var (x, i) -> var ~exp:i (VarSet.var_of_int x)
  | Sum (p1, p2) -> gen_poly p1 +.. gen_poly p2
  | Mult (p1, p2) -> gen_poly p1 *.. gen_poly p2

let p0 = var x +.. pow (var y) 3 +.. const (F.of_float 12.0) +.. pow (var x) 0

let test_integral () =
  Alcotest.(check poly)
    "same value"
    (integral p0 y zero (const (F.of_float 12.0)))
    (const (F.of_float 5340.0) +.. (const (F.of_float 12.0) *.. var x))

let p1 =
  gen_poly
    (Sum
       ( Sum
           ( Mult
               ( Sum
                   ( Sum
                       ( Mult
                           (Sum (Const (-0.712508), Var (0, 1)), Const 1.86708),
                         Mult
                           ( Sum
                               ( Sum (Const (-1.49116), Const (-0.919918)),
                                 Mult
                                   ( Const 0.109705,
                                     Sum
                                       ( Mult
                                           ( Sum
                                               ( Sum
                                                   ( Var (0, 1),
                                                     Mult
                                                       ( Mult
                                                           ( Mult
                                                               ( Var (1, 0),
                                                                 Const 0.117616
                                                               ),
                                                             Const 0.375283 ),
                                                         Mult
                                                           ( Const (-1.545),
                                                             Mult
                                                               ( Sum
                                                                   ( Const
                                                                       1.65734,
                                                                     Mult
                                                                       ( Sum
                                                                           ( Sum
                                                                               ( 
                                                                               Const
                                                                                (-1.28601),
                                                                                Var
                                                                                ( 
                                                                                1,
                                                                                1
                                                                                )
                                                                               ),
                                                                             Sum
                                                                               ( 
                                                                               Const
                                                                                1.49235,
                                                                                Sum
                                                                                ( 
                                                                                Const
                                                                                1.16595,
                                                                                Const
                                                                                (-0.00564408)
                                                                                )
                                                                               )
                                                                           ),
                                                                         Mult
                                                                           ( Mult
                                                                               ( 
                                                                               Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Const
                                                                                (-1.31104),
                                                                                Const
                                                                                1.58718
                                                                                ),
                                                                                Mult
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Mult
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Mult
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Const
                                                                                (-0.521839),
                                                                                Var
                                                                                ( 
                                                                                1,
                                                                                0
                                                                                )
                                                                                ),
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Const
                                                                                0.899878,
                                                                                Const
                                                                                (-0.381689)
                                                                                ),
                                                                                Var
                                                                                ( 
                                                                                0,
                                                                                0
                                                                                )
                                                                                )
                                                                                ),
                                                                                Const
                                                                                (-0.266984)
                                                                                ),
                                                                                Var
                                                                                ( 
                                                                                0,
                                                                                0
                                                                                )
                                                                                ),
                                                                                Const
                                                                                (-0.510893)
                                                                                ),
                                                                                Var
                                                                                ( 
                                                                                1,
                                                                                1
                                                                                )
                                                                                ),
                                                                                Var
                                                                                ( 
                                                                                0,
                                                                                1
                                                                                )
                                                                                ),
                                                                                Const
                                                                                (-1.8416)
                                                                                ),
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Var
                                                                                ( 
                                                                                0,
                                                                                0
                                                                                ),
                                                                                Sum
                                                                                ( 
                                                                                Mult
                                                                                ( 
                                                                                Const
                                                                                0.824652,
                                                                                Const
                                                                                (-0.272448)
                                                                                ),
                                                                                Var
                                                                                ( 
                                                                                0,
                                                                                0
                                                                                )
                                                                                )
                                                                                ),
                                                                                Mult
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Mult
                                                                                ( 
                                                                                Var
                                                                                ( 
                                                                                0,
                                                                                0
                                                                                ),
                                                                                Sum
                                                                                ( 
                                                                                Const
                                                                                (-1.26636),
                                                                                Mult
                                                                                ( 
                                                                                Mult
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Mult
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Const
                                                                                1.52467,
                                                                                Const
                                                                                (-1.44636)
                                                                                ),
                                                                                Const
                                                                                (-1.81246)
                                                                                ),
                                                                                Sum
                                                                                ( 
                                                                                Sum
                                                                                ( 
                                                                                Var
                                                                                ( 
                                                                                0,
                                                                                1
                                                                                ),
                                                                                Const
                                                                                (-0.194834)
                                                                                ),
                                                                                Mult
                                                                                ( 
                                                                                Const
                                                                                0.0232993,
                                                                                Const
                                                                                1.24118
                                                                                )
                                                                                )
                                                                                ),
                                                                                Mult
                                                                                ( 
                                                                                Const
                                                                                1.10444,
                                                                                Const
                                                                                0.325027
                                                                                )
                                                                                ),
                                                                                Var
                                                                                ( 
                                                                                1,
                                                                                0
                                                                                )
                                                                                ),
                                                                                Mult
                                                                                ( 
                                                                                Const
                                                                                0.620903,
                                                                                Const
                                                                                0.454195
                                                                                )
                                                                                ),
                                                                                Const
                                                                                (-0.930799)
                                                                                ),
                                                                                Var
                                                                                ( 
                                                                                0,
                                                                                1
                                                                                )
                                                                                ),
                                                                                Const
                                                                                (-1.97381)
                                                                                )
                                                                                )
                                                                                ),
                                                                                Var
                                                                                ( 
                                                                                0,
                                                                                1
                                                                                )
                                                                                ),
                                                                                Const
                                                                                (-0.80359)
                                                                                )
                                                                                ),
                                                                                Var
                                                                                ( 
                                                                                1,
                                                                                0
                                                                                )
                                                                                ),
                                                                                Const
                                                                                (-1.00803)
                                                                                )
                                                                                )
                                                                                ),
                                                                                Const
                                                                                (-1.43013)
                                                                               ),
                                                                             Sum
                                                                               ( 
                                                                               Var
                                                                                ( 
                                                                                0,
                                                                                1
                                                                                ),
                                                                                Sum
                                                                                ( 
                                                                                Mult
                                                                                ( 
                                                                                Const
                                                                                1.33374,
                                                                                Var
                                                                                ( 
                                                                                0,
                                                                                1
                                                                                )
                                                                                ),
                                                                                Const
                                                                                0.640638
                                                                                )
                                                                               )
                                                                           ) )
                                                                   ),
                                                                 Var (1, 1) ) )
                                                       ) ),
                                                 Const 0.304289 ),
                                             Mult (Const 0.263273, Var (1, 1))
                                           ),
                                         Mult
                                           ( Var (0, 0),
                                             Sum
                                               ( Const (-1.2114),
                                                 Const (-0.0080391) ) ) ) ) ),
                             Mult
                               ( Mult
                                   ( Const (-0.222252),
                                     Mult
                                       ( Mult
                                           ( Sum
                                               ( Var (1, 1),
                                                 Mult
                                                   ( Const (-1.33187),
                                                     Const (-1.28762) ) ),
                                             Const (-1.35882) ),
                                         Mult
                                           ( Sum
                                               ( Sum
                                                   (Const 0.0746461, Var (0, 1)),
                                                 Const (-1.40822) ),
                                             Var (0, 1) ) ) ),
                                 Const 0.820138 ) ) ),
                     Sum (Const (-1.50144), Const 1.55265) ),
                 Sum (Const 1.5829, Const (-1.04482)) ),
             Var (1, 0) ),
         Const (-1.36201) ))

let p2 =
  gen_poly
    (Sum
       ( Const (-0.865672),
         Sum
           ( Mult (Const (-1.10576), Var (1, 0)),
             Sum
               ( Const (-0.376951),
                 Sum
                   ( Const 0.65211,
                     Sum
                       ( Mult
                           ( Const 0.213829,
                             Sum
                               ( Sum (Const 0.224512, Var (1, 0)),
                                 Mult
                                   ( Var (1, 0),
                                     Sum (Const (-1.50878), Var (0, 0)) ) ) ),
                         Const (-0.302529) ) ) ) ) ))

let p3 =
  gen_poly
    (Mult
       ( Sum
           ( Mult
               ( Sum
                   ( Sum
                       ( Var (1, 1),
                         Mult
                           ( Sum
                               ( Sum
                                   ( Sum
                                       ( Mult
                                           ( Sum
                                               ( Mult
                                                   ( Mult
                                                       ( Mult
                                                           ( Const 0.685563,
                                                             Var (1, 1) ),
                                                         Var (1, 1) ),
                                                     Const 1.64208 ),
                                                 Const (-1.90356) ),
                                             Const 1.33615 ),
                                         Var (1, 0) ),
                                     Mult
                                       ( Mult
                                           ( Sum
                                               ( Sum
                                                   ( Sum
                                                       ( Mult
                                                           ( Sum
                                                               ( Var (0, 0),
                                                                 Var (0, 1) ),
                                                             Mult
                                                               ( Sum
                                                                   ( Const
                                                                       0.315383,
                                                                     Const
                                                                       1.7297 ),
                                                                 Const
                                                                   (-1.40727) )
                                                           ),
                                                         Mult
                                                           ( Sum
                                                               ( Var (0, 1),
                                                                 Var (0, 1) ),
                                                             Const 0.428504 ) ),
                                                     Var (0, 0) ),
                                                 Const 1.84694 ),
                                             Mult
                                               ( Const 1.4396,
                                                 Sum
                                                   ( Const (-0.199541),
                                                     Const (-0.38356) ) ) ),
                                         Mult
                                           ( Mult
                                               ( Const 1.93179,
                                                 Mult
                                                   ( Const (-1.53149),
                                                     Const (-1.72291) ) ),
                                             Var (0, 1) ) ) ),
                                 Mult
                                   ( Sum
                                       ( Mult
                                           ( Sum
                                               ( Var (0, 1),
                                                 Sum
                                                   ( Const 0.429328,
                                                     Sum
                                                       ( Const 1.43481,
                                                         Const (-1.34698) ) ) ),
                                             Const (-1.34663) ),
                                         Mult
                                           ( Sum
                                               ( Mult
                                                   ( Const 1.39306,
                                                     Const 0.882996 ),
                                                 Const (-0.122817) ),
                                             Sum
                                               ( Const 1.96669,
                                                 Mult
                                                   ( Sum
                                                       ( Mult
                                                           ( Const 0.0203522,
                                                             Const (-1.94229) ),
                                                         Const 1.81295 ),
                                                     Mult
                                                       ( Var (1, 1),
                                                         Mult
                                                           ( Sum
                                                               ( Mult
                                                                   ( Const
                                                                       1.93618,
                                                                     Const
                                                                       (-0.134514)
                                                                   ),
                                                                 Mult
                                                                   ( Var (1, 1),
                                                                     Mult
                                                                       ( Const
                                                                           0.389754,
                                                                         Sum
                                                                           ( Const
                                                                               0.348551,
                                                                             Sum
                                                                               ( 
                                                                               Sum
                                                                                ( 
                                                                                Mult
                                                                                ( 
                                                                                Const
                                                                                (-1.31856),
                                                                                Const
                                                                                1.66497
                                                                                ),
                                                                                Mult
                                                                                ( 
                                                                                Const
                                                                                0.55114,
                                                                                Const
                                                                                (-1.72732)
                                                                                )
                                                                                ),
                                                                                Var
                                                                                ( 
                                                                                0,
                                                                                1
                                                                                )
                                                                               )
                                                                           ) )
                                                                   ) ),
                                                             Const 0.614747 ) )
                                                   ) ) ) ),
                                     Const 0.505168 ) ),
                             Sum
                               ( Mult (Const (-1.1361), Var (1, 1)),
                                 Sum (Const (-1.36438), Var (0, 1)) ) ) ),
                     Sum
                       ( Mult
                           ( Const 0.203666,
                             Mult
                               ( Const 1.37536,
                                 Sum (Const (-0.364922), Const (-0.620037)) ) ),
                         Var (0, 0) ) ),
                 Sum
                   ( Const (-0.258687),
                     Mult
                       ( Mult
                           ( Var (0, 0),
                             Mult (Const (-1.78965), Const (-0.162534)) ),
                         Const 1.80837 ) ) ),
             Const 1.45342 ),
         Mult (Const (-1.02642), Sum (Var (0, 1), Const (-0.464846))) ))

let p4 =
  gen_poly
    (Mult
       ( Mult (Var (0, 0), Const (-0.0212035)),
         Sum
           ( Sum
               ( Var (0, 0),
                 Sum (Mult (Var (0, 1), Const 1.5982), Const (-1.30035)) ),
             Sum (Const (-1.59391), Mult (Const (-1.26347), Const 1.77068)) ) ))

let p5 =
  gen_poly
    (Mult
       ( Mult
           ( Const (-0.532449),
             Sum
               ( Const 0.178978,
                 Sum
                   ( Const (-1.96509),
                     Mult
                       ( Sum
                           ( Mult (Const 0.675274, Const 0.845333),
                             Mult
                               ( Sum
                                   ( Sum
                                       ( Sum
                                           ( Mult (Const 0.344589, Const 1.4286),
                                             Const (-1.7311) ),
                                         Mult (Const (-0.459082), Var (1, 1)) ),
                                     Const 0.209464 ),
                                 Const 1.69843 ) ),
                         Var (0, 1) ) ) ) ),
         Const 1.6956 ))

let expoly = Alcotest.testable EP.print (fun x y -> x = y)

let heaviside =
  Alcotest.testable EP.Heaviside.print (fun x y ->
      let open EP.Heaviside in
      x -.. y = zero)

let ep1 = EP.term (var x) (Const 5)
let ep2 = EP.term p0 (Finite (3, x))
let ep3 = EP.term (const (F.of_float 12.0)) (Const 0)
let p6 = var ~exp:(-2) s -.. var ~exp:(-3) s
let ep4 = EP.term p6 (Const 1)
let p7 = const (F.of_float 0.5) *.. (const (F.of_float 1.0) -.. var ~exp:2 s)
let ep5 = EP.term p7 (Finite (1, s))

let pfrac =
  let open Papprox in
  let c x = const (F.of_float x) in
  var x -.. var y +.. c 0.5
  +.. (c 0.25 *.. var ~exp:2 x)
  +.. (c 0.75 *.. var ~exp:3 x)
  -.. (c 0.25 *.. var ~exp:2 y)
  -.. (c 0.75 *.. var ~exp:3 y)
  +.. var VarSet.zero_var

(* Run it *)
let () =
  let open Alcotest in
  run "Unitaire"
    [
      ( "Num.of_float",
        [
          test_case "num of_float" `Quick (fun () ->
              Alcotest.(check string)
                "same value" "122393/34359738368"
                Fl.Num.(
                  to_string @@ max (of_float 3.56213e-6) (of_float 3.562e-6)));
        ] );
      ( "Num.to_int",
        [
          test_case "num to_int" `Quick (fun () ->
              Alcotest.(check int)
                "same value" 3562112
                Fl.Num.(to_int @@ of_float 3.56213e+6));
        ] );
      ( "Num.powi",
        [
          test_case "num powi" `Quick (fun () ->
              Alcotest.(check string)
                "same value" "1/512"
                Fl.Num.(to_string @@ powi (powi (of_int 2) (-3)) 3));
        ] );
      ( "Zarith.powi",
        [
          test_case "Q powi" `Quick (fun () ->
              Alcotest.(check string)
                "same value" "1"
                Flq.Q.(to_string @@ powi (powi (of_int 2) (-3)) 0));
        ] );
      ("print_zero", [ test_case "test polynomials" `Quick test_zero ]);
      ("print_one", [ test_case "test polynomials" `Quick test_one ]);
      ( "print_frac",
        [
          test_case "test degree" `Quick (fun () ->
              Alcotest.(check string)
                "same value" "½-y-¼y²-¾y³+x+¼x²+¾x³"
                (Papprox.print Format.str_formatter pfrac;
                 Format.flush_str_formatter ()));
        ] );
      ( "find_root_sturm",
        [
          test_case "find root" `Quick (fun () ->
              Alcotest.(check (float 1e-9))
                "same value" 1.0993925636829928907
                Papprox.(
                  let p = apply pfrac y (pow (var x) 2) in
                  find_root_sturm p x 0.0));
          test_case "find root" `Quick (fun () ->
              Alcotest.(check (float 1e-9))
                "same value" (-0.36103611672660552275)
                Papprox.(
                  let p = apply pfrac y (pow (var x) 2) in
                  find_root_sturm p x (-1.0)));
        ] );
      ( "find_root",
        [
          test_case "find root" `Quick (fun () ->
              Alcotest.(check (float 1e-9))
                "same value" 1.0993925636829928907
                Papprox.(
                  let p = apply pfrac y (pow (var x) 2) in
                  find_root p x 1.0));
          test_case "find root" `Quick (fun () ->
              Alcotest.(check (float 1e-9))
                "same value" (-0.36103611672660552275)
                Papprox.(
                  let p = apply pfrac y (pow (var x) 2) in
                  find_root p x (-1.0)));
        ] );
      ("test_sum", [ test_case "test polynomials" `Quick test_sum ]);
      ("test_integral", [ test_case "test polynomials" `Quick test_integral ]);
      ( "test_degree",
        [
          test_case "test degree" `Quick (fun () ->
              Alcotest.(check int) "same value" 10 (P.degree p1));
          test_case "test degree" `Quick (fun () ->
              Alcotest.(check int) "same value" 0 (P.degree p2));
          test_case "test degree" `Quick (fun () ->
              Alcotest.(check int) "same value" 5 (P.degree p3));
        ] );
      ( "test_gen",
        [
          test_case "test distributivity" `Quick (fun () ->
              Alcotest.(check poly)
                "same value" zero
                (((p1 +.. p2) *.. p3) -.. ((p1 *.. p3) +.. (p2 *.. p3))));
          test_case "test distributivity" `Quick (fun () ->
              Alcotest.(check poly)
                "same value" zero
                ((p3 *.. (p1 +.. p2)) -.. ((p1 *.. p3) +.. (p2 *.. p3))));
        ] );
      ( "test_diff",
        [
          test_case "test primitive diff" `Quick (fun () ->
              Alcotest.(check poly)
                "same value" zero
                (diff (primitive p1 x) x -.. p1));
        ] );
      ( "test_gen_poly",
        let open EP in
        [
          test_case "test commute" `Quick (fun () ->
              Alcotest.(check expoly) "same value" ep1 (ep1 +.. ep2 -.. ep2));
          (* test_case "test distributivity" `Quick
             (fun () ->(Alcotest.(check expoly) "same value" ( (ep1 +.. ep2) *.. ep3 ) ( ep1*..ep3 +.. ep2 *.. ep3 )));
                       test_case "test distributivity" `Quick
             (fun () ->(Alcotest.(check expoly) "same value" zero (( ep3 *.. (ep1 +.. ep2)) -.. ( ep1*..ep3 +.. ep2 *.. ep3 ))
               ))*)
        ] );
      ( "test_inverse_laplace",
        let open EP.Heaviside in
        [
          test_case "test inv laplace" `Quick (fun () ->
              Alcotest.(check heaviside)
                "same value" (inverse_laplace s ep4) ep5);
        ] );
    ]
