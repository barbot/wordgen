echo "== First table of example 3"
echo "n=0"
wordgen --debug --poly 0 --traj 0 --exact-rational --frequency 2 example/thick-twin.prism | sed -n -e '/Saving*/,$p' |grep -C 3 "is_accepting" | grep "id\|weight" 
echo "n=1"
wordgen --debug --poly 1 --traj 0 --exact-rational --frequency 2 example/thick-twin.prism | sed -n -e '/Saving*/,$p' |grep -C 3 "is_accepting" | grep "id\|weight"  
echo "n=2"
wordgen --debug --poly 2 --traj 0 --exact-rational --frequency 2 example/thick-twin.prism | sed -n -e '/Saving*/,$p' |grep -C 3 "is_accepting" | grep "id\|weight"

echo "
== Generating Fig 2"
wordgen --poly 2 --no-cache --traj 50000 --expected-duration 0.4 example/thick-twin.prism --output-format timestamp /tmp_wordgen/fig2a.dat --gnuplot-driver png
wordgen --poly 2 --no-cache --traj 50000 example/thick-twin.prism --output-format timestamp /tmp_wordgen/fig2b.dat --gnuplot-driver png
wordgen --poly 2 --no-cache --traj 50000 --expected-duration 0.9 example/thick-twin.prism --output-format timestamp /tmp_wordgen/fig2c.dat --gnuplot-driver png

echo "
== Generating Fig 3"
wordgen --poly 3 --traj 100000 --no-cache --output-format void --expected-duration 5.5 --apericube example/bimodal.prism /tmp_wordgen/fig3.out --gnuplot-driver png --gnuplot-cmd "set xrange [-0.5 :*]
binwidth = 0.25
binstart= -0.5
set ytics nomirror
set boxwidth 0.9*binwidth
set style fill solid 0.5
plot '/tmp_wordgen/fig3.out' i 0 u (binwidth*(floor((\$3-binstart)/binwidth)+0.5)+binstart):(1.0) smooth freq w boxes axes x1y1 title 'duration'"

echo "
== Generating Fig 5"
wordgen example/TrainGate/train-gate_3.xml --no-cache --expected-duration 240 --poly 10 --output-format timeline --receding 40 out.traj --traj 1
sed -e 's/Train..Safe/1/g' -e 's/Train..Appr/2/g' -e 's/Train..Cross/5/g' -e 's/Train..Stop/3/g' -e 's/Train..Start/4/g' out.traj > out2.traj
gnuplot example/TrainGate/gnuplot_train.gp
mv out.pdf /tmp_wordgen/fig5a.pdf

wordgen example/TrainGate/train-gate_3.xml --no-cache --expected-duration 80 --poly 10 --output-format timeline --receding 40 out.traj --traj 1
sed -e 's/Train..Safe/1/g' -e 's/Train..Appr/2/g' -e 's/Train..Cross/5/g' -e 's/Train..Stop/3/g' -e 's/Train..Start/4/g' out.traj > out2.traj
gnuplot example/TrainGate/gnuplot_train.gp
mv out.pdf /tmp_wordgen/fig5b.pdf 



echo "
== Generating Table 1"
echo "N=1 n=10"
/usr/bin/time --format="memory:%MkB" wordgen example/TrainGate/train-gate_1.xml --poly 10 --traj 1000 --expected-duration 50 out.dat --no-cache
echo "
N=2 n=10"
/usr/bin/time --format="memory:%MkB" wordgen example/TrainGate/train-gate_2.xml --poly 10 --traj 1000 --expected-duration 50 out.dat --no-cache
echo "
N=3 n=10"
/usr/bin/time --format="memory:%MkB" wordgen example/TrainGate/train-gate_3.xml --poly 10 --traj 1000 --expected-duration 50 out.dat --no-cache
echo "
N=4 n=10"
/usr/bin/time --format="memory:%MkB" wordgen example/TrainGate/train-gate_4.xml --poly 10 --traj 1000 --expected-duration 50 out.dat --no-cache
echo "
N=5 n=7"
/usr/bin/time --format="memory:%MkB" wordgen example/TrainGate/train-gate_5.xml --poly 7 --traj 1000 --expected-duration 50 out.dat --no-cache
echo "
N=5 n=10"
/usr/bin/time --format="memory:%MkB" wordgen example/TrainGate/train-gate_5.xml --poly 10 --traj 1000 --expected-duration 50 out.dat --no-cache
