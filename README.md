# WordGen

*WordGen : Time Word generator.* WordGen is a tool taking as input a time automaton and which sample time words from the language of the automaton. Time words are sampled according to the uniform distribution i.e. each time word is sampled with the same probability. Details the theory behind WordGen can be found in the following papers:

A web application demonstrating this tool is available here https://barbot.pages.lacl.fr/wordgen/.

>To cite Wordgen, please use the most recent tool paper, from HSCC'23: 
>
>Benoit Barbot, Nicolas Basset, and Alexandre Donze. 2023. [Wordgen : a Timed word Generation Tool](https://lacl.fr/~barbot/file/wordgen.pdf). In Proceedings of the 26th ACM International Conference on Hybrid Systems: Computation and Control (HSCC '23). Association for Computing Machinery, New York, NY, USA, Article 16, 1–7. https://doi.org/10.1145/3575870.3587116


## Publications:
- Benoît Barbot, Nicolas Basset, Thao Dang, Alexandre Donzé, James Kapinski, Tomoya Yamaguchi:
[Falsification of Cyber-Physical Systems with Constrained Signal Spaces](https://lacl.fr/~barbot/file/MetaSquare_NFM20.pdf). NFM 2020: 420-439

- B. Barbot, N. Basset and T. Dang. [Generation of signals under temporal constraints for CPS testing](http://www.lacl.fr/~barbot/file/BBD19.pdf).  In NASA Formal Methods 11th International Symposium, NFM 2019, Houston, TX, USA, May 7-9, 2019, volume 11460 of LNCS, pages 54-70. Springer, 2019.

- B. Barbot, N. Basset, M. Beunardeau and M. Kwiatkowska. [Uniform Sampling for Timed Automata with Application to Language Inclusion Measurement](https://www.irif.univ-paris-diderot.fr/_media/users/nbasset/unif-sampling-ta.pdf).  In QEST'16, volume 9826 of LNCS, pages 175-190. Springer, 2016.


## Compilation

### Dependencies
 - install ocaml >= 4.08
 - using opam (install opam : https://opam.ocaml.org/doc/Install.html):
```bash 
opam install depext ocamlfind ocamlbuild dune
opam depext -i xml-light yojson zarith num
```
 - without opam on Debian : 
```bash
apt install libyojson-ocaml-dev libxml-light-ocaml-dev libyojson-ocaml-dev libzarith-ocaml-dev ocamlbuild dune ocaml-findlib
```

### Compile
Run 
```
make
```
## Usage

Run with
```
./wordgen example/simpleTriAngle.prism --poly 5 --traj 100 --sampler random --output-format timeword trajectory.out
```

This generates 100 trajectories (--traj 100) of length 5 (--poly 5) using a random number generator (--sampler random) and put them in trajectory.out, trajectory are saved as timeword (--output-format timeword).

Alternatively you can use one of the example files which are already zone graph (.json files) produce buy prism.

You can plot the trajectories if gnuplot is installed with:
```
./wordgen example/simpleTriAngle.prism --poly 2 --traj 5000 simpleTriAngle.out --gnuplot-driver png
```

Should produce an image in simpleTriAngle.png :

![SimpleTriangleSampling](example/simpleTriAngle.png)

You can see the detail of the computation with the options --debug which print intermediate representations (zone graph, splitted zone graph and polynomials).


### Output Format
Wordgen provides 5 output format for trajectory which is specified with the --output-format option.
Trajectories are saved to a file (2nd arguments) if specified and printed on the standard output otherwise.

Timestamp (default).
Each trajectory is printed on a single line only delays between events are printed.
```
>./wordgen example/simpleTriAngle.prism -v 0 --traj 1 --output-format timestamp
0.400893	0.358138	0.148591
```

Timeword.
Same as timestamp but the label of each transition is printed between timestamps.
```
>./wordgen example/simpleTriAngle.prism --traj 1 --output-format timeword -v 0
0.334164[a] 0.396973[b] 0.827334[a]
```

Timeword with state.
States of the automaton (location + clock valuation) are printed between events.
```
>./wordgen.native example/simpleTriAngle.prism --traj 1 --output-format timeword_state -v 0
(0  0.000000  0.000000  )
0.178213[a] (1  0.000000  0.178213  )
0.113232[b] (0  0.000000  0.000000  )
0.652722[a]
```

State list.
Only the state of the automaton is printed each on a new line, trajectories are separated by empty lines.
```
>./wordgen example/simpleTriAngle.prism --traj 1 --output-format state_list -v 0
0  0.000000  0.000000
0  0.127534  0.127534
1  0.000000  0.127534
1  0.557333  0.684867
0  0.000000  0.000000
0  0.653971  0.653971
```

Void.
Output nothing. For debug purpose.

### Template mode
You can use wordgen to compute the mapping between a hypercube and the associated polytope with the option "--template" which take a string as an argument.

```
>./wordgen example/simpleTriAngle.prism --template "0.5[a]0.3[b]" --traj 1
0.292893 0.212132
```

The argument is a time word template. It specifies a time word where some time and/or action are left for wordgen to compute. Actions are indicated in square bracket, they can be of three types:
 - A single character specifying the name of an action in the automaton;
 - the action `[_]` lets wordgen choose uniformly at random an action;
 - a real number between 0 and 1, then an action is chosen uniformly by using the value as a random sample.

Similarly the delay between events are of three types:
 - A timestamp prefixed by the character '@' specify the time of the delay;
 - the delay `_` lets wordgen choose uniformly at random the delay;
 - a real number between 0 and 1, then a delay is chosen uniformly by using the value as a random sample.

 By default when no template is provided the template `( _ [_] )^n` is used with n the length of the time word.
