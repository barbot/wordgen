system:ad94_fig10

clock:1:x

event:a
event:b
event:t

process:P1
location:P1:l0{initial:}
location:P1:l1{}
location:P1:l2{}
location:P1:l3{}
edge:P1:l0:l1:t{provided: x<10}
edge:P1:l1:l2:a{provided: x>5 && x<10}
edge:P1:l1:l3:b{provided: x<5}

process:P2
location:P2:l0{initial:}
location:P2:l1{}
location:P2:l2{}
location:P2:l3{}
edge:P2:l0:l1:t{provided: x<10}
edge:P2:l1:l2:a{provided: x>5 && x<10}
edge:P2:l1:l3:b{provided: x<5}


process:S
location:S:l0{initial:}
location:S:l1{}
location:S:l2{}
edge:S:l0:l1:a{}
edge:S:l0:l2:b{}

sync:P1@a:S@a
sync:P2@a:S@a

sync:P1@b:S@b
sync:P2@b:S@b
