set terminal pdf size 10,1.3
set output 'out.pdf'
set pointsize 0.1


set tics font "Computer Modern,18"
set key font "Computer Modern,18"

#set ytics ("Safe" 0.1667, "Appr" 0.333, "Cross" 0.8333, "Stop" 0.5, "Start" 0.666,\
	"Safe" 1.1667, "Appr" 1.333, "Cross" 1.8333, "Stop" 1.5, "Start" 1.666,\
	"Safe" 2.1667, "Appr" 2.333, "Cross" 2.8333, "Stop" 2.5, "Start" 2.666)
	
set ytics ("Safe" 1, "Appr" 2, "Cross" 5, "Stop" 3, "Start" 4)

#      set y2range [0.0:3.4]
      set yrange [0.8:5.2]
#      plot 'out2.traj' u 1:($19/6) w line title "Train0" lw 2 axes x1y1,\
#      	   '' u 1:($20/6+1) w line title "Train1" lw 2 axes x1y1,\
#      	   '' u 1:($21/6+2) w line title "Train2" lw 2 axes x1y1
      	   
      plot 'out2.traj' u 1:18 w line title "Train0" lw 4 axes x1y1,\
      	   '' u 1:19 w line title "Train1" lw 4 axes x1y1,\
      	   '' u 1:20 w line title "Train2" lw 4 axes x1y1      	   
      	   
#      plot for [col=19:21] 'out2.traj' u 1:col w line title "train" lw 2 axes x1y1
#      	   for [col=6:8] '' u 1:col w line title columnhead ls 1 lw 4 axes x1y2,\
#      	   for [col=9:11] '' u 1:col w line title columnhead ls 2 lw 4 axes x1y2,\
#      	   for [col=12:14] '' u 1:col w line title columnhead ls 3 lw 4 axes x1y2,\
#      	   for [col=15:17] '' u 1:col w line title columnhead ls 4 lw 4 axes x1y2,\
#      	   for [col=18:18] '' u 1:col w line title columnhead ls 5 lw 4 axes x1y2
      	   
      	   
#for [col=5:5] 'out2.traj' u 1:col w line title columnhead lw 2 axes x1y1,\
	   
