

#../../wordgen train-gate_3.xml --expected-duration 100 --poly 10 --output-format timeline --receding 10 out.traj --traj 1
#sed -e 's/Train..Safe/1/g' -e 's/Train..Appr/2/g' -e 's/Train..Cross/5/g' -e 's/Train..Stop/3/g' -e 's/Train..Start/4/g' out.traj > out2.traj
#gnuplot gnuplot_train.gp
#mv out.pdf out_10.pdf

../../wordgen train-gate_3.xml --expected-duration 240 --poly 10 --output-format timeline --receding 40 out.traj --traj 1
sed -e 's/Train..Safe/1/g' -e 's/Train..Appr/2/g' -e 's/Train..Cross/5/g' -e 's/Train..Stop/3/g' -e 's/Train..Start/4/g' out.traj > out2.traj
gnuplot gnuplot_train.gp
mv out.pdf out_20.pdf


#../../wordgen train-gate_3.xml --expected-duration 100 --poly 10 --output-format timeline --receding 30 out.traj --traj 1
#sed -e 's/Train..Safe/1/g' -e 's/Train..Appr/2/g' -e 's/Train..Cross/5/g' -e 's/Train..Stop/3/g' -e 's/Train..Start/4/g' out.traj > out2.traj
#gnuplot gnuplot_train.gp
#mv out.pdf out_30.pdf


#../../wordgen train-gate_3.xml --expected-duration 100 --poly 10 --output-format timeline --receding 40 out.traj --traj 1
#sed -e 's/Train..Safe/1/g' -e 's/Train..Appr/2/g' -e 's/Train..Cross/5/g' -e 's/Train..Stop/3/g' -e 's/Train..Start/4/g' out.traj > out2.traj
#gnuplot gnuplot_train.gp
#mv out.pdf out_40.pdf


../../wordgen train-gate_3.xml --expected-duration 80 --poly 10 --output-format timeline out.traj --receding 40 --traj 1
sed -e 's/Train..Safe/1/g' -e 's/Train..Appr/2/g' -e 's/Train..Cross/5/g' -e 's/Train..Stop/3/g' -e 's/Train..Start/4/g' out.traj > out2.traj
gnuplot gnuplot_train.gp
mv out.pdf out_60.pdf


#../../wordgen train-gate_3.xml --expected-duration 100 --poly 10 --output-format timeline --receding 70 out.traj --traj 1
#sed -e 's/Train..Safe/1/g' -e 's/Train..Appr/2/g' -e 's/Train..Cross/5/g' -e 's/Train..Stop/3/g' -e 's/Train..Start/4/g' out.traj > out2.traj
#gnuplot gnuplot_train.gp
#mv out.pdf out_70.pdf


