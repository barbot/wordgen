open Ocamlbuild_plugin

let () = Options.use_ocamlfind := true
let _ = Bisect_ppx_plugin.handle_coverage ()

(*let () = Ocamlbuild_plugin.dispatch Ocamlbuild_js_of_ocaml.dispatcher*)
