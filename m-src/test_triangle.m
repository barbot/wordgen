num_evt = 3;
sg = var_cp_signal_gen('in',num_evt+1, 'linear');
S = BreachSignalGen(sg);


pg = TA_param_gen('in','../example/simpleTriAngle.prism', 2);
pg.set_template_in('%g[a]%g[b]')

%%
pg.computeParams([0 1 0 1;0 0 1 1])

%%
S.SetParamGen(pg);
S.SetParamRanges({'in_e0', 'in_e1'}, [0 1]);
S.QuasiRandomSample(10) 


%%
S.GetParam({'in_dt0', 'in_dt1'})

%%
Fi= BreachSamplesPlot(S, {'in_e0', 'in_e1'});
Fo= BreachSamplesPlot(S, {'in_dt0', 'in_dt1'});
plot([0 1], [1 0]);

%%
figure;
S.Sim(0:.001:1)
S.PlotSignals();