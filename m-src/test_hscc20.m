num_evt = 51;
sg = var_cp_signal_gen('in',num_evt+2, 'linear');
S = BreachSignalGen(sg);

%%
pg = TA_param_gen('in','../example/hscc20.json', num_evt);

sdt_template = '%g[b]%g[c]%g[d]';
num = 3;
while num<num_evt
    num = num +4;
    sdt_template = [sdt_template '%g[a]%g[b]%g[c]%g[d]'];
end
sdt_template = sdt_template(1:5*num_evt);

%%
pg.set_template_in(sdt_template);
pg.template_in = regexprep(pg.template_in,'\[(\w+)\]', '\[_\]');

%%
S.SetParamGen(pg);
S.SetParamRanges(pg.params, [0 1]);
S.QuasiRandomSample(4) 


%%
figure;
S.Sim(0:0.001:33);
for i =1:3
    subplot(3,1,i);
    S.PlotSignals('in', i, [], true);
end