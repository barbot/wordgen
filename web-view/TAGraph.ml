open Prism
open GraphEditor

type def = string list
type invariant = string
type guard = bool Type.expr'
type state = string * invariant * bool * bool
type arc = string * guard * string list * float
type attribute_id = int

type attribute =
  [ `Choice of string list
  | `Check of bool
  | `ControlPoint of DrawingGeom.point
  | `String of string
  | `Color of string ]

let string_of_reset r =
  Printf.sprintf "{%s}"
    (List.fold_left (fun x s -> if x = "" then s else x ^ "," ^ s) "" r)

(* let reset_of_string (label,guard,_,bend) s =
    let ins = String.sub v 1 (String.length v-2) in
    Some (label,guard, (String.split_on_char ',' ins), bend ) with
   _ -> None)*)

let reset_of_string s =
  let ll = Lexing.from_string s in
  ParserPrism.resetPlain LexerPrism.token ll

let _ = PrismType.allReal := true

let string_of_guard g =
  Format.fprintf Format.str_formatter "%a" Type.printH_expr g;
  Format.flush_str_formatter ()

let guard_of_string s =
  let ll = Lexing.from_string s in
  ParserPrism.stateCondition LexerPrism.token ll

let init_def () = []
let init_arc _ _ = Some ("a", Type.Bool true, [], 0.0)
let state_id = ref 0

let init_state () =
  incr state_id;
  (string_of_int !state_id, "true", !state_id = 1, false)

let draw_state (s, inv, init, final) p =
  let l =
    [
      `Circle (p, 10.0); `Text (p, s ^ if inv <> "true" then "," ^ inv else "");
    ]
  in
  let x, y = p in
  let l2 =
    if init then
      `Line (p, (x -. 25.0, y)) :: `Arrow ((x -. 10.0, y), (x -. 25.0, y)) :: l
    else l
  in
  if final then `Circle (p, 13.0) :: l2 else l2

let arc_offset = 8.0

let draw_arc (label, guard, reset, bend) (source_sh, target_sh) =
  let open DrawingGeom in
  let open Point in
  let pos1 = center_shape source_sh in
  let pos2 = center_shape target_sh in
  let text =
    Format.sprintf "[%s]%s%s" label
      (if guard = Type.Bool true then "" else "," ^ string_of_guard guard)
      (if reset = [] then "" else "," ^ string_of_reset reset)
  in

  if source_sh <> target_sh then
    if bend <> 0.0 then
      let p =
        mult 0.5
          (pos1 +.. pos2
          -.. mult (sin (bend /. 180.0 *. pi)) (rot (pi /. 2.0) (pos2 -.. pos1))
          )
      in
      shapes_of_path source_sh
        [ `Text (0.5, arc_offset, text); `ControlPoint p ]
        ~arrow2:(fun x y -> `Arrow (x, y))
        target_sh
    else
      shapes_of_path source_sh
        [ `Text (0.5, arc_offset, text) ]
        ~arrow2:(fun x y -> `Arrow (x, y))
        target_sh
  else
    let rho = bend /. 180.0 *. pi in
    let p1 = pos1 +.. rot rho (50.0, -30.0)
    and p2 = pos1 +.. rot rho (50.0, 30.0) in
    shapes_of_path source_sh
      [ `ControlPoint p1; `Text (0.5, arc_offset, text); `ControlPoint p2 ]
      ~arrow2:(fun x y -> `Arrow (x, y))
      target_sh

let get_def_attr def =
  ( "Clocks",
    List.mapi (fun i c -> (i, "clock " ^ string_of_int i, None, `String c)) def
  )

let get_new_def_attr def =
  let n = List.length def in
  [
    ( "clock " ^ string_of_int n,
      fun d2 -> (d2 @ [ "clock_" ^ string_of_int n ], n) );
  ]

let update_def_attr def attr_id = function
  | Some (`String st) ->
      Printf.printf "Updating attr:%i -> %s\n" attr_id st;
      let _, listat =
        List.fold_left
          (fun (i, l) at ->
            if i = attr_id then (i + 1, st :: l) else (i + 1, at :: l))
          (0, []) def
      in
      Some (List.rev listat)
  | None ->
      Printf.printf "Updating attr:%i -> none\n" attr_id;
      let _, listat =
        List.fold_left
          (fun (i, l) nat ->
            match attr_id with
            | j when i = j -> (i + 1, l)
            | _ -> (i + 1, nat :: l))
          (0, []) def
      in
      Some (List.rev listat)
  | Some _ ->
      Printf.printf "Updating attr:%i -> ??\n" attr_id;
      None

let get_state_attr (s, inv, init, final) =
  ( "State",
    [
      (0, "content", Some "Label of the state", `String s);
      (1, "invariant", Some "Invarient on clock", `String inv);
      (2, "initial", Some "Is it the initial state", `Check init);
      (3, "final", Some "Is the state accepting", `Check final);
    ] )

let update_state_attr (s, inv, init, final) attr_id = function
  | None -> (
      match attr_id with
      | 0 -> Some ("", inv, init, final)
      | 1 -> Some (s, "true", init, final)
      | _ -> Some (s, inv, init, final))
  | Some (`String newv) when attr_id = 0 -> Some (newv, inv, init, final)
  | Some (`String newinv) when attr_id = 1 -> Some (s, newinv, init, final)
  | Some (`Check b) when attr_id = 2 -> Some (s, inv, b, final)
  | Some (`Check b) when attr_id = 3 -> Some (s, inv, init, b)
  | _ -> None

let get_new_state_attr _ _ = []

let get_arc_attr (label, guard, reset, bend) =
  ( "Arc",
    [
      (0, "label", None, `String label);
      (1, "guard", None, `String (string_of_guard guard));
      (2, "reset", None, `String (string_of_reset reset));
      (3, "bend", None, `String (string_of_float bend));
    ] )

let update_arc_attr (label, guard, reset, bend) attr_id = function
  | Some (`String v) when attr_id = 0 -> Some (v, guard, reset, bend)
  | Some (`String v) when attr_id = 1 -> (
      try
        let g = guard_of_string v in
        Some (label, g, reset, bend)
      with _ -> None)
  | Some (`String v) when attr_id = 2 -> (
      try
        let r = reset_of_string v in
        Some (label, guard, r, bend)
      with _ -> None)
  | Some (`String b) when attr_id = 3 -> (
      try Some (label, guard, reset, float_of_string b) with _ -> None)
  | _ -> None

let get_new_arc_attr _ _ = []

let string_of_attribute = function
  | `Prob arc -> string_of_float arc
  | `StringExpr s -> s
  | `Choice (t :: _) -> t
  | `Choice [] -> ""
  | `Check b -> string_of_bool b
  | `ControlPoint _ -> ""

let print_to_prism f def stateit arcit =
  let n = ref 0 in
  let init = ref 0 in
  stateit (fun id (_, _, isinit, _) _ ->
      if isinit then init := id;
      n := max !n id);
  Format.fprintf f "pta\nmodule m\n\tstate:[0..%i] init %i;\n" !n !init;
  List.iter (fun s -> Format.fprintf f "\t%s:clock;\n" s) def;

  arcit
    (fun
      _
      (label, guard, reset, _)
      ((source, (_, invarient, _, _), _), (target, _, _))
    ->
      let pg f a =
        if a = Type.Bool true then ()
        else Format.fprintf f "&%a" Type.printH_expr a
      in
      let g3 = if invarient = "true" then "" else "&" ^ invarient in
      Format.fprintf f "\t[%s] (state=%i)%a%s -> (state'=%i)" label source pg
        guard g3 target;
      List.iter (fun r -> Format.fprintf f "&(%s'=0)" r) reset;
      Format.fprintf f ";\n");
  Format.fprintf f "endmodule\n";

  let exist_accept = ref false in
  stateit (fun _ (_, _, _, isfin) _ -> exist_accept := !exist_accept || isfin);
  if !exist_accept then (
    Format.fprintf f "label \"accepting\" = true ";
    stateit (fun id (_, _, _, isfin) _ ->
        if not isfin then Format.fprintf f "&(state!=%i)" id);
    Format.fprintf f ";\n")

let print =
  [
    ( "dot",
      (fun f _ stateit arcit ->
        Format.fprintf f "digraph {\n";
        stateit (fun _ (s, _, _, _) (x, y) ->
            Format.fprintf f "%s [pos=\"%f,%f\"];\n" s x y);
        arcit (fun _ _ ((_, (source, _, _, _), _), (_, (target, _, _, _), _)) ->
            Format.fprintf f "%s -> %s;\n" source target);
        Format.fprintf f "}"),
      "markovChain.dot" );
    ("prism", print_to_prism, "pta.prism");
  ]

let parse_file _ _ _ = ()
