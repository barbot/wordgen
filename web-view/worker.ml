open Worker_sync
open Arguments
open Wordgen_lib

let _ = Js_of_ocaml.Worker.import_scripts [ "viz.js" ]
let is_stopped = ref true

exception Interrupted

let only_parse = ref false

let patternreg =
  Str.regexp
    "<text text-anchor=\"middle\" x=\"\\(.*\\)\" y=\"\\(.*\\)\" \
     font-family=\".*\" font-size=\"14.00\">\\([0-9]*\\)<.text>"

let svgpattern =
  Str.regexp
    "<svg version=\"1.1\" width=\"\\(.*\\)\" height=\"\\(.*\\)\" \
     xmlns=\"http://www.w3.org/2000/svg\">"

let add_pos x y (svg, _, _) =
  let open Str in
  try
    let n = Str.search_forward svgpattern svg 0 in
    let s1 = String.sub svg 0 n in
    let w = float_of_string @@ matched_group 1 svg in
    let h = float_of_string @@ matched_group 2 svg in
    s1
    ^ Printf.sprintf
        "<svg version=\"1.1\" x=\"%f\" y=\"%f\" width=\"%f\" height=\"%f\" \
         xmlns=\"http://www.w3.org/2000/svg\">"
        (x -. (w /. 2.0))
        (y -. (h /. 2.0))
        w h
    ^ String.sub svg (match_end ()) (String.length svg - match_end ())
  with Not_found -> svg

let rec replace_svg dot imagefile i =
  let open Str in
  try
    let n = Str.search_forward patternreg dot i in
    let n2 = match_end () in
    let s1 = String.sub dot i (n - i) in
    let x = float_of_string @@ matched_group 1 dot in
    let y = float_of_string @@ matched_group 2 dot in
    let id = int_of_string (matched_group 3 dot) in
    print_endline ("match" ^ matched_string dot);
    s1 ^ add_pos x y imagefile.(id) ^ replace_svg dot imagefile n2
  with Not_found -> String.sub dot i (String.length dot - i)

let computation ta outfile =
  let module Bound =
    (val if !npoly < 0 then
           (module ZoneGraphInput.GeneralBound : ZoneGraphInput.BoundType)
         else (module ZoneGraphInput.LinearBound))
  in
  let module ZGI = ZoneGraphInput.Make (Bound) in
  let rg =
    match !regexp with
    | Some re ->
        let lexbuf = Lexing.from_string re in
        let open Regexp_lib in
        let re = Regexp.main Regexplex.tok lexbuf in
        let regauto = Automata.Notdet.of_regexp re in
        let a = Automata.Notdet.to_det regauto in
        let ta = To_ta.ta_from_regexp a in
        ZGI.zone_graph_from_ta ta
    | _ ->
        ZGI.input_from_string
          ?bound_all_guard:
            (if !is_duration_exact then
               match !expected_duration with
               | None -> None
               | Some f when Float.is_integer f -> Some (int_of_float f)
               | Some _ -> None
             else None)
          ta
  in
  Js_of_ocaml.Worker.post_message ParseOK;
  let module Param =
    (val Compute.gen_param ~out_format:!out_style ~perturbation:None
           ~is_interactive:false rg.ZoneGraph.var_string !rational_impl
           !frequency !expected_duration !is_duration_exact !boltzmann_param
           !expected_size !verbose !npoly true)
  in
  if not !only_parse then (
    let module Weight =
      Compute.Instantiate (Weight_structure.Fl.Num) (Bound) (Param) ()
    in
    let rgpoly = Weight.compute_weight !print_rg !infile rg in

    let boltz, smp =
      Weight.compute_params ?seed:!random_seed !template rgpoly
    in

    if !print_rg then (
      let buff = Buffer.create 100 in
      let form = Format.formatter_of_buffer buff in
      let svg_array = ZoneGraph.print_dot rgpoly Bound.print form in
      Format.fprintf form "@.";
      Format.printf "%a@." (ZoneGraph.print Bound.print) rgpoly;
      let dot = Buffer.contents buff in
      let open Js_of_ocaml.Js in
      let svgdot =
        Unsafe.fun_call (Unsafe.js_expr "Viz")
          [| Unsafe.inject (string dot); Unsafe.inject (string "svg") |]
      in
      (*print_endline (to_string svgdot);*)
      let rep = replace_svg svgdot svg_array 0 in
      Js_of_ocaml.Worker.post_message (Graph (String.escaped rep)));

    Weight.sample smp ~outfile ?seed:!Arguments.random_seed ?boltz
      ~max_iter:!max_iteration
      ?exact_duration:(if !is_duration_exact then !expected_duration else None)
      ~sampler:(Low_disc_sampler.get_sampler !sampler)
      ~store_traj:!store_traj rgpoly !template !nbtraj;

    let data = "myData.dat" in
    String.escaped
    @@ Option.value ~default:""
         (OutFormat.plot_of_style rgpoly !out_style !store_traj !nbtraj data
            (!expected_duration <> None)
            !template))
  else ""

let main cmd ta out_formatter =
  reset spec_short;
  (*if !only_parse then npoly := -1 else npoly := 3;*)
  only_parse := false;
  Arg.current := 0;
  let argv =
    cmd |> Scanf.unescaped |> String.split_on_char ' ' |> Array.of_list
  in
  Arg.parse_argv argv
    (("--only-parse", Arg.Set only_parse, "") :: spec_caml_arg)
    (fun _ -> ())
    usage_str;
  post_parse ();
  computation ta out_formatter

let _ =
  Js_of_ocaml.Worker.set_onmessage (function cmd, ta ->
      (* | Stop ->
            is_stopped := true;
            print_endline "Stop"

         | Task x ->*)
      (is_stopped := false;
       print_endline ("New Job :" ^ cmd);
       let buff_std = Buffer.create 10 in
       Format.set_formatter_output_functions (Buffer.add_substring buff_std)
         (fun () ->
           let m = Buffer.contents buff_std in
           Buffer.clear buff_std;
           Js_of_ocaml.Worker.post_message (StdOut m));
       let buff_out = Buffer.create 100 in
       let out_formatter =
         Format.make_formatter (Buffer.add_substring buff_out) (fun () ->
             let m = Buffer.contents buff_out in
             Buffer.clear buff_out;
             Js_of_ocaml.Worker.post_message (Data m))
       in
       (*computation out_formatter x;*)
       try
         let draw_cmd = main cmd ta out_formatter in
         Js_of_ocaml.Worker.post_message (Finish draw_cmd)
       with
       | Interrupted -> ()
       | x ->
           Js_of_ocaml.Worker.post_message
             (Error (String.escaped (Printexc.to_string x)))))
