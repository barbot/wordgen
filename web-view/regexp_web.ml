open Html
open Common
open Js_of_ocaml

let _ =
  let open Arguments in
  regexp := Some "a*";
  expected_size := Some 5.0;
  out_style := Wordgen_lib.OutFormat.Word;
  print_rg := false

let build_interface_elem up (opt, x, help) =
  let open Arguments in
  let input =
    match x with
    | Bool f ->
        [
          checkbox_input
            ~on_change:(fun x ->
              f := x;
              up ())
            !f;
        ]
    | Set_int a ->
        [
          text_input ~_type:"number"
            ~on_change:(fun x ->
              a := int_of_string_opt x;
              up ())
            (!a |>>> string_of_int |>>| "");
        ]
    | Set_float a ->
        [
          text_input ~_type:"text"
            ~on_change:(fun x ->
              a := float_of_string_opt x;
              up ())
            (!a |>>> string_of_float |>>| "");
        ]
    | Set_string f ->
        [
          text_input ~_type:"text"
            ~on_change:(fun x ->
              f := if x = "" then None else Some x;
              up ())
            (!f |>>| "");
        ]
    | Int (f, df) ->
        [
          text_input ~_type:"number"
            ~on_change:(fun x ->
              (match int_of_string_opt x with
              | None -> f := df
              | Some v -> f := v);
              up ())
            (string_of_int !f);
        ]
    | Float (f, df) ->
        [
          text_input ~_type:"text"
            ~on_change:(fun x ->
              (match float_of_string_opt x with
              | None -> f := df
              | Some v -> f := v);
              up ())
            (string_of_float !f);
        ]
    | String (a, _) ->
        [
          text_input ~_type:"text"
            ~on_change:(fun x ->
              a := x;
              up ())
            !a;
        ]
    | Symbol s ->
        let module S = (val s : SYM) in
        [
          select S.list ~def:(S.to_string !S.content) ~on_change:(fun x ->
              S.content := S.of_string x;
              up ());
        ]
  in
  div ~class_:"cmdelem" ~title:help ([ text opt; text " : " ] @ input)

let build_cmd l = Arguments.build_cmd l
let only_parse = ref false

let build_interface ?class_:_ ?stop:_ up l =
  let cmd, cmdup = kbd' (build_cmd l) in
  let up2 () =
    cmdup (build_cmd l);
    up (not !only_parse)
  in
  let elemlist = List.map (build_interface_elem up2) l in

  let bottom =
    [
      button ~on_click:(fun _ -> up true) [ text "Compute" ];
      (*button ~on_click:(fun _ -> stop ()) [text "Stop"];*)
      text " autocompute: ";
      checkbox_input
        ~on_change:(fun x ->
          only_parse := not x;
          up2 ())
        (not !only_parse);
    ]
  in

  (text "arguments : " :: elemlist) @ (br () :: cmd :: br () :: bottom)

let _ =
  run' @@ fun () ->
  let open Js_of_ocaml.Url in
  let args = Current.arguments in
  (match List.assoc_opt "cmd" args with
  | None -> ()
  | Some cmd ->
      cmd |> urldecode |> String.split_on_char ' ' |> Array.of_list |> fun a ->
      Arg.parse_argv a Arguments.spec_caml_arg (fun _ -> ()) Arguments.usage_str);

  let stdoutput_up = update_value_by_id "cmd out" in
  let outfile_up = update_value_by_id "data out" in

  let container, _, toggle_container = get_by_id' "graphvis" in
  let _, alert_up, _ = get_by_id' "alert_pb" in
  let _, sig_up, _ = get_by_id' "sig" in
  let gnuplotvis, _, toogle_gnuplot = get_by_id' "gnuplotvis img" in

  let worker = Js_of_ocaml.Worker.create "worker.js" in
  let buffstd = Buffer.create 100 in
  let buffdata = Buffer.create 100 in
  let dirup () =
    let open Js_of_ocaml.Url in
    let link = Current.path_string in
    (*let link_code_list = match Current.path with
        | ""::"index.html"::q -> "index-code.html"::q
        | "index.html"::q -> "index-code.html"::q
        | l -> "index-code.html" :: l in
      let link_code = List.fold_left (fun acc v -> if acc <>"" then if v<>"" then v^"/"^acc else acc else v) "" link_code_list in*)
    let _, dirlink_up, _ = get_by_id' "directlink" in

    dirlink_up
      [
        a
          ~href:
            (Printf.sprintf "%s?cmd=%s" link
               (urlencode (build_cmd Arguments.spec_minimal)))
          [ text "Direct link" ];
        text "  ";
      ]
  in

  worker##.onmessage :=
    Dom.handler (fun m ->
        (match m##.data with
        | Worker_sync.StdOut s ->
            Buffer.add_string buffstd s;
            stdoutput_up (Buffer.contents buffstd)
        | Data d ->
            Buffer.add_string buffdata d;
            if Buffer.length buffdata < 10000 then
              outfile_up (Buffer.contents buffdata)
        | Graph g ->
            let g2 = Scanf.unescaped g in
            toggle_container "block";
            Js.Unsafe.fun_call
              (Js.Unsafe.js_expr "loadVis")
              [| Js.Unsafe.inject container; Js.Unsafe.inject (Js.string g2) |]
        | Error err ->
            let eerr = Scanf.unescaped err in
            outfile_up "";
            sig_up [];
            alert_up [ text eerr ]
        | ParseOK ->
            sig_up [ text "Parsing OK" ];
            alert_up []
        | Finish gnucmd ->
            sig_up [ text "Computation Finished" ];
            outfile_up (Buffer.contents buffdata);
            (match gnucmd with
            | "" -> ()
            | plot ->
                toogle_gnuplot "block";
                Js.Unsafe.fun_call
                  (Js.Unsafe.js_expr "compute_gnuplot")
                  [|
                    Js.Unsafe.inject gnuplotvis;
                    Js.Unsafe.inject (Js.string (Buffer.contents buffdata));
                    Js.Unsafe.inject (Js.string (Scanf.unescaped plot));
                  |]);
            print_endline "Computation finished");
        Js._true);
  let launch_compt b =
    let open Arguments in
    dirup ();
    (*post_parse ();*)
    stdoutput_up "";
    outfile_up "";
    sig_up [];
    alert_up [];
    toggle_container "none";
    toogle_gnuplot "none";
    Buffer.clear buffstd;
    Buffer.clear buffdata;
    let cmd =
      Arguments.spec_minimal @ spec_short
      @ [ ("--only-parse", Arguments.Bool (ref b), "") ]
      |> build_cmd |> String.escaped
    in
    worker##postMessage (cmd, "")
  in

  set_by_id "argument"
  @@ build_interface ~class_:""
       (*~stop:(fun () -> worker##postMessage (Worker_sync.Stop))*) launch_compt
       Arguments.
         [
           ("--debug", Bool print_rg, "Print the intermediate representation");
           ("--traj", Int (nbtraj, 10), "Number of trajectories");
           ("--seed", Set_int random_seed, "seed of the random sampler");
           ("-v", Int (verbose, 1), "Verbose level");
           ("--regexp", Set_string regexp, "Take a regular expression as input");
           ( "--expected-size",
             Set_float expected_size,
             "time word size parameter" );
           ( "--exact-rational",
             Bool rational_impl,
             "Use exact arithmetic for computation" );
         ]
