(*
type order = Task of task | Stop
*)

type result_type =
  | StdOut of string
  | Data of string
  | Finish of string
  | ParseOK
  | Graph of string
  | Error of string
