function loadVis(container,data){
    /*var dot = vis.parseDOTNetwork(data);
    var opt = {
	physics: {
	    stabilization: false,
	    barnesHut: {
		springLength: 250
	    }
	}
    };
    return new vis.Network(container, dot, opt);*/
    container.innerHTML=""; 
//    var svg = Viz(data, "svg");
//    container.innerHTML = (svg);
    container.innerHTML = (data);

}

function genSvgViz(data){
    /*var dot = vis.parseDOTNetwork(data);
    var opt = {
	physics: {
	    stabilization: false,
	    barnesHut: {
		springLength: 250
	    }
	}
    };
    return new vis.Network(container, dot, opt);*/
    container.innerHTML=""; 
    return Viz(data, "svg");

}

function upload(cb,inputId) {
    var files = document.getElementById(inputId).files;
    
    if (!files.length) {
      alert('Please select a file!');
      return;
    }

    var file = files[0];
   
    var reader = new FileReader();

    // If we use onloadend, we need to check the readyState.
    reader.onloadend = function(evt) {
        if (evt.target.readyState == FileReader.DONE) { // DONE == 2
            cb(evt.target.result);
      }
    };

    reader.readAsText(file);
  }

function compute_gnuplot(img, data, plotscript){
    gnuplot = new Gnuplot('gnuplot.js');
    gnuplot.putFile("myData.dat", data); // No callback needed here
    gnuplot.run("set terminal svg\nset output 'out.svg'\n"+plotscript, function(e) {
	gnuplot.getFile('out.svg', function(e) {
	    if (!e.content) {
		alert("Output file out.svg not found!");
		return;
            }
            //var img = document.getElementById(imgid);
            try {
		var ab = new Uint8Array(e.content);
		var blob = new Blob([ab], {"type": "image\/svg+xml"});
		window.URL = window.URL || window.webkitURL;
		img.src = window.URL.createObjectURL(blob);
            } catch (err) { // in case blob / URL missing, fallback to data-uri
                var rstr = '';
	        for (var i = 0; i < e.content.length; i++)
	            rstr += String.fromCharCode(e.content[i]);
	        img.src = 'data:image\/svg+xml;base64,' + btoa(rstr);
            }
	});
    });
}

function removecontext(e) { e.preventDefault();}