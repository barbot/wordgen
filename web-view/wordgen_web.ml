open Html
open Common
open Js_of_ocaml
module TAGraphG = GraphEditor.SimpleGraph.S (TAGraph)
module TAGraphEd = GraphEditor.GraphDrawing.Make (TAGraphG)

let build_interface_elem up (opt, x, help) =
  let open Arguments in
  let input =
    match x with
    | Bool f ->
        [
          checkbox_input
            ~on_change:(fun x ->
              f := x;
              up ())
            !f;
        ]
    | Set_int a ->
        [
          text_input ~_type:"number"
            ~on_change:(fun x ->
              a := int_of_string_opt x;
              up ())
            (!a |>>> string_of_int |>>| "");
        ]
    | Set_float a ->
        [
          text_input ~_type:"text"
            ~on_change:(fun x ->
              a := float_of_string_opt x;
              up ())
            (!a |>>> string_of_float |>>| "");
        ]
    | Set_string f ->
        [
          text_input ~_type:"text"
            ~on_change:(fun x ->
              f := if x = "" then None else Some x;
              up ())
            (!f |>>| "");
        ]
    | Int (f, df) ->
        [
          text_input ~_type:"number"
            ~on_change:(fun x ->
              (match int_of_string_opt x with
              | None -> f := df
              | Some v -> f := v);
              up ())
            (string_of_int !f);
        ]
    | Float (f, df) ->
        [
          text_input ~_type:"text"
            ~on_change:(fun x ->
              (match float_of_string_opt x with
              | None -> f := df
              | Some v -> f := v);
              up ())
            (string_of_float !f);
        ]
    | String (a, _) ->
        [
          text_input ~_type:"text"
            ~on_change:(fun x ->
              a := x;
              up ())
            !a;
        ]
    | Symbol s ->
        let module S = (val s : SYM) in
        [
          select S.list ~def:(S.to_string !S.content) ~on_change:(fun x ->
              S.content := S.of_string x;
              up ());
        ]
  in
  div ~class_:"cmdelem" ~title:help ([ text opt; text " : " ] @ input)

let build_cmd l = Arguments.build_cmd l ^ " automata.prism"
let only_parse = ref true

let build_interface ?class_:_ ?stop:_ up l =
  let cmd, cmdup = kbd' (build_cmd l) in
  let up2 () =
    cmdup (build_cmd l);
    up !only_parse
  in
  let elemlist = List.map (build_interface_elem up2) l in
  let bottom =
    [
      button ~on_click:(fun _ -> up false) [ text "Compute" ];
      (*button ~on_click:(fun _ -> stop ()) [text "Stop"];*)
      text " autocompute: ";
      checkbox_input
        ~on_change:(fun x ->
          only_parse := not x;
          up2 ())
        (not !only_parse);
    ]
  in

  (text "arguments : " :: elemlist) @ (br () :: cmd :: br () :: bottom)

let compact s =
  String.init (String.length s) (fun i ->
      match s.[i] with
      | ' ' -> '_'
      | '_' -> ' '
      | '\n' -> '.'
      | '.' -> '\n'
      | x -> x)

(*let gex_content = ref "GEXNAmrEksUlTMY29udGVudA==SEMQ==lTMaW52YXJpYW50SIdHJ1ZQ==lTMaW5pdGlhbA==BtlTIZmluYWw=BtAAAlTIbGFiZWw=SEYQ==lTIZ3VhcmQ=SQeDwyICYgeTwgNA==lTIcmVzZXQ=SEe3l9lTIYmVuZA==SILTQ1Lg==AAAlTIbGFiZWw=SEYg==lTIZ3VhcmQ=SMeDwzICYgeTw0lTIcmVzZXQ=SEe3h9lTIYmVuZA==SENDUuDlTMY2xvY2sgMA==SEeA==lTMY2xvY2sgMQ==SEeQ=="*)

(*let gex_content =
  ref
    "GEXNAmrEksUlTMY29udGVudA==SEMQ==lTMaW52YXJpYW50SIdHJ1ZQ==lTMaW5pdGlhbA==BtlTIZmluYWw=BtAAAlTIbGFiZWw=SEYQ==lTIZ3VhcmQ=ScKCh4IDwgMikgJiAoeSA8IDQpKQ==lTIcmVzZXQ=SEe3l9lTIYmVuZA==SILTQ1Lg==AAAlTIbGFiZWw=SEYg==lTIZ3VhcmQ=ScKCh4IDwgMykgJiAoeSA8IDIpKQ==lTIcmVzZXQ=SEe3h9lTIYmVuZA==SENDUuDlTMY2xvY2sgMA==SEeA==lTMY2xvY2sgMQ==SEeQ=="*)
let gex_content = ref ""

let _ =
  run' @@ fun () ->
  let open Js_of_ocaml.Url in
  let args = Current.arguments in
  (match List.assoc_opt "automata" args with
  | None -> ()
  | Some q -> gex_content := q);
  (match List.assoc_opt "cmd" args with
  | None -> ()
  | Some cmd ->
      cmd |> urldecode |> String.split_on_char ' ' |> Array.of_list |> fun a ->
      Arg.parse_argv a Arguments.spec_caml_arg (fun _ -> ()) Arguments.usage_str);

  let stdoutput_up = update_value_by_id "cmd out" in
  let outfile_up = update_value_by_id "data out" in

  let container, _, toggle_container = get_by_id' "graphvis" in
  let _, alert_up, _ = get_by_id' "alert_pb" in
  let _, sig_up, _ = get_by_id' "sig" in
  let gnuplotvis, _, toogle_gnuplot = get_by_id' "gnuplotvis img" in

  let worker = ref (Js_of_ocaml.Worker.create "worker.js") in
  let worker_active = ref false in

  let buffstd = Buffer.create 100 in
  let buffdata = Buffer.create 100 in
  let dirup () =
    let open Js_of_ocaml.Url in
    let link = Current.path_string in
    let qu =
      Printf.sprintf "automata=%s&cmd=%s" !gex_content
        (urlencode (build_cmd Arguments.(spec_minimal @ spec_short)))
    in
    (*let link_code_list = match Current.path with
        | ""::"index.html"::q -> "index-code.html"::q
        | "index.html"::q -> "index-code.html"::q
        | l -> "index-code.html" :: l in
      let link_code = List.fold_left (fun acc v -> if acc <>"" then if v<>"" then v^"/"^acc else acc else v) "" link_code_list in*)
    let link_code = "index-code.html" in
    let _, dirlink_up, _ = get_by_id' "directlink" in

    dirlink_up
      [
        a ~href:(Printf.sprintf "%s?%s" link qu) [ text "Direct link" ];
        text "  ";
        a ~href:(Printf.sprintf "%s?%s" link_code qu) [ text "Edit Prism file" ];
      ]
  in

  let handler =
    Dom.handler (fun m ->
        (match m##.data with
        | Worker_sync.StdOut s ->
            Buffer.add_string buffstd s;
            stdoutput_up (Buffer.contents buffstd)
        | Data d ->
            Buffer.add_string buffdata d;
            if Buffer.length buffdata < 10000 then
              outfile_up (Buffer.contents buffdata)
        | Graph g ->
            let g2 = Scanf.unescaped g in
            toggle_container "block";

            Js.Unsafe.fun_call
              (Js.Unsafe.js_expr "loadVis")
              [| Js.Unsafe.inject container; Js.Unsafe.inject (Js.string g2) |]
        | Error err ->
            worker_active := false;
            let eerr = Scanf.unescaped err in
            outfile_up "";
            sig_up [];
            alert_up [ text eerr ]
        | ParseOK ->
            sig_up [ text "Parsing OK" ];
            alert_up []
        | Finish gnucmd ->
            worker_active := false;
            sig_up [ text "Computation Finished" ];
            outfile_up (Buffer.contents buffdata);
            (match gnucmd with
            | "" -> ()
            | plot ->
                toogle_gnuplot "block";
                Js.Unsafe.fun_call
                  (Js.Unsafe.js_expr "compute_gnuplot")
                  [|
                    Js.Unsafe.inject gnuplotvis;
                    Js.Unsafe.inject (Js.string (Buffer.contents buffdata));
                    Js.Unsafe.inject (Js.string (Scanf.unescaped plot));
                  |]);
            print_endline "Computation finished");
        Js._true)
  in
  !worker##.onmessage := handler;

  let file_content = ref "" in
  let launch_compt b =
    if !worker_active then (
      !worker##terminate;
      worker := Js_of_ocaml.Worker.create "worker.js";
      !worker##.onmessage := handler)
    else worker_active := true;
    let open Arguments in
    dirup ();
    (*post_parse ();*)
    stdoutput_up "";
    outfile_up "";
    sig_up [];
    alert_up [];
    toggle_container "none";
    toogle_gnuplot "none";
    Buffer.clear buffstd;
    Buffer.clear buffdata;
    let cmd =
      Arguments.spec_minimal @ spec_short
      @ [ ("--only-parse", Arguments.Bool (ref b), "") ]
      |> build_cmd |> String.escaped
    in
    !worker##postMessage (cmd, !file_content)
  in

  (match Dom_html.(getElementById_coerce "ta_drawing" CoerceTo.canvas) with
  | Some canvas ->
      let ed =
        TAGraphEd.init
          ~callback:(fun g ->
            let _, s, _ =
              List.find (fun (x, _, _) -> x = "prism") TAGraphG.print_graph
            in
            Format.fprintf Format.str_formatter "%a" s g;
            let content = Format.flush_str_formatter () in
            gex_content := TAGraphEd.get_exchange_string g;
            update_value_by_id "graph editor out" content;
            file_content := content;
            launch_compt !only_parse)
          ?new_button:
            Dom_html.(getElementById_coerce "new_button" CoerceTo.input)
          canvas
          Dom_html.(getElementById "attr_list")
      in
      ed.graph <-
        (try TAGraphEd.parse_exchange_string !gex_content
         with _ -> TAGraphG.new_graph ());
      ed.callback ed.graph;

      (*let txin = text_area ~class_:"input_file col-sm-6" ~on_change:(fun x->
                       file_content:=x                            ;
                       launch_compt false)  !file_content in
        launch_compt true;*)
      TAGraphEd.init_client ed
  | None ->
      let g = TAGraphEd.parse_exchange_string !gex_content in
      let _, s, _ =
        List.find (fun (x, _, _) -> x = "prism") TAGraphG.print_graph
      in
      Format.fprintf Format.str_formatter "%a" s g;
      let content = Format.flush_str_formatter () in
      file_content := content;
      update_value_by_id "graph editor out" content;
      ignore
      @@ get_by_id
           ~on_change:(fun content ->
             file_content := content;
             launch_compt !only_parse)
           "graph editor out");
  set_by_id "argument"
  @@ build_interface ~class_:""
       (*~stop:(fun () -> worker##postMessage (Worker_sync.Stop))*) launch_compt
       Arguments.(spec_minimal @ spec_short)
