all:
	dune build -j 8 @install
	ln -sf _build/default/src/wordgen.exe wordgen

allocamlbuild:
	ocamlbuild -I src -use-ocamlfind -plugin-tag 'package(bisect_ppx-ocamlbuild)' wordgen.native
	cp wordgen.native wordgen

test: 
	BISECT_COVERAGE=YES ocamlbuild -I src -use-ocamlfind -plugin-tag 'package(bisect_ppx-ocamlbuild)' -package yojson wordgen.native test.t/poly_unitaire.native
	cd test.t; ./integration.sh
	cd test.t; ../poly_unitaire.native
	cd test.t; bisect-ppx-report -html _coverage -I ../_build bisect*.*
	cd test.t; bisect-ppx-report -text - -summary-only bisect*.*

testBisect2:
	BISECT_COVERAGE=YES ocamlbuild -I src -use-ocamlfind -plugin-tag 'package(bisect_ppx-ocamlbuild)' -package yojson wordgen.native test/poly_unitaire.native
	cd test.t; ./integration.sh
	cd test.t; ../poly_unitaire.native
	cd test.t; bisect-ppx-report html --source-path ../_build bisect*.*
	cd test.t; bisect-ppx-report summary bisect*.*

doc:
	ocamlbuild -I src -use-ocamlfind -plugin-tag 'package(bisect_ppx-ocamlbuild)' -docflags -charset,utf8,-dot  src/doc.docdir/index.html

clean:
	ocamlbuild -clean
	rm test/*.coverage
	rm -rf src/doc.docdir
	rm wordgen

.PHONY : clean test all doc
