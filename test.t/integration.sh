set -e

WG="dune exec wordgen --"
$WG ../example/nfm19.json --sampler kronecker 
$WG ../example/nfm19.prism --output-format state_list_full --sampler halton -v 3 
$WG ../example/nfm19.prism --traj 100 --receding 10 --export-splitreach test.json --debug
$WG ../example/nfm19.prism --frequency 1.0 data.out --gnuplot-driver png --output-format state_list_full
$WG ../example/nfm19.prism --frequency 1.0 --exact-rational
$WG ../example/nfm19.prism --expected-duration 1.0
$WG ../example/nfm19.prism --expected-duration 1.0 --exact-rational
$WG ../example/test2mod.prism --frequency 1
$WG ../example/test2mod.prism --expected-duration 9 --exact-rational
$WG ../example/bimodal.prism --expected-duration 4.4
$WG ../example/nfm19.prism --template "@1[b]0.2[0.3]_[_]"
$WG ../example/nfm19.prism --template "@1[b]0.2[0.3]_[_]" --poly -1
$WG ../example/nfm19.prism --debug --output-format debug --splitting-debug test.tex
$WG ../example/nfm19.prism --expected-duration 1.0 --debug --splitting-debug test.tex
$WG ../example/twoears.xml --expected-duration 1 
