Simple interaction

  $ wordgen
  wordgen automaton.prism [outputfile]
    --debug Print the intermediate representation
    --poly Number of polynome Iteration (default:3)
    --traj Number of trajectories (default:10)
    --receding trajectory length
    --sampler {random|halton|kronecker} choose sampler (default:random)
    --seed seed of the random sampler
    --output-format {timeword|timestamp|word|timeword_state|state_list|state_list_delay|state_list_full|time_and_label|debug|void|cosim|json|timeline} choose the format of the output (default:timeword)
    -v Verbose level (default:1)
    --exact-rational Use exact arithmetic for computation
    --regexp Take a regular expression as input
    --expected-duration time word duration parameter
    --exact-duration sample duration exactly
    --expected-size time word size parameter
    --template specify a template for time words; template look like '0.2[a]_[b]'
    --max-iteration Number of iteration for Newton method (default:20)
    --apericube Compute apericube
    --boltzmann boltzmann parameter z
    --frequency frequency parameter s
    --interactive Interactive mode
    -i Interactive mode
    --no-cache disable caching
    --gnuplot-driver launch gnuplot
    --export-splitreach Export the splitted zone graph
    --splitting-debug Export debug information on the splitting in tex format (default:)
    --generate-perturbation generate perturbated timed word
    --gnuplot-cmd Command to pass to gnuplot for display
    -help  Display this list of options
    --help  Display this list of options
  [1]

  $ wordgen toto tata titi
  wordgen: Too many arguments.
  wordgen automaton.prism [outputfile]
    --debug Print the intermediate representation
    --poly Number of polynome Iteration (default:3)
    --traj Number of trajectories (default:10)
    --receding trajectory length
    --sampler {random|halton|kronecker} choose sampler (default:random)
    --seed seed of the random sampler
    --output-format {timeword|timestamp|word|timeword_state|state_list|state_list_delay|state_list_full|time_and_label|debug|void|cosim|json|timeline} choose the format of the output (default:timeword)
    -v Verbose level (default:1)
    --exact-rational Use exact arithmetic for computation
    --regexp Take a regular expression as input
    --expected-duration time word duration parameter
    --exact-duration sample duration exactly
    --expected-size time word size parameter
    --template specify a template for time words; template look like '0.2[a]_[b]'
    --max-iteration Number of iteration for Newton method (default:20)
    --apericube Compute apericube
    --boltzmann boltzmann parameter z
    --frequency frequency parameter s
    --interactive Interactive mode
    -i Interactive mode
    --no-cache disable caching
    --gnuplot-driver launch gnuplot
    --export-splitreach Export the splitted zone graph
    --splitting-debug Export debug information on the splitting in tex format (default:)
    --generate-perturbation generate perturbated timed word
    --gnuplot-cmd Command to pass to gnuplot for display
    -help  Display this list of options
    --help  Display this list of options
  [2]

Timed test without s nor z

  $ wordgen twoears.prism --seed 42 twoears.data --gnuplot-driver png --output-format state_list_full | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Reading Prism automaton file. 
  Computing forward reachability graph ... 3 states found, automaton is deterministic. 
  Splitting reachability graph ... 4 states and 15 DBMs found. 
  Computing Distribution[floating point; Poly; no s; clocks:{y; x; }; vars:{t; z; s; T; }] -> 3: [|||] 
  Volume in initial state:31.6666666667, degree of liberty:3
  Saving Precomputation 
  Sampling: [] 

  $ wordgen nfm19.prism --seed 42 --traj 5 --output-format state_list_full --sampler halton -v 3 | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  parameters : wordgen --traj 5 --sampler halton --seed 42 --output-format state_list_full -v 3
  Reading Prism automaton file. 
  Computing forward reachability graph ... 7 states found, automaton is deterministic. 
  Splitting reachability graph ... 75 states and 430 DBMs found. 
  Computing Distribution[floating point; Poly; no s; clocks:{x4; x3; x2; x1; x; }; vars:{t; z; s; T; }] -> 3: [|||] 
  Volume in initial state:8, degree of liberty:3
  Saving Precomputation 
  0  0  0  0  0  0  	1
  0  0  0  0  0  0  	98
  48  0  0  0  0  0  	1
  48  0  0  0  0  0  	99
  71  0  0  0  0  0  	1
  71  1  1  1  1  1  	100
  
  0  0  0  0  0  0  	1
  0  0.5  0.5  0.5  0.5  0.5  	98
  48  0.5  0.5  0.5  0  0  	1
  48  0.666667  0.666667  0.666667  0.166667  0.166667  	99
  71  0.666667  0.666667  0  0.166667  0  	1
  71  1.73333  1.73333  1.06667  1.23333  1.06667  	100
  
  0  0  0  0  0  0  	1
  0  1.25  1.25  1.25  1.25  1.25  	98
  39  1.25  1.25  1.25  0  0  	1
  39  2.75  2.75  2.75  1.5  1.5  	99
  49  2.75  2.75  0  1.5  0  	1
  49  3.85  3.85  1.1  2.6  1.1  	100
  
  0  0  0  0  0  0  	1
  0  1.75  1.75  1.75  1.75  1.75  	98
  39  1.75  1.75  1.75  0  0  	1
  39  2.08333  2.08333  2.08333  0.333333  0.333333  	99
  3  2.08333  2.08333  0  0.333333  0  	1
  3  2.95  2.95  0.866667  1.2  0.866667  	100
  
  0  0  0  0  0  0  	1
  0  1.125  1.125  1.125  1.125  1.125  	98
  39  1.125  1.125  1.125  0  0  	1
  39  2.51389  2.51389  2.51389  1.38889  1.38889  	99
  49  2.51389  2.51389  0  1.38889  0  	1
  49  3.43611  3.43611  0.922222  2.31111  0.922222  	100
  
  $ wordgen twoears.prism --seed 42 --traj 2 --receding 10 --export-splitreach test.json --output-format timeword --splitting-debug test.tex --debug -v 5 | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  parameters : wordgen --debug --traj 2 --receding 10 --seed 42 -v 5 --export-splitreach test.json --splitting-debug test.tex
  Reading Prism automaton file.module m
  	x: clock;
  	y: clock;
  	[a] (  x < 2  ) -> 1 :(y'=0);
  	[b] (  x < 3 & y < 2  ) -> 1 :(x'=0);
  endmodule
   
  Timed automaton with 3 clocks and 1 processes
  Process(0): m
  Location 0: m.UID m.y <= 4 && m.x <= 4
  Has 2 edges:
  m.UID -> m.UID 	DiscGuard:  	DiscUpdate:  	Guard: m.y < 2 && m.x < 3 	Resets:m.x  	Sync:b!	Prob:1
  m.UID -> m.UID 	DiscGuard:  	DiscUpdate:  	Guard: m.x < 2 	Resets:m.y  	Sync:a!	Prob:1
  Initial location id: 0
  
  -----
  Computing forward reachability graph ... 3 states found, automaton is deterministic. 
  Forward Reach : 
  LocZone 0: m.UID  y=0; x=0 -> { ([x in [0,2)],0-[b]->2), ([x in [0,2)],0-[a]->1),  }
  LocZone 2: m.UID  y in [0,2); x=0 -> { ([y < 2;
  x >= 0],2-[b]->2), ([x in [0,2)],2-[a]->1),  }
  LocZone 1: m.UID  y=0; x in [0,2) -> { ([y in [0,2); x < 3],1-[b]->2), ([y >= 0;
  x < 2],1-[a]->1),  }
  
  Splitting reachability graph ... .UID  y=0; x=0 -> { ([x in [0,1)],0-[a]->3), ([x in [0,2)],0-[b]->2), ([x in (1,2)],0-[a]->1),  }
  LocZone 2: m.UID  y in [0,2); x=0 -> { ([x in [0,1)],2-[a]->3), ([y < 2;
  x >= 0],2-[b]->2), ([x in (1,2)],2-[a]->1),  }
  LocZone 3: m.UID  y=0; x in [0,1) -> { ([y >= 0; x < 1],3-[a]->3), ([x in (1,2)],3-[a]->1), ([y in [0,2)],3-[b]->2),  }
  LocZone 1: m.UID  y=0;
  x in (1,2) -> { ([y >= 0; x < 3],1-[b]->2), ([y >= 0;
  x < 2],1-[a]->1),  }
  
  {
     "init":0,
     "cardclocks":2,
     "statelist":[
     {
        "id":0,
        "disc_name":"m.UID",
        "init_zone":"x=0",
        "is_accepting":true,
        "redcoord":[[0, 1, 2]],
        "clockmap":[0 ,0 ,0 ],
        "weight":,
        "transitions":[
                         {"action":"a", "zone":[0 , 1],  "is_contained_in_zone": false, "target":2, "reset": [ y]  },
                         {"action":"b", "zone":[0 , 2],  "is_contained_in_zone": false, "target":1, "reset": [ x]  },
                         {"action":"a", "zone":[1 , 2],  "is_contained_in_zone": false, "target":3, "reset": [ y]  }]
       }
       ,{
           "id":1,
           "disc_name":"m.UID",
           "init_zone":"x=0",
           "is_accepting":true,
           "redcoord":[[0, 2], [1]],
           "clockmap":[0 ,y ,0 ],
           "weight":,
           "transitions":[
                            {"action":"a", "zone":[0 , 1],  "is_contained_in_zone": false, "target":2, "reset": [ y]  },
                            {"action":"b", "zone":[0 , 2-y],  "is_contained_in_zone": false, "target":1, "reset": [ x]  },
                            {"action":"a", "zone":[1 , 2],  "is_contained_in_zone": false, "target":3, "reset": [ y]  }]
       }
       ,{
           "id":2,
           "disc_name":"m.UID",
           "init_zone":"y=0",
           "is_accepting":true,
           "redcoord":[[0, 1], [2]],
           "clockmap":[0 ,0 ,x ],
           "weight":,
           "transitions":[
                            {"action":"a", "zone":[0 , 1-x],  "is_contained_in_zone": false, "target":2, "reset": [ y]  },
                            {"action":"a", "zone":[1-x , 2-x],  "is_contained_in_zone": false, "target":3, "reset": [ y]  },
                            {"action":"b", "zone":[0 , 2],  "is_contained_in_zone": false, "target":1, "reset": [ x]  }]
       }
       ,{
           "id":3,
           "disc_name":"m.UID",
           "init_zone":"y=0",
           "is_accepting":true,
           "redcoord":[[0, 1], [2]],
           "clockmap":[0 ,0 ,x ],
           "weight":,
           "transitions":[
                            {"action":"b", "zone":[0 , 3-x],  "is_contained_in_zone": false, "target":1, "reset": [ x]  },
                            {"action":"a", "zone":[0 , 2-x],  "is_contained_in_zone": false, "target":3, "reset": [ y]  }]
       }
       
     ]}
  Computing Distribution[floating point; Poly; no s; clocks:{y; x; }; vars:{t; z; s; T; }] -> 3: [|||] 
  {
     "init":0,
     "cardclocks":2,
     "statelist":[
     {
        "id":0,
        "disc_name":"m.UID",
        "init_zone":"x=0",
        "is_accepting":true,
        "redcoord":[[0, 1, 2]],
        "clockmap":[0 ,0 ,0 ],
        "weight":31.6666666667,
        "transitions":[
                         {"action":"a", "zone":[0 , 1],  "is_contained_in_zone": false, "target":2, "reset": 
                            [ y] , "weight":9.66666666667, "degree_of_liberty":3, "cdf":11.5t-2t²+0.166666666667t³, "pdf":11.5-4t+½t²,  },
                         {"action":"b", "zone":[0 , 2],  "is_contained_in_zone": false, "target":1, "reset": 
                            [ x] , "weight":16.3333333333, "degree_of_liberty":3, "cdf":11.5t-2t²+0.166666666667t³, "pdf":11.5-4t+½t²,  },
                         {"action":"a", "zone":[1 , 2],  "is_contained_in_zone": false, "target":3, "reset": 
                            [ y] , "weight":5.66666666667, "degree_of_liberty":3, "cdf":-10.6666666667+13.5t-3t²+0.166666666667t³, "pdf":13.5-6t+½t²,  }]
       }
       ,{
           "id":1,
           "disc_name":"m.UID",
           "init_zone":"x=0",
           "is_accepting":true,
           "redcoord":[[0, 2], [1]],
           "clockmap":[0 ,y ,0 ],
           "weight":31.6666666667-11.5*y+2.0*y**2-0.166666666667*y**3,
           "transitions":[
                            {"action":"a", "zone":[0 , 1],  "is_contained_in_zone": false, "target":2, "reset": 
                               [ y] , "weight":9.66666666667, "degree_of_liberty":3, "cdf":11.5t-2t²+0.166666666667t³, "pdf":11.5-4t+½t²,  },
                            {"action":"b", "zone":[0 , 2-y],  "is_contained_in_zone": false, "target":1, "reset": 
                               [ x] , "weight":16.3333333333-11.5y+2y²-0.166666666667y³, "degree_of_liberty":3, "cdf":11.5t-4ty+½ty²-2t²+½t²y+0.166666666667t³,
                               "pdf":11.5-4y+½y²-4t+ty+½t²,  },
                            {"action":"a", "zone":[1 , 2],  "is_contained_in_zone": false, "target":3, "reset": 
                               [ y] , "weight":5.66666666667, "degree_of_liberty":3, "cdf":-10.6666666667+13.5t-3t²+0.166666666667t³, "pdf":13.5-6t+½t²,  }]
       }
       ,{
           "id":2,
           "disc_name":"m.UID",
           "init_zone":"y=0",
           "is_accepting":true,
           "redcoord":[[0, 1], [2]],
           "clockmap":[0 ,0 ,x ],
           "weight":31.6666666667-11.5*x+2.0*x**2-0.166666666667*x**3,
           "transitions":[
                            {"action":"a", "zone":[0 , 1-x],  "is_contained_in_zone": false, "target":2, "reset": 
                               [ y] , "weight":9.66666666667-11.5x+2x²-0.166666666667x³, "degree_of_liberty":3, "cdf":11.5t-4tx+½tx²-2t²+½t²x+0.166666666667t³,
                               "pdf":11.5-4x+½x²-4t+tx+½t²,  },
                            {"action":"a", "zone":[1-x , 2-x],  "is_contained_in_zone": false, "target":3, "reset": 
                               [ y] , "weight":5.66666666667-2.77555756156e-17x³, "degree_of_liberty":3,
                               "cdf":-10.6666666667+13.5x-3x²+0.166666666667x³+13.5t-6tx+½tx²-3t²+½t²x+0.166666666667t³, "pdf":13.5-6x+½x²-6t+tx+½t²,  },
                            {"action":"b", "zone":[0 , 2],  "is_contained_in_zone": false, "target":1, "reset": 
                               [ x] , "weight":16.3333333333, "degree_of_liberty":3, "cdf":11.5t-2t²+0.166666666667t³, "pdf":11.5-4t+½t²,  }]
       }
       ,{
           "id":3,
           "disc_name":"m.UID",
           "init_zone":"y=0",
           "is_accepting":true,
           "redcoord":[[0, 1], [2]],
           "clockmap":[0 ,0 ,x ],
           "weight":37.3333333333-17.5*x+2.5*x**2-0.333333333333*x**3,
           "transitions":[
                            {"action":"b", "zone":[0 , 3-x],  "is_contained_in_zone": false, "target":1, "reset": 
                               [ x] , "weight":21-4x-½x²-0.166666666667x³, "degree_of_liberty":3, "cdf":11.5t-2t²+0.166666666667t³, "pdf":11.5-4t+½t²,  },
                            {"action":"a", "zone":[0 , 2-x],  "is_contained_in_zone": false, "target":3, "reset": 
                               [ y] , "weight":16.3333333333-13.5x+3x²-0.166666666667x³, "degree_of_liberty":3, "cdf":13.5t-6tx+½tx²-3t²+½t²x+0.166666666667t³,
                               "pdf":13.5-6x+½x²-6t+tx+½t²,  }]
       }
       
     ]}
  Volume in initial state:31.6666666667, degree of liberty:3
  Saving Precomputation 
  {
     "init":0,
     "cardclocks":2,
     "statelist":[
     {
        "id":0,
        "disc_name":"m.UID",
        "init_zone":"x=0",
        "is_accepting":true,
        "redcoord":[[0, 1, 2]],
        "clockmap":[0 ,0 ,0 ],
        "weight":31.6666666667,
        "transitions":[
                         {"action":"a", "zone":[0 , 1],  "is_contained_in_zone": false, "target":2, "reset": 
                            [ y] , "weight":9.66666666667, "degree_of_liberty":3, "cdf":11.5t-2t²+0.166666666667t³, "pdf":11.5-4t+½t²,  },
                         {"action":"b", "zone":[0 , 2],  "is_contained_in_zone": false, "target":1, "reset": 
                            [ x] , "weight":16.3333333333, "degree_of_liberty":3, "cdf":11.5t-2t²+0.166666666667t³, "pdf":11.5-4t+½t²,  },
                         {"action":"a", "zone":[1 , 2],  "is_contained_in_zone": false, "target":3, "reset": 
                            [ y] , "weight":5.66666666667, "degree_of_liberty":3, "cdf":-10.6666666667+13.5t-3t²+0.166666666667t³, "pdf":13.5-6t+½t²,  }]
       }
       ,{
           "id":1,
           "disc_name":"m.UID",
           "init_zone":"x=0",
           "is_accepting":true,
           "redcoord":[[0, 2], [1]],
           "clockmap":[0 ,y ,0 ],
           "weight":31.6666666667-11.5*y+2.0*y**2-0.166666666667*y**3,
           "transitions":[
                            {"action":"a", "zone":[0 , 1],  "is_contained_in_zone": false, "target":2, "reset": 
                               [ y] , "weight":9.66666666667, "degree_of_liberty":3, "cdf":11.5t-2t²+0.166666666667t³, "pdf":11.5-4t+½t²,  },
                            {"action":"b", "zone":[0 , 2-y],  "is_contained_in_zone": false, "target":1, "reset": 
                               [ x] , "weight":16.3333333333-11.5y+2y²-0.166666666667y³, "degree_of_liberty":3, "cdf":11.5t-4ty+½ty²-2t²+½t²y+0.166666666667t³,
                               "pdf":11.5-4y+½y²-4t+ty+½t²,  },
                            {"action":"a", "zone":[1 , 2],  "is_contained_in_zone": false, "target":3, "reset": 
                               [ y] , "weight":5.66666666667, "degree_of_liberty":3, "cdf":-10.6666666667+13.5t-3t²+0.166666666667t³, "pdf":13.5-6t+½t²,  }]
       }
       ,{
           "id":2,
           "disc_name":"m.UID",
           "init_zone":"y=0",
           "is_accepting":true,
           "redcoord":[[0, 1], [2]],
           "clockmap":[0 ,0 ,x ],
           "weight":31.6666666667-11.5*x+2.0*x**2-0.166666666667*x**3,
           "transitions":[
                            {"action":"a", "zone":[0 , 1-x],  "is_contained_in_zone": false, "target":2, "reset": 
                               [ y] , "weight":9.66666666667-11.5x+2x²-0.166666666667x³, "degree_of_liberty":3, "cdf":11.5t-4tx+½tx²-2t²+½t²x+0.166666666667t³,
                               "pdf":11.5-4x+½x²-4t+tx+½t²,  },
                            {"action":"a", "zone":[1-x , 2-x],  "is_contained_in_zone": false, "target":3, "reset": 
                               [ y] , "weight":5.66666666667-2.77555756156e-17x³, "degree_of_liberty":3,
                               "cdf":-10.6666666667+13.5x-3x²+0.166666666667x³+13.5t-6tx+½tx²-3t²+½t²x+0.166666666667t³, "pdf":13.5-6x+½x²-6t+tx+½t²,  },
                            {"action":"b", "zone":[0 , 2],  "is_contained_in_zone": false, "target":1, "reset": 
                               [ x] , "weight":16.3333333333, "degree_of_liberty":3, "cdf":11.5t-2t²+0.166666666667t³, "pdf":11.5-4t+½t²,  }]
       }
       ,{
           "id":3,
           "disc_name":"m.UID",
           "init_zone":"y=0",
           "is_accepting":true,
           "redcoord":[[0, 1], [2]],
           "clockmap":[0 ,0 ,x ],
           "weight":37.3333333333-17.5*x+2.5*x**2-0.333333333333*x**3,
           "transitions":[
                            {"action":"b", "zone":[0 , 3-x],  "is_contained_in_zone": false, "target":1, "reset": 
                               [ x] , "weight":21-4x-½x²-0.166666666667x³, "degree_of_liberty":3, "cdf":11.5t-2t²+0.166666666667t³, "pdf":11.5-4t+½t²,  },
                            {"action":"a", "zone":[0 , 2-x],  "is_contained_in_zone": false, "target":3, "reset": 
                               [ y] , "weight":16.3333333333-13.5x+3x²-0.166666666667x³, "degree_of_liberty":3, "cdf":13.5t-6tx+½tx²-3t²+½t²x+0.166666666667t³,
                               "pdf":13.5-6x+½x²-6t+tx+½t²,  }]
       }
       
     ]}
  1.403895[b] 0.292973[a] 1.310431[a] 0.727593[b] 1.179736[a] 0.140982[a] 0.092747[a] 0.298145[a] 0.051611[b] 0.322311[a] 
  0.860710[b] 1.025874[b] 1.224175[a] 0.200337[b] 0.102102[a] 0.894940[a] 0.776098[b] 0.182698[a] 0.372311[b] 1.046068[a] 
	x: clock;
	y: clock;
	[a] (  x < 2  ) -> 1 :(y'=0);
	[b] (  x < 3 & y < 2  ) -> 1 :(x'=0);
	"id":0,
	"name":"m.UID  x=0",
	"is_accepting":true,
	"redcoord":[[0, 2], [1, 2]],
	"clockmap":[],
	"weight":,
	"transitions":[
			 {"action":"a", "zone":[0 ; 1-x],  "is_contained_in_zone": false, "target":2, "reset": [ y]  },
			 {"action":"b", "zone":[0 ; 2-x],  "is_contained_in_zone": false, "target":1, "reset": [ x]  },
			 {"action":"a", "zone":[1-x ; 2-x],  "is_contained_in_zone": false, "target":3, "reset": [ y]  }]
	   "id":1,
	   "name":"m.UID  x=0",
	   "is_accepting":true,
	   "redcoord":[[0, 2], [1]],
	   "clockmap":[],
	   "weight":,
	   "transitions":[
			    {"action":"a", "zone":[0 ; 1-x],  "is_contained_in_zone": false, "target":2, "reset": [ y]  },
			    {"action":"b", "zone":[0 ; 2-y],  "is_contained_in_zone": false, "target":1, "reset": [ x]  },
			    {"action":"a", "zone":[1-x ; 2-x],  "is_contained_in_zone": false, "target":3, "reset": [ y]  }]
	   "id":2,
	   "name":"m.UID  y=0",
	   "is_accepting":true,
	   "redcoord":[[0, 1], [2]],
	   "clockmap":[],
	   "weight":,
	   "transitions":[
			    {"action":"a", "zone":[0 ; 1-x],  "is_contained_in_zone": false, "target":2, "reset": [ y]  },
			    {"action":"a", "zone":[1-x ; 2-x],  "is_contained_in_zone": false, "target":3, "reset": [ y]  },
			    {"action":"b", "zone":[0 ; 2-y],  "is_contained_in_zone": false, "target":1, "reset": [ x]  }]
	   "id":3,
	   "name":"m.UID  y=0",
	   "is_accepting":true,
	   "redcoord":[[0, 1], [2]],
	   "clockmap":[],
	   "weight":,
	   "transitions":[
			    {"action":"b", "zone":[0 ; 3-x],  "is_contained_in_zone": false, "target":1, "reset": [ x]  },
			    {"action":"a", "zone":[0 ; 2-x],  "is_contained_in_zone": false, "target":3, "reset": [ y]  }]
	"id":0,
	"name":"m.UID  x=0",
	"is_accepting":true,
	"redcoord":[[0, 2], [1, 2]],
	"clockmap":[0; 0; 0; ],
	"weight":31.6666666667,
	"transitions":[
			 {"action":"a", "zone":[0 ; 1],  "is_contained_in_zone": false, "target":2, "reset":
			    [ y] , "weight":9.66666666667, "cdf":11.5t-2t²+0.166666666667t³, "pdf":11.5-4t+½t²,  },
			 {"action":"b", "zone":[0 ; 2],  "is_contained_in_zone": false, "target":1, "reset":
			    [ x] , "weight":16.3333333333, "cdf":11.5t-2t²+0.166666666667t³, "pdf":11.5-4t+½t²,  },
			 {"action":"a", "zone":[1 ; 2],  "is_contained_in_zone": false, "target":3, "reset":
			    [ y] , "weight":5.66666666667, "cdf":-10.6666666667+13.5t-3t²+0.166666666667t³, "pdf":13.5-6t+½t²,  }]
	   "id":1,
	   "name":"m.UID  x=0",
	   "is_accepting":true,
	   "redcoord":[[0, 2], [1]],
	   "clockmap":[0; y; 0; ],
	   "weight":31.6666666667-11.5y+2y²-0.166666666667y³,
	   "transitions":[
			    {"action":"a", "zone":[0 ; 1],  "is_contained_in_zone": false, "target":2, "reset":
			       [ y] , "weight":9.66666666667, "cdf":11.5t-2t²+0.166666666667t³, "pdf":11.5-4t+½t²,  },
			    {"action":"b", "zone":[0 ; 2-y],  "is_contained_in_zone": false, "target":1, "reset":
			       [ x] , "weight":16.3333333333-11.5y+2y²-0.166666666667y³, "cdf":11.5t-4ty+½ty²-2t²+½t²y+0.166666666667t³, "pdf":11.5-4y+½y²-4t+ty+½t²,  },
			    {"action":"a", "zone":[1 ; 2],  "is_contained_in_zone": false, "target":3, "reset":
			       [ y] , "weight":5.66666666667, "cdf":-10.6666666667+13.5t-3t²+0.166666666667t³, "pdf":13.5-6t+½t²,  }]
	   "id":2,
	   "name":"m.UID  y=0",
	   "is_accepting":true,
	   "redcoord":[[0, 1], [2]],
	   "clockmap":[0; 0; x; ],
	   "weight":31.6666666667-11.5x+2x²-0.166666666667x³,
	   "transitions":[
			    {"action":"a", "zone":[0 ; 1-x],  "is_contained_in_zone": false, "target":2, "reset":
			       [ y] , "weight":9.66666666667-11.5x+2x²-0.166666666667x³, "cdf":11.5t-4tx+½tx²-2t²+½t²x+0.166666666667t³, "pdf":11.5-4x+½x²-4t+tx+½t²,  },
			    {"action":"a", "zone":[1-x ; 2-x],  "is_contained_in_zone": false, "target":3, "reset":
			       [ y] , "weight":5.66666666667-2.77555756156e-17x³, "cdf":-10.6666666667+13.5x-3x²+0.166666666667x³+13.5t-6tx+½tx²-3t²+½t²x+0.166666666667t³,
			       "pdf":13.5-6x+½x²-6t+tx+½t²,  },
			    {"action":"b", "zone":[0 ; 2],  "is_contained_in_zone": false, "target":1, "reset":
			       [ x] , "weight":16.3333333333, "cdf":11.5t-2t²+0.166666666667t³, "pdf":11.5-4t+½t²,  }]
	   "id":3,
	   "name":"m.UID  y=0",
	   "is_accepting":true,
	   "redcoord":[[0, 1], [2]],
	   "clockmap":[0; 0; x; ],
	   "weight":37.3333333333-17.5x+2.5x²-0.333333333333x³,
	   "transitions":[
			    {"action":"b", "zone":[0 ; 3-x],  "is_contained_in_zone": false, "target":1, "reset":
			       [ x] , "weight":21-4x-½x²-0.166666666667x³, "cdf":11.5t-2t²+0.166666666667t³, "pdf":11.5-4t+½t²,  },
			    {"action":"a", "zone":[0 ; 2-x],  "is_contained_in_zone": false, "target":3, "reset":
			       [ y] , "weight":16.3333333333-13.5x+3x²-0.166666666667x³, "cdf":13.5t-6tx+½tx²-3t²+½t²x+0.166666666667t³, "pdf":13.5-6x+½x²-6t+tx+½t²,  }]
	"id":0,
	"name":"m.UID  x=0",
	"is_accepting":true,
	"redcoord":[[0, 2], [1, 2]],
	"clockmap":[0; 0; 0; ],
	"weight":31.6666666667,
	"transitions":[
			 {"action":"a", "zone":[0 ; 1],  "is_contained_in_zone": false, "target":2, "reset":
			    [ y] , "weight":9.66666666667, "cdf":11.5t-2t²+0.166666666667t³, "pdf":11.5-4t+½t²,  },
			 {"action":"b", "zone":[0 ; 2],  "is_contained_in_zone": false, "target":1, "reset":
			    [ x] , "weight":16.3333333333, "cdf":11.5t-2t²+0.166666666667t³, "pdf":11.5-4t+½t²,  },
			 {"action":"a", "zone":[1 ; 2],  "is_contained_in_zone": false, "target":3, "reset":
			    [ y] , "weight":5.66666666667, "cdf":-10.6666666667+13.5t-3t²+0.166666666667t³, "pdf":13.5-6t+½t²,  }]
	   "id":1,
	   "name":"m.UID  x=0",
	   "is_accepting":true,
	   "redcoord":[[0, 2], [1]],
	   "clockmap":[0; y; 0; ],
	   "weight":31.6666666667-11.5y+2y²-0.166666666667y³,
	   "transitions":[
			    {"action":"a", "zone":[0 ; 1],  "is_contained_in_zone": false, "target":2, "reset":
			       [ y] , "weight":9.66666666667, "cdf":11.5t-2t²+0.166666666667t³, "pdf":11.5-4t+½t²,  },
			    {"action":"b", "zone":[0 ; 2-y],  "is_contained_in_zone": false, "target":1, "reset":
			       [ x] , "weight":16.3333333333-11.5y+2y²-0.166666666667y³, "cdf":11.5t-4ty+½ty²-2t²+½t²y+0.166666666667t³, "pdf":11.5-4y+½y²-4t+ty+½t²,  },
			    {"action":"a", "zone":[1 ; 2],  "is_contained_in_zone": false, "target":3, "reset":
			       [ y] , "weight":5.66666666667, "cdf":-10.6666666667+13.5t-3t²+0.166666666667t³, "pdf":13.5-6t+½t²,  }]
	   "id":2,
	   "name":"m.UID  y=0",
	   "is_accepting":true,
	   "redcoord":[[0, 1], [2]],
	   "clockmap":[0; 0; x; ],
	   "weight":31.6666666667-11.5x+2x²-0.166666666667x³,
	   "transitions":[
			    {"action":"a", "zone":[0 ; 1-x],  "is_contained_in_zone": false, "target":2, "reset":
			       [ y] , "weight":9.66666666667-11.5x+2x²-0.166666666667x³, "cdf":11.5t-4tx+½tx²-2t²+½t²x+0.166666666667t³, "pdf":11.5-4x+½x²-4t+tx+½t²,  },
			    {"action":"a", "zone":[1-x ; 2-x],  "is_contained_in_zone": false, "target":3, "reset":
			       [ y] , "weight":5.66666666667-2.77555756156e-17x³, "cdf":-10.6666666667+13.5x-3x²+0.166666666667x³+13.5t-6tx+½tx²-3t²+½t²x+0.166666666667t³,
			       "pdf":13.5-6x+½x²-6t+tx+½t²,  },
			    {"action":"b", "zone":[0 ; 2],  "is_contained_in_zone": false, "target":1, "reset":
			       [ x] , "weight":16.3333333333, "cdf":11.5t-2t²+0.166666666667t³, "pdf":11.5-4t+½t²,  }]
	   "id":3,
	   "name":"m.UID  y=0",
	   "is_accepting":true,
	   "redcoord":[[0, 1], [2]],
	   "clockmap":[0; 0; x; ],
	   "weight":37.3333333333-17.5x+2.5x²-0.333333333333x³,
	   "transitions":[
			    {"action":"b", "zone":[0 ; 3-x],  "is_contained_in_zone": false, "target":1, "reset":
			       [ x] , "weight":21-4x-½x²-0.166666666667x³, "cdf":11.5t-2t²+0.166666666667t³, "pdf":11.5-4t+½t²,  },
			    {"action":"a", "zone":[0 ; 2-x],  "is_contained_in_zone": false, "target":3, "reset":
			       [ y] , "weight":16.3333333333-13.5x+3x²-0.166666666667x³, "cdf":13.5t-6tx+½tx²-3t²+½t²x+0.166666666667t³, "pdf":13.5-6x+½x²-6t+tx+½t²,  }]

  $ wordgen fromregexp3.prism --poly 2 --seed 65234 --no-cache | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Reading Prism automaton file. 
  Computing forward reachability graph ... 3 states found, automaton is deterministic. 
  Splitting reachability graph ... 3 states and 5 DBMs found. 
  Computing Distribution[floating point; Poly; no s; clocks:{y; x; }; vars:{t; z; s; T; }] -> 2: [||] 
  Volume in initial state:½, degree of liberty:2
  0.824974[a] 0.880743[b] 
  0.784165[a] 0.681760[b] 
  0.358816[a] 0.931201[b] 
  0.508239[a] 0.783647[b] 
  0.430741[a] 0.864276[b] 
  0.957794[a] 0.779234[b] 
  0.607920[a] 0.922799[b] 
  0.986227[a] 0.965226[b] 
  0.893729[a] 0.909389[b] 
  0.861384[a] 0.442579[b] 

  $ wordgen --regexp "<a<b<cc>_[0,1]>_[0,2]>_[0,3]" --poly 4 --exact-rational --expected-duration 2.1 --exact-duration --traj 0 --no-cache | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Computing forward reachability graph ... 5 states found, automaton is deterministic. 
  Splitting reachability graph ... 17 states and 39 DBMs found. 
  Computing Distribution[exact rational; ExpPoly; s formal; clocks:{dummy_clock; c0; c1; c2; }; vars:{t; z; s; T; }] -> 4: [||||] 
  Volume in initial state:-0.666667*u(+T-3)+(1-1/2.0 T)*u(+T-2)+(-1/3.0 +1/2.0 T-1/6.0 T³)*u(+T-1)+(1/6.0 T³), degree of liberty:4

  $ wordgen --regexp "<a<b<c*>_[0,1]>_[0,2]>_[0,3]" --poly 4 --exact-rational --expected-duration 2.1 --exact-duration --traj 0 --no-cache | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Computing forward reachability graph ... 4 states found, automaton is deterministic. 
  Splitting reachability graph ... 16 states and 38 DBMs found. 
  Computing Distribution[exact rational; ExpPoly; s formal; clocks:{dummy_clock; c0; c1; c2; }; vars:{t; z; s; T; }] -> 4: [||||] 
  Volume in initial state:-0.666667*u(+T-3)+(1-1/2.0 T)*u(+T-2)+(-1/3.0 +1/2.0 T-1/6.0 T³)*u(+T-1)+(1/6.0 T³), degree of liberty:4


Test with s<>0

  $ wordgen nfm19.prism --seed 42 --frequency 1.0 data.out --output-format state_list_full | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Precomputation file found but file have change discard !
  Reading Prism automaton file. 
  Computing forward reachability graph ... 7 states found, automaton is deterministic. 
  Splitting reachability graph ... 75 states and 430 DBMs found. 
  Computing Distribution[floating point; ExpPoly; s=1 inlined; clocks:{x4; x3; x2; x1; x; }; vars:{t; z; s; T; }] -> 3: [|||] 
  Volume in initial state:0.646462, degree of liberty:3
  Saving Precomputation 
  Sampling: [] 

  $ echo "s\nhelp\nreset\ns\ns" | wordgen twoears.prism --seed 42 --output-format debug -i
  Precomputation file found ! [0.00]
  Reading Distribution[floating point; Poly; no s; clocks:{y; x; }; vars:{t; z; s; T; }] -> 3
  Using rejection sampling to solve non-determinism !
  id:0 m.UID y:0.000000  x:0.000000  
     [a]-[0;1]--(9.66666666667,3)->9.66667->2:"m.UID"
    *[a]-[1;2]--(5.66666666667,3)->5.66667->3:"m.UID"
     [b]-[0;2]--(16.3333333333,3)->16.3333->1:"m.UID"
    
  >((11.5t-2t²+0.166666666667t³)/(16.3333333333))->1.403895[b]
  id:1 m.UID y:1.403895  x:0.000000  
    *[a]-[0;1]--(3.5,2)->3.5->2:"m.UID"
     [a]-[1;2]--(2,2)->2->3:"m.UID"
     [b]-[0;0.596105]--(6-4y+½y²,2)->1.36988->1:"m.UID"
    
  >available command : help, step, reset, exit, "t[a]" with t a time and a an action
  >
  id:0 m.UID y:0.000000  x:0.000000  
     [a]-[0;1]--(9.66666666667,3)->9.66667->2:"m.UID"
     [a]-[1;2]--(5.66666666667,3)->5.66667->3:"m.UID"
    *[b]-[0;2]--(16.3333333333,3)->16.3333->1:"m.UID"
    
  >((11.5t-2t²+0.166666666667t³)/(16.3333333333))->0.781747[b]
  id:1 m.UID y:0.781747  x:0.000000  
     [a]-[0;1]--(3.5,2)->3.5->2:"m.UID"
    *[a]-[1;2]--(2,2)->2->3:"m.UID"
     [b]-[0;1.21825]--(6-4y+½y²,2)->3.17858->1:"m.UID"
    
  >((4t-ty-½t²)/(6-4y+½y²))->0.988335[b]
  id:1 m.UID y:1.770081  x:0.000000  
    *[a]-[0;1]--(1,1)->1->2:"m.UID"
     [a]-[1;2]--(1,1)->1->3:"m.UID"
     [b]-[0;0.229919]--(2-y,1)->0.229919->1:"m.UID"
    
  >
  [1]

  $ wordgen nfm19.prism --seed 42 --frequency 1.0 --exact-rational --output-format state_list | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Precomputation file found but file have change discard !
  Reading Prism automaton file. 
  Computing forward reachability graph ... 7 states found, automaton is deterministic. 
  Splitting reachability graph ... 75 states and 430 DBMs found. 
  Computing Distribution[exact rational; ExpPoly; s formal; clocks:{x4; x3; x2; x1; x; }; vars:{t; z; s; T; }] -> 3: [|||] 
  Volume in initial state:e^{-6s}(-1s⁻³)+e^{-4s}(3s⁻³)+e^{-2s}(-3s⁻³)+1s⁻³, degree of liberty:3
  Saving Precomputation 
  0  0  0  0  0  0  
  48  0.673574  0.673574  0.673574  0  0  
  2  1.17713  1.17713  0  0.50356  0  
  
  0  0  0  0  0  0  
  48  0.766615  0.766615  0.766615  0  0  
  2  1.74715  1.74715  0  0.980531  0  
  
  0  0  0  0  0  0  
  39  1.45154  1.45154  1.45154  0  0  
  2  1.47177  1.47177  0  0.0202308  0  
  
  0  0  0  0  0  0  
  48  0.400243  0.400243  0.400243  0  0  
  71  0.935147  0.935147  0  0.534904  0  
  
  0  0  0  0  0  0  
  48  0.223529  0.223529  0.223529  0  0  
  6  1.27667  1.27667  0  1.05314  0  
  
  0  0  0  0  0  0  
  48  0.142821  0.142821  0.142821  0  0  
  71  0.289269  0.289269  0  0.146448  0  
  
  0  0  0  0  0  0  
  48  0.0017045  0.0017045  0.0017045  0  0  
  71  0.842562  0.842562  0  0.840857  0  
  
  0  0  0  0  0  0  
  48  0.0609211  0.0609211  0.0609211  0  0  
  71  0.855503  0.855503  0  0.794582  0  
  
  0  0  0  0  0  0  
  48  0.457084  0.457084  0.457084  0  0  
  71  0.953947  0.953947  0  0.496863  0  
  
  0  0  0  0  0  0  
  48  0.533429  0.533429  0.533429  0  0  
  71  0.559803  0.559803  0  0.0263735  0  
  




  $ wordgen nfm19.prism --seed 42 --expected-duration 1.0 -v 6 --output-format timestamp | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  parameters : wordgen --seed 42 --output-format timestamp -v 6 --expected-duration 1
  Precomputation file found but file have change discard !
  Reading Prism automaton file. 
  Computing forward reachability graph ... 7 states found, automaton is deterministic. 
  Splitting reachability graph ... 75 states and 430 DBMs found. 
  Computing Distribution[floating point; ExpPoly; s formal; clocks:{x4; x3; x2; x1; x; }; vars:{t; z; s; T; }] -> 3: [|||] 
  Volume in initial state:e^{-6s}(-s⁻³)+e^{-4s}(3s⁻³)+e^{-2s}(-3s⁻³)+s⁻³, degree of liberty:3
  Saving Precomputation 
  Computing laplace parameter s for E[T]=1 
  Weight : e^{-6s}(-s⁻³)+e^{-4s}(3s⁻³)+e^{-2s}(-3s⁻³)+s⁻³
  Weight': e^{-6s}(3s^-4+6s⁻³)+e^{-4s}(-9s^-4-12s⁻³)+e^{-2s}(9s^-4+6s⁻³)+-3s^-4
  Weight2 : e^{-6s}(-s)+e^{-4s}(3s)+e^{-2s}(-3s)+s
  Weight2': e^{-6s}(3+6s)+e^{-4s}(-9-12s)+e^{-2s}(9+6s)-3
  Weight_taylor : 8-24s
  Weight_taylor': -24+80.s-288s²
  wdom: (-6,-3,-1);(0,-3,1)
  dwdom: (-6,-3,6);(0,-4,-3)
  range : [6; 0]
  Time duration for s: 0 -> (2,8)
  Time duration for s: 1 -> (1.06089,-0.827815)
  Time duration for s: 2.28156 -> (0.251654,-0.448506)
  Time duration for s: 2.84265 -> (0.03491,-0.330232)
  Time duration for s: 2.94837 -> (0.000976868,-0.311948)
  Time duration for s: 2.9515 -> (8.2032e-07,-0.311424)
  Time duration for s: 2.9515 -> (5.79758e-13,-0.311424)
  Time duration for s: 2.9515 -> (2.22045e-16,-0.311424)
  range:[0; 3; 6] -> s=2.9515;E[T]=1  
  0.449735	0.104746	0.241948	
  0.549987	0.977259	0.560939	
  0.266630	0.014535	0.113040	
  0.231162	0.587637	0.044101	
  0.121002	0.031465	0.335570	
  0.075410	0.084705	0.014610	
  0.000866	0.650344	0.029516	
  0.031437	0.639563	0.455645	
  0.270582	0.592628	0.269845	
  0.327673	0.021041	0.175725	

  $ wordgen nfm19.prism --seed 42 --expected-duration 1.0 --exact-rational --output-format time_and_label | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Precomputation file found but file have change discard !
  Reading Prism automaton file. 
  Computing forward reachability graph ... 7 states found, automaton is deterministic. 
  Splitting reachability graph ... 75 states and 430 DBMs found. 
  Computing Distribution[exact rational; ExpPoly; s formal; clocks:{x4; x3; x2; x1; x; }; vars:{t; z; s; T; }] -> 3: [|||] 
  Volume in initial state:e^{-6s}(-1s⁻³)+e^{-4s}(3s⁻³)+e^{-2s}(-3s⁻³)+1s⁻³, degree of liberty:3
  Saving Precomputation 
  Computing laplace parameter s for E[T]=1 range:[0; 3; 6] -> s=2.9515;E[T]=1  
  0.449735	98
  0.554481	99
  0.796429	100
  
  0.549987	98
  1.527246	99
  2.088184	100
  
  0.266630	98
  0.281166	99
  0.394206	100
  
  0.231162	98
  0.818798	99
  0.862899	100
  
  0.121002	98
  0.152466	99
  0.488036	100
  
  0.075410	98
  0.160115	99
  0.174725	100
  
  0.000866	98
  0.651210	99
  0.680726	100
  
  0.031437	98
  0.671001	99
  1.126645	100
  
  0.270582	98
  0.863210	99
  1.133055	100
  
  0.327673	98
  0.348714	99
  0.524438	100
  

  $ wordgen twoears.prism -v 0 --traj 1 --expected-size 10 --template "0.5[b]0.25[b]0.65[0.2]0.3[0.1]" --output-format void
  Using rejection sampling to solve non-determinism !


Test uppal input

  $ wordgen twoears.xml --receding 10 --traj 1 --exact-rational --output-format debug --seed 42 -v 5 | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  parameters : wordgen --traj 1 --receding 10 --seed 42 --output-format debug -v 5 --exact-rational
  Precomputation file found but file have change discard !
  Reading UPPAAL automaton file. 
  Computing forward reachability graph ... 3 states found, automaton is not deterministic. 
  Splitting reachability graph ... 4 states and 15 DBMs found. 
  Computing Distribution[exact rational; Poly; no s; clocks:{x; y; }; vars:{t; z; s; T; }] -> 3: [|||] 
  Volume in initial state:95/3.0 , degree of liberty:3
  Saving Precomputation 
  Using rejection sampling to solve non-determinism !
  ((1t)/(2))->0.049526[t]
  id:0 Process.id0 x:0.000000  y:0.000000  
     [t]-[0;1]--(29/3.0 ,3)->9.66667->1:"Process.id0"
     [t]-[0;2]--(49/3.0 ,3)->16.3333->3:"Process.id0"
    *[t]-[1;2]--(17/3.0 ,3)->5.66667->2:"Process.id0"
    
  ((23/2.0 t-2t²+1/6.0 t³)/(49/3.0 ))->1.646962[t]
  id:3 Process.id0
                                                     x:0.000000  y:1.646962  
    *[t]-[0;1]--(29/3.0 ,3)->9.66667->1:"Process.id0"
     [t]-[0;0.353038]--(49/3.0 -23/2.0 y+2y²-1/6.0 y³,3)->2.07368->3:"Process.id0"
     [t]-[1;2]--(17/3.0 ,3)->5.66667->2:"Process.id0"
    
  ((-32/3.0 +27/2.0 t-3t²+1/6.0 t³)/(17/3.0 ))->1.856278[t]
  id:2 Process.id0
                                                              x:1.856278 
                                                              y:0.000000  
     [t]-[0;0.143722]--(49/3.0 -27/2.0 x+3x²-1/6.0 x³,3)->0.544834->2:"Process.id0"
    *[t]-[0;1.14372]--(21-4x-1/2.0 x²-1/6.0 x³,3)->10.786->3:"Process.id0"
    
  ((23/2.0 t-2t²+1/6.0 t³)/(21-4x-1/2.0 x²-1/6.0 x³))->0.115594[t]
  
  id:3 Process.id0 x:0.000000  y:0.115594  
    *[t]-[0;1]--(29/3.0 ,3)->9.66667->1:"Process.id0"
     [t]-[0;1.88441]--(49/3.0 -23/2.0 y+2y²-1/6.0 y³,3)->15.0305->3:"Process.id0"
     [t]-[1;2]--(17/3.0 ,3)->5.66667->2:"Process.id0"
    
  ((-32/3.0 +27/2.0 t-3t²+1/6.0 t³)/(17/3.0 ))->1.298090[t]
  id:2 Process.id0
                                                              x:1.298090 
                                                              y:0.000000  
     [t]-[0;0.70191]--(49/3.0 -27/2.0 x+3x²-1/6.0 x³,3)->3.49968->2:"Process.id0"
    *[t]-[0;1.70191]--(21-4x-1/2.0 x²-1/6.0 x³,3)->14.6006->3:"Process.id0"
    
  ((23/2.0 t-2t²+1/6.0 t³)/(21-4x-1/2.0 x²-1/6.0 x³))->0.084392[t]
  
  id:3 Process.id0 x:0.000000  y:0.084392  
     [t]-[0;1]--(29/3.0 ,3)->9.66667->1:"Process.id0"
     [t]-[0;1.91561]--(49/3.0 -23/2.0 y+2y²-1/6.0 y³,3)->15.377->3:"Process.id0"
    *[t]-[1;2]--(17/3.0 ,3)->5.66667->2:"Process.id0"
    
  ((23/2.0 t-4ty+1/2.0 ty²-2t²+1/2.0 t²y+1/6.0 t³)/(49/3.0 -23/2.0 y+2y²-1/6.0 y³))->0.011520[t]
  
  id:3 Process.id0 x:0.000000  y:0.095912  
     [t]-[0;1]--(29/3.0 ,3)->9.66667->1:"Process.id0"
    *[t]-[0;1.90409]--(49/3.0 -23/2.0 y+2y²-1/6.0 y³,3)->15.2486->3:"Process.id0"
     [t]-[1;2]--(17/3.0 ,3)->5.66667->2:"Process.id0"
    
  ((23/2.0 t-4ty+1/2.0 ty²-2t²+1/2.0 t²y+1/6.0 t³)/(49/3.0 -23/2.0 y+2y²-1/6.0 y³))->0.469718[t]
  
  id:3 Process.id0 x:0.000000  y:0.565630  
     [t]-[0;1]--(29/3.0 ,3)->9.66667->1:"Process.id0"
    *[t]-[0;1.43437]--(49/3.0 -23/2.0 y+2y²-1/6.0 y³,3)->10.4383->3:"Process.id0"
     [t]-[1;2]--(17/3.0 ,3)->5.66667->2:"Process.id0"
    
  ((23/2.0 t-2t²+1/6.0 t³)/(29/3.0 ))->0.670353[t]
  id:1 Process.id0
                                                     x:0.670353  y:0.000000  
     [t]-[0;0.329647]--(7/2.0 -4x+1/2.0 x²,2)->1.04328->1:"Process.id0"
    *[t]-[0;2]--(6,2)->6->3:"Process.id0"
     [t]-[0.329647;1.32965]--(2,2)->2->2:"Process.id0"
    
  ((4t-1/2.0 t²)/(6))->0.312191[t]
  id:3 Process.id0 x:0.000000  y:0.312191  
    *[t]-[0;1]--(1,1)->1->1:"Process.id0"
     [t]-[0;1.68781]--(2-1y,1)->1.68781->3:"Process.id0"
     [t]-[1;2]--(1,1)->1->2:"Process.id0"
    
  ((-1+1t)/(1))->1.340900[t]
  

Test Template

  $ wordgen twoears.prism --template "0.5[a]_[b]@0.5[0.5]_[_]" --traj 2 --seed 42 --gnuplot-cmd '' --gnuplot-driver 'png size 10,10' | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  No output file specified, cannot use gnuplot
  Precomputation file found but file have change discard !
  Reading Prism automaton file. 
  Computing forward reachability graph ... 3 states found, automaton is deterministic. 
  Splitting reachability graph ... 4 states and 15 DBMs found. 
  Computing Distribution[floating point; Poly; no s; clocks:{y; x; }; vars:{t; z; s; T; }] -> 3: [|||] 
  Volume in initial state:31.6666666667, degree of liberty:3
  Saving Precomputation 
  0.454917[a] 0.514476[b] 0.500000[b] 0.576032[b] 
  0.454917[a] 1.569497[b] 0.500000[a] 1.061474[b] 

  $ wordgen twoears.prism --template "0.5[a]_[b]@0.5[0.5]_[_]" out.dat --traj 10 --seed 42 --gnuplot-cmd '' --gnuplot-driver 'png size 10,10' | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Precomputation file found ! [0.00]
  Reading Distribution[floating point; Poly; no s; clocks:{y; x; }; vars:{t; z; s; T; }] -> 3
  Sampling: [Using rejection sampling to solve non-determinism !
  ] 

  $ wordgen twoears.prism --template "0.5[a]_[b]@0.5[0.5]_[_]" out.dat --output-format void --traj 10 --seed 42 --gnuplot-driver 'png size 10,10' | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Nothing to plot, cannot use gnuplot
  Precomputation file found ! [0.00]
  Reading Distribution[floating point; Poly; no s; clocks:{y; x; }; vars:{t; z; s; T; }] -> 3
  Sampling: [Using rejection sampling to solve non-determinism !
  ] 


  $ wordgen nfm19.prism --seed 42 --template "@1[b]0.2[0.3]_[_]" --output-format timeword_state --gnuplot-driver 'png size 10,10' | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  No output file specified, cannot use gnuplot
  Precomputation file found but file have change discard !
  Reading Prism automaton file. 
  Computing forward reachability graph ... 7 states found, automaton is deterministic. 
  Splitting reachability graph ... 75 states and 430 DBMs found. 
  Computing Distribution[floating point; Poly; no s; clocks:{x4; x3; x2; x1; x; }; vars:{t; z; s; T; }] -> 3: [|||] 
  Volume in initial state:8, degree of liberty:3
  Saving Precomputation 
  (0  0  0  0  0  0  )
  1.000000[b] (48  1  1  1  0  0  )
  0.200000[c] (2  1.2  1.2  0  0.2  0  )
  1.558156[d] 
  (0  0  0  0  0  0  )
  1.000000[b] (39  1  1  1  0  0  )
  1.200000[c] (49  2.2  2.2  0  1.2  0  )
  0.674299[d] 
  (0  0  0  0  0  0  )
  1.000000[b] (39  1  1  1  0  0  )
  1.200000[c] (49  2.2  2.2  0  1.2  0  )
  1.864462[d] 
  (0  0  0  0  0  0  )
  1.000000[b] (39  1  1  1  0  0  )
  1.200000[c] (49  2.2  2.2  0  1.2  0  )
  1.235714[d] 
  (0  0  0  0  0  0  )
  1.000000[b] (48  1  1  1  0  0  )
  0.200000[c] (2  1.2  1.2  0  0.2  0  )
  0.936941[d] 
  (0  0  0  0  0  0  )
  1.000000[b] (48  1  1  1  0  0  )
  0.200000[c] (2  1.2  1.2  0  0.2  0  )
  0.036854[d] 
  (0  0  0  0  0  0  )
  1.000000[b] (48  1  1  1  0  0  )
  0.200000[c] (2  1.2  1.2  0  0.2  0  )
  0.103834[d] 
  (0  0  0  0  0  0  )
  1.000000[b] (48  1  1  1  0  0  )
  0.200000[c] (2  1.2  1.2  0  0.2  0  )
  1.230953[d] 
  (0  0  0  0  0  0  )
  1.000000[b] (48  1  1  1  0  0  )
  0.200000[c] (2  1.2  1.2  0  0.2  0  )
  1.472358[d] 
  (0  0  0  0  0  0  )
  1.000000[b] (48  1  1  1  0  0  )
  0.200000[c] (2  1.2  1.2  0  0.2  0  )
  1.894803[d] 

  $ wordgen nfm19.prism --seed 42 --template "@1[b]0.2[0.3]_[_]" --poly -1 --output-format debug --traj 2 | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Precomputation file found but file have change discard !
  Reading Prism automaton file. 
  Computing forward reachability graph ... 7 states found, automaton is deterministic. 
  Computing Distribution[floating point; Poly; no s; clocks:{x4; x3; x2; x1; x; }; vars:{t; z; s; T; }] -> -1: [] 
  Volume in initial state:1, degree of liberty:-1
  Saving Precomputation 
  id:0 m.UID  m.s:0 x4:0.000000  x3:0.000000  x2:0.000000  x1:0.000000 
  x:0.000000  
    *[b]-[0;2]
    
  ->1.000000[b]
  id:6 m.UID  m.s:1 x4:1.000000  x3:1.000000  x2:1.000000 
                x1:0.000000  x:0.000000  
    *[c]-[0;2]
    
  ->0.400000[c]
  id:2 m.UID  m.s:2 x4:1.400000  x3:1.400000  x2:0.000000 
                x1:0.400000  x:0.000000  
    *[d]-[0;2]
    
  ->1.395390[d]
  
  id:0 m.UID  m.s:0 x4:0.000000  x3:0.000000  x2:0.000000  x1:0.000000 
  x:0.000000  
    *[b]-[0;2]
    
  ->1.000000[b]
  id:6 m.UID  m.s:1 x4:1.000000  x3:1.000000  x2:1.000000 
                x1:0.000000  x:0.000000  
    *[c]-[0;2]
    
  ->0.400000[c]
  id:2 m.UID  m.s:2 x4:1.400000  x3:1.400000  x2:0.000000 
                x1:0.400000  x:0.000000  
    *[d]-[0;2]
    
  ->1.685747[d]
  

Test Monitor

  $ wordgen test2mod.prism --frequency 1 --seed 42 --apericube --traj 10 | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Reading Prism automaton file. 
  Computing forward reachability graph ... 13 states found, automaton is deterministic. 
  Splitting reachability graph ... 41 states and 79 DBMs found. 
  Computing Distribution[floating point; ExpPoly; s=1 inlined; clocks:{y; x; }; vars:{t; z; s; T; }] -> 3: [|||] 
  Volume in initial state:0.331042, degree of liberty:3
  Saving Precomputation 
  0.690173[a] 0.494443[a] 0.492636[a] 0	3	1.67725
  0.780660[a] 0.980983[a] 0.834989[a] 0	3	2.59663
  0.468517[a] 0.019998[a] 0.138173[a] 0	3	0.626688
  0.416332[a] 0.522076[a] 0.259268[a] 0	3	1.19768
  0.234277[a] 0.786110[a] 3.567071[c] 3	3	4.58746
  0.150130[a] 0.147679[a] 4.047163[c] 4	3	4.34497
  0.001800[a] 0.844453[a] 0.230746[a] 0	3	1.077
  0.064212[a] 0.795239[a] 0.038588[a] 0	3	0.898039
  0.474140[a] 0.482317[a] 0.495419[a] 0	3	1.45188
  0.551118[a] 0.025854[a] 0.178536[a] 0	3	0.755508

Test time duration

  $ wordgen test2mod.prism --expected-duration 9 --exact-rational --seed 42 --output-format state_list_delay | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Precomputation file found but file have change discard !
  Reading Prism automaton file. 
  Computing forward reachability graph ... 13 states found, automaton is deterministic. 
  Splitting reachability graph ... 41 states and 79 DBMs found. 
  Computing Distribution[exact rational; ExpPoly; s formal; clocks:{y; x; }; vars:{t; z; s; T; }] -> 3: [|||] 
  Volume in initial state:e^{-8s}(1s⁻³+4s⁻²)+e^{-6s}(1s⁻³)+e^{-5s}(-6s⁻³-2s⁻²)+e^{-4s}(5s⁻³+1s⁻²+1s⁻¹)+e^{-3s}(-1s⁻³)+e^{-2s}(3s⁻³)+e^{-s}(-3s⁻³)+1s⁻³, degree of liberty:3
  Saving Precomputation 
  Computing laplace parameter s for E[T]=9 range:[0; inf; inf] -> s=0.416001;E[T]=9  
  0  0  0  	1
  0  7.58948  7.58948  	99
  
  2  7.58948  0  	1
  2  12.5565  4.96704  	99
  
  1  0  4.96704  	1
  1  0.651619  5.61865  	97
  
  
  0  0  0  	1
  0  0.838599  0.838599  	97
  
  35  0  0.838599  	1
  35  0.984436  1.82304  	97
  
  32  0  1.82304  	1
  32  0.850527  2.67356  	97
  
  
  0  0  0  	1
  0  0.559585  0.559585  	97
  
  35  0  0.559585  	1
  35  4.11683  4.67642  	99
  
  29  4.11683  0  	1
  29  5.39612  1.27929  	99
  
  
  0  0  0  	1
  0  0.506383  0.506383  	97
  
  35  0  0.506383  	1
  35  10.0255  10.5319  	99
  
  29  10.0255  0  	1
  29  10.2795  0.253999  	98
  
  
  0  0  0  	1
  0  4.91608  4.91608  	99
  
  2  4.91608  0  	1
  2  6.00331  1.08724  	99
  
  6  0  1.08724  	1
  6  0.972152  2.05939  	97
  
  
  0  0  0  	1
  0  4.5683  4.5683  	99
  
  2  4.5683  0  	1
  2  4.78329  0.214992  	99
  
  30  0  0.214992  	1
  30  0.0310522  0.246044  	97
  
  
  0  0  0  	1
  0  4.00649  4.00649  	99
  
  2  4.00649  0  	1
  2  4.88616  0.879671  	98
  
  2  4.88616  0  	1
  2  4.99475  0.108597  	98
  
  
  0  0  0  	1
  0  4.23597  4.23597  	99
  
  2  4.23597  0  	1
  2  5.11528  0.879312  	98
  
  2  5.11528  0  	1
  2  6.36387  1.24859  	99
  
  
  0  0  0  	1
  0  6.08753  6.08753  	99
  
  2  6.08753  0  	1
  2  7.00781  0.920276  	98
  
  2  7.00781  0  	1
  2  8.54704  1.53923  	99
  
  
  0  0  0  	1
  0  6.55127  6.55127  	99
  
  2  6.55127  0  	1
  2  6.60907  0.0577994  	98
  
  2  6.60907  0  	1
  2  12.1535  5.54441  	99
  
  



  $ wordgen bimodal.prism --expected-duration 4.4 --seed 42 | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Reading Prism automaton file. 
  Computing forward reachability graph ... 3 states found, automaton is deterministic. 
  Splitting reachability graph ... 3 states and 4 DBMs found. 
  Computing Distribution[floating point; ExpPoly; s formal; clocks:{x; }; vars:{t; z; s; T; }] -> 3: [|||] 
  Volume in initial state:e^{-6s}(s⁻³)+e^{-3s}(-s⁻³)+e^{-2s}(3s⁻³)+e^{-s}(-3s⁻³)+s⁻³, degree of liberty:3
  Saving Precomputation 
  Computing laplace parameter s for E[T]=4.4 range:[0; inf; inf] -> s=0.560743;E[T]=4.4  
  0.721642[a] 0.273457[c] 0.634704[c] 
  0.805812[a] 0.977269[c] 0.800836[c] 
  0.505252[a] 0.036688[c] 0.265533[c] 
  0.452184[a] 0.893870[c] 0.241122[c] 
  2.679616[b] 2.179682[c] 4.058419[c] 
  2.421609[b] 2.481586[c] 2.084108[c] 
  2.004811[b] 6.115247[c] 2.247927[c] 
  0.073043[a] 0.870943[c] 0.235914[c] 
  3.548686[b] 6.864533[c] 3.591977[c] 
  0.587594[a] 0.054252[c] 0.405658[c] 

Test Low discrepancy sequence

  $ wordgen nfm19.json --sampler kronecker --seed 42 --output-format debug --exact-rational --traj 1 --poly 7 | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Precomputation file found but file have change discard !
  Reading splitted reachability graph ... 74 states found. 
  Computing Distribution[exact rational; Poly; no s; clocks:{x; y; x_3; x_4; x_5; }; vars:{t; s; z; T; }] -> 7: [|||||||] 
  Volume in initial state:35389/315.0 , degree of liberty:7
  Saving Precomputation 
  Using rejection sampling to solve non-determinism !
  ((1t)/(2-1y))->0.000000[d]
  id:0
                             ((s=0),{x=0,x1=0,x2=0,x3=0,x4=0,x=x1,x=x2,x=x3,x=x4,x1=x2,x1=x3,x1=x4,x2=x3,x2=x4,x3=x4})
                             x:0.000000  y:0.000000  x_3:0.000000 
                             x_4:0.000000  x_5:0.000000  
     [b]-[0;1]--(36181/630.0 ,7)->57.4302->1:"((s=1),{x=0,x1=0,0<x2<1,0<x3<1,0<x4<1,x=x1,-1<x-x2<0,-1<x-x3<0,-1<x-x4<0,-1<x1-x2<0,-1<x1-x3<0,-1<x1-x4<0,x2=x3,x2=x4,x3=x4})"
    *[b]-[1;2]--(34597/630.0 ,7)->54.9159->10:"(CP_10(s=1),{x=0,x1=0,1<x2<2,1<x3<2,1<x4<2,x=x1,-2<x-x2<-1,-2<x-x3<-1,-2<x-x4<-1,-2<x1-x2<-1,-2<x1-x3<-1,-2<x1-x4<-1,x2=x3,x2=x4,x3=x4})"
    
  ((-36347/630.0 +1387/24.0 t-1/240.0 t²+1/144.0 t³-1/144.0 t⁴-23/240.0 t⁵+1/720.0 t^6+1/1680.0 t^7)/(34597/630.0 ))->1.891366[b]
  
  id:10
  (CP_10(s=1),{x=0,x1=0,1<x2<2,1<x3<2,1<x4<2,x=x1,-2<x-x2<-1,-2<x-x3<-1,-2<x-x4<-1,-2<x1-x2<-1,-2<x1-x3<-1,-2<x1-x4<-1,x2=x3,x2=x4,x3=x4})
  x:0.000000  y:0.000000  x_3:1.891366  x_4:1.891366  x_5:1.891366  
     [c]-[0;0.108634]--(2602/45.0 -3127/120.0 x_3-59/16.0 x_3²+71/36.0 x_3³-29/48.0 x_3⁴+11/120.0 x_3⁵,6)->3.17635->35:"(CP_35CP_22CP_9(s=2),{x=0,0<x1<1,x2=0,1<x3<2,1<x4<2,-1<x-x1<0,x=x2,-2<x-x3<-1,-2<x-x4<-1,0<x1-x2<1,-2<x1-x3<0,-2<x1-x4<0,-2<x2-x3<-1,-2<x2-x4<-1,x3=x4})"
    *[c]-[0.108634;1]--(-1129/40.0 +79/3.0 x_3+119/48.0 x_3²-11/18.0 x_3³+5/48.0 x_3⁴-1/12.0 x_3⁵+1/360.0 x_3^6,6)->25.7581->22:"(CP_22CP_9(s=2),{x=0,0<x1<1,x2=0,2<x3<3,2<x4<3,-1<x-x1<0,x=x2,-3<x-x3<-2,-3<x-x4<-2,0<x1-x2<1,-2<x1-x3<-1,-2<x1-x4<-1,-3<x2-x3<-2,-3<x2-x4<-2,x3=x4})"
     [c]-[1;1.10863]--(4369/90.0 -507/40.0 x_3-167/16.0 x_3²+43/18.0 x_3³-1/16.0 x_3⁴+1/60.0 x_3⁵-1/720.0 x_3^6,6)->2.93672->36:"(CP_36CP_23CP_9(s=2),{x=0,1<x1<2,x2=0,2<x3<3,2<x4<3,-2<x-x1<-1,x=x2,-3<x-x3<-2,-3<x-x4<-2,1<x1-x2<2,-2<x1-x3<-1,-2<x1-x4<-1,-3<x2-x3<-2,-3<x2-x4<-2,x3=x4})"
     [c]-[1.10863;2]--(-407/20.0 +1487/120.0 x_3+35/3.0 x_3²-34/9.0 x_3³+1/12.0 x_3⁴-1/60.0 x_3⁵+1/360.0 x_3^6,6)->20.052->9:"(CP_9(s=2),{x=0,1<x1<2,x2=0,3<x3<4,3<x4<4,-2<x-x1<-1,x=x2,-4<x-x3<-3,-4<x-x4<-3,1<x1-x2<2,-2<x1-x3<-1,-2<x1-x4<-1,-4<x2-x3<-3,-4<x2-x4<-3,x3=x4})"
    
  ((-1563/20.0 +607/40.0 x_3+22/3.0 x_3²-29/18.0 x_3³+1/4.0 x_3⁴-1/20.0 x_3⁵+1/360.0 x_3^6+1257/40.0 t-53/8.0 tx_3+61/12.0 tx_3²-11/12.0 tx_3³-1/8.0 tx_3⁴+1/60.0 tx_3⁵-175/48.0 t²+23/4.0 t²x_3-15/8.0 t²x_3²-1/12.0 t²x_3³+1/48.0 t²x_3⁴+101/36.0 t³-9/4.0 t³x_3+1/4.0 t³x_3²-47/48.0 t⁴+3/8.0 t⁴x_3-1/48.0 t⁴x_3²+11/120.0 t⁵-1/60.0 t⁵x_3-1/360.0 t^6)/(-407/20.0 +1487/120.0 x_3+35/3.0 x_3²-34/9.0 x_3³+1/12.0 x_3⁴-1/60.0 x_3⁵+1/360.0 x_3^6))->1.792657[c]
  
  id:9
  (CP_9(s=2),{x=0,1<x1<2,x2=0,3<x3<4,3<x4<4,-2<x-x1<-1,x=x2,-4<x-x3<-3,-4<x-x4<-3,1<x1-x2<2,-2<x1-x3<-1,-2<x1-x4<-1,-4<x2-x3<-3,-4<x2-x4<-3,x3=x4})
  x:0.000000  y:1.792657  x_3:0.000000  x_4:3.684023  x_5:3.684023  
     [d]-[0;0.207343]--(446/15.0 -40/3.0 y-2y²+13/12.0 y³-1/3.0 y⁴+1/20.0 y⁵,5)->3.12821->20:"(CP_20CP_7(s=3),{x=0,1<x1<2,0<x2<1,x3=0,1<x4<4,-2<x-x1<-1,-1<x-x2<0,x=x3,-4<x-x4<-1,0<x1-x2<2,1<x1-x3<2,-2<x1-x4<0,0<x2-x3<1,-4<x2-x4<0,-4<x3-x4<-1})"
     [d]-[0.207343;0.315977]--(6+47/3.0 x_4-29/2.0 x_4²+37/12.0 x_4³-1/4.0 x_4⁴+46/3.0 y-4yx_4+3yx_4²-1yx_4³+1/8.0 yx_4⁴+2/3.0 y²+1y²x_4-1/12.0 y²x_4³-29/12.0 y³+2/3.0 y³x_4+1/12.0 y⁴-1/24.0 y⁵,5)->1.64928->7:"(CP_7(s=3),{x=0,2<x1<3,0<x2<1,x3=0,2<x4<4,-3<x-x1<-2,-1<x-x2<0,x=x3,-4<x-x4<-2,1<x1-x2<2,2<x1-x3<3,-2<x1-x4<0,0<x2-x3<1,-4<x2-x4<-1,-4<x3-x4<-2})"
     [d]-[0.315977;1]--(-92/5.0 -721/24.0 x_4+143/6.0 x_4²-19/4.0 x_4³+1/4.0 x_4⁴+1/120.0 x_4⁵-3y+11/2.0 yx_4-4yx_4²+4/3.0 yx_4³-1/6.0 yx_4⁴+21/4.0 y²-4y²x_4+1/2.0 y²x_4²+1/12.0 y²x_4³+2y³-2/3.0 y³x_4,5)->8.78513->8:"(CP_8(s=3),{x=0,2<x1<3,0<x2<1,x3=0,4<x4<5,-3<x-x1<-2,-1<x-x2<0,x=x3,-5<x-x4<-4,1<x1-x2<2,2<x1-x3<3,-2<x1-x4<-1,0<x2-x3<1,-4<x2-x4<-3,-5<x3-x4<-4})"
     [d]-[1;1.20734]--(737/30.0 +26/3.0 x_4-4x_4²-9/4.0 y-31/6.0 yx_4+2yx_4²-131/12.0 y²+13/4.0 y²x_4+11/6.0 y³-5/3.0 y³x_4+17/24.0 y⁴+1/8.0 y⁴x_4-3/40.0 y⁵,5)->1.97841->52:"(CP_52CP_33CP_21CP_8(s=3),{x=0,2<x1<3,1<x2<2,x3=0,4<x4<5,-3<x-x1<-2,-2<x-x2<-1,x=x3,-5<x-x4<-4,1<x1-x2<2,2<x1-x3<3,-2<x1-x4<-1,1<x2-x3<2,-4<x2-x4<-3,-5<x3-x4<-4})"
     [d]-[1.20734;1.31598]--(-201/10.0 +437/12.0 x_4-75/4.0 x_4²+5/2.0 x_4³+5/24.0 x_4⁴-1/30.0 x_4⁵+139/12.0 y-17/2.0 yx_4+11/2.0 yx_4²-11/6.0 yx_4³+1/6.0 yx_4⁴+21/4.0 y²+1/2.0 y²x_4-1/4.0 y²x_4²-1/12.0 y²x_4³-9/2.0 y³+7/3.0 y³x_4-17/24.0 y⁴-1/8.0 y⁴x_4+3/40.0 y⁵,5)->0.903379->54:"(CP_54CP_34CP_21CP_8(s=3),{x=0,3<x1<4,1<x2<2,x3=0,4<x4<5,-4<x-x1<-3,-2<x-x2<-1,x=x3,-5<x-x4<-4,1<x1-x2<2,3<x1-x3<4,-2<x1-x4<-1,1<x2-x3<2,-4<x2-x4<-3,-5<x3-x4<-4})"
    *[d]-[1.31598;2]--(77/8.0 -112/3.0 x_4+37/2.0 x_4²-7/4.0 x_4³-1/3.0 x_4⁴+1/24.0 x_4⁵-9y+27/2.0 yx_4-15/2.0 yx_4²+11/6.0 yx_4³-1/6.0 yx_4⁴+15/4.0 y²-11/4.0 y²x_4+1/4.0 y²x_4²+1/12.0 y²x_4³+2y³-2/3.0 y³x_4,5)->3.54553->21:"(CP_21CP_8(s=3),{x=0,3<x1<4,1<x2<2,x3=0,5<x4<6,-4<x-x1<-3,-2<x-x2<-1,x=x3,-6<x-x4<-5,1<x1-x2<2,3<x1-x3<4,-2<x1-x4<-1,1<x2-x3<2,-4<x2-x4<-3,-6<x3-x4<-5})"
    
  ((-401/10.0 -125/8.0 x_4+6x_4²-1/12.0 y+19/3.0 yx_4-2yx_4²+37/3.0 y²-17/4.0 y²x_4-7/6.0 y³+5/3.0 y³x_4-17/24.0 y⁴-1/8.0 y⁴x_4+3/40.0 y⁵+271/24.0 t+28/3.0 tx_4-2tx_4²+4ty-2tyx_4-1ty²+1ty²x_4-2/3.0 ty³+20/3.0 t²-3t²x_4-2t²y+1t²yx_4-1/2.0 t²y²-8/3.0 t³+2/3.0 t³x_4+1/3.0 t³y-1/6.0 t³yx_4+1/12.0 t³y²+1/4.0 t⁴-1/24.0 t⁴x_4-1/120.0 t⁵)/(-201/10.0 +437/12.0 x_4-75/4.0 x_4²+5/2.0 x_4³+5/24.0 x_4⁴-1/30.0 x_4⁵+139/12.0 y-17/2.0 yx_4+11/2.0 yx_4²-11/6.0 yx_4³+1/6.0 yx_4⁴+21/4.0 y²+1/2.0 y²x_4-1/4.0 y²x_4²-1/12.0 y²x_4³-9/2.0 y³+7/3.0 y³x_4-17/24.0 y⁴-1/8.0 y⁴x_4+3/40.0 y⁵))->1.285052[d]
  
  id:54
  (CP_54CP_34CP_21CP_8(s=3),{x=0,3<x1<4,1<x2<2,x3=0,4<x4<5,-4<x-x1<-3,-2<x-x2<-1,x=x3,-5<x-x4<-4,1<x1-x2<2,3<x1-x3<4,-2<x1-x4<-1,1<x2-x3<2,-4<x2-x4<-3,-5<x3-x4<-4})
  x:0.000000  y:3.077710  x_3:1.285052  x_4:0.000000  x_5:4.969076  
    *[a]-[0;0.714948]--(46/3.0 -41/6.0 x_3-3/4.0 x_3²+1/6.0 x_3³,4)->5.6673->15:"(CP_15(s=4),{x=0,1<x1<4,1<x2<2,0<x3<1,x4=0,-4<x-x1<-1,-2<x-x2<-1,-1<x-x3<0,x=x4,0<x1-x2<2,0<x1-x3<4,1<x1-x4<4,0<x2-x3<2,1<x2-x4<2,0<x3-x4<1})"
     [a]-[0.714948;0.92229]--(10+41/6.0 x_3-1/4.0 x_3²-5/6.0 x_3³-1/8.0 x_3⁴-19/6.0 y+1yx_3²+1/3.0 yx_3³-3/4.0 y²-1/4.0 y²x_3²-1/6.0 y³+1/24.0 y⁴,4)->1.63737->28:"(CP_28CP_15(s=4),{x=0,2<x1<4,2<x2<3,0<x3<1,x4=0,-4<x-x1<-2,-3<x-x2<-2,-1<x-x3<0,x=x4,0<x1-x2<2,1<x1-x3<4,2<x1-x4<4,1<x2-x3<2,2<x2-x4<3,0<x3-x4<1})"
     [a]-[0.92229;1]--(-139/8.0 +3x_3²+1x_3³-17/6.0 y-5/2.0 yx_3²-1/3.0 yx_3³+19/4.0 y²+1/2.0 y²x_3²-1/2.0 y³-1/24.0 y⁴,4)->0.597408->12:"(CP_12(s=4),{x=0,4<x1<5,2<x2<3,0<x3<1,x4=0,-5<x-x1<-4,-3<x-x2<-2,-1<x-x3<0,x=x4,1<x1-x2<2,3<x1-x3<4,4<x1-x4<5,1<x2-x3<2,2<x2-x4<3,0<x3-x4<1})"
     [a]-[1;1.03092]--(10/3.0 +28/3.0 x_5-2x_5²-5x_3²+1x_3²x_5-5/3.0 x_3³+1/3.0 x_3³x_5+10y-2yx_5+5/2.0 yx_3²-1/2.0 yx_3²x_5-5y²+1y²x_5,4)->0.231019->68:"(CP_68CP_58CP_39CP_25CP_12(s=4),{x=0,4<x1<5,2<x2<3,1<x3<2,x4=0,-5<x-x1<-4,-3<x-x2<-2,-2<x-x3<-1,x=x4,1<x1-x2<2,3<x1-x3<4,4<x1-x4<5,1<x2-x3<2,2<x2-x4<3,1<x3-x4<2})"
    
  ((47/6.0 t+1/4.0 t²-1/6.0 t³)/(46/3.0 -41/6.0 x_3-3/4.0 x_3²+1/6.0 x_3³))->0.467051[a]
  
  id:15
  (CP_15(s=4),{x=0,1<x1<4,1<x2<2,0<x3<1,x4=0,-4<x-x1<-1,-2<x-x2<-1,-1<x-x3<0,x=x4,0<x1-x2<2,0<x1-x3<4,1<x1-x4<4,0<x2-x3<2,1<x2-x4<2,0<x3-x4<1})
  x:0.000000  y:3.544761  x_3:1.752103  x_4:0.467051  x_5:0.000000  
     [b]-[0;0.532949]--(23/6.0 -7/2.0 x_4-1/2.0 x_4²+1/6.0 x_4³,3)->2.10657->13:"(CP_13(s=5),{x=0,x1=0,1<x2<3,0<x3<1,0<x4<1,x=x1,-3<x-x2<-1,-1<x-x3<0,-1<x-x4<0,-3<x1-x2<-1,-1<x1-x3<0,-1<x1-x4<0,0<x2-x3<2,0<x2-x4<3,0<x3-x4<1})"
     [b]-[0.532949;1]--(4x_4,3)->1.8682->57:"(CP_57CP_38CP_24CP_13(s=5),{x=0,x1=0,1<x2<4,1<x3<2,0<x4<1,x=x1,-4<x-x2<-1,-2<x-x3<-1,-1<x-x4<0,-4<x1-x2<-1,-2<x1-x3<-1,-1<x1-x4<0,0<x2-x3<2,0<x2-x4<4,0<x3-x4<2})"
     [b]-[1;1.53295]--(4-4x_4,3)->2.1318->66:"(CP_66CP_56CP_38CP_24CP_13(s=5),{x=0,x1=0,1<x2<4,1<x3<2,1<x4<2,x=x1,-4<x-x2<-1,-2<x-x3<-1,-2<x-x4<-1,-4<x1-x2<-1,-2<x1-x3<-1,-2<x1-x4<-1,0<x2-x3<2,0<x2-x4<3,0<x3-x4<1})"
    *[b]-[1.53295;2]--(4x_4-1/6.0 x_4³,3)->1.85122->56:"(CP_56CP_38CP_24CP_13(s=5),{x=0,x1=0,2<x2<4,2<x3<3,1<x4<2,x=x1,-4<x-x2<-2,-3<x-x3<-2,-2<x-x4<-1,-4<x1-x2<-2,-3<x1-x3<-2,-2<x1-x4<-1,0<x2-x3<2,0<x2-x4<3,0<x3-x4<1})"
    
  ((-4+4t)/(4-4x_4))->1.312354[b]
  id:66
                                  (CP_66CP_56CP_38CP_24CP_13(s=5),{x=0,x1=0,1<x2<4,1<x3<2,1<x4<2,x=x1,-4<x-x2<-1,-2<x-x3<-1,-2<x-x4<-1,-4<x1-x2<-1,-2<x1-x3<-1,-2<x1-x4<-1,0<x2-x3<2,0<x2-x4<3,0<x3-x4<1})
                                  x:0.000000  y:0.000000  x_3:3.064458 
                                  x_4:1.779405  x_5:1.312354  
    *[c]-[0;0.687646]--(4-2x_5,2)->1.37529->43:"(CP_43CP_29CP_16(s=6),{x=0,0<x1<1,x2=0,1<x3<4,1<x4<2,-1<x-x1<0,x=x2,-4<x-x3<-1,-2<x-x4<-1,0<x1-x2<1,-4<x1-x3<0,-2<x1-x4<0,-4<x2-x3<-1,-2<x2-x4<-1,0<x3-x4<2})"
     [c]-[0.687646;1]--(-2+2x_5,2)->0.624709->44:"(CP_44CP_29CP_16(s=6),{x=0,0<x1<1,x2=0,2<x3<4,2<x4<3,-1<x-x1<0,x=x2,-4<x-x3<-2,-3<x-x4<-2,0<x1-x2<1,-4<x1-x3<-1,-2<x1-x4<-1,-4<x2-x3<-2,-3<x2-x4<-2,0<x3-x4<2})"
     [c]-[1;1.68765]--(4-2x_5,2)->1.37529->72:"(CP_72CP_64CP_45CP_30CP_16(s=6),{x=0,1<x1<2,x2=0,2<x3<4,2<x4<3,-2<x-x1<-1,x=x2,-4<x-x3<-2,-3<x-x4<-2,1<x1-x2<2,-3<x1-x3<-1,-2<x1-x4<-1,-4<x2-x3<-2,-3<x2-x4<-2,0<x3-x4<2})"
     [c]-[1.68765;2]--(-2+2x_5,2)->0.624709->49:"(CP_49CP_31CP_16(s=6),{x=0,1<x1<2,x2=0,3<x3<4,3<x4<4,-2<x-x1<-1,x=x2,-4<x-x3<-3,-4<x-x4<-3,1<x1-x2<2,-3<x1-x3<-1,-2<x1-x4<-1,-4<x2-x3<-3,-4<x2-x4<-3,0<x3-x4<1})"
    
  ((2t)/(4-2x_5))->0.362175[c]
  id:43
                               (CP_43CP_29CP_16(s=6),{x=0,0<x1<1,x2=0,1<x3<4,1<x4<2,-1<x-x1<0,x=x2,-4<x-x3<-1,-2<x-x4<-1,0<x1-x2<1,-4<x1-x3<0,-2<x1-x4<0,-4<x2-x3<-1,-2<x2-x4<-1,0<x3-x4<2})
                               x:0.000000  y:0.362175  x_3:0.000000 
                               x_4:2.141581  x_5:1.674530  
     [d]-[0;0.637825]--(1-1y,1)->0.637825->19:"(CP_19CP_7(s=3),{x=0,0<x1<1,0<x2<1,x3=0,1<x4<3,-1<x-x1<0,-1<x-x2<0,x=x3,-3<x-x4<-1,0<x1-x2<1,0<x1-x3<1,-2<x1-x4<0,0<x2-x3<1,-3<x2-x4<0,-3<x3-x4<-1})"
     [d]-[0.637825;1]--(1y,1)->0.362175->20:"(CP_20CP_7(s=3),{x=0,1<x1<2,0<x2<1,x3=0,1<x4<4,-2<x-x1<-1,-1<x-x2<0,x=x3,-4<x-x4<-1,0<x1-x2<2,1<x1-x3<2,-2<x1-x4<0,0<x2-x3<1,-4<x2-x4<0,-4<x3-x4<-1})"
     [d]-[1;1.63782]--(1-1y,1)->0.637825->50:"(CP_50CP_32CP_18CP_7(s=3),{x=0,1<x1<2,1<x2<2,x3=0,1<x4<4,-2<x-x1<-1,-2<x-x2<-1,x=x3,-4<x-x4<-1,0<x1-x2<1,1<x1-x3<2,-2<x1-x4<0,1<x2-x3<2,-3<x2-x4<0,-4<x3-x4<-1})"
    *[d]-[1.63782;2]--(1y,1)->0.362175->51:"(CP_51CP_32CP_18CP_7(s=3),{x=0,2<x1<3,1<x2<2,x3=0,2<x4<4,-3<x-x1<-2,-2<x-x2<-1,x=x3,-4<x-x4<-2,0<x1-x2<1,2<x1-x3<3,-2<x1-x4<0,1<x2-x3<2,-3<x2-x4<0,-4<x3-x4<-2})"
    
  ((-2+1y+1t)/(1y))->1.809246[d]
  
			       x:0.000000  y:0.000000  x_3:2.000000  x_4:1.000000  x_5:0.000000
				  x:0.000000  y:1.000000  x_3:0.000000  x_4:2.000000  x_5:1.000000





  $ wordgen twoears.prism --seed 42 --expected-duration 1.5 --exact-rational --traj 1 --poly 5 --output-format debug | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Precomputation file found but file have change discard !
  Reading Prism automaton file. 
  Computing forward reachability graph ... 3 states found, automaton is deterministic. 
  Splitting reachability graph ... 4 states and 15 DBMs found. 
  Computing Distribution[exact rational; ExpPoly; s formal; clocks:{y; x; }; vars:{t; z; s; T; }] -> 5: [|||||] 
  Volume in initial state:e^{-8s}(-3s^-5+5s^-4-2s⁻³)+e^{-7s}(11s^-5-2s^-4-15/2.0 s⁻³)+e^{-6s}(8s^-5-24s^-4-12s⁻³+3s⁻²)+e^{-5s}(-64s^-5-6s^-4+26s⁻³+37/3.0 s⁻²)+e^{-4s}(56s^-5+90s^-4+115/2.0 s⁻³+101/6.0 s⁻²)+e^{-3s}(56s^-5-2s^-4-19/2.0 s⁻³-10/3.0 s⁻²-5/8.0 s⁻¹)+e^{-2s}(-96s^-5-80s^-4-32s⁻³-8s⁻²-4/3.0 s⁻¹)+32s^-5, degree of liberty:5
  Saving Precomputation 
  Computing laplace parameter s for E[T]=1.5 range:[0; 3.92002; 8] -> s=3.15227;E[T]=1.5  
  id:0 m.UID y:0.000000  x:0.000000  
    *[a]-[0;1]--(e^{-8s}(-1s^-5+1s^-4)+e^{-7s}(2s^-5)+e^{-6s}(6s^-5-1s^-4-13/2.0 s⁻³+1s⁻²)+e^{-5s}(-25s^-5-26s^-4-2s⁻³+3s⁻²)+e^{-4s}(12s^-5+24s^-4+27/2.0 s⁻³+3s⁻²)+e^{-3s}(38s^-5+33s^-4+9/2.0 s⁻³-1/2.0 s⁻²-7/12.0 s⁻¹)+e^{-2s}(-32s^-5-32s^-4-14s⁻³-11/3.0 s⁻²-5/8.0 s⁻¹)+e^{-s}(-16s^-5)+16s^-5,5)->0.0466043->2:"m.UID"
     [a]-[1;2]--(e^{-8s}(1s^-4-1s⁻³)+e^{-7s}(-2s⁻³)+e^{-6s}(6s^-5-9s^-4-1s⁻³+2s⁻²)+e^{-5s}(-12s^-5+23s^-4+37/2.0 s⁻³+19/6.0 s⁻²)+e^{-4s}(6s^-5+9s^-4+17/2.0 s⁻³+19/6.0 s⁻²)+e^{-3s}(-32s^-4-10s⁻³-5/3.0 s⁻²-1/24.0 s⁻¹)+e^{-2s}(-16s^-5-8s^-4-2s⁻³-1/3.0 s⁻²-1/24.0 s⁻¹)+e^{-s}(16s^-5),5)->0.0016916->3:"m.UID"
     [b]-[0;2]--(e^{-8s}(-2s^-5+3s^-4-1s⁻³)+e^{-7s}(9s^-5-2s^-4-11/2.0 s⁻³)+e^{-6s}(-4s^-5-14s^-4-9/2.0 s⁻³)+e^{-5s}(-27s^-5-3s^-4+19/2.0 s⁻³+37/6.0 s⁻²)+e^{-4s}(38s^-5+57s^-4+71/2.0 s⁻³+32/3.0 s⁻²)+e^{-3s}(18s^-5-3s^-4-4s⁻³-7/6.0 s⁻²)+e^{-2s}(-48s^-5-40s^-4-16s⁻³-4s⁻²-2/3.0 s⁻¹)+16s^-5,5)->0.0483187->1:"m.UID"
    
  ((e^{-s(7+t)}(-1s^-5+1s^-4)+e^{-s(6+t)}(1s^-5+1s^-4)+e^{-s(5+t)}(7s^-5-2s^-4-7/2.0 s⁻³)+e^{-s(4+t)}(-18s^-5-21s^-4-8s⁻³)+e^{-s(3+t)}(-6s^-5+3s^-4+3/2.0 s⁻³)+e^{-s(2+t)}(32s^-5+24s^-4+8s⁻³+4/3.0 s⁻²)+e^{-st}(-16s^-5)+e^{-7s}(1s^-5-1s^-4)+e^{-6s}(-1s^-5-1s^-4+2ts^-4-3ts⁻³+1ts⁻²)+e^{-5s}(-7s^-5+2s^-4+7/2.0 s⁻³-7ts^-4+2ts⁻³+7/2.0 ts⁻²+1/2.0 t²s⁻³-1/2.0 t²s⁻²)+e^{-4s}(18s^-5+21s^-4+8s⁻³+4ts⁻³+7/2.0 ts⁻²-1/2.0 t²s⁻²)+e^{-3s}(6s^-5-3s^-4-3/2.0 s⁻³+12ts^-4-5/2.0 ts⁻²-7/6.0 ts⁻¹-2t²s⁻³+1/2.0 t²s⁻²+3/4.0 t²s⁻¹+1/6.0 t³s⁻²-1/6.0 t³s⁻¹)+e^{-2s}(-32s^-5-24s^-4-8s⁻³-4/3.0 s⁻²-8ts^-4-8ts⁻³-4ts⁻²-4/3.0 ts⁻¹+2t²s⁻³+2t²s⁻²+1t²s⁻¹-1/3.0 t³s⁻²-1/3.0 t³s⁻¹+1/24.0 t⁴s⁻¹)+16s^-5)/(e^{-8s}(-1s^-5+1s^-4)+e^{-7s}(2s^-5)+e^{-6s}(6s^-5-1s^-4-13/2.0 s⁻³+1s⁻²)+e^{-5s}(-25s^-5-26s^-4-2s⁻³+3s⁻²)+e^{-4s}(12s^-5+24s^-4+27/2.0 s⁻³+3s⁻²)+e^{-3s}(38s^-5+33s^-4+9/2.0 s⁻³-1/2.0 s⁻²-7/12.0 s⁻¹)+e^{-2s}(-32s^-5-32s^-4-14s⁻³-11/3.0 s⁻²-5/8.0 s⁻¹)+e^{-s}(-16s^-5)+16s^-5))->0.423270[a]
  
  id:2 m.UID y:0.000000  x:0.423270  
    *[a]-[0;0.57673]--(e^{-s(6-x)}(1s^-4-1s⁻³)+e^{-s(5-x)}(-5s^-4-5s⁻³+1s⁻²+1xs⁻³-1xs⁻²)+e^{-s(4-x)}(-1s^-4+1s⁻³+1s⁻²-1xs⁻²)+e^{-s(3-x)}(12s^-4+12s⁻³+1/2.0 s⁻²-1s⁻¹-4xs⁻³+1xs⁻²+3/2.0 xs⁻¹+1/2.0 x²s⁻²-1/2.0 x²s⁻¹)+e^{-s(2-x)}(-4s⁻³-3s⁻²-7/6.0 s⁻¹+4xs⁻³+4xs⁻²+2xs⁻¹-1x²s⁻²-1x²s⁻¹+1/6.0 x³s⁻¹)+e^{-s(1-x)}(-8s^-4)+e^{-5s}(-1s^-4+1s⁻³)+e^{-4s}(5s^-4+4s⁻³)+e^{-3s}(1s^-4-1s⁻³)+e^{-2s}(-12s^-4-8s⁻³-2s⁻²)+8s^-4,4)->0.0643101->2:"m.UID"
     [a]-[0.57673;1.57673]--(e^{-s(6-x)}(1s^-4-2s⁻³+1s⁻²)+e^{-s(5-x)}(-2s^-4+7s⁻³+5/2.0 s⁻²)+e^{-s(4-x)}(1s^-4+3s⁻³+5/2.0 s⁻²)+e^{-s(3-x)}(-12s⁻³-3s⁻²-1/6.0 s⁻¹)+e^{-s(2-x)}(-8s^-4-4s⁻³-1s⁻²-1/6.0 s⁻¹)+e^{-s(1-x)}(8s^-4),4)->0.0104251->3:"m.UID"
     [b]-[0;2]--(e^{-7s}(1s^-4-1s⁻³)+e^{-6s}(-1s^-4-1s⁻³)+e^{-5s}(-6s^-4+1s⁻³+7/2.0 s⁻²)+e^{-4s}(13s^-4+17s⁻³+8s⁻²)+e^{-3s}(5s^-4-2s⁻³-3/2.0 s⁻²)+e^{-2s}(-20s^-4-16s⁻³-6s⁻²-4/3.0 s⁻¹)+8s^-4,4)->0.0778317->1:"m.UID"
    
  ((e^{-s(5+t)}(1s^-4-1s⁻³)+e^{-s(5-x)}(-1ts⁻³+1ts⁻²)+e^{-s(4+t)}(-5s^-4-4s⁻³)+e^{-s(4-x)}(1ts⁻²)+e^{-s(3+t)}(-1s^-4+1s⁻³)+e^{-s(3-x)}(4ts⁻³-1ts⁻²-3/2.0 ts⁻¹-1txs⁻²+1txs⁻¹-1/2.0 t²s⁻²+1/2.0 t²s⁻¹)+e^{-s(2+t)}(12s^-4+8s⁻³+2s⁻²)+e^{-s(2-x)}(-4ts⁻³-4ts⁻²-2ts⁻¹+2txs⁻²+2txs⁻¹-1/2.0 tx²s⁻¹+1t²s⁻²+1t²s⁻¹-1/2.0 t²xs⁻¹-1/6.0 t³s⁻¹)+e^{-st}(-8s^-4)+e^{-5s}(-1s^-4+1s⁻³)+e^{-4s}(5s^-4+4s⁻³)+e^{-3s}(1s^-4-1s⁻³)+e^{-2s}(-12s^-4-8s⁻³-2s⁻²)+8s^-4)/(e^{-s(6-x)}(1s^-4-1s⁻³)+e^{-s(5-x)}(-5s^-4-5s⁻³+1s⁻²+1xs⁻³-1xs⁻²)+e^{-s(4-x)}(-1s^-4+1s⁻³+1s⁻²-1xs⁻²)+e^{-s(3-x)}(12s^-4+12s⁻³+1/2.0 s⁻²-1s⁻¹-4xs⁻³+1xs⁻²+3/2.0 xs⁻¹+1/2.0 x²s⁻²-1/2.0 x²s⁻¹)+e^{-s(2-x)}(-4s⁻³-3s⁻²-7/6.0 s⁻¹+4xs⁻³+4xs⁻²+2xs⁻¹-1x²s⁻²-1x²s⁻¹+1/6.0 x³s⁻¹)+e^{-s(1-x)}(-8s^-4)+e^{-5s}(-1s^-4+1s⁻³)+e^{-4s}(5s^-4+4s⁻³)+e^{-3s}(1s^-4-1s⁻³)+e^{-2s}(-12s^-4-8s⁻³-2s⁻²)+8s^-4))->0.101399[a]
  
  id:2 m.UID y:0.000000  x:0.524669  
     [a]-[0;0.475331]--(e^{-s(5-x)}(-1s⁻³)+e^{-s(3-x)}(4s⁻³+3s⁻²-1s⁻¹-1xs⁻²+1xs⁻¹)+e^{-s(2-x)}(-2s⁻²-3/2.0 s⁻¹+2xs⁻²+2xs⁻¹-1/2.0 x²s⁻¹)+e^{-s(1-x)}(-4s⁻³)+e^{-4s}(1s⁻³)+e^{-2s}(-4s⁻³-2s⁻²)+4s⁻³,3)->0.095953->2:"m.UID"
    *[a]-[0.475331;1.47533]--(e^{-s(5-x)}(1s⁻²)+e^{-s(4-x)}(1s⁻²)+e^{-s(3-x)}(-4s⁻²-1/2.0 s⁻¹)+e^{-s(2-x)}(-4s⁻³-2s⁻²-1/2.0 s⁻¹)+e^{-s(1-x)}(4s⁻³),3)->0.0236534->3:"m.UID"
     [b]-[0;2]--(e^{-5s}(-1s⁻³+1s⁻²)+e^{-4s}(4s⁻³+4s⁻²)+e^{-3s}(1s⁻³-1s⁻²)+e^{-2s}(-8s⁻³-6s⁻²-2s⁻¹)+4s⁻³,3)->0.124965->1:"m.UID"
    
  ((e^{-s(5-x)}(-1s⁻²+1xs⁻²+1ts⁻²)+e^{-s(3-x)}(3/2.0 s⁻¹-2xs⁻²-2xs⁻¹+1/2.0 x²s⁻¹-2ts⁻²-2ts⁻¹+1txs⁻¹+1/2.0 t²s⁻¹)+e^{-s(2+t)}(3s⁻²-1xs⁻²-1ts⁻²)+e^{-s(2-x)}(2s⁻²+3/2.0 s⁻¹-2xs⁻²-2xs⁻¹+1/2.0 x²s⁻¹-2ts⁻²-2ts⁻¹+1txs⁻¹+1/2.0 t²s⁻¹)+e^{-s(1-x)}(4s⁻³)+e^{-st}(-4s⁻³))/(e^{-s(5-x)}(1s⁻²)+e^{-s(4-x)}(1s⁻²)+e^{-s(3-x)}(-4s⁻²-1/2.0 s⁻¹)+e^{-s(2-x)}(-4s⁻³-2s⁻²-1/2.0 s⁻¹)+e^{-s(1-x)}(4s⁻³)))->0.792522[a]
  
  id:3 m.UID y:0.000000  x:1.317191  
     [a]-[0;0.682809]--(e^{-s(3-x)}(-2s⁻¹+1xs⁻¹)+e^{-s(2-x)}(-2s⁻²-2s⁻¹+1xs⁻¹)+2s⁻²,2)->0.151636->3:"m.UID"
    *[b]-[0;1.68281]--(e^{-s(5-x)}(1s⁻²)+e^{-s(3-x)}(-2s⁻²)+e^{-2s}(-1s⁻²-3s⁻¹+1xs⁻¹)+2s⁻²,2)->0.199113->1:"m.UID"
    
  ((e^{-s(2+t)}(1s⁻²)+e^{-st}(-2s⁻²)+e^{-2s}(-1s⁻²-1ts⁻¹)+2s⁻²)/(e^{-s(5-x)}(1s⁻²)+e^{-s(3-x)}(-2s⁻²)+e^{-2s}(-1s⁻²-3s⁻¹+1xs⁻¹)+2s⁻²))->0.274855[b]
  
  id:1 m.UID y:0.274855  x:0.000000  
     [a]-[0;1]--(e^{-s}(-1s⁻¹)+1s⁻¹,1)->0.303668->2:"m.UID"
     [a]-[1;2]--(e^{-2s}(-1s⁻¹)+e^{-s}(1s⁻¹),1)->0.0129833->3:"m.UID"
    *[b]-[0;1.72514]--(e^{-s(2-y)}(-1s⁻¹)+1s⁻¹,1)->0.315852->1:"m.UID"
    
  ((e^{-st}(-1s⁻¹)+1s⁻¹)/(e^{-s(2-y)}(-1s⁻¹)+1s⁻¹))->0.086485[b]
  


Test boltzmann

  $ wordgen twoears.prism --seed 42 --boltzmann 0.5 --exact-rational --traj 0 --poly 5 | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Precomputation file found but file have change discard !
  Reading Prism automaton file. 
  Computing forward reachability graph ... 3 states found, automaton is deterministic. 
  Splitting reachability graph ... 4 states and 15 DBMs found. 
  Computing Distribution[exact rational; Poly; no s; clocks:{y; x; }; vars:{t; z; s; T; }] -> 5: [|||||] 
  Volume in initial state:1+4z+23/2.0 z²+95/3.0 z³+2093/24.0 z⁴+14417/60.0 z⁵, degree of liberty:1
  Saving Precomputation 

  $ wordgen twoears.prism --seed 42 --output-format timeline --boltzmann 0.5 --exact-rational --traj 1 --poly 5 | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Precomputation file found ! [0.00]
  Reading Distribution[exact rational; Poly; no s; clocks:{y; x; }; vars:{t; z; s; T; }] -> 5
  Using rejection sampling to solve non-determinism !
  0.000000 y x  a b  id:0 m.UID y:0.000000  x:0.000000  
  0.000000 0.000000 0.000000 0 0  m.UID " "
  1.422860 1.422860 1.422860 0 0  m.UID " "
  1.422860 1.422860 0.000000 0 1  m.UID "b"
  1.422860 1.422860 0.000000 0 0  m.UID " "
  1.719520 1.719520 0.296661 0 0  m.UID " "
  1.719520 0.000000 0.296661 1 0  m.UID "a"
  1.719520 0.000000 0.296661 0 0  m.UID " "
  3.035955 1.316435 1.613096 0 0  m.UID " "
  3.035955 0.000000 1.613096 1 0  m.UID "a"
  3.035955 0.000000 1.613096 0 0  m.UID " "
  3.769040 0.733085 2.346181 0 0  m.UID " "
  3.769040 0.733085 0.000000 0 1  m.UID "b"
  3.769040 0.733085 0.000000 0 0  m.UID " "
  4.953811 1.917856 1.184770 0 0  m.UID " "
  4.953811 0.000000 1.184770 1 0  m.UID "a"
  4.953811 0.000000 1.184770 0 0  m.UID " "
  5.097614 0.143804 1.328574 0 0  m.UID " "
  5.097614 0.000000 1.328574 1 0  m.UID "a"
  5.097614 0.000000 1.328574 0 0  m.UID " "
  5.191579 0.093964 1.422538 0 0  m.UID " "
  5.191579 0.000000 1.422538 1 0  m.UID "a"
  5.191579 0.000000 1.422538 0 0  m.UID " "
  5.489371 0.297792 1.720330 0 0  m.UID " "
  5.489371 0.000000 1.720330 1 0  m.UID "a"
  5.489371 0.000000 1.720330 0 0  m.UID " "
  5.539452 0.050082 1.770412 0 0  m.UID " "
  5.539452 0.050082 0.000000 0 1  m.UID "b"
  5.539452 0.050082 0.000000 0 0  m.UID " "
  5.827758 0.338387 0.288306 0 0  m.UID " "
  5.827758 0.000000 0.288306 1 0  m.UID "a"
  5.827758 0.000000 0.288306 0 0  m.UID " "
  6.243094 0.415336 0.703642 0 0  m.UID " "
  6.243094 0.000000 0.703642 1 0  m.UID "a"
  6.243094 0.000000 0.703642 0 0  m.UID " "
  7.224995 0.981901 1.685542 0 0  m.UID " "
  7.224995 0.981901 0.000000 0 1  m.UID "b"
  7.224995 0.981901 0.000000 0 0  m.UID " "
  9.163893 2.920799 1.938899 0 0  m.UID " "
  9.163893 0.000000 1.938899 1 0  m.UID "a"
  9.163893 0.000000 1.938899 0 0  m.UID " "
  9.959674 0.795781 2.734679 0 0  m.UID " "
  9.959674 0.795781 0.000000 0 1  m.UID "b"
  9.959674 0.795781 0.000000 0 0  m.UID " "
  10.242895 1.079002 0.283221 0 0  m.UID " "
  10.242895 0.000000 0.283221 1 0  m.UID "a"
  10.242895 0.000000 0.283221 0 0  m.UID " "
  10.304791 0.061895 0.345117 0 0  m.UID " "
  10.304791 0.000000 0.345117 1 0  m.UID "a"
  10.304791 0.000000 0.345117 0 0  m.UID " "
  11.522787 1.217996 1.563113 0 0  m.UID " "
  11.522787 1.217996 0.000000 0 1  m.UID "b"
  11.522787 1.217996 0.000000 0 0  m.UID " "
  11.664761 1.359970 0.141974 0 0  m.UID " "
  11.664761 0.000000 0.141974 1 0  m.UID "a"
  11.664761 0.000000 0.141974 0 0  m.UID " "
  12.127381 0.462621 0.604594 0 0  m.UID " "
  12.127381 0.000000 0.604594 1 0  m.UID "a"
  12.127381 0.000000 0.604594 0 0  m.UID " "
  13.242810 1.115429 1.720023 0 0  m.UID " "
  13.242810 0.000000 1.720023 1 0  m.UID "a"
  13.242810 0.000000 1.720023 0 0  m.UID " "
  13.242810 0.000000 1.720023 0 0  m.UID " "
  

Test expected size

  $ wordgen twoears.prism --seed 56864 --expected-size 10 -v 5 --traj 4 --poly 3 | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  parameters : wordgen --traj 4 --seed 56864 -v 5 --expected-size 10
  Precomputation file found but file have change discard !
  Reading Prism automaton file. 
  Computing forward reachability graph ... 3 states found, automaton is deterministic. 
  Splitting reachability graph ... 4 states and 15 DBMs found. 
  Computing Distribution[floating point; Poly; no s; clocks:{y; x; }; vars:{t; z; s; T; }] -> 3: [|||] 
  Volume in initial state:1+4z+11.5z²+31.6666666667z³, degree of liberty:1
  Saving Precomputation 
  Computing boltzmann parameter z for E[N]=10 |100->33.67 samples with z:1 s:? -> size:33.67 duration:25.3145
  |100->6.30 samples with z:0.5 s:? -> size:6.3 duration:4.97232
  |100->17.54 samples with z:0.75 s:? -> size:17.54 duration:13.3664
  |100->9.03 samples with z:0.575 s:? -> size:9.03 duration:6.9911
  |100->13.81 samples with z:0.6975 s:? -> size:13.81 duration:10.6632
  |223->10.39 samples with z:0.61175 s:? -> size:10.3901 duration:7.93291
  |100->8.70 samples with z:0.551725 s:? -> size:8.7 duration:6.74857
  |115->9.39 samples with z:0.593743 s:? -> size:9.3913 duration:7.27296
  |122->10.73 samples with z:0.623155 s:? -> size:10.7295 duration:8.31501
  |115->9.39 samples with z:0.602566 s:? -> size:9.3913 duration:7.25446
  |131->10.66 samples with z:0.616978 s:? -> size:10.6565 duration:8.21145
  |155->9.56 samples with z:0.60689 s:? -> size:9.56129 duration:7.40526
  |172->10.33 samples with z:0.613952 s:? -> size:10.3256 duration:7.97522
  |172->9.67 samples with z:0.609008 s:? -> size:9.67442 duration:7.49853
  |223->10.39 samples with z:0.612469 s:? -> size:10.3901 duration:7.93213
  |223->10.39 samples with z:0.610046 s:? -> size:10.3901 duration:7.93144
  |172->9.67 samples with z:0.608351 s:? -> size:9.67442 duration:7.49866
  |288->9.82 samples with z:0.609538 s:? -> size:9.81944 duration:7.55972
  |223->10.39 samples with z:0.610369 s:? -> size:10.3901 duration:7.93343
  |223->10.39 samples with z:0.609787 s:? -> size:10.3901 duration:7.93388
  |172->9.67 samples with z:0.60938 s:? -> size:9.67442 duration:7.49845
   -> z=0.609266;E[N]=9.67442  
  1.446241[b] 0.084585[b] 0.327285[b] 0.742413[a] 0.637588[b] 0.874912[b] 0.635826[a] 0.071371[b] 0.325112[a] 0.735821[a] 1.923296[b] 0.026404[b] 
  0.291630[a] 0.340693[b] 1.542302[b] 1.192910[a] 0.357887[a] 
  0.818323[b] 1.145227[b] 0.623756[a] 0.278987[a] 1.645998[b] 1.419694[a] 1.500529[b] 0.419847[a] 0.166856[b] 1.319287[a] 0.926736[b] 0.069351[a] 1.930857[b] 1.215624[a] 0.924742[b] 0.891950[a] 0.172390[b] 0.466733[b] 0.534762[a] 0.595471[a] 1.455687[b] 0.092502[a] 0.149877[b] 1.538578[a] 0.596470[b] 0.551620[b] 0.396479[b] 0.813332[a] 1.187690[b] 0.603320[b] 0.136994[a] 1.044024[b] 
  0.072462[a] 0.698188[a] 0.792833[a] 1.066742[b] 0.069626[b] 1.243882[a] 


  $ wordgen noclock.prism --seed 42 -v 5 --output-format word --debug --boltzmann 0.48 --receding 10 | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  parameters : wordgen --debug --receding 10 --seed 42 --output-format word -v 5 --boltzmann 0.48
  Reading Prism automaton file.module m
  	state: [0..1] init 0;
  	[c] (  state = 0  ) -> 1 :(state'=1);
  	[c] (  state = 1  ) -> 1 :(state'=0);
  	[b] (  state = 1  ) -> 1 :(state'=1);
  	[a] (  state = 0  ) -> 1 :(state'=0);
  endmodule
  label "accepting" = (state = 0)
   
  Timed automaton with 1 clocks and 1 processes
  Process(0): m
  Location 0: m.UID 
  Has 4 edges:
  m.UID -> m.UID 	DiscGuard: m.state == 0 	DiscUpdate: m.state = 0 	Guard:  	Resets: 	Sync:a!	Prob:1
  m.UID -> m.UID 	DiscGuard: m.state == 1 	DiscUpdate: m.state = 1 	Guard:  	Resets: 	Sync:b!	Prob:1
  m.UID -> m.UID 	DiscGuard: m.state == 1 	DiscUpdate: m.state = 0 	Guard:  	Resets: 	Sync:c!	Prob:1
  m.UID -> m.UID 	DiscGuard: m.state == 0 	DiscUpdate: m.state = 1 	Guard:  	Resets: 	Sync:c!	Prob:1
  accepting:m.state == 0
  Initial location id: 0
  
  -----
  Computing forward reachability graph ... 2 states found, automaton is deterministic. 
  Forward Reach : 
  LocZone 0: m.UID  m.state:0  true -> { ([true],0-[a]->0), ([true],0-[c]->1),  }
  LocZone 1: m.UID  m.state:1  true -> { ([true],1-[b]->1), ([true],1-[c]->0),  }
  
  Splitting reachability graph ... 2 states and 1 DBMs found. 
  After split : 
  LocZone 0: m.UID  m.state:0  true -> { ([true],0-[a]->0), ([true],0-[c]->1),  }
  LocZone 1: m.UID  m.state:1  true -> { ([true],1-[b]->1), ([true],1-[c]->0),  }
  
  {
     "init":0,
     "cardclocks":0,
     "statelist":[
     {
        "id":0,
        "disc_name":"m.UID  m.state:0",
        "init_zone":"",
        "is_accepting":true,
        "redcoord":[[0]],
        "clockmap":[0 ],
        "weight":,
        "transitions":[
                         {"action":"a", "zone":[0],  "is_contained_in_zone": false, "target":0, "reset": []  },
                         {"action":"c", "zone":[0],  "is_contained_in_zone": false, "target":1, "reset": []  }]
       }
       ,{
           "id":1,
           "disc_name":"m.UID  m.state:1",
           "init_zone":"",
           "is_accepting":false,
           "redcoord":[[0]],
           "clockmap":[0 ],
           "weight":,
           "transitions":[
                            {"action":"b", "zone":[0],  "is_contained_in_zone": false, "target":1, "reset": []  },
                            {"action":"c", "zone":[0],  "is_contained_in_zone": false, "target":0, "reset": []  }]
       }
       
     ]}
  Computing Distribution[floating point; RationalFraction; clocks:{}; vars:{t; z; s; T; }] -> 3: [] 
  {
     "init":0,
     "cardclocks":0,
     "statelist":[
     {
        "id":0,
        "disc_name":"m.UID  m.state:0",
        "init_zone":"",
        "is_accepting":true,
        "redcoord":[[0]],
        "clockmap":[0 ],
        "weight":(1.0-1.0*z)/(1.0-2.0*z),
        "transitions":[
                         {"action":"a", "zone":[0],  "is_contained_in_zone": false, "target":0, "reset": [] , "weight":(1-z)/(1-2z), "degree_of_liberty":-1, "cdf":0, "pdf":0,  },
                         {"action":"c", "zone":[0],  "is_contained_in_zone": false, "target":1, "reset": [] , "weight":(-z)/(-1+2z), "degree_of_liberty":-1, "cdf":0, "pdf":0,  }]
       }
       ,{
           "id":1,
           "disc_name":"m.UID  m.state:1",
           "init_zone":"",
           "is_accepting":false,
           "redcoord":[[0]],
           "clockmap":[0 ],
           "weight":(-1.0*z)/(-1.0+2.0*z),
           "transitions":[
                            {"action":"b", "zone":[0],  "is_contained_in_zone": false, "target":1, "reset": [] , "weight":(-z)/(-1+2z), "degree_of_liberty":-1, "cdf":0, "pdf":0,  },
                            {"action":"c", "zone":[0],  "is_contained_in_zone": false, "target":0, "reset": [] , "weight":(1-z)/(1-2z), "degree_of_liberty":-1, "cdf":0, "pdf":0,  }]
       }
       
     ]}
  Volume in initial state:(1-z)/(1-2z), degree of liberty:-1
  Saving Precomputation 
  {
     "init":0,
     "cardclocks":0,
     "statelist":[
     {
        "id":0,
        "disc_name":"m.UID  m.state:0",
        "init_zone":"",
        "is_accepting":true,
        "redcoord":[[0]],
        "clockmap":[0 ],
        "weight":13.,
        "transitions":[
                         {"action":"a", "zone":[0],  "is_contained_in_zone": false, "target":0, "reset": [] , "weight":13., "degree_of_liberty":-1, "cdf":0, "pdf":0,  },
                         {"action":"c", "zone":[0],  "is_contained_in_zone": false, "target":1, "reset": [] , "weight":12., "degree_of_liberty":-1, "cdf":0, "pdf":0,  }]
       }
       ,{
           "id":1,
           "disc_name":"m.UID  m.state:1",
           "init_zone":"",
           "is_accepting":false,
           "redcoord":[[0]],
           "clockmap":[0 ],
           "weight":12.,
           "transitions":[
                            {"action":"b", "zone":[0],  "is_contained_in_zone": false, "target":1, "reset": [] , "weight":12., "degree_of_liberty":-1, "cdf":0, "pdf":0,  },
                            {"action":"c", "zone":[0],  "is_contained_in_zone": false, "target":0, "reset": [] , "weight":13., "degree_of_liberty":-1, "cdf":0, "pdf":0,  }]
       }
       
     ]}
  aaaaccccaaaacbbbcaaa
  
  ccacbbcaacbbccbca
  
  a
  cbbca
  ccaacbcaccacbbbbccccbbbccbcccccccaa
  ccaaacbccbbbccbbbbbcacbbcaacbbcaaaccaccaaaccaacca
  accccaccaaaacbccbcaaaaccaaacccbccbbbcaacbc
  aaacbbccccbbccc
	state: [0..1] init 0;
	[c] (  state = 0  ) -> 1 :(state'=1);
	[c] (  state = 1  ) -> 1 :(state'=0);
	[b] (  state = 1  ) -> 1 :(state'=1);
	[a] (  state = 0  ) -> 1 :(state'=0);
	"id":0,
	"name":"m.UID  m.state = 0,  true",
	"is_accepting":true,
	"redcoord":[[0]],
	"clockmap":[],
	"weight":,
	"transitions":[
			 {"action":"a", "zone":[0 ; 0],  "is_contained_in_zone": false, "target":0, "reset": []  },
			 {"action":"c", "zone":[0 ; 0],  "is_contained_in_zone": false, "target":1, "reset": []  }]
	   "id":1,
	   "name":"m.UID  m.state = 1,  true",
	   "is_accepting":false,
	   "redcoord":[[0]],
	   "clockmap":[],
	   "weight":,
	   "transitions":[
			    {"action":"b", "zone":[0 ; 0],  "is_contained_in_zone": false, "target":1, "reset": []  },
			    {"action":"c", "zone":[0 ; 0],  "is_contained_in_zone": false, "target":0, "reset": []  }]
	"id":0,
	"name":"m.UID  m.state = 0,  true",
	"is_accepting":true,
	"redcoord":[[0]],
	"clockmap":[0; ],
	"weight":(1-z)/(1-2z),
	"transitions":[
			 {"action":"a", "zone":[0 ; 0],  "is_contained_in_zone": false, "target":0, "reset": [] , "weight":(1-z)/(1-2z), "cdf":0, "pdf":0,  },
			 {"action":"c", "zone":[0 ; 0],  "is_contained_in_zone": false, "target":1, "reset": [] , "weight":(-z)/(-1+2z), "cdf":0, "pdf":0,  }]
	   "id":1,
	   "name":"m.UID  m.state = 1,  true",
	   "is_accepting":false,
	   "redcoord":[[0]],
	   "clockmap":[0; ],
	   "weight":(-z)/(-1+2z),
	   "transitions":[
			    {"action":"b", "zone":[0 ; 0],  "is_contained_in_zone": false, "target":1, "reset": [] , "weight":(-z)/(-1+2z), "cdf":0, "pdf":0,  },
			    {"action":"c", "zone":[0 ; 0],  "is_contained_in_zone": false, "target":0, "reset": [] , "weight":(1-z)/(1-2z), "cdf":0, "pdf":0,  }]
	"id":0,
	"name":"m.UID  m.state = 0,  true",
	"is_accepting":true,
	"redcoord":[[0]],
	"clockmap":[0; ],
	"weight":13.,
	"transitions":[
			 {"action":"a", "zone":[0 ; 0],  "is_contained_in_zone": false, "target":0, "reset": [] , "weight":13., "cdf":0, "pdf":0,  },
			 {"action":"c", "zone":[0 ; 0],  "is_contained_in_zone": false, "target":1, "reset": [] , "weight":12., "cdf":0, "pdf":0,  }]
	   "id":1,
	   "name":"m.UID  m.state = 1,  true",
	   "is_accepting":false,
	   "redcoord":[[0]],
	   "clockmap":[0; ],
	   "weight":12.,
	   "transitions":[
			    {"action":"b", "zone":[0 ; 0],  "is_contained_in_zone": false, "target":1, "reset": [] , "weight":12., "cdf":0, "pdf":0,  },
			    {"action":"c", "zone":[0 ; 0],  "is_contained_in_zone": false, "target":0, "reset": [] , "weight":13., "cdf":0, "pdf":0,  }]

  $ wordgen noclock.prism --seed 42 --output-format word -v 5 --expected-size 20 --exact-rational --receding 10 | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  parameters : wordgen --receding 10 --seed 42 --output-format word -v 5 --exact-rational --expected-size 20
  Precomputation file found but file have change discard !
  Reading Prism automaton file. 
  Computing forward reachability graph ... 2 states found, automaton is deterministic. 
  Splitting reachability graph ... 2 states and 1 DBMs found. 
  Computing Distribution[exact rational; RationalFraction; clocks:{}; vars:{t; z; s; T; }] -> 3: [] 
  Volume in initial state:(1-1z)/(1-2z), degree of liberty:-1
  Saving Precomputation 
  Computing boltzmann parameter z for E[N]=20 
  Generating function : (-2z)/(-2+6z-4z²)
  Size length for z: 0.25 -> (14.5)
  Size length for z: 0.426829 -> (2.50149)
  Size length for z: 0.473406 -> (0.173548)
  Size length for z: 0.477158 -> (0.00112619)
  Size length for z: 0.477182 -> (4.86785e-08)
  Size length for z: 0.477182 -> (-7.61667e-16)
  radius of convergence:  0.5 -> z=0.477182;E[N]=20  
  aaaaccccaaaacbbbcaaa
  
  ccacbbcaacbbccbca
  
  a
  cbbca
  ccaacbcaccacbbbbccccbbbccbcccccccaa
  ccaaacbccbbbcaaaaaacbc
  aaaaccacbccbbccbbbcacbccbccbcccaaccaaaacbccbcaaaaccaaacccbccbbbcaacbc
  aaacbbccccbbccc


  $ wordgen --regexp "(ab(c*)|ba)*" --seed 5642 --expected-size 100 --output-format word | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Computing forward reachability graph ... 6 states found, automaton is deterministic. 
  Splitting reachability graph ... 6 states and 1 DBMs found. 
  Computing Distribution[floating point; RationalFraction; clocks:{}; vars:{t; z; s; T; }] -> 3: [] 
  Volume in initial state:(-1+z)/(-1+z+z²+z⁴-z⁵), degree of liberty:-1
  Computing boltzmann parameter z for E[N]=100 radius of convergence:  0.595089 -> z=0.589294;E[N]=100  
  abcccabcccabccabccabccabcccabccccccccabbaabcccabbaabababc
  ababcabcccababcabccccabcabcabbaababccababcabababababcabcabcabababcabcccccabababbaabccababccababccccababcabccabccabcccc
  ababcabcccabccababbaabcccccabcabcababccabbaabbaabbaababcccabababbaabcababccabababcabbaababccccababccabababccab
  
  ababbaabcababccabbaababccababcabcccabcababababccabcabcabbaabcccccabcc
  ababcc
  ababcabcababcccababcabccabccccabcababccabccabccabcabababcccabababcccccabcabcabccababababcccabababccabccabbaababbaabc
  abababcabcababcccababababcabcccccabcababccccccabababcabcccababcabcabcabbaabccabababcccabccccabcccabcccabccabcabcab
  abbaabccccc
  abccabc

Test Uppaal file format

  $ wordgen testCompo.xml --seed 8513 --output-format json | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Reading UPPAAL automaton file. 
  Computing forward reachability graph ... 3 states found, automaton is deterministic. 
  Splitting reachability graph ... 3 states and 6 DBMs found. 
  Computing Distribution[floating point; Poly; no s; clocks:{y; x; }; vars:{t; z; s; T; }] -> 3: [|||] 
  Volume in initial state:½, degree of liberty:3
  Saving Precomputation 
  [[{ "state":"0  0  0  ",
     "action":"b",
     "delay":0.098949},
     { "state":"2  0  0.0989494  ",
       "action":"a",
       "delay":0.257340},
     { "state":"1  0.25734  0  ",
       "action":"b",
       "delay":0.413164},
     {"final":true}],
  
  [{ "state":"0  0  0  ",
     "action":"b",
     "delay":0.036758},
     { "state":"2  0  0.0367577  ",
       "action":"a",
       "delay":0.537755},
     { "state":"1  0.537755  0  ",
       "action":"b",
       "delay":0.874794},
     {"final":true}],
  
  [{ "state":"0  0  0  ",
     "action":"b",
     "delay":0.777205},
     { "state":"2  0  0.777205  ",
       "action":"a",
       "delay":0.016722},
     { "state":"1  0.0167215  0  ",
       "action":"b",
       "delay":0.209580},
     {"final":true}],
  
  [{ "state":"0  0  0  ",
     "action":"b",
     "delay":0.618468},
     { "state":"2  0  0.618468  ",
       "action":"a",
       "delay":0.044596},
     { "state":"1  0.0445956  0  ",
       "action":"b",
       "delay":0.528411},
     {"final":true}],
  
  [{ "state":"0  0  0  ",
     "action":"b",
     "delay":0.169711},
     { "state":"2  0  0.169711  ",
       "action":"a",
       "delay":0.460396},
     { "state":"1  0.460396  0  ",
       "action":"b",
       "delay":0.119619},
     {"final":true}],
  
  [{ "state":"0  0  0  ",
     "action":"b",
     "delay":0.172910},
     { "state":"2  0  0.17291  ",
       "action":"a",
       "delay":0.761236},
     { "state":"1  0.761236  0  ",
       "action":"b",
       "delay":0.026365},
     {"final":true}],
  
  [{ "state":"0  0  0  ",
     "action":"b",
     "delay":0.195095},
     { "state":"2  0  0.195095  ",
       "action":"a",
       "delay":0.486398},
     { "state":"1  0.486398  0  ",
       "action":"b",
       "delay":0.890589},
     {"final":true}],
  
  [{ "state":"0  0  0  ",
     "action":"b",
     "delay":0.495132},
     { "state":"2  0  0.495132  ",
       "action":"a",
       "delay":0.498620},
     { "state":"1  0.49862  0  ",
       "action":"b",
       "delay":0.153221},
     {"final":true}],
  
  [{ "state":"0  0  0  ",
     "action":"b",
     "delay":0.399048},
     { "state":"2  0  0.399048  ",
       "action":"a",
       "delay":0.265481},
     { "state":"1  0.265481  0  ",
       "action":"b",
       "delay":0.189345},
     {"final":true}],
  
  [{ "state":"0  0  0  ",
     "action":"b",
     "delay":0.289245},
     { "state":"2  0  0.289245  ",
       "action":"a",
       "delay":0.600551},
     { "state":"1  0.600551  0  ",
       "action":"b",
       "delay":0.659881},
     {"final":true}],
  
  []]

  $ echo "@0.5[a]\n_[t]\n>0.1\n>0.2\n>1.0\nexit" | wordgen testSync2.xml --seed 8513 --output-format cosim --interactive | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Reading UPPAAL automaton file. 
  Computing forward reachability graph ... 3 states found, automaton is deterministic. 
  Splitting reachability graph ... 3 states and 7 DBMs found. 
  Computing Distribution[floating point; Poly; no s; clocks:{z; y; }; vars:{t; z; s; T; }] -> 3: [|||] 
  Volume in initial state:2.83333333333, degree of liberty:3
  Saving Precomputation 
  {"state":"Process.id0 Process2.id1", "clocks":[ 0.000000, 0.000000], "current_time": 0, "constrained_delay": null,
   "transition":[{"label":"a", "weight":1.66667, "is_selected":true, "min_time": 0, "max_time":1, "time":0.132247, "relative_time":0.132247, "target":"Process.id0 Process2.id1"},
                 {"label":"t", "weight":1.16667, "is_selected":false, "min_time": 0, "max_time":1, "time":0.0989494, "relative_time":0.0989494, "target":"Process.id0 Process2.id1"}]}
  {"state":"Process.id0 Process2.id1", "clocks":[ 0.500000, 0.000000], "current_time": 0.5, "constrained_delay": null,
   "transition":[{"label":"a", "weight":0.625, "is_selected":true, "min_time": 0.5, "max_time":1, "time":0.619974, "relative_time":0.119974, "target":"Process.id0 Process2.id1"},
                 {"label":"t", "weight":1, "is_selected":false, "min_time": 0.5, "max_time":1.5, "time":0.654778, "relative_time":0.154778, "target":"Process.id0 Process2.id1"}]}
  {"state":"Process.id0 Process2.id1", "clocks":[ 0.000000, 0.154778], "current_time": 0.654778, "constrained_delay": null,
   "transition":[{"label":"a", "weight":0.845222, "is_selected":false, "min_time": 0.654778, "max_time":1.5, "time":0.931849, "relative_time":0.277071, "target":"Process.id0 Process2.id1"},
                 {"label":"t", "weight":0.845222, "is_selected":true, "min_time": 0.654778, "max_time":1.5, "time":0.852516, "relative_time":0.197738, "target":"Process.id0 Process2.id1"}]}
  {"state":"Process.id0 Process2.id1", "clocks":[ 0.000000, 0.154778], "current_time": 0.654778, "constrained_delay": 0.1,
   "transition":[{"label":"a", "weight":0.745222, "is_selected":false, "min_time": 0.654778, "max_time":1.5, "time":1.00518, "relative_time":0.350401, "target":"Process.id0 Process2.id1"},
                 {"label":"t", "weight":0.745222, "is_selected":true, "min_time": 0.654778, "max_time":1.5, "time":0.929121, "relative_time":0.274343, "target":"Process.id0 Process2.id1"}]}
  {"state":"Process.id0 Process2.id1", "clocks":[ 0.000000, 0.154778], "current_time": 0.654778, "constrained_delay": 0.3,
   "transition":[{"label":"a", "weight":0.545222, "is_selected":false, "min_time": 0.654778, "max_time":1.5, "time":1.14777, "relative_time":0.49299, "target":"Process.id0 Process2.id1"},
                 {"label":"t", "weight":0.545222, "is_selected":true, "min_time": 0.654778, "max_time":1.5, "time":1.08233, "relative_time":0.427553, "target":"Process.id0 Process2.id1"}]}
  {"state":"Process.id0 Process2.id1", "clocks":[ 0.000000, 0.154778], "current_time": 0.654778, "constrained_delay": 1.3, "transition":[]}

  $ wordgen traingate.xml --seed 8513 --poly 7 --receding 10 --seed 85163 --expected-duration 20 | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Reading UPPAAL automaton file. 
  Computing forward reachability graph ... 57 states found, automaton is deterministic. 
  Splitting reachability graph ... 79 states and 212 DBMs found. 
  Computing Distribution[floating point; ExpPoly; s formal; clocks:{x; x; manage; }; vars:{t; z; s; T; }] -> 7: [|||||||] 
  Volume in initial state:e^{-50s}(12s^-7+20s^-6)+e^{-48s}(-20s^-7-20s^-6)+e^{-46s}(8s^-7)+e^{-45s}(-4s^-7-40s^-6-600s^-5)+e^{-43s}(4s^-7+40s^-6+600s^-5)+e^{-40s}(-28s^-7-100s^-6-650s^-5)+e^{-38s}(44s^-7+92s^-6+618s^-5)+e^{-36s}(-16s^-7)+e^{-35s}(8s^-7+80s^-6+800s^-5)+e^{-33s}(-8s^-7-80s^-6-800s^-5)+e^{-32s}(2s^-7+30s^-6+325s^-5)+e^{-30s}(14s^-7+54s^-6-59s^-5)+e^{-28s}(-24s^-7-72s^-6-218s^-5)+e^{-26s}(8s^-7)+e^{-25s}(-2s^-7-10s^-6+125s^-5)+e^{-23s}(2s^-7+14s^-6-109s^-5)+e^{-22s}(-2s^-7-30s^-6-125s^-5)+e^{-20s}(2s^-7+26s^-6+109s^-5)+e^{-15s}(-2s^-7-30s^-6-125s^-5)+e^{-13s}(2s^-7+26s^-6+109s^-5), degree of liberty:7
  Saving Precomputation 
  Computing laplace parameter s for E[T]=14 range:[0; inf; inf] -> s=5.046;E[T]=14  
  0.021295[appr[1]] 10.210659[t] 1.035315[appr[0]] 0.600477[stop[0]] 1.369530[leave[1]] 0.060719[go[0]] 7.396417[t] 1.765230[appr[1]] 1.090800[stop[1]] 0.261835[leave[0]] 
  1.118484[appr[1]] 6.223547[appr[0]] 3.262990[stop[0]] 0.599378[t] 3.506910[leave[1]] 0.017124[go[0]] 0.759775[appr[1]] 5.723754[stop[1]] 0.693815[t] 3.406870[leave[0]] 
  0.182190[appr[1]] 2.192378[appr[0]] 3.595490[stop[0]] 4.382702[t] 3.049838[leave[1]] 0.078092[go[0]] 2.727105[appr[1]] 0.973900[stop[1]] 3.800223[t] 3.171437[leave[0]] 
  0.171565[appr[1]] 7.462889[appr[0]] 0.969300[stop[0]] 1.701130[t] 3.151574[leave[1]] 0.265162[go[0]] 1.483339[appr[1]] 5.986003[stop[1]] 0.046722[t] 3.078240[leave[0]] 
  0.190007[appr[1]] 2.940102[appr[0]] 1.094152[stop[0]] 5.975118[t] 3.187893[leave[1]] 0.313288[go[0]] 4.847658[appr[1]] 0.685373[stop[1]] 1.484466[t] 3.058356[leave[0]] 
  0.004446[appr[0]] 7.307558[appr[1]] 2.763829[stop[1]] 0.075107[t] 3.009822[leave[0]] 0.468554[go[1]] 5.180616[appr[0]] 1.628518[stop[0]] 0.499360[t] 3.380473[leave[1]] 
  0.117631[appr[0]] 3.598797[appr[1]] 3.485091[stop[1]] 3.391735[t] 3.217411[leave[0]] 0.767789[go[1]] 2.711895[appr[0]] 2.449410[stop[0]] 1.882011[t] 3.040861[leave[1]] 
  0.017635[appr[1]] 1.559392[appr[0]] 0.325467[stop[0]] 8.205785[t] 3.159054[leave[1]] 0.055715[go[0]] 1.752758[appr[1]] 5.577670[stop[1]] 0.180197[t] 3.227901[leave[0]] 
  0.081347[appr[0]] 6.280322[appr[1]] 0.715846[stop[1]] 3.083809[t] 3.517501[leave[0]] 0.650524[go[1]] 0.563875[appr[0]] 0.953462[stop[0]] 5.562443[t] 3.178251[leave[1]] 
  0.822394[appr[1]] 3.571280[appr[0]] 4.864364[stop[0]] 1.571352[t] 3.068991[leave[1]] 0.424487[go[0]] 2.084832[appr[1]] 3.627379[stop[1]] 1.315386[t] 3.013670[leave[0]] 

Test Isotropic
  $ wordgen tunnel.prism --receding 100 --traj 1 --seed 8513 | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Reading Prism automaton file. 
  Computing forward reachability graph ... 9 states found, automaton is deterministic. 
  Splitting reachability graph ... 45 states and 58 DBMs found. 
  Computing Distribution[floating point; Poly; no s; clocks:{x; y; }; vars:{t; z; s; T; }] -> 3: [|||] 
  Volume in initial state:27, degree of liberty:3
  Saving Precomputation 
  1.188108[a] 0.231876[a] 0.993179[a] 2.730187[a] 0.805483[a] 2.145473[a] 1.311664[b] 1.654169[a] 1.422267[a] 1.138246[a] 1.305792[a] 1.951797[a] 0.149796[a] 1.915230[b] 0.310620[a] 2.243881[a] 0.567300[a] 2.276661[a] 1.088114[a] 1.824084[b] 1.083749[a] 0.322637[a] 0.358717[a] 1.125486[a] 1.745835[a] 1.575428[a] 1.900578[b] 0.363975[a] 2.274882[a] 0.809423[a] 0.106168[a] 2.150378[a] 0.502823[a] 0.314695[a] 0.134909[a] 1.658381[a] 1.311297[b] 0.314056[a] 2.061705[a] 2.721889[a] 0.655538[a] 0.620685[a] 1.785992[b] 0.445964[a] 1.683311[a] 2.338521[a] 1.991508[a] 0.348334[a] 1.597587[b] 1.198628[a] 0.278011[a] 1.895440[a] 0.874312[a] 2.812673[a] 1.057225[a] 0.025250[a] 1.731752[b] 0.318405[a] 0.602873[a] 2.090363[a] 2.897844[a] 0.506032[a] 0.733391[a] 1.076492[b] 0.706394[a] 2.488106[a] 0.801968[a] 2.658402[a] 0.502914[a] 1.043145[b] 0.906338[a] 1.336131[a] 1.016946[a] 1.430693[a] 2.450502[a] 0.264261[a] 0.464384[a] 0.712467[a] 0.718595[a] 0.087557[a] 0.126451[a] 0.229536[a] 0.132960[a] 0.060676[a] 0.002504[a] 0.002598[a] 0.001252[a] 0.001448[a] 0.040362[a] 0.006862[a] 0.000014[a] 0.002327[a] 0.000733[a] 0.001553[a] 0.000477[a] 0.000254[a] 0.000345[a] 0.000912[a] 0.000678[a] 0.000023[a] 

  $ wordgen tunnel.prism --receding 100 --traj 1 --seed 8513 --poly -1 | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Precomputation file found but file have change discard !
  Reading Prism automaton file. 
  Computing forward reachability graph ... 9 states found, automaton is deterministic. 
  Computing Distribution[floating point; Poly; no s; clocks:{x; y; }; vars:{t; z; s; T; }] -> -1: [] 
  Volume in initial state:1, degree of liberty:-1
  Saving Precomputation 
  0.564324[a] 0.856800[a] 1.239491[a] 1.046118[a] 2.831290[a] 0.504766[a] 1.212985[a] 1.707284[b] 0.097023[a] 0.628739[a] 1.600677[a] 1.648327[a] 0.961371[a] 2.823274[a] 0.695971[a] 1.301992[b] 0.203114[a] 1.230063[a] 1.526981[a] 2.050893[a] 1.428215[a] 1.715412[b] 0.776267[a] 2.671766[a] 1.893229[a] 0.677750[a] 1.663666[a] 1.363975[b] 1.045186[a] 1.325302[a] 0.568035[a] 2.151168[a] 0.664484[a] 1.246296[a] 0.878755[a] 1.045895[a] 0.543944[a] 0.166744[a] 0.136849[a] 0.059341[a] 0.123979[a] 0.017360[a] 0.012428[a] 0.006348[a] 0.001019[a] 0.003168[a] 0.001781[a] 0.001262[a] 0.000376[a] 0.000138[a] 0.000049[a] 0.000034[a] 0.000014[a] 0.000011[a] 0.000007[a] 0.000001[a] 0.000022[a] 0.000001[a] 0.000002[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 0.000000[a] 

  $ wordgen tunnel.prism --receding 10 --traj 1 --seed 8513 --poly 0 --output-format cosim | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Precomputation file found but file have change discard !
  Reading Prism automaton file. 
  Computing forward reachability graph ... 9 states found, automaton is deterministic. 
  Splitting reachability graph ... 45 states and 58 DBMs found. 
  Computing Distribution[floating point; Poly; no s; clocks:{x; y; }; vars:{t; z; s; T; }] -> 0: [] 
  Volume in initial state:1, degree of liberty:1
  Saving Precomputation 
  {"state":"m.UID", "clocks":[ 0.000000, 0.000000], "current_time": 0,
   "constrained_delay": null,
   "transition":[{"label":"a", "weight":1, "is_selected":true, "min_time": 0, "max_time":1, "time":0.188108, "relative_time":0.188108, "target":"m.UID"},
                 {"label":"a", "weight":1, "is_selected":false, "min_time": 1, "max_time":2, "time":1.18811, "relative_time":1.18811, "target":"m.UID"},
                 {"label":"a", "weight":1, "is_selected":false, "min_time": 2, "max_time":3, "time":2.18811, "relative_time":2.18811, "target":"m.UID"}]}
  {"state":"m.UID", "clocks":[ 1.188108, 0.000000], "current_time": 1.18811,
   "constrained_delay": null,
   "transition":[{"label":"a", "weight":1, "is_selected":true, "min_time": 1.18811, "max_time":2, "time":1.41998, "relative_time":0.231876, "target":"m.UID"},
                 {"label":"a", "weight":1, "is_selected":false, "min_time": 2, "max_time":3, "time":2.2856, "relative_time":1.09749, "target":"m.UID"},
                 {"label":"a", "weight":1, "is_selected":false, "min_time": 3, "max_time":4, "time":3.2856, "relative_time":2.09749, "target":"m.UID"},
                 {"label":"a", "weight":1, "is_selected":false, "min_time": 4, "max_time":4.18811, "time":4.05372, "relative_time":2.86562, "target":"m.UID"}]}
  {"state":"m.UID", "clocks":[ 1.419984, 0.000000], "current_time": 1.41998,
   "constrained_delay": null,
   "transition":[{"label":"a", "weight":1, "is_selected":false, "min_time": 1.41998, "max_time":2, "time":1.65963, "relative_time":0.239641, "target":"m.UID"},
                 {"label":"a", "weight":1, "is_selected":false, "min_time": 2, "max_time":3, "time":2.41316, "relative_time":0.993179, "target":"m.UID"},
                 {"label":"a", "weight":1, "is_selected":true, "min_time": 3, "max_time":4, "time":3.41316, "relative_time":1.99318, "target":"m.UID"},
                 {"label":"a", "weight":1, "is_selected":false, "min_time": 4, "max_time":4.41998, "time":4.17352, "relative_time":2.75354, "target":"m.UID"}]}
  {"state":"m.UID", "clocks":[ 3.413164, 0.000000], "current_time": 3.41316,
   "constrained_delay": null,
   "transition":[{"label":"a", "weight":1, "is_selected":false, "min_time": 3.41316, "max_time":4, "time":3.6178, "relative_time":0.204633, "target":"m.UID"},
                 {"label":"a", "weight":1, "is_selected":false, "min_time": 4, "max_time":5, "time":4.34871, "relative_time":0.935542, "target":"m.UID"},
                 {"label":"a", "weight":1, "is_selected":false, "min_time": 5, "max_time":6, "time":5.34871, "relative_time":1.93554, "target":"m.UID"},
                 {"label":"a", "weight":1, "is_selected":true, "min_time": 6, "max_time":6.41316, "time":6.14407, "relative_time":2.73091, "target":"m.UID"}]}
  {"state":"m.UID", "clocks":[ 6.144073, 0.000000], "current_time": 6.14407,
   "constrained_delay": null,
   "transition":[{"label":"a", "weight":1, "is_selected":true, "min_time": 6.14407, "max_time":7, "time":6.95187, "relative_time":0.807793, "target":"m.UID"},
                 {"label":"a", "weight":1, "is_selected":false, "min_time": 7, "max_time":8, "time":7.94376, "relative_time":1.79969, "target":"m.UID"},
                 {"label":"a", "weight":1, "is_selected":false, "min_time": 8, "max_time":9, "time":8.94376, "relative_time":2.79969, "target":"m.UID"},
                 {"label":"a", "weight":1, "is_selected":false, "min_time": 9, "max_time":9.14407, "time":9.13597, "relative_time":2.9919, "target":"m.UID"},
                 {"label":"b", "weight":1, "is_selected":false, "min_time": 8, "max_time":8.14407, "time":8.13597, "relative_time":1.9919, "target":"m.UID"}]}
  {"state":"m.UID", "clocks":[ 9.135971, 0.000000], "current_time": 9.13597,
   "constrained_delay": null,
   "transition":[{"label":"a", "weight":1, "is_selected":true, "min_time": 9.13597, "max_time":10, "time":9.28135, "relative_time":0.145377, "target":"m.UID"}]}
  {"state":"m.UID", "clocks":[ 9.281348, 0.000000], "current_time": 9.28135,
   "constrained_delay": null,
   "transition":[{"label":"a", "weight":1, "is_selected":true, "min_time": 9.28135, "max_time":10, "time":9.57612, "relative_time":0.294776, "target":"m.UID"}]}
  {"state":"m.UID", "clocks":[ 9.576124, 0.000000], "current_time": 9.57612,
   "constrained_delay": null,
   "transition":[{"label":"a", "weight":1, "is_selected":true, "min_time": 9.57612, "max_time":10, "time":9.97896, "relative_time":0.402836, "target":"m.UID"}]}
  {"state":"m.UID", "clocks":[ 9.978960, 0.000000], "current_time": 9.97896,
   "constrained_delay": null,
   "transition":[{"label":"a", "weight":1, "is_selected":true, "min_time": 9.97896, "max_time":10, "time":9.98054, "relative_time":0.00157914, "target":"m.UID"}]}
  {"state":"m.UID", "clocks":[ 9.980539, 0.000000], "current_time": 9.98054,
   "constrained_delay": null,
   "transition":[{"label":"a", "weight":1, "is_selected":true, "min_time": 9.98054, "max_time":10, "time":9.98462, "relative_time":0.00407863, "target":"m.UID"}]}
  


Exact duration 

  $ wordgen twoears.prism --seed 65447 --exact-duration --expected-duration 4  | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Precomputation file found but file have change discard !
  Reading Prism automaton file. 
  Computing forward reachability graph ... 16 states found, automaton is deterministic. 
  Splitting reachability graph ... 51 states and 102 DBMs found. 
  Computing Distribution[floating point; Poly; no s; clocks:{y; x; theta_glob; }; vars:{t; z; s; T; }] -> 3: [|||] 
  Volume in initial state:30.3333333333, degree of liberty:3
  Saving Precomputation 
  0.312020[a] 0.542215[a] 3.145765[b] 
  1.264155[b] 0.756198[a] 1.979648[a] 
  1.749865[a] 0.236764[b] 2.013372[a] 
  0.566242[b] 0.192807[b] 3.240952[b] 
  0.734842[a] 0.861081[b] 2.404077[a] 
  0.796317[b] 0.287752[a] 2.915930[b] 
  1.063107[a] 0.078171[b] 2.858722[b] 
  0.324151[a] 1.557668[b] 2.118181[a] 
  1.281969[a] 0.786334[b] 1.931697[b] 
  0.543915[b] 0.847462[a] 2.608624[a] 


  $ wordgen twoears.prism --seed 65654447 --poly 5 --traj 2 --max-iteration 100 --exact-duration --exact-rational --expected-duration 3.12 --no-cache | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Reading Prism automaton file. 
  Computing forward reachability graph ... 3 states found, automaton is deterministic. 
  Splitting reachability graph ... 4 states and 15 DBMs found. 
  Computing Distribution[exact rational; ExpPoly; s formal; clocks:{y; x; }; vars:{t; z; s; T; }] -> 5: [|||||] 
  Volume in initial state:(-3008/3.0 +432T-69T²+29/6.0 T³-1/8.0 T⁴)*u(+T-8)+(24745/24.0 -1876/3.0 T+138T²-79/6.0 T³+11/24.0 T⁴)*u(+T-7)+(1062-645T+138T²-12T³+1/3.0 T⁴)*u(+T-6)+(-3835/3.0 +3422/3.0 T-372T²+157/3.0 T³-8/3.0 T⁴)*u(+T-5)+(30-181/2.0 T+291/4.0 T²-67/3.0 T³+7/3.0 T⁴)*u(+T-4)+(1317/8.0 -1415/6.0 T+497/4.0 T²-85/3.0 T³+7/3.0 T⁴)*u(+T-3)+(-20/3.0 +24T-32T²+56/3.0 T³-4T⁴)*u(+T-2)+(4/3.0 T⁴), degree of liberty:5
  0.423340[b] 0.147980[a] 0.768557[a] 1.205729[b] 0.574394[a] 
  1.446571[a] 0.130909[a] 0.210503[a] 1.211738[b] 0.120279[a] 

  $ wordgen caveatUppaal2.xml --seed 5465447 --poly 3 --expected-duration 5.1 --exact-duration --no-cache | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Parse warning: broadcast channels are not supported, assuming regular channel
  Parse warning: broadcast channels are not supported, assuming regular channel
  Parse warning: broadcast channels are not supported, assuming regular channel
  Reading UPPAAL automaton file. 
  Computing forward reachability graph ... 14 states found, automaton is not deterministic. 
  Splitting reachability graph ... 17 states and 10 DBMs found. 
  Computing Distribution[floating point; ExpPoly; s formal; clocks:{c; d; }; vars:{t; z; s; T; }] -> 3: [|||] 
  Volume in initial state:(-3T²)*u(+T-10)+(3T²), degree of liberty:3
  Using rejection sampling to solve non-determinism !
  0.299904[t] 3.550867[a] 1.249229[t] 
  1.364739[t] 1.495447[a] 2.239814[t] 0.805265[t] 2.942953[t] 1.351781[b] 0.847016[t] 2.325697[t] 1.927287[b] 
  2.049346[t] 0.201322[a] 2.849333[t] 
  0.566169[t] 3.577899[a] 0.955932[t] 
  0.292428[t] 3.195005[t] 1.612566[b] 
  0.185376[t] 3.572323[t] 1.342301[b] 1.599431[t] 3.358840[t] 0.141729[b] 
  0.206840[t] 0.802994[t] 4.090166[b] 
  0.802166[t] 2.131974[t] 2.165860[b] 2.725264[t] 1.369483[t] 1.005254[b] 
  1.785291[t] 1.827355[t] 1.487354[b] 2.143836[t] 2.585470[a] 0.370693[t] 
  2.028753[t] 2.259234[a] 0.812013[t] 


Perturbation
  $ wordgen twoears.prism --seed 65654447 --poly 5 --traj 10 --generate-perturbation 0.1 --template "@1.347303[b] @1.125394[a] @0.279739[b] @0.869138[b] @1.505120[a]" | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Precomputation file found but file have change discard !
  Reading Prism automaton file. 
  Computing forward reachability graph ... 3 states found, automaton is deterministic. 
  Splitting reachability graph ... 4 states and 15 DBMs found. 
  Computing Distribution[floating point; Poly; no s; clocks:{y; x; }; vars:{t; z; s; T; }] -> 5: [|||||] 
  Volume in initial state:240.283333333, degree of liberty:5
  Saving Precomputation 
  1.317647[b] 1.201316[a] 0.195699[b] 0.947260[b] 1.470830[a] 
  1.447257[b] 1.107907[a] 0.189679[b] 0.805230[b] 1.580323[a] 
  1.295834[b] 1.047110[a] 0.309897[b] 0.779930[b] 1.502768[a] 
  1.401506[b] 1.181554[a] 0.305404[b] 0.851005[b] 1.520792[a] 
  1.328299[b] 1.109559[a] 0.228801[b] 0.903034[b] 1.578822[a] 
  1.354875[b] 1.195055[a] 0.225184[b] 0.881018[b] 1.560881[a] 
  1.437882[b] 1.129410[a] 0.267089[b] 0.954735[b] 1.483016[a] 
  1.278239[b] 1.026387[a] 0.353777[b] 0.795735[b] 1.407041[a] 
  1.364071[b] 1.203399[a] 0.216101[b] 0.777776[b] 1.564100[a] 
  1.299468[b] 1.210061[a] 0.209204[b] 0.866651[b] 1.449862[a] 
 
Not Deterministic
  $ wordgen thick-twin-notdet.prism --output-format timeword --poly 10 --seed 653477 --no-cache | sed 's/[[][0-9]*[.][0-9]*s[]]//g'
  Reading Prism automaton file. 
  Computing forward reachability graph ... 4 states found, automaton is not deterministic. 
  Splitting reachability graph ... 4 states and 8 DBMs found. 
  Computing Distribution[floating point; Poly; no s; clocks:{dummy_clock; y; x; }; vars:{t; z; s; T; }] -> 10: [||||||||||] 
  Volume in initial state:0.13499393739, degree of liberty:10
  Using rejection sampling to solve non-determinism !
  0.392091[a] 0.537194[a] 0.254117[a] 0.521723[a] 0.358783[a] 0.248963[a] 0.080644[a] 0.403180[a] 0.291132[a] 0.123301[a] 
  0.146667[a] 0.118430[a] 0.804141[a] 0.093610[a] 0.256899[a] 0.154671[a] 0.423909[a] 0.353082[a] 0.272370[a] 0.229223[a] 
  0.215760[a] 0.194709[a] 0.057030[a] 0.759965[a] 0.008500[a] 0.659502[a] 0.278643[a] 0.413476[a] 0.180225[a] 0.604537[a] 
  0.756055[a] 0.162742[a] 0.162495[a] 0.212962[a] 0.397246[a] 0.017822[a] 0.125235[a] 0.246866[a] 0.509296[a] 0.224289[a] 
  0.201763[a] 0.083111[a] 0.574866[a] 0.275411[a] 0.140860[a] 0.638128[a] 0.255466[a] 0.334419[a] 0.177386[a] 0.024062[a] 
  0.622339[a] 0.302198[a] 0.230575[a] 0.044899[a] 0.001894[a] 0.344147[a] 0.262561[a] 0.624279[a] 0.021705[a] 0.757965[a] 
  0.660121[a] 0.149637[a] 0.033094[a] 0.205513[a] 0.153108[a] 0.278209[a] 0.068481[a] 0.467103[a] 0.085245[a] 0.349361[a] 
  0.207227[a] 0.439975[a] 0.017883[a] 0.575250[a] 0.221678[a] 0.276097[a] 0.175861[a] 0.320836[a] 0.312782[a] 0.212129[a] 
  0.420353[a] 0.353486[a] 0.514234[a] 0.429219[a] 0.131522[a] 0.007226[a] 0.495191[a] 0.217562[a] 0.038407[a] 0.457520[a] 
  0.444628[a] 0.533207[a] 0.064730[a] 0.751088[a] 0.162955[a] 0.375805[a] 0.037377[a] 0.449352[a] 0.256356[a] 0.494394[a] 
